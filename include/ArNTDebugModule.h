#pragma once

///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTIPCModule.h
// 作  者 ：杨朝霖
// 用  途 ：ArNTDebugModule模块接口定义文件,该模块用于输出程序调试信息
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>
///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖     2010-8-10    创建

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
///</header_info>

///<datastruct_info>
//==============================================================================
// 基本数据类型定义
// 定义调试信息结构
typedef struct tagDebugMessage
{
	LongInt iSize;
	enum DebugType {enWarn = 0, enError, enNotice};
	DebugType type;
	SHORTSTR strTime;
	LONGSTR strInfo;
	LONGSTR strFromApp;
	SHORTSTR strFromFile;
	SHORTSTR  strFromFunc;
	LongInt  iFromLine;
}DebugMessage, *PDebugMessage;
///</datastruct_info>

//==============================================================================
///<export_info>
//==============================================================================
//dll中导出的函数
extern "C"{

///<func_info>
//描述：获取版本信息
//参数：LONGSTR& strVersion 获取的版本信息
void   __stdcall ArNTDBMGetVersion(LONGSTR& strVersion);
///</func_info>

///<func_info>
//描述：注册进程通道，用于发送和接收数据
//参数：ArNTIPCInfo& Info 注册的通道信息
//      PRcvDebugMessageFuncDef pFunc 用于指定接收到调试信息的处理函数
//      PMemPointer pContext 传递的回调函数环境指针，可以在pFunc中被调用

//接收到其他进程通道发送过来的数据
typedef void (__stdcall *PRcvDebugMessageFuncDef)(PMemPointer pContext, PDebugMessage pMessage,  ShortInt iMessageNum); 
ArNT_ERROR_CODE   __stdcall ArNTDBMRegMessage(PRcvDebugMessageFuncDef pFunc, PMemPointer pContext);
///</func_info>

///<func_info>
//描述：发送警告信息
//参数：StrPointer strInfo需要写入的内容
ArNT_ERROR_CODE   __stdcall ArNTDBMWarnInfo(StrPointer strInfo, StrPointer strFile, StrPointer strFunc, LongInt iLine, ArNT_BOOL bInstant = false);
///</func_info>

///<func_info>
//描述：写入错误信息
//参数：StrPointer strInfo 要输出的错误信息
//备注：
ArNT_ERROR_CODE   __stdcall ArNTDBMErrorInfo(StrPointer strInfo, StrPointer strFile, StrPointer strFunc, LongInt iLine, ArNT_BOOL bInstant = ArNT_FALSE);
///</func_info>

///<func_info>
//描述：写入提示信息
//参数：StrPointer strInfo 要输出的错误信息
//备注：
ArNT_ERROR_CODE   __stdcall ArNTDBMNoticeInfo(StrPointer strInfo, StrPointer strFile, StrPointer strFunc, LongInt iLine, ArNT_BOOL bInstant = ArNT_FALSE);
///</func_info>

///<func_info>
//描述：获取错误描述码
//参数：LONGSTR& strVersion 获取的版本信息
ArNT_BOOL   __stdcall ArNTDBMGetErrorInfo(ArNT_ERROR_CODE code, LONGSTR& strInfo);
///</func_info>

}
//==============================================================================
///</export_info>


//==============================================================================
///<macro_info>

#define WRITE_WARN(Info)	ArNTDBMWarnInfo(Info, __WFILE__, __WFUNCTION__, __LINE__)
#define WRITE_ERROR(Info)	ArNTDBMErrorInfo(Info, __WFILE__, __WFUNCTION__, __LINE__)
#define WRITE_NOTICE(Info)	ArNTDBMNoticeInfo(Info, __WFILE__, __WFUNCTION__, __LINE__)

#define WRITE_WARN_INSTANT(Info)	ArNTDBMWarnInfo(Info, __WFILE__, __WFUNCTION__, __LINE__, ArNT_TRUE)
#define WRITE_ERROR_INSTANT(Info)	ArNTDBMErrorInfo(Info, __WFILE__, __WFUNCTION__, __LINE__, ArNT_TRUE)
#define WRITE_NOTICE_INSTANT(Info)	ArNTDBMNoticeInfo(Info, __WFILE__, __WFUNCTION__, __LINE__, ArNT_TRUE)



//==============================================================================
///</macro_info>

//错误码
//错误码所属类别为IPC类
#define  ARNT_DBM_FAMILY   0x300
/////////////////////////////////////////////////////
//成功或警告类的错误码
//表示成功的调试操作
#define   SUCCESS_DBM_OPER 					MAKE_SUCCESS_CODE(ARNT_DBM_FAMILY, 0)



/////////////////////////////////////////////////////
//错误类型的错误码

//注册调试通道失败
#define  ERRO_DBM_FAILREGCHANNEL						MAKE_ERROR_CODE(ARNT_DBM_FAMILY, 1)

//向调试通道发送调试数据失败
#define  ERRO_DBM_FAILSENDINFO_TOCHANNEL				MAKE_ERROR_CODE(ARNT_DBM_FAMILY, 2)


//---------------------------------------------------------------------------------

#ifndef DBMNOAutoLink
#define DBMNOAutoLink
	#ifdef _WIN64
		#pragma comment( lib, "ArNTDebugModule64.lib" )
		#pragma message("======== 程序隐式链接到 ArNTImage64.dll...")
	#else
		#pragma comment( lib, "ArNTDebugModule.lib" )
		#pragma message("======== 程序隐式链接到 ArNTDebugModule.dll...")
	#endif
#endif

