///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArWatch.h
// 作  者 ：杨朝霖
// 用  途 ：用于测试函数或功能代码的运行时间
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖      2009-03-23   创建模块
//---------------------------------------------------------------------------
///</ver_info>

#ifndef ArWatchH
#define ArWatchH

///<header_info>
//添加所需的头文件
#include <windows.H>
#include "ArNtBaseDataStruct.h"
///</header_info>

///<class_info>

///<demo_info>
//该类利用构造函数和析构函数来完成代码运行时间的测试,如：
// double dTestTime;
//{ArWatch watch; watch.SetData(&dTestTime);
//  待测试的代码
// }
//  dTestTime中保存了代码的运行时间，可以通过加上{}号来控制ArWatch变量的生命期
///</demo_info>


//==============================================================================
//名称:ArWatch
//功能:通过Windows高精度计时器来计算代码的执行时间
class ArWatch
{
public:
	///<func_info>
	//描述：设置一个用于保存测试时间的double变量
	//参数：LongFloat* pTime 保存运行时间的外部变量指针
	 void SetData(LongFloat* pTime);
	 ///</func_info>
	
    ///<func_info>
	//描述：构造函数
	//参数：无
	  ArWatch();
	 ///</func_info>

	///<func_info>
	//描述：构造函数
	//参数：double* pTime保存运行时间的外部变量指针
	ArWatch(LongFloat* pTime);
	///</func_info>

    ~ArWatch();
private:
    LARGE_INTEGER m_liStart, m_liEnd, m_liFrequency;
    LongFloat m_dCycle;
    LongFloat*  m_pTime;
};
#endif
//==============================================================================
///</class_info>
