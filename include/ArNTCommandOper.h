#pragma once
#include "ArNtBaseDataStruct.h"
//#include "ArNTIPCModule.h"

class ArNTCommandOper
{
private:
	ArNTCommandOper(void);
	~ArNTCommandOper(void);
public:
	static HANDLE m_hCmdHeap;

	static ArNT_BOOL   PostCmdToChannel(ShortInt iID, PMemPointer pPara, LongInt iSize, StrPointer strChannel,  StrPointer strSenderID = NULL, ArNT_BOOL bTrace = ArNT_TRUE);
	static ArNT_BOOL   PostCmdToChannel(ShortInt iID,PArNTCommand pCmdBuffer, PMemPointer pPara, LongInt iSize, StrPointer strChannel, StrPointer strSenderID = NULL, ArNT_BOOL bTrace = ArNT_TRUE);
	

};
