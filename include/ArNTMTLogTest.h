#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：MCSISTConsoleTest.h
// 作  者 ：杨朝霖
// 用  途 ：定义了单线程测试类
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

#include "ArNTTestUint.h"
#include <stdio.h>


class  CMTTestLog:public ArNTTestUnitOutPut
{
public:
	virtual void  WriteInfo(StrChar* strInfo);
	virtual bool  Valid();
	void  CreateLog(StrChar* strName);
public:
	CMTTestLog();
	~CMTTestLog();
private:
	LONGSTR  m_strLogPathName;
	FILE*   m_hLog;
};

typedef struct tagMTLogContex
{
	ArNTTestUnit*	pTest;
	CMTTestLog*     pLog;
	unsigned int	iThreadID;
	HANDLE			hThreadTest;
	bool			bExit;
}MTLogContex, *PMTLogContex;


class CMTLogTestGroup:public ArNTTestGroup
{
public:
	bool StartTest(void);
	void AddTest(ArNTTestUnit* pTest);
	void  Free();
	static CMTLogTestGroup& GetObj();
	


private:
	CMTLogTestGroup();
	~CMTLogTestGroup();

	enum {MAX_TEST_NUM = 1024};
	MTLogContex  m_TestArray[MAX_TEST_NUM];
	int   m_iTestNum;
	int   m_iValidTestNum;
	static unsigned int __stdcall threadTest(void* pPara);

	CMTTestLog  m_TestOutPut;
	friend class CMTLogTestFactory;	
};

class CMTLogTestFactory:public  ArNTTestFactory
{
public:
	virtual ArNTTestGroup* GetTestGroup(StrChar* strPara);
	virtual void Free();
public:
	static CMTLogTestFactory& GetObj();
private:
	~CMTLogTestFactory() {};
	CMTLogTestFactory() {};	
};

