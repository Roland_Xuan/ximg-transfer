#pragma once

///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：MCSISTConsoleTest.h
// 作  者 ：杨朝霖
// 用  途 ：定义了单线程测试类,输出结果到控制台窗口
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>
#include "ArNTTestUint.h"
class CConsoleTestUnitOutPut:public ArNTTestUnitOutPut
{
public:
	void  WriteInfo(StrChar* strInfo);
	bool  Valid() { return true;}
public:
	CConsoleTestUnitOutPut();
	~CConsoleTestUnitOutPut();
};


class  CConsoleTestGroup:public ArNTTestGroup
{
public:
	bool StartTest(void);
	void AddTest(ArNTTestUnit* pTest);
	void  Free();
	static CConsoleTestGroup& GetObj();	
private:
	CConsoleTestGroup();
	~CConsoleTestGroup();


	enum {MAX_TEST_NUM = 1024};
	ArNTTestUnit* m_TestArray[MAX_TEST_NUM];
	int   m_iTestNum;
	int   m_iValidTestNum;

	ArNTTestUnitOutPut* m_pOutPut;

	friend class CSTConsoleTestFactory;
};

class CSTConsoleTestFactory:public  ArNTTestFactory
{
public:
	virtual ArNTTestGroup* GetTestGroup(StrChar* strPara);
	virtual void Free(){};
public:
	static CSTConsoleTestFactory& GetObj();
private:
	~CSTConsoleTestFactory() {};
	CSTConsoleTestFactory() {};
};










