#pragma once
#include "ArNtBaseDataStruct.h"
///<erro_info>

//错误码
//错误码所属类别为IPC类
#define  ARNT_NET_FAMILY   0x300

//成功类操作码
//表示成功的通道操作
#define   SUCCESS_NET_OPERCHANNEL						MAKE_SUCCESS_CODE(ARNT_NET_FAMILY, 0)
#define   SUCCESS_NET_OPERCHANNEL_DESC					L"成功的网络传输模块操作"


//错误类操作码

#define		ERROR_NET_SENDDATAT_TOOMUCH					MAKE_ERROR_CODE(ARNT_NET_FAMILY, 0)
#define		ERROR_NET_SENDDATAT_TOOMUCH_DESC			L"发送的数据量太多"

#define		ERROR_NET_CHANNEL_NOEXIST					MAKE_ERROR_CODE(ARNT_NET_FAMILY, 1)	
#define     ERROR_NET_CHANNEL_NOEXIST_DESC				L"指定的通道不存在"			
typedef struct tagErrorNetChannelNotExist
{
	LongInt		iSize;
	NetIP		strIP;
	IPv4Addr	IP;
	NetPORT		iPort;
}ErrorNetChannelNotExist, *PErrorNetChannelNotExist;


#define		ERROR_NET_ADAPTER_MAC_NOEXIST				MAKE_ERROR_CODE(ARNT_NET_FAMILY, 2)	
#define     ERROR_NET_ADAPTER_MAC_NOEXIST_DESC			L"指定MAC的网卡通道不存在"	


#define		ERROR_NET_REGOPER							MAKE_ERROR_CODE(ARNT_NET_FAMILY, 3)	
#define     ERROR_NET_REGOPER_DESC						L"注册表操作错误"	

#define		ERROR_NET_CHANNEL_NOTAVALIABLE				MAKE_ERROR_CODE(ARNT_NET_FAMILY, 4)	
#define     ERROR_NET_CHANNEL_NOTAVALIABLE_DESC			L"没有可用的通道"	

#define		ERROR_NET_REG_CHANNEL						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 5)	
#define     ERROR_NET_REG_CHANNEL_DESC					L"注册通道出错"	

#define     ERROR_NET_POSTDATA							MAKE_ERROR_CODE(ARNT_NET_FAMILY, 6)	
#define     ERROR_NET_POSTDATA_DESC						L"投递数据出错"	

#define     ERROR_NET_TOOMANY_POSTDATA					MAKE_ERROR_CODE(ARNT_NET_FAMILY, 7)	
#define     ERROR_NET_TOOMANY_POSTDATA_DESC				L"投递的数据过多"	

#define     ERROR_NET_RCVDATA							MAKE_ERROR_CODE(ARNT_NET_FAMILY, 8)	
#define     ERROR_NET_RCVDATA_DESC						L"接收数据出错"	

#define     ERROR_NET_SERVER_NORESPONSE					MAKE_ERROR_CODE(ARNT_NET_FAMILY, 9)	
#define		ERROR_NET_SERVER_NORESPONSE_DESC			L"连接服务端无响应"

#define     ERROR_NET_CONNECT							MAKE_ERROR_CODE(ARNT_NET_FAMILY, 10)	
#define		ERROR_NET_CONNECT_DESC						L"连接服务端失败"

#define     ERROR_NET_CONNECTED							MAKE_ERROR_CODE(ARNT_NET_FAMILY, 11)	
#define		ERROR_NET_CONNECTED_DESC					L"需要建立的连接已经存在"

#define     ERROR_NET_NOTCONNECT						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 12)	
#define		ERROR_NET_NOTCONNECT_DESC					L"尚未建立连接"

#define     ERROR_NET_DISCONNECT						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 13)	
#define		ERROR_NET_DISCONNECT_DESC					L"尝试断开连接出错"

#define     ERROR_NET_SENDDATA							MAKE_ERROR_CODE(ARNT_NET_FAMILY, 14)	
#define     ERROR_NET_SENDDATA_DESC						L"发送数据出错"	

#define     ERROR_NET_IPNOEXISIT						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 15)	
#define     ERROR_NET_IPNOEXISIT_DESC					L"指定的IP地址在主机上不存在"	

#define     ERROR_NET_INVALIDDEST						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 16)	
#define     ERROR_NET_INVALIDDEST_DESC					L"无效的目标地址"
typedef struct tagErrorNetInvalidDest
{
	LongInt iSize;
	IPv4Addr	DestIP;
	NetPORT		iDestPort;
}ErrorNetInvalidDest, *PErrorNetInvalidDest;


#define     ERROR_NET_NOTINRANGE						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 17)	
#define     ERROR_NET_NOTINRANGE_DESC					L"发送方与接收方不在同一网段"

#define     ERROR_NET_ACCEPTCLIENT						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 18)	
#define     ERROR_NET_ACCEPTCLIENT_DESC					L"无法接收连接的客户端"

#define     ERROR_NET_POSTTIMETOOLONG					MAKE_ERROR_CODE(ARNT_NET_FAMILY, 19)	
#define     ERROR_NET_POSTTIMETOOLONG_DESC				L"投递数据的时间过长"	

#define     ERROR_NET_NOSENDCONTEX						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 20)	
#define     ERROR_NET_NOSENDCONTEX_DESC					L"无可用的发送相关对象"	
typedef struct tagErrorNetNoSendContext
{
	LongInt		iSize;
	NetIP		strIP;
	IPv4Addr	IP;
	NetPORT		iPort;
}ErrorNetNoSendContext, *PErrorNetNoSendContext;




#define     ERROR_NET_SENDTIMEOUT						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 21)	
#define     ERROR_NET_SENDTIMEOUT_DESC					L"发送数据超时"	

#define     ERROR_NET_PKGTTLTIMEOUT						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 22)	
#define     ERROR_NET_PKGTTLTIMEOUT_DESC			    L"数据包的TTL超时"	

#define     ERROR_NET_CHANELNOTREADY					MAKE_ERROR_CODE(ARNT_NET_FAMILY, 23)	
#define     ERROR_NET_CHANELNOTREADY_DESC			    L"通道尚未初始化完成！"
typedef struct tagErrorNetChannelNotReady
{
	LongInt		iSize;
	NetIP		strIP;
	IPv4Addr	IP;
	NetPORT		iPort;
}ErrorNetChannelNotReady, *PErrorNetChannelNotReady;



#define     ERROR_NET_RCVDUPLICATE						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 24)	
#define     ERROR_NET_RCVDUPLICATE_DESC					L"接收到重复的数据包！"	

#define     ERROR_NET_REGCHANNELEXIST					MAKE_ERROR_CODE(ARNT_NET_FAMILY, 25)	
#define     ERROR_NET_REGCHANNELEXIST_DESC				L"注册的通道已经存在！"	

#define     ERROR_NET_CREATESOCKE						MAKE_ERROR_CODE(ARNT_NET_FAMILY, 26)	
#define     ERROR_NET_CREATESOCKE_DESC					L"创建SOCKET出错！"	

#define     ERROR_NET_BINDSOCKE							MAKE_ERROR_CODE(ARNT_NET_FAMILY, 27)	
#define     ERROR_NET_BINDSOCKE_DESC					L"绑定SOCKET出错！"	
#define     ERROR_NET_CREATESOCKETTHREAD				MAKE_ERROR_CODE(ARNT_NET_FAMILY, 28)	
#define     ERROR_NET_CREATESOCKETTHREAD_DESC			L"创建SOCKET读取线程出错！"	

#define     ERROR_NET_NOTIMPL							MAKE_ERROR_CODE(ARNT_NET_FAMILY, 29)	
#define     ERROR_NET_NOTIMPL_DESC						L"该函数没有实现！"	







//警告类操作码
///<erro_info>

DECL_ERROR_LIST(NET)