#pragma once
#include "ArNtBaseDataStruct.h"

class ArNTPEOperator
{
public:
	ArNTPEOperator(StrPointer strExeFilePath);
	~ArNTPEOperator(void);
	
	//获取指定节的数据
	// LongInt& iDataLen 输入的是pData的最大长度，函数执行完成后返回实际节的长度
	//PBufferByte pData 用于接收节中的数据
	ArNT_BOOL  FindSection(StrPointer strSectionName, PBufferByte pData, LongInt& iDataLen);

	//更新指定节的数据
	// LongInt& iDataLen 输入的是pData的最大长度，函数执行完成后返回实际节的长度
	//PBufferByte pData 用于更新节中的数据
	ArNT_BOOL  UpdateSection(StrPointer strSectionName, PBufferByte pData, LongInt& iDataLen);

private:
	bool  m_bValidPEFile;
	HANDLE m_hFile;
	LONG  m_lSectionHeaderStart;
	LONG  m_lSectionNum;

};
