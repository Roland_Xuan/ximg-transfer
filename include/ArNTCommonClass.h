#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTCommonClass.h
// 作  者 ：杨朝霖
// 用  途 ：定义了图像处理平台中的常用类
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖       2012-12-1    创建模块
//1.3     杨朝霖       2015-4-8   添加了ArNTLogOperator

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
//包括线程类
#include "ArNTThread.h"
//包括用户模式下的同步类
#include "ArNTCriticalSection.h"
//包括计时器类
#include "ArNTTimer.h"
//包括了图像平台中用到的XML文件操作类
#include "ArNTXMLOperator.h"
//包括了调试类
#include "ArNTDebugHelper.h"
//包括了任务队列类
#include "ArNTTaskArray.h"
//包括了计时类
#include "ArWatch.h"
//包括了内存池对象
#include "ArNTMemPool.h"
//包括了运行信息对象
#include "ArNTRunInfoOper.h"
//包括了PE操作类
#include "ArNTPEOperator.h"
//包括了命令操作类
#include "ArNTCommandOper.h"
//包括了线程池类
#include "ArNTThreadPool.h"
//包括了控制台界面类
#include "ArNTConsoleUI.h"
//包括了日志类
#include "ArNTLogOperator.h"
//包括数据列表类
#include "ArNTDataArray.h"
#include "ArNTDetectLive.h"

///</header_info>



void _stdcall  GetCommonClassVersion(LONGSTR& strVersion);

//==============================================================================
///链接信息
#ifndef  AUTOLINK_COMMONCLASS
#define  AUTOLINK_COMMONCLASS
#ifdef _WIN64
#pragma comment( lib, "ArNTCommonClass64.lib" )
#else
#pragma comment( lib, "ArNTCommonClass.lib" )
#endif

#endif