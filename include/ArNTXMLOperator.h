///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTXMLOperator.h
// 作  者 ：杨朝霖
// 用  途 ：该类用于对XML文件进行操作(仅限于普通格式的XML文件，非通用的XML文件)
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>
#pragma once

///<header_info>
//添加所需的头文件
//#import "msxml3.dll"
//using namespace MSXML2;
#include "ArNtBaseDataStruct.h"
///</header_info>

///<datastruct_info>
//==============================================================================
//常用数据结构定义
//----------------------------------------------------------
//描述：节点的数据结构
typedef struct tagNode
{
	TINYSTR   m_strName;//节点名，不超过20个字节
	LONGSTR   m_strText; //节点数值, 
	tagNode*  m_pNextBrother;
	tagNode*  m_pPerioBrother;
	tagNode*  m_pParent;
	tagNode*  m_pChild;
	tagNode(tagNode* pParent = NULL)
	{
		m_pParent = pParent;
		m_pNextBrother =  NULL;
		m_pChild = NULL;
		m_pPerioBrother = NULL;
		if(m_pParent)
		{
			PNode pNode = m_pParent->m_pChild;
			if(NULL == m_pParent->m_pChild)
			{
				m_pParent->m_pChild = this;
			}else
			{
				PNode pBrotherNode = m_pParent->m_pChild;
				while(pBrotherNode->m_pNextBrother) pBrotherNode = pBrotherNode->m_pNextBrother;
				this->m_pNextBrother = pBrotherNode->m_pNextBrother;
				pBrotherNode->m_pNextBrother = this;
				this->m_pPerioBrother = pBrotherNode;
			}
		}
	}
}Node, *PNode;

//参数信息数据结构
typedef struct tagXMLParaInfo
{
	LongInt iSize;
	LONGSTR strName;		//参数名称
	LONGSTR strText;		//参数值

}XMLParaInfo, *PXMLParaInfo;
///</datastruct_info>

///<class_info>
//==============================================================================
//名称:ArNTXMLOperator
//功能:XML操作对象
class ArNTXMLOperator
{
public:
	///<func_info>
	//描述：以格式化参数字符串的方式添加参数
	//参数：StrPointer strPara 指定参数名，由于XML中的参数是树状结构，
	//所以参数名采用格式为：参数名1#参数名2#..，需要指定其所属的父参数
	//		StrPointer strValue 指定的参数值。
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL	AddValueByString(StrPointer strPara, StrPointer strValue);
	///</func_info>
	///<func_info>
	//描述：以参数名数组的方式添加参数
	//参数： PSHORTSTR strNameArray 指定的参数名数组,该数组表明了要添加参数的层次关系。
	//		 LongInt iDepth 数组中参数的数量
	//		 StrPointer strValue 指定的参数值
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL	AddValue(PSHORTSTR strNameArray, LongInt iDepth, StrPointer strValue);
	///</func_info>

	///<func_info>
	//描述：保存XML文件
	//参数：StrPointer strFileName 指定需要保存的文件名
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL	SaveXML(StrPointer strFileName);
	///</func_info>
	
	///<func_info>
	//描述：将XML参数文件保存指定的字节流
	//参数：PBufferByte pBuffer 字节流首地址
	//		LongInt& iBufferLen 字节流长度
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL	ExportToStream(PBufferByte pBuffer, LongInt& iBufferLen);
	///</func_info>

	///<func_info>
	//描述：将更改指定参数的参数值
	//参数：StrPointer strPara 采用格式为：参数名1#参数名2#..样式的字符串来标定参数位置
	//		StrPointer strValue 需要更改为的新参数值。
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL	ChangeValueByString(StrPointer strPara, StrPointer strValue);
	///</func_info>

	///<func_info>
	//描述：将更改指定参数的参数值
	//参数：PSHORTSTR strNameArray 用于标定参数位置的字符串数组
	//		LongInt iDepth 字符串数组中的成员数量
	//		StrPointer strValue 需要更改为的新参数值。
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL	ChangeValue(PSHORTSTR strNameArray,LongInt iDepth,StrPointer strValue);
	///</func_info>

	///<func_info>
	//描述：删除指定的参数值
	//参数：StrPointer strPara 采用格式为：参数名1#参数名2#..样式的字符串来标定参数位置
	//		LongInt& iDelValueNum 删除的参数数量，通常是指该参数下的子参数数量。
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL	DelValueByString(StrPointer strPara, LongInt& iDelValueNum);
	///</func_info>

	///<func_info>
	//描述：删除指定的参数值
	//参数：PSHORTSTR strNameArray 用于标定参数位置的字符串数组
	//		LongInt iDepth 字符串数组中的成员数量
	//		LongInt& iDelValueNum  删除的参数数量，通常是指该参数下的子参数数量。
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL	DelValue(PSHORTSTR strNameArray, LongInt iDepth, LongInt& iDelValueNum);
	///</func_info>

	///<func_info>
	//描述：读取指定参数的参数值
	//参数：StrPointer strPara 采用格式为：参数名1#参数名2#..样式的字符串来标定参数位置
	//		 LONGSTR& strValue 用于存放读取到的参数值
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL GetValueByString(StrPointer strPara,  LONGSTR& strValue);
	///</func_info>
	
	//<func_info>
	//描述：读取指定参数的参数值
	//参数：PSHORTSTR strNameArray 用于标定参数位置的字符串数组
	//		LongInt iDepth 字符串数组中的成员数量
	//		LONGSTR& strValue 用于存放读取到的参数值
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL GetValue(PSHORTSTR strNameArray, LongInt iDepth, LONGSTR& strValue);
	///</func_info>

	//<func_info>
	//描述：加载XML文件
	//参数：指定的参数文件名
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL LoadXML(StrPointer strFileName);
	///</func_info>

	//<func_info>
	//描述：获取根参数的名称
	//参数：LONGSTR& strName 用于存放根参数的字符串
	//返回值：ArNT_BOOL类型，ArNT_TRUE表示成功
	ArNT_BOOL	GetRootName(LONGSTR& strName);
	///</func_info>
	
	//<func_info>
	//描述：获取一个参数的子参数名称列表
	//参数：StrPointer strParent 指定的参数名称
	//		PLONGSTR pChildName 子参数列表
	//		ShortInt& iChildNum 子参数数量
	ArNT_BOOL   GetChilds(StrPointer strParent, PLONGSTR pChildName, ShortInt& iChildNum); 
	///</func_info>
	
	//<func_info>
	//描述： 遍历出所有参数信息
	//参数：PXMLParaInfo pInfoArray 参数信息列表
	//		LongInt& iParaNum 参数信息数量
	ArNT_BOOL   Traverse(PXMLParaInfo pInfoArray, LongInt& iParaNum);
	///</func_info>

	//<func_info>
	//描述：获取参数文件的注释
	//参数：LONGSTR& strComment 用于存放参数文件注释的字符串
	ArNT_BOOL	GetComment(LONGSTR& strComment);
	///</func_info>
	
	//<func_info>
	//描述：给参数文件添加注释
	//参数：LONGSTR& strComment 需要添加的参数文件注释
	ArNT_BOOL	AddComment(StrPointer strComment);
	///</func_info>

	//<func_info>
	//描述：判断指定的参数是否存在
	//参数：StrPointer strPara 指定的参数名称
	ArNT_BOOL   IsParaExsitByString(StrPointer strPara);
	///</func_info>
public:
	ArNTXMLOperator(void);
	~ArNTXMLOperator(void);
private:
	PNode			m_pRoot;
	LONGSTR			m_strComment; //参数注释
	LONGSTR         m_strXMLVersion;

	
	void			AddRoot(StrPointer strName, StrPointer strText = NULL);
	PNode           FindNode(PSHORTSTR strNameArray, LongInt iDepth, PNode& pPreNode);

	PSHORTSTR       StringToParaName(StrPointer strPara, ShortInt& iParaNum);

	void			DestroyRoot(PNode  pRoot);
	void			DestroyNode(PNode pNode, LongInt& iDelNodeNum);

	void			CountNodeBuffer(PNode pNode, LongInt& iBufferLen, ShortInt& iLevel, ArNT_BOOL bChild = ArNT_TRUE);
	void			WriteParaInBuffer(PBufferByte pBuffer, PNode pNode, LongInt& iBufferLen, ShortInt& iLevel, ArNT_BOOL bChild = ArNT_TRUE);

	ArNT_BOOL		ReadUnicodeFile(PBufferByte pBuffer, LongInt iBufferSize);
	ArNT_BOOL       ReadUnicodeVersion(PBufferByte pBuffer, LongInt& iHasRead, ArNT_BOOL& bFind, LONGSTR& strVersion);
	ArNT_BOOL       ReadUnicodeCommonent(PBufferByte pBuffer, LongInt& iHasRead, ArNT_BOOL& bFind, LONGSTR& strComment);
	ArNT_BOOL		ReadUnicodeElement(PBufferByte pBuffer, LongInt& iHasRead, LongInt iBufferSize);
	ArNT_BOOL       IsTitleLeft(PBufferByte pBuffer, ShortInt& iTagSize);
	ArNT_BOOL       IsTitleRight(PBufferByte pBuffer, ShortInt& iTagSize);
	ArNT_BOOL       IsEndTitleLeft(PBufferByte pBuffer, ShortInt& iTagSize);
	ArNT_BOOL       IsEndTitleRight(PBufferByte pBuffer, ShortInt& iTagSize);
	LongInt		    SetData(PBufferByte pBuffer, LongInt iStart, LongInt  iEnd, PBufferByte pData);

	void			TrimString(StrPointer strSrc, LONGSTR& strDest);

	void			Traverse(PNode pNode, StrPointer strNodeName,PXMLParaInfo pInfoArray, LongInt& iParaNum);
};
///</class_info>
