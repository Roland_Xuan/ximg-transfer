#pragma once
/*
* Copyright (c) 2004, 北京科技大学高效轧制国家工程研究中心
* All rights reserved.
* 
* 文件名称：ArNTIppImageData.h
* 摘    要：定义了Intel IPP数据和常用操作
* 版本历史：
* 2012-10-31 在GrayBmpData中添加获取像素值的函数
*/


#include "ArNtBaseDataStruct.h"
#include "ArNTGrayBmp.h"
#include "ippi.h"
#include "ipps.h"


class CIppImageData
{
public:
	static void TransposeImage(CIppImageData& SrcImg, CIppImageData& DstImg);//图像进行转置
	static void MutCImage(CIppImageData& DstImg, LongFloat dRation); //图像中每隔像素乘以一个系数

public:
	void  FromGrayBmpData(GrayBmpData& data);
	void  ToGrayBmpData(GrayBmpData& data);
	void  Alloc(LongInt iImgWidth, LongInt iImgHeight);
	void  SetData(PImgDataType  pSrcData, LongInt iImgWidth, LongInt iImgHeight, LongInt iWidthStep);


	inline Ipp8u* GetLineData(LongInt iLine) {return (m_pData + m_iWidthStep * iLine);};
	inline Ipp8u* GetData() {return m_pData;}
	inline LongInt GetWidth() {return m_iWidth;}
	inline LongInt GetHeight() {return m_iHeight;}
	inline LongInt GetWidthStep() {return m_iWidthStep;}
public:
	CIppImageData(void);
	~CIppImageData(void);
private:
	int  m_iWidth;
	int  m_iHeight;
	int  m_iWidthStep;
	IppiSize  m_IppiSize;

	Ipp8u* m_pData;

public:
	class IppFloat
	{
		public:
			void  FromIppImageData(CIppImageData& data);
			void  ToIppImageData(CIppImageData& data);

			void Alloc(LongInt iWidth, LongInt iHeight);
		public:
			IppFloat();
			~IppFloat();

		public:
			int  m_iWidth;
			int  m_iHeight;
			int  m_iWidthStep;
			IppiSize  m_IppiSize;
			Ipp32f* m_pData;
	};
};






#ifndef IPPAutoLink
#define IPPAutoLink
	#ifdef WIN64
		#pragma comment( lib, "C:\\Program Files (x86)\\IntelSWTools\\compilers_and_libraries\\windows\\ipp\\lib\\intel64_win\\ippi.lib" )
		#pragma comment( lib, "C:\\Program Files (x86)\\IntelSWTools\\compilers_and_libraries\\windows\\ipp\\lib\\intel64_win\\ipps.lib" )
		#pragma comment( lib, "C:\\Program Files (x86)\\IntelSWTools\\compilers_and_libraries\\windows\\ipp\\lib\\intel64_win\\ippcv.lib" )
	#else
		#pragma comment(lib,  "ippi.lib")
		#pragma comment(lib,  "ipps.lib")
		#pragma comment(lib,  "ippj.lib")
		#pragma message("======== 程序隐式链接到 IPP模块=======")
	#endif
#endif

