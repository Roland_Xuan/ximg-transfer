#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTIPCModule.h
// 作  者 ：杨朝霖
// 用  途 ：ArNTIPCModule模块接口定义文件
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>
///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖     2010-8-10    创建

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
#include "ArNTIPCErrorList.h"
///</header_info>

///<datastruct_info>
//==============================================================================
// 基本数据类型定义
#define QoS_Speed		0x01		//优先考虑速度
#define QoS_Stabilize   0x00		//优先考虑稳定性
#define QoS_Accuracy	0x02		//优先考虑准确性

#define IPC_NAME  L"63E27DB6-332C-427e-A00B-6ACDE3F47F2F"
// tagMCSIIPCInfo定义用于描述进程通道的结构
typedef struct tagArNTIPCInfo
{
	LongInt			iSize;
	SHORTSTR		strChannelName; //进程下的通道名称
	LongLongInt		ID; //注册后返回的通道ID
	Byte			QosType;//通道的传输质量要求
	BufferByte		Reserve[7]; //预留的7个字节数据
}ArNTIPCInfo, *PArNTIPCInfo;

///</datastruct_info>

//==============================================================================
///<export_info>
//==============================================================================
//dll中导出的函数
extern "C"{

///<func_info>
//描述：获取版本信息
//参数：LONGSTR& strVersion 获取的版本信息
void   __stdcall ArNTPCModuleGetVersion(LONGSTR& strVersion);
///</func_info>

///<func_info>
//描述：注册进程通道，用于发送和接收数据
//参数：ArNTIPCInfo& Info 注册的通道信息
//      pIPCMessageFuncDef pFunc 用于处理接收到数据的回调函数,如果为NULL表示该通道仅用于发送数据
//      PMemPointer pContext 传递的回调函数环境指针，可以在pFunc中被调用
//      MCSI_BOOL bEnableBuffer 是否支持缓冲进程间消息块, MCSI_TRUE表示支持，默认为MCSI_FALSE表示不支持

//接收到其他进程通道发送过来的数据
typedef void (__stdcall *PIPCMessageFuncDef)(ULongInt iMemSize, PMemPointer pMem, PMemPointer pContext); 
typedef void (__stdcall *PIPCInitFuncDef)(PMemPointer pContext); 
typedef void (__stdcall *PIPCExitFuncDef)(PMemPointer pContext); 

ArNT_ERROR_CODE   __stdcall ArNTIPCRegChannel(ArNTIPCInfo& Info, PIPCMessageFuncDef pFunc, PMemPointer pContext, PIPCInitFuncDef pInit = NULL, PIPCExitFuncDef pExit = NULL);
///</func_info>

///<func_info>
//描述：获取错误描述码
//参数：LONGSTR& strVersion 获取的版本信息
ArNT_BOOL   __stdcall ArNTIPCGetErrorInfo(ArNT_ERROR_CODE code, LONGSTR& strInfo);
///</func_info>

///<func_info>
//描述：删除通道
//参数：
//备注：只有创建该通道的进程调用该函数才有效
ArNT_ERROR_CODE   __stdcall ArNTRemoveChannel(StrPointer strName);

///</func_info>

///<func_info>
//描述：获取通道中注册的子项数量
//参数：StrPointer strName 指定要判断的通道名,  ShortInt& iRegItemNum表示在该通道上注册的项数，0表示通道不存在
//备注:
ArNT_ERROR_CODE   __stdcall ArNTChannelRegNum(StrPointer strName,  ShortInt& iRegItemNum);

///</func_info>

///<func_info>
//描述：向指定的通道发送数据
//参数：StrPointer strName	指定要判断的通道名
//		PMemPointer pData	表示需要发送的数据
//		LongInt iDataLen	需要发送的数据长度
//		ArNTIPCInfo::QosType type 传输质量
//备注:
ArNT_ERROR_CODE   __stdcall ArNTIPCSendData(StrPointer strName,  PMemPointer pData, LongInt iDataLen, Byte type = QoS_Speed);

///</func_info>

///<func_info>
//描述：获取通道ID
//参数：StrPointer strName 指定的通道名,  LongLongInt& iID 保存返回的通道ID
//备注:
ArNT_BOOL   __stdcall ArNTIPCGetChannelID(StrPointer strName,  LongLongInt& iID);

///</func_info>
}
//==============================================================================
///</export_info>

//==============================================================================
///<macro_info>

//</macro_info>
#ifndef IPCModuleNOAutoLink
#define IPCModuleNOAutoLink
//#pragma comment( lib, "ArNTIPCModule.lib" )
#endif

