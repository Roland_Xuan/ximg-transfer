#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTTimer.h
// 作  者 ：杨朝霖
// 用  途 ：定义了用于非GUI界面下的计时器类
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖       2012-12-1    创建模块

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
#include "ArNTCriticalSection.h"
///</header_info>

///<datastruct_info>
//==============================================================================
//常用数据结构定义
//计时器入口函数指针类型
typedef void (ARNT_CALL* pTimerFuncDef)(ShortInt iTimerID, PObjPointer pPara);

//计时器相关信息
typedef struct tagTimerContex
{
	ShortInt		iID;		//计时器的ID，用于区分不同计时器的整数值
	ShortInt		iInterval;	//计时器的时间间隔
	ShortInt		iCounter;	//计时器的计数值
}TimerContex, *PTimerContex;

///</datastruct_info>

///<class_info>
//==============================================================================
//名称:ArNTTimer
//功能:实现用于计时器的类对象
class ArNTTimer
{
public:
	///<func_info>
	//描述：注册计时器回调函数
	//参数：pTimerFuncDef pFunc	计时器回调函数
	//		PObjPointer pPara	传递给计时器回调函数的相关环境参数
	//返回值：无
	void RegTimer(pTimerFuncDef pFunc, PObjPointer pPara);
	///</func_info>

	///<func_info>
	//描述：添加一个计时器
	//参数：ShortInt iTiemrID	计时器ID,用于在计时器回调函数中区分不同计时器
	//		ShortInt iInterval  计时器的计时长度，以TIMER_INTERVAL为单位
	//返回值：无
	void SetTimer(ShortInt iTiemrID, ShortInt iInterval);
	///</func_info>

	///<func_info>
	//描述：删除一个计时器
	//参数：ShortInt iTiemrID	计时器ID
	//返回值：无
	void KillTimer(ShortInt iTiemrID);
	///</func_info>
public:
	enum {TIMER_INTERVAL = 100};
	ArNTTimer(void);
	~ArNTTimer(void);
private:
	enum {MAX_ITEM = 32};//最多允许添加MAX_ITEM个计时器
	TimerContex  m_TimerArray[MAX_ITEM];
	ShortInt     m_iTimerNum;

	pTimerFuncDef  m_pTimerFunc;
	PObjPointer    m_pPara;

	CTaskCriticalSection m_csOper;
	ArNT_BOOL		 m_bExit;
	
	HANDLE   m_hThreadTimer;
	void   TimerProcess(void);
	
	static unsigned int __stdcall threadTimerProcess(void* pPara);
};

///</class_info>

///<macro_info>
/////////////////////////////////////////////////////////////////////////////
//定义辅助宏

#define  DECL_TIMER(classobj)		ArNTTimer  m_Timer;\
									static  void __stdcall TimerFunc(ShortInt iTimerID,  PObjPointer pPara);

#define  IMPL_TIMER(classobj)	void __stdcall classobj##::TimerFunc(ShortInt iTimerID,  PObjPointer pPara){\
								classobj* pParent = (classobj*)	pPara;\
								pParent->OnTimer(iTimerID); }

#define  REG_TIMER() m_Timer.RegTimer(TimerFunc, this);
#define  SET_TIMER(ID, Interval)  m_Timer.SetTimer(ID, Interval);
#define  KILL_TIMER(ID)  m_Timer.KillTimer(ID);

///</macro_info>
	
