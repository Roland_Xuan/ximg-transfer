#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTRunInfoOper.h
// 作  者 ：杨朝霖
// 用  途 ：用来操作ArNTRunINfo
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
///</header_info>

///<class_info>
//名称:ArNTRunInfoOper 运行信息操作类
//功能:用于记录运行信息的类

class ArNTRunInfoOper
{
public: 
	///<func_info>
	//描述：添加一条运行信息
	//参数：StrPointer strTitle运行信息的标题，NULL值表示单条无标题的信息
	// StrPointer strInfo 信息内容
	ArNT_BOOL  AddRunInfo(StrPointer strTitle, StrPointer strInfo);
	///</func_info>
	
	///<func_info>
	//描述：获取运行信息列表
	//参数：ArNTRunInfo* pInfo保存信息指针
	//ShortInt& iInfoNum 信息的数量
	void  GetRunInfo(ArNTRunInfo* pInfo, ShortInt& iInfoNum);
	///</func_info>
public:
	///<func_info>
	//描述：类构造函数
	//参数：ShortInt iMaxRunInfoNum 指定可以保存的最大信息数量
	ArNTRunInfoOper(ShortInt iMaxRunInfoNum = 512);
	///</func_info>
	~ArNTRunInfoOper(void);

private:
	ShortInt m_iMaxRunInfoNum;
	ShortInt m_iMaxChangeRunInfoNum;
	ArNTRunInfo*  m_pRunInfo;
	ArNTRunInfo*  m_pChangeRunInfo;
	ShortInt  m_iRunInfoNum;
	ShortInt  m_iChangeRunInfoNum;
	ShortInt  m_iCurrChangeRunInfoIndex;
};
//==============================================================================
///</class_info>