#define  NOAUTOLINK_IOPHELPER
#include "ArNTIOPMsgHandler.h"
#include "CommonFunc.h"
#include "ArNTDebugModule.h"
#include "ArNTCommandOper.h"


//////////////////////////////////////////////////////////////
//定义命令列表
BEGIN_CMD_LIST(IOP)
CMD_ENTRY(IOP_ASK_READIMAGE)
CMD_ENTRY(IOP_ACK_READIMAGE)
CMD_ENTRY(IOP_ASK_WRITEIMAGE)
CMD_ENTRY(IOP_ASK_CMDLIST)
CMD_ENTRY(IOP_ACK_CMDLIST)
CMD_ENTRY(IOP_ASK_SUPPORT_TYPE)
CMD_ENTRY(IOP_ACK_SUPPORT_TYPE)
CMD_ENTRY(IOP_ASK_RUNINFO)
CMD_ENTRY(IOP_ACK_RUNINFO)
CMD_ENTRY(IOP_ASK_SEARCHIMAGE)
CMD_ENTRY(IOP_ACK_SEARCHIMAGE)

CMD_ENTRY(IOP_ASK_ERROR_LIST)
CMD_ENTRY(IOP_ACK_ERROR_LIST)
CMD_ENTRY(IOP_NTF_ERROR)
END_CMD_LIST
///////////////////////////////////////////////////////////
//消息响应类
ArNTIOPEvtHandler::ArNTIOPEvtHandler(void)
{
}

ArNTIOPEvtHandler::~ArNTIOPEvtHandler(void)
{
}
//------------------------------------------------------------
//接口函数
//----------------------------------------------------------
//
void  ArNTIOPEvtHandler::RegParent(void* pParent)
{
	m_pParent = pParent;

}
//----------------------------------------------------------
//
void  ArNTIOPEvtHandler::OnIOPAskReadImage(LONGSTR&  strFromChannel, StrPointer strType, StrPointer strDesc, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ASK_READIMAGE_DESC);
}

//----------------------------------------------------------
//
void  ArNTIOPEvtHandler::OnIOPAskWriteImage(LONGSTR&  strFromChannel,
											StrPointer strType, 
											StrPointer strDesc, 
											PRawCaptureImage pRawImg,
											PBufferByte pImgContext,
											ShortInt    iContextSize,
											StrPointer strSenderID)
{

	DefautNotImplFunc(IOP_ASK_WRITEIMAGE_DESC);
}

//----------------------------------------------------------
//
void  ArNTIOPEvtHandler::OnIOPAskAppRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID)
{

	DefautNotImplFunc(IOP_ASK_RUNINFO_DESC);
}


//----------------------------------------------------------
//
void  ArNTIOPEvtHandler::OnIOPAskSupportType(LONGSTR& strFromChannel, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ASK_SUPPORT_TYPE_DESC);
}
//----------------------------------------------------------
//
void ArNTIOPEvtHandler::OnIOPAskSearchImg(LONGSTR& strFromChannel, StrPointer strType,  StrPointer strCondition, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ASK_SEARCHIMAGE_DESC);
}
//----------------------------------------------------------
//
void ArNTIOPEvtHandler::OnIOPAskService(LONGSTR& strFromChannel, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ASK_SEARCHIMAGE_DESC);
}
//----------------------------------------------------------
//询问错误列表
void ArNTIOPEvtHandler::OnIOPAskErrorList(LONGSTR& strFromChannel, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ASK_ERROR_LIST_DESC);

}

////////////////////////////////////////////////////////////
//私有函数
void ArNTIOPEvtHandler::DefautNotImplFunc(StrPointer strFuncInfo)
{
	SHORTSTR strInfo = {0};
	CCommonFunc::SafeStringPrintf(strInfo, STR_LEN(strInfo), L"消息%s未实现", strFuncInfo); 	
	WRITE_WARN(strInfo);

}

///////////////////////////////////////////////////////////////////////////////////////
//消息处理类

//--------------------------------------------------------------
//构造函数
ArNTIOPMsgHandler::ArNTIOPMsgHandler()
{



}

ArNTIOPMsgHandler::~ArNTIOPMsgHandler()
{
	UnRegIOP();
}
////////////////////////////////////////////////////////////
//接口函数
//---------------------------------------------------------
//注册响应事件处理类
ArNT_BOOL ArNTIOPMsgHandler::RegEvtHandler(ArNTIOPEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace,  ShortInt iObjID)
{
	m_pEvtHandler = pEvtHandler;
	m_bEnableTrace = bEnableTrace;
	ArNT_BOOL bResutl = ArNT_TRUE;
	bResutl = RegIOP(m_strRegChannel, 1024* 1024, iObjID);
	return bResutl;
}
//---------------------------------------------------------
//取消通道注册
void ArNTIOPMsgHandler::UnRegEvtHandler(ShortInt iObjID)
{
	UnRegIOP(iObjID);
}


//////////////////////////////////////////////////////////////
//消息处理函数

//定义消息映射
BEGIN_IOP_LOCAL_MESSAGE(ArNTIOPMsgHandler)

IOP_MESSAGE_ENTRY(IOP_ASK_READIMAGE, ProcessIOPAskReadImage, IOPAskReadImage)
IOP_MESSAGE_ENTRY(IOP_ASK_WRITEIMAGE, ProcessIOPAskWriteImage, IOPAskWriteImage);
IOP_MESSAGE_ENTRY(IOP_ASK_CMDLIST,  ProcessIOPAskCmdList, IOPAskCmdList);
IOP_MESSAGE_ENTRY(IOP_ASK_SUPPORT_TYPE, ProcessIOPAskSupportType, IOPAskSupportType);
IOP_MESSAGE_ENTRY(IOP_ASK_RUNINFO,   ProcessIOPAskRunInfo, IOPAskRunInfo);
IOP_MESSAGE_ENTRY(IOP_ASK_SEARCHIMAGE, ProcessIOPAskSearchImage,IOPAskSearchImage); 
IOP_MESSAGE_ENTRY(IOP_ASK_SERVICE, ProcessIOPAskService,IOPAskService); 
IOP_MESSAGE_ENTRY(IOP_ASK_ERROR_LIST, ProcessIOPAskErrorList,IOPAskErrorList); 

END_IOP_MESSAGE(ArNTIOPMsgHandler)


//--------------------------------------------------
//消息处理函数
//--------------------------------------------------
//请求读取图像
void   ArNTIOPMsgHandler::ProcessIOPAskReadImage(PIOPAskReadImage pPara,  StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ASK_READIMAGE_DESC);
	if(m_pEvtHandler) m_pEvtHandler->OnIOPAskReadImage(pPara->strFromChannel, pPara->ImgDesc.strType, pPara->ImgDesc.strInfo, strSenderID);
}
//--------------------------------------------------
//请求写入图像
void   ArNTIOPMsgHandler::ProcessIOPAskWriteImage(PIOPAskWriteImage pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ASK_WRITEIMAGE_DESC);
	if(m_pEvtHandler) m_pEvtHandler->OnIOPAskWriteImage(pPara->strFromChannel,
														pPara->ImgDesc.strType,
														pPara->ImgDesc.strInfo, 
														pPara->RawImg, 
														pPara->ImgageContex, 
														pPara->iContextSize, 
														strSenderID);

}
//--------------------------------------------------
//询问IOP服务支持的命令

void   ArNTIOPMsgHandler::ProcessIOPAskCmdList(PIOPAskCmdList pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ASK_CMDLIST_DESC);
	ShortInt iCmdNum = sizeof(gCmdDescListIOP) / sizeof(gCmdDescListIOP[0]) - 1;
	if(iCmdNum > 0)
	{
		LongInt iSize = sizeof(IOPAckCmdList) + (iCmdNum - 1) * sizeof(ArNTCommandDesc);
		PBufferByte pBuffer = new BufferByte[iSize];
		PIOPAckCmdList pAckInfo = (PIOPAckCmdList)pBuffer;
		pAckInfo->iSize = sizeof(IOPAckCmdList);
		pAckInfo->iCmdNum = iCmdNum;
		for(int i = 0; i < iCmdNum; i++)
		{
			pAckInfo->Desc[i].iID = gCmdDescListIOP[i].iID;
			CCommonFunc::SafeStringCpy(pAckInfo->Desc[i].strName, STR_LEN(pAckInfo->Desc[i].strName), gCmdDescListIOP[i].strName);
			CCommonFunc::SafeStringCpy(pAckInfo->Desc[i].strDesc, STR_LEN(pAckInfo->Desc[i].strDesc), gCmdDescListIOP[i].strDesc);
		}
		ArNTCommandOper::PostCmdToChannel(IOP_ACK_CMDLIST, pBuffer, iSize, pPara->strFromChannel);

		delete pBuffer;
	}
}
//--------------------------------------------------
//询问支持的图像存储类别
void   ArNTIOPMsgHandler::ProcessIOPAskSupportType(PIOPAskSupportType pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ASK_SUPPORT_TYPE_DESC);
	if(m_pEvtHandler) m_pEvtHandler->OnIOPAskSupportType(pPara->strFromChannel, strSenderID);
}
//--------------------------------------------------
//询问程序的运行信息
void   ArNTIOPMsgHandler::ProcessIOPAskRunInfo(PIOPAskRunInfo pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ASK_RUNINFO_DESC);
	if(m_pEvtHandler) m_pEvtHandler->OnIOPAskAppRunInfo(pPara->strFromChannel, strSenderID);

}
//--------------------------------------------------
//查询满足条件的图像
void   ArNTIOPMsgHandler::ProcessIOPAskSearchImage(PIOPAskSearchImage pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ASK_SEARCHIMAGE_DESC);
	if(m_pEvtHandler) m_pEvtHandler->OnIOPAskSearchImg(pPara->strFromChannel, pPara->strType, pPara->strCondition, strSenderID);
}

//--------------------------------------------------
//查询支持IOP协议的服务
void   ArNTIOPMsgHandler::ProcessIOPAskService(PIOPAskService pPara,  StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ASK_SERVICE_DESC);
	if(m_pEvtHandler) m_pEvtHandler->OnIOPAskService(pPara->strFromChannel, strSenderID);

}
//--------------------------------------------------
//查询错误列表
void   ArNTIOPMsgHandler::ProcessIOPAskErrorList(PIOPAskErrorList pPara,  StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ASK_ERROR_LIST_DESC);
	if(m_pEvtHandler) m_pEvtHandler->OnIOPAskErrorList(pPara->strFromChannel, strSenderID);

}

//--------------------------------------------------
//回复函数

//----------------------------------------------------------------------------------------------------------
//回复读取的图像
void  ArNTIOPMsgHandler::AckReadImage(LONGSTR& strFromChannel, PRawCaptureImage pImage, StrPointer strType, PBufferByte pImgageContex, ShortInt iContextSize, StrPointer strSenderID)
{
	LongInt iSize = sizeof(IOPAckReadImage) + pImage->iToatalImgSize;
	PBufferByte pBuffer = new BufferByte[iSize];
	PIOPAckReadImage pAckInfo = (PIOPAckReadImage)pBuffer;

	pAckInfo->iSize = sizeof(IOPAckReadImage);
	CCommonFunc::SafeStringCpy(pAckInfo->strType, STR_LEN(pAckInfo->strType), strType);
	pAckInfo->iContextSize = iContextSize;
	memcpy(pAckInfo->ImageContex, pImgageContex, iContextSize);
	
	pAckInfo->RawImg->iChannelNum = pImage->iChannelNum;
	pAckInfo->RawImg->iHeight = pImage->iHeight;
	pAckInfo->RawImg->iWidth = pImage->iWidth;
	pAckInfo->RawImg->iWidthStep = pImage->iWidthStep;
	CCommonFunc::SafeStringCpy(pAckInfo->RawImg->strID, STR_LEN(pAckInfo->RawImg->strID), pImage->strID);
	memcpy(pAckInfo->RawImg->pImgData,  pImage->pImgData, pImage->iToatalImgSize);

	pAckInfo->RawImg->iSize = sizeof(RawCaptureImage);
	pAckInfo->RawImg->iToatalImgSize = pImage->iToatalImgSize;

	ArNTCommandOper::PostCmdToChannel(IOP_ACK_READIMAGE, pBuffer, iSize, strFromChannel, strSenderID);
	delete pBuffer;
}

//----------------------------------------------------------------------------------------------------------
//回复支持的保存类型
void  ArNTIOPMsgHandler::AckSupportType(LONGSTR& strFromChannel, StrPointer strAppName, ShortInt iTypeNum, PArNTImageSupportType pType, StrPointer strSenderID)
{
	IOPAckSupportType AckInfo = {sizeof(AckInfo)};
	if(iTypeNum > IOPAckSupportType::MAX_ITEM_NUM) iTypeNum = IOPAckSupportType::MAX_ITEM_NUM;
	AckInfo.iTypeNum = iTypeNum;
	CCommonFunc::SafeStringCpy(AckInfo.strAppName, STR_LEN(AckInfo.strAppName), strAppName);
	memcpy(AckInfo.Items, pType, iTypeNum * sizeof(ArNTImageSupportType));
	ArNTCommandOper::PostCmdToChannel(IOP_ACK_SUPPORT_TYPE, &AckInfo, AckInfo.iSize, strFromChannel, strSenderID);
}

//--------------------------------------------------
//回复服务的运行信息
void  ArNTIOPMsgHandler::AckRunInfo(LONGSTR& strFromChannel, PArNTRunInfo pInfo, ShortInt iInfoNum, StrPointer strSenderID)
{
	LongInt iSize = sizeof(IOPAckRunInfo) + (iInfoNum - 1) * sizeof(ArNTRunInfo);
	PBufferByte pBuffer = new BufferByte[iSize];
	PIOPAckRunInfo pAckInfo = (PIOPAckRunInfo)pBuffer;
	pAckInfo->iSize = sizeof(IOPAckRunInfo);
	pAckInfo->iInfoNum = iInfoNum;
	memcpy(pAckInfo->RunInfo, pInfo, iInfoNum * sizeof(ArNTRunInfo));
	ArNTCommandOper::PostCmdToChannel(IOP_ACK_RUNINFO, pBuffer, iSize, strFromChannel, strSenderID);
	delete pBuffer;
}

//--------------------------------------------------
//回复查询到的图像信息
void  ArNTIOPMsgHandler::AckSearchImage(LONGSTR& strFromChannel,  PArNTImageDesc pImageDesc, ShortInt iImgNum, StrPointer strSenderID)
{
	LongInt iSize = sizeof(IOPAckSearchImage) + (iImgNum - 1) * sizeof(ArNTImageDesc);
	PBufferByte pBuffer = new BufferByte[iSize];
	PIOPAckSearchImage pAckInfo = (PIOPAckSearchImage)pBuffer;
	pAckInfo->iSize = sizeof(IOPAckSearchImage);
	pAckInfo->iImgNum = iImgNum;
	memcpy(pAckInfo->ImgDesc, pImageDesc, iImgNum *  sizeof(ArNTImageDesc));
	ArNTCommandOper::PostCmdToChannel(IOP_ACK_SEARCHIMAGE, pBuffer, iSize, strFromChannel, strSenderID);
	delete pBuffer;
}

//--------------------------------------------------
//回复支持IOP协议的服务
void  ArNTIOPMsgHandler::AckService(LONGSTR& strFromChannel, StrPointer strApp, StrPointer strVersion, StrPointer strSenderID)
{
	IOPAckService Para = {sizeof(IOPAckService)};
	CCommonFunc::SafeStringCpy(Para.Service.strAppName, STR_LEN(Para.Service.strAppName), strApp);
	CCommonFunc::SafeStringCpy(Para.Service.strVersion, STR_LEN(Para.Service.strVersion), strVersion);

	ArNTCommandOper::PostCmdToChannel(IOP_ACK_SERVICE, &Para, Para.iSize, strFromChannel, strSenderID);
}

//--------------------------------------------------
//通知错误消息
void  ArNTIOPMsgHandler::NtfErrorInfo(LONGSTR& strFromChannel, StrPointer strCmdName, ArNT_ERROR_CODE ErrorCode, StrPointer strErrorInfo, StrPointer strSenderID)
{
	IOPNtfError Para = {sizeof(IOPNtfError)};
	CCommonFunc::GetAppPathName(Para.strAppName, STR_LEN(Para.strAppName));
	CCommonFunc::SafeStringCpy(Para.strCmdName, STR_LEN(Para.strCmdName), strCmdName);
	Para.dwErrorCode = ErrorCode;
	CCommonFunc::SafeStringCpy(Para.strErrorInfo, STR_LEN(Para.strErrorInfo), strErrorInfo);
	ArNTCommandOper::PostCmdToChannel(IOP_NTF_ERROR, &Para, Para.iSize, strFromChannel, strSenderID);
}

//--------------------------------------------------
//回复错误列表
void  ArNTIOPMsgHandler::AckErrorList(PArNTErrorInfo pError, ShortInt iErrorNum, LONGSTR& strFromChannel, StrPointer strSenderID)
{
	LongInt iSize = sizeof(IOPAckErrorList) + (iErrorNum - 1) * sizeof(ArNTErrorInfo);
	PBufferByte pBuffer = new BufferByte[iSize];
	PIOPAckErrorList pAckInfo = (PIOPAckErrorList)pBuffer;
	pAckInfo->iSize = sizeof(IOPAckErrorList);
	pAckInfo->iErrorNum = iErrorNum;
	memcpy(pAckInfo->ErrorInfo, pError, iErrorNum * sizeof(ArNTErrorInfo));
	ArNTCommandOper::PostCmdToChannel(IOP_ACK_ERROR_LIST, pBuffer, iSize, strFromChannel, strSenderID);
	delete pBuffer;

}