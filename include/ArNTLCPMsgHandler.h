#pragma once

#include "LocalConfigProtocal.h"
//--------------------------------------
//消息响应类
class ArNTLCPEvtHandler
{
public:
	virtual  void OnLCPAskRunInfo(LONGSTR& strFromChannel,  StrPointer strSenderID);
	virtual  void OnLCPAskCmdList(LONGSTR& strFromChannel,  StrPointer strSenderID);
	virtual  void OnLCPAskSysPara(LONGSTR& strName, LONGSTR& strFromChannel, StrPointer strSenderID);
	virtual  void OnLCPAskChangeSysPara(LONGSTR& strName, LONGSTR& strValue, LONGSTR& strFromChannel, StrPointer strSenderID);
	virtual  void OnLCPAskServece( LONGSTR& strFromChannel, StrPointer strSenderID);
	virtual  void OnLCPAskErrorList(LONGSTR& strFromChannel, StrPointer strSenderID);
	virtual  void OnLCPAskAddSysPara(LONGSTR& strName,  LONGSTR& strValue, LONGSTR& strFromChannel, StrPointer strSenderID);
	virtual  void OnLCPAskDelSysPara(LONGSTR& strName, LONGSTR& strFromChannel, StrPointer strSenderID);
public:
	ArNTLCPEvtHandler();
	~ArNTLCPEvtHandler();

public:
	void* m_pParent;
	void  RegParent(void* pParent);
private:
	void DefautNotImplFunc(StrPointer strFuncInfo);
};
//----------------------------------------
//消息处理类
class ArNTLCPMsgHandler
{
public:
	DECL_LCP_LOCAL_MESSAGE(ArNTLCPMsgHandler)
public:
	ArNTLCPMsgHandler(void);
	~ArNTLCPMsgHandler(void);
public:
	
	ArNT_BOOL RegEvtHandler(ArNTLCPEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace);
	void UnRegEvtHandler(void);
private:
	ArNT_BOOL  m_bTraceMsg;	
	ArNTLCPEvtHandler* m_pEvtHandler;
	//--------------------------------------------------
	//消息处理函数
	void   ProcessAskRunInfo(PLCPAskRunInfo pPara,  StrPointer strSenderID);
	void   ProcessAskCmdList(PLCPAskCmdList pPara,  StrPointer strSenderID);
	void   ProcessAskSysPara(PLCPAskSysPara	pPara, StrPointer strSenderID);
	void   ProcessAskChangeSysPara(PLCPAskChangeSysPara pPara,  StrPointer strSenderID);
	void   ProcessAskService(PLCPAskService pPara,  StrPointer strSenderID);
	void   ProcessAskErrorList(PLCPAskErrorList pPara, StrPointer strSenderID);
	void   ProcessAskAddSysPara(PLCPAskAddSysPara	pPara, StrPointer strSenderID);
	void   ProcessAskDelSysPara(PLCPAskDelSysPara	pPara, StrPointer strSenderID);

public:
	LONGSTR   m_strLPCUIChannel;

public:
	//-------------------------------------------------
	//回复函数
	void  AckRunInfo(StrPointer strAppName, PArNTRunInfo pInfo, ShortInt iInfoNum, LONGSTR& strFromChannel,  StrPointer strSenderID = NULL);
	void  AckSysPara(StrPointer strName, StrPointer strValue,  LONGSTR& strFromChannel, StrPointer strSenderID = NULL);
	void  AckService(StrPointer strAppName, StrPointer strVersion, LONGSTR& strFromChannel, StrPointer strSenderID = NULL);
	void  AckErrorList(StrPointer strAppName, PArNTErrorInfo pError, ShortInt iErrorNum, LONGSTR& strFromChannel, StrPointer strSenderID = NULL);
	void  NtfErrorInfo(ArNT_ERROR_CODE code, StrPointer strCmd, StrPointer strDesc, LONGSTR& strFromChannel, StrPointer strSenderID);
	void  NtfErrorInfo(ArNT_ERROR_CODE code, StrPointer strCmd, StrPointer strDesc);

};
//////////////////////////////////////////////////////////////////////////////////////
//宏定义

//将ArNTLCPEvtHandler作为内嵌类进行定义
#define BEGIN_LCPEVT_NEST(classobj) class  ArNTLCPEvt:public ArNTLCPEvtHandler{ 

#define DECL_LCP_DEFALUT_FUNC()		DECL_LCP_ASK_RUNINFO();\
									DECL_LCP_ASK_SYSPARA();\
									DECL_LCP_ASK_CHANGE_SYSPARA();\
									DECL_LCP_ASK_ERRORLIST();\
									DECL_LCP_ASK_ADD_SYSPARA();\
									DECL_LCP_ASK_DEL_SYSPARA();

#define DECL_LCP_ASK_RUNINFO()					public:	virtual  void  OnLCPAskRunInfo(LONGSTR& strFromChannel,  StrPointer strSenderID);
#define DECL_LCP_ASK_SYSPARA()					public:	virtual  void  OnLCPAskSysPara(LONGSTR& strName, LONGSTR& strFromChannel,  StrPointer strSenderID);
#define DECL_LCP_ASK_CHANGE_SYSPARA()			public:	virtual  void  OnLCPAskChangeSysPara(LONGSTR& strName, LONGSTR& strValue, LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_LCP_ASK_SERVICE()					public: virtual  void  OnLCPAskServece( LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_LCP_ASK_CMDLIST()					public: virtual  void  OnLCPAskCmdList(LONGSTR& strFromChannel,  StrPointer strSenderID);
#define DECL_LCP_ASK_ERRORLIST()				public: virtual  void  OnLCPAskErrorList(LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_LCP_ASK_ADD_SYSPARA()				public: virtual  void  OnLCPAskAddSysPara(LONGSTR& strName, LONGSTR& strValue,  LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_LCP_ASK_DEL_SYSPARA()				public: virtual  void  OnLCPAskDelSysPara(LONGSTR& strName, LONGSTR& strFromChannel, StrPointer strSenderID);

#define END_LCPEVT_NEST(classobj)		};\
										ArNTLCPEvt				m_LCPEvtHandler;\
										ArNTLCPMsgHandler		m_LCPMsgHandler;\
										ArNTLCPMsgHandler*		GetLCPMsgHandler() {return &m_LCPMsgHandler;}

#define  REG_LCPEVT_NEST(bEnableTrace)  m_LCPEvtHandler.RegParent(this);\
								if(ArNT_FALSE == m_LCPMsgHandler.RegEvtHandler(&m_LCPEvtHandler, bEnableTrace))\
								{return ArNT_FALSE;}

#define  UNREG_LCPEVT_NEST()  

//定义实现类
#define  IMPL_LCP_DEFAULT_FUNC(classobj)	IMPL_LCP_ASK_RUNINFO(classobj);\
											IMPL_LCP_ASK_SYSPARA(classobj);\
											IMPL_LCP_ASK_CHANGE_SYSPARA(classobj);\
											IMPL_LCP_ASK_ERRORLIST(classobj);\
											IMPL_LCP_ASK_ADD_SYSPARA(classobj);\
											IMPL_LCP_ASK_DEL_SYSPARA(classobj);

#define  IMPL_LCP_ASK_RUNINFO(classobj) void classobj::ArNTLCPEvt::OnLCPAskRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPAskRunInfo(strFromChannel, strSenderID);}}

#define  IMPL_LCP_ASK_SYSPARA(classobj) void classobj::ArNTLCPEvt::OnLCPAskSysPara(LONGSTR& strName, LONGSTR& strFromChannel, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPAskSysPara(strName, strFromChannel, strSenderID);}}

#define  IMPL_LCP_ASK_CHANGE_SYSPARA(classobj) void classobj::ArNTLCPEvt::OnLCPAskChangeSysPara(LONGSTR& strName, LONGSTR& strValue, LONGSTR& strFromChannel, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPAskChangeSysPara(strName, strValue, strFromChannel, strSenderID);}}

#define  IMPL_LCP_ASK_SERVICE(classobj) void classobj::ArNTLCPEvt::OnLCPAskServece( LONGSTR& strFromChannel, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPAskServece(strFromChannel, strSenderID);}}

#define  IMPL_LCP_ASK_CMDLIST(classobj) void classobj::ArNTLCPEvt::OnLCPAskCmdList( LONGSTR& strFromChannel, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPAskCmdList(strFromChannel, strSenderID);}}

#define  IMPL_LCP_ASK_ERRORLIST(classobj) void classobj::ArNTLCPEvt::OnLCPAskErrorList(LONGSTR& strFromChannel, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPAskErrorList(strFromChannel, strSenderID);}}

#define  IMPL_LCP_ASK_ADD_SYSPARA(classobj) void classobj::ArNTLCPEvt::OnLCPAskAddSysPara(LONGSTR& strName,  LONGSTR& strValue,  LONGSTR& strFromChannel, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPAskAddSysPara(strName, strValue, strFromChannel, strSenderID);}}

#define  IMPL_LCP_ASK_DEL_SYSPARA(classobj) void classobj::ArNTLCPEvt::OnLCPAskDelSysPara(LONGSTR& strName, LONGSTR& strFromChannel, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPAskDelSysPara(strName, strFromChannel, strSenderID);}}



//==============================================================================
///链接信息
#ifndef  NOAUTOLINK_LPCHELPER
#define  NOAUTOLINK_LPCHELPER
#pragma comment( lib, "ArNTLCPHelper.lib" )

#endif
