#pragma once
#include "ImageCaptureProtocal.h"

//--------------------------------------
//消息响应类
class ArNTICPEvtHandler
{
public:
	//询问当前采集程序上的相机信息
	virtual  void OnICPAskCameralInfo(LONGSTR& strFromChannel, StrPointer strSenderID);
	//询问指定相机的当前曝光时间
	virtual  void OnICPAskExpTime(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID);
	//请求更改指定相机的曝光时间
	virtual  void OnICPAskChangeExpTime(LONGSTR& strFromChannel, StrPointer strCameralID, LongFloat dTime, StrPointer strSenderID);
	//询问指定相机的当前增益
	virtual  void OnICPAskGain(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID);
	//请求更改指定相机的当前增益
	virtual  void OnICPAskChangeGain(LONGSTR& strFromChannel, StrPointer strCameralID,LongFloat dGain,  StrPointer strSenderID);
	//询问指定相机的当前温度
	virtual  void OnICPAskTemperature(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID);
	//询问指定相机的采集速度
	virtual  void OnICPAskCaptureSpeed(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID);
	//请求更改指定相机的采集速度
	virtual  void OnICPAskChangeCaptureSpeed(LONGSTR& strFromChannel, StrPointer strCameralID, LongFloat dSpeed, StrPointer strSenderID);
	//询问采集程序的运行状态
	virtual  void OnICPAskCaptureAppRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID);
	//请求指定的相机开始采集
	virtual  void OnICPAskStartCapture(StrPointer strCameralID, StrPointer strSenderID);
	//请求指定的相机停止采集
	virtual  void OnICPAskStopCapture(StrPointer strCameralID, StrPointer strSenderID);	
	//询问指定的相机采集状态
	virtual  void OnICPAskCaptureStatus(LONGSTR& strFromChannel, StrPointer strCameralID, StrPointer strSenderID);
	//询问服务支持的错误列表
	virtual  void OnICPAskErrorList(LONGSTR& strFromChannel,  StrPointer strSenderID);
	//询问服务信息
	virtual  void OnICPAskService(LONGSTR& strFromChannel,  StrPointer strSenderID);

public:
	ArNTICPEvtHandler();
	~ArNTICPEvtHandler();
public:
	void* m_pParent;
	void  RegParent(void* pParent);

private:
	void DefautNotImplFunc(StrPointer strFuncInfo);
};
//-----------------------------------------------------------------
//消息处理类

class ArNTICPMsgHandler
{
	DECL_ICP_LOCAL_MESSAGE(ArNTICPMsgHandler)
public:
	ArNTICPMsgHandler(void);
	~ArNTICPMsgHandler(void);
public:
	
	ArNT_BOOL RegEvtHandler(ArNTICPEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace, ShortInt iCameralIndex);
	void  UnRegEvtHandler(ShortInt iCameralIndex);
private:
	ArNT_BOOL			m_bEnableTrace;	
	ArNTICPEvtHandler* m_pEvtHandler;
	//--------------------------------------------------
	//消息处理函数
	void   ProcessICPAskCameralInfo(PICPAskCameralInfo pPara,  StrPointer strSenderID); 
	void   ProcessICPAskExpTime(PICPAskExpTime pPara, StrPointer strSenderID); 
	void   ProcessICPAskChangeExpTime(PICPAskChangeExptime pPara, StrPointer strSenderID);
	void   ProcessICPAskGain(PICPAskGain pPara, StrPointer strSenderID); 
	void   ProcessICPAskChangeGain(PICPAskChangeGain pPara, StrPointer strSenderID); 
	void   ProcessICPAskCmdList(PICPAskCmdList pPara,  StrPointer strSenderID);  
	void   ProcessICPAskTemperature(PICPAskTemperature pPara,  StrPointer strSenderID);  
	void   ProcessICPAskCaptrueSpeed(PICPAskCaptureSpeed pPara, StrPointer strSenderID); 
	void   ProcessICPAskChangeCaptureSpeed(PICPAskChangeCaptureSpeed pPara, StrPointer strSenderID); 
	void   ProcessICPAskCaptureAppRunInfo(PICPAskRunInfo pPara,  StrPointer strSenderID); 
	void   ProcessICPAskStartCapture(PICPAskStartCapture pPara,StrPointer strSenderID);  
	void   ProcessICPAskStopCapture(PICPAskStopCapture pPara,StrPointer strSenderID);  
	void   ProcessICPAskCaptureStatus(PICPAskCaptureStatus pPara,StrPointer strSenderID); 
	void   ProcessICPAskErrorList(PICPAskErrorList pPara, StrPointer strSenderID); 
	void   ProcessICPAskService(PICPAskService pPara, StrPointer strSenderID); 

public:
	void  AckCameralInfo(ImgCaptureCameralInfo& Info, LONGSTR& strFromChannel, StrPointer strSenderID);
	void  AckCameralExpTime(StrPointer strCameralID, LongFloat dExpTime, LONGSTR& strFromChannel, StrPointer strSenderID);
	void  AckCameralGain(StrPointer strCameralID, LongFloat dGain, LONGSTR& strFromChannel, StrPointer strSenderID);
	void  AckCameralTemperature(StrPointer strCameralID, LongFloat dTemperature, LONGSTR& strFromChannel, StrPointer strSenderID);
	void  AckCaptureSpeed(StrPointer strCameralID, LongFloat dSpeed, ArNT_BOOL bLine, LONGSTR& strFromChannel, StrPointer strSenderID);
	void  AckCaptureFrameNum(StrPointer strCameralID, LongLongInt iFrameNum, LONGSTR& strFromChannel, StrPointer strSenderID);
	void  AckCaptureStatus(StrPointer strCameralID, ArNT_BOOL bIsCapture,  LONGSTR& strFromChannel, StrPointer strSenderID);
	void  AckRunInfo(PArNTRunInfo pInfo, ShortInt iInfoNum, LONGSTR& strFromChannel, StrPointer strSenderID);
	void  NtfRawImage(PRawCaptureImage pImage, PBufferByte pCmbBuffer, ShortInt iCameralIndex, ArNT_BOOL bTraceError = ArNT_FALSE);
	void  AckErrorList(PArNTErrorInfo pError, ShortInt iErrorNum, LONGSTR& strFromChannel, StrPointer strSenderID);
	void  NtfErrorInfo(ArNT_ERROR_CODE code, StrPointer strCmd, StrPointer strDesc, LONGSTR& strFromChannel, StrPointer strSenderID);
	void  NtfErrorInfo(ArNT_ERROR_CODE code, StrPointer strCmd, StrPointer strDesc);

	void  AckService(StrPointer strApp, StrPointer strVersion, LONGSTR& strFromChannel, StrPointer strSenderID);
	
};
//////////////////////////////////////////////////////////////////////////////////////
//宏定义
#define BEGIN_ICPEVT_NEST(classobj) class  ArNTICPEvt:public ArNTICPEvtHandler{ 

#define DECL_ICP_DEFAULT_FUNC()				DECL_ICP_ASK_CAMERALINFO();\
										DECL_ICP_ASK_EXPTIME();\
										DECL_ICP_ASK_CHANGE_EXPTIME();\
										DECL_ICP_ASK_GAIN();\
										DECL_ICP_ASK_TEMPERATURE();\
										DECL_ICP_ASK_CHANGE_GAIN();\
										DECL_ICP_ASK_CAPTURESPEED();\
										DECL_ICP_ASK_CHANGE_CAPTURESPEED();\
										DECL_ICP_ASK_RUNINFO();\
										DECL_ICP_ASK_START_CAPTURE();\
										DECL_ICP_ASK_STOP_CAPTURE();\
										DECL_ICP_ASK_CAPTURE_STATUS();\
										DECL_ICP_ASK_ERRORLIST();\
										DECL_ICP_ASK_SERVICE();


#define DECL_ICP_ASK_CAMERALINFO()				public:	virtual		void OnICPAskCameralInfo(LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_ICP_ASK_EXPTIME()					public: virtual		void OnICPAskExpTime(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID);
#define DECL_ICP_ASK_CHANGE_EXPTIME()			public: virtual     void OnICPAskChangeExpTime(LONGSTR& strFromChannel, StrPointer strCameralID, LongFloat dTime, StrPointer strSenderID);
#define DECL_ICP_ASK_GAIN()						public: virtual		void OnICPAskGain(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID);
#define DECL_ICP_ASK_CHANGE_GAIN()              public: virtual		void OnICPAskChangeGain(LONGSTR& strFromChannel, StrPointer strCameralID,LongFloat dGain,  StrPointer strSenderID);
#define DECL_ICP_ASK_TEMPERATURE()              public: virtual		void OnICPAskTemperature(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID);
#define DECL_ICP_ASK_CAPTURESPEED()				public:	virtual		void OnICPAskCaptureSpeed(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID);
#define DECL_ICP_ASK_CHANGE_CAPTURESPEED()		public: virtual		void OnICPAskChangeCaptureSpeed(LONGSTR& strFromChannel, StrPointer strCameralID, LongFloat dSpeed, StrPointer strSenderID);
#define DECL_ICP_ASK_RUNINFO()					public: virtual		void OnICPAskCaptureAppRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_ICP_ASK_START_CAPTURE()			public: virtual     void OnICPAskStartCapture(StrPointer strCameralID, StrPointer strSenderID);
#define DECL_ICP_ASK_STOP_CAPTURE()				public: virtual		void OnICPAskStopCapture(StrPointer strCameralID, StrPointer strSenderID);
#define DECL_ICP_ASK_CAPTURE_STATUS()			public: virtual		void OnICPAskCaptureStatus(LONGSTR& strFromChannel, StrPointer strCameralID, StrPointer strSenderID);
#define DECL_ICP_ASK_ERRORLIST()				public: virtual		void OnICPAskErrorList(LONGSTR& strFromChannel,  StrPointer strSenderID);
#define DECL_ICP_ASK_SERVICE()					public: virtual		void OnICPAskService(LONGSTR& strFromChannel,  StrPointer strSenderID);

#define END_ICPEVT_NEST(classobj)		};\
										private:	ArNTICPEvt				m_ICPEvtHandler;\
										private:    ArNTICPMsgHandler		m_ICPMsgHandler;\
										public:     ArNTICPMsgHandler*      GetICPMsgHandler() {return &m_ICPMsgHandler;};

#define  REG_ICPEVT_NEST(bEnableTrace, CameralID)  m_ICPEvtHandler.RegParent(this);\
								if(ArNT_FALSE == m_ICPMsgHandler.RegEvtHandler(&m_ICPEvtHandler, bEnableTrace, CameralID))\
								{return ArNT_FALSE;}

#define  UNREG_ICPEVT_NEST(CameralID)  m_ICPMsgHandler.UnRegEvtHandler(CameralID);

//定义实现类
#define  IMPL_ICP_DEFAULT_FUNC(classobj)	IMPL_ICP_ASK_CAMERALINFO(classobj); \
										IMPL_ICP_ASK_EXPTIME(classobj);\
										IMPL_ICP_ASK_CHANGE_EXPTIME(classobj);\
										IMPL_ICP_ASK_GAIN(classobj); \
										IMPL_ICP_ASK_TEMPERATURE(classobj);\
										IMPL_ICP_ASK_CHANGE_GAIN(classobj);\
										IMPL_ICP_ASK_CAPTURESPEED(classobj);\
										IMPL_ICP_ASK_CHANGE_CAPTURESPEED(classobj);\
										IMPL_ICP_ASK_RUNINFO(classobj);\
										IMPL_ICP_ASK_START_CAPTURE(classobj);\
										IMPL_ICP_ASK_STOP_CAPTURE(classobj);\
										IMPL_ICP_ASK_CAPTURE_STATUS(classobj);\
										IMPL_ICP_ASK_ERRORLIST(classobj);\
										IMPL_ICP_ASK_SERVICE(classobj);


#define  IMPL_ICP_ASK_CAMERALINFO(classobj)		void classobj::ArNTICPEvt::OnICPAskCameralInfo(LONGSTR& strFromChannel, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskCameralInfo(strFromChannel, strSenderID);}}

#define  IMPL_ICP_ASK_EXPTIME(classobj)			void classobj::ArNTICPEvt::OnICPAskExpTime(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskExpTime(strFromChannel, strCameralID, strSenderID);}}

#define  IMPL_ICP_ASK_CHANGE_EXPTIME(classobj)	void classobj::ArNTICPEvt::OnICPAskChangeExpTime(LONGSTR& strFromChannel, StrPointer strCameralID, LongFloat dTime, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskChangeExpTime(strFromChannel, strCameralID, dTime, strSenderID);}}

#define  IMPL_ICP_ASK_GAIN(classobj)			void classobj::ArNTICPEvt::OnICPAskGain(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskGain(strFromChannel, strCameralID, strSenderID);}}

#define  IMPL_ICP_ASK_CHANGE_GAIN(classobj)		void classobj::ArNTICPEvt::OnICPAskChangeGain(LONGSTR& strFromChannel, StrPointer strCameralID,LongFloat dGain,  StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskChangeGain(strFromChannel, strCameralID, dGain, strSenderID);}}

#define  IMPL_ICP_ASK_TEMPERATURE(classobj)		void classobj::ArNTICPEvt::OnICPAskTemperature(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskTemperature(strFromChannel, strCameralID, strSenderID);}}

#define IMPL_ICP_ASK_CAPTURESPEED(classobj)		void classobj::ArNTICPEvt::OnICPAskCaptureSpeed(LONGSTR& strFromChannel,StrPointer strCameralID, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskCaptureSpeed(strFromChannel, strCameralID, strSenderID);}}

#define IMPL_ICP_ASK_CHANGE_CAPTURESPEED(classobj) void classobj::ArNTICPEvt::OnICPAskChangeCaptureSpeed(LONGSTR& strFromChannel, StrPointer strCameralID, LongFloat dSpeed, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskChangeCaptureSpeed(strFromChannel, strCameralID, dSpeed, strSenderID);}}

#define IMPL_ICP_ASK_RUNINFO(classobj)	void classobj::ArNTICPEvt:: OnICPAskCaptureAppRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskCaptureAppRunInfo(strFromChannel, strSenderID);}}

#define IMPL_ICP_ASK_START_CAPTURE(classobj)	void classobj::ArNTICPEvt::OnICPAskStartCapture(StrPointer strCameralID, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskStartCapture(strCameralID, strSenderID);}}

#define IMPL_ICP_ASK_STOP_CAPTURE(classobj)	void classobj::ArNTICPEvt::OnICPAskStopCapture(StrPointer strCameralID, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskStopCapture(strCameralID, strSenderID);}}

#define IMPL_ICP_ASK_CAPTURE_STATUS(classobj)	void classobj::ArNTICPEvt::OnICPAskCaptureStatus(LONGSTR& strFromChannel, StrPointer strCameralID, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskCaptureStatus(strFromChannel, strCameralID, strSenderID);}}

#define IMPL_ICP_ASK_ERRORLIST(classobj)	void classobj::ArNTICPEvt::OnICPAskErrorList(LONGSTR& strFromChannel,  StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskErrorList(strFromChannel, strSenderID);}}


#define IMPL_ICP_ASK_SERVICE(classobj)		void classobj::ArNTICPEvt::OnICPAskService(LONGSTR& strFromChannel,  StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnICPAskService(strFromChannel, strSenderID);}}
										
//==============================================================================
///链接信息
#ifndef  NOAUTOLINK_ICPHELPER
#define  NOAUTOLINK_ICPHELPER
#pragma comment( lib, "ArNTICPHelper.lib" )

#endif