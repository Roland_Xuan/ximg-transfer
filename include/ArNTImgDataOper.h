#pragma once
#include "ArNtBaseDataStruct.h"
#include "ArNTGrayBmp.h"
#ifndef _WIN64
#include "ArNTImgCompress.h"
#include "IppImageCompress.h"
#endif
#ifdef _WIN64
#include "IppCompressX64.h"
#endif
#include "ArNTImageCommonClass.h"


class ArNTImgDataOper
{
public:
	//д��ͼ��
	ArNT_BOOL  WriteImg(LONGSTR strImgFile, ImgDataType* pImgData, ImgDataHeadExt& HeadInfo, ArNT_BOOL bCompress = ArNT_TRUE, LongInt iQuality = 70);

	//��ȡͼ��
	ArNT_BOOL  ReadImg(LONGSTR strImgFile, ImgDataHeadExt& HeadInfo, GrayBmpData& Data);
	//��ȡͼ�����Ϣ
	ArNT_BOOL  ReadImgHeadInfo(LONGSTR strImgFile, ImgDataHeadExt& HeadInfo);

	ArNT_BOOL  ReadSubImg(LONGSTR strImgFile, 
						ImgDataHeadExt& HeadInfo, 
						LongInt& iLeftPos, 
						LongInt& iRightPos, 
						LongInt& iTopPos,
						LongInt& iBottomPos,
						GrayBmpData& Data);
	bool  Compress(unsigned char* ucSrcImgData, int iImgWidth, int iImgHeight, int iActrualImgWidth, unsigned char* ucDestData, int& iDstDatalen,  int& iEncodeBitSize);
	bool  DeCompress(unsigned char* ucDestData, int iDstDatalen, int iEncodeBitSize, LongInt iImgWidth, LongInt iImgHeight, GrayBmpData& BmpData);

public:
	ArNTImgDataOper(void);
	~ArNTImgDataOper(void);
private:
	PBufferByte		m_ucCompressData;
	LongInt			m_iMaxucCompressDataLen;
#ifndef _WIN64
	CImgCompress	m_ImgCompress;

#endif

#ifdef _WIN64
	CIppCompressX64 m_ImgCompress;
#endif
	static  HANDLE m_hHeapImage;
};