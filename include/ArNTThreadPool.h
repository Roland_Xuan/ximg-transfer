#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTThreadPool.h
// 作  者 ：杨朝霖
// 用  途 ：定义了平台中用到的线程池类
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖       2012-12-1    创建模块

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNTThread.h"
#include "ArNTCriticalSection.h"
///</header_info>

///<datastruct_info>
//==============================================================================
//常用数据结构定义
//任务处理回调函数
//PMemPointer pData 与任务相关的数据首地址。
// LongInt iDataLen 与任务相关的数据长度，以字节为单位。
// PObjPointer pContext 任务投递时的相关环境参数。
typedef void (ARNT_CALL* pTPProcssFuncDef)(PMemPointer pData, LongInt iDataLen,  PObjPointer pContext);

//初始化回调函数
//ShortInt iID 线程池中的每个线程都有一个与其相关的整数值，表示其ID。
//PObjPointer pContext 线程池初始化时的相关环境参数。
typedef void (ARNT_CALL* pTPInitFuncDef)(PObjPointer pContext, ShortInt iID);

//线程的相关信息
typedef struct tagThreadContext
{
	tagThreadContext* pNext;					//下一个线程相关信息
	ShortInt			iID;					//线程相关ID
	ArNTThread			threadObj;				//线程对象
	pTPProcssFuncDef	pProcessFunc;			//需要处理的任务回调函数
	pTPInitFuncDef		pInitFunc;				//线程初始化回调函数
	PObjPointer			pContext;				//任务回调函数相关环境参数
	HANDLE				hEvtAvaliable;			//表示线程是否可用的内核事件
	HANDLE				hEvtWaitProcessFunc;	//表示线程是否在处理任务的内核事件
	enum {RESERVE_SIZE = 2048};					//预留的和任务相关的缓冲区大小
	BufferByte			Reserve[RESERVE_SIZE];	//预留的和任务相关的缓冲区
	LongInt				iMaxBufferSize;			//当前缓冲区的最大尺寸，以字节为单位
	LongInt				iDataLen;				//当前使用的缓冲区尺寸，以字节为单位
	PBufferByte			pAllocBuffer;			//当前预留空间不够分配的新缓冲区地址
	ArNT_BOOL			bExit;					//表示是否需要消耗线程的布尔变量，ArNT_TRUE表示销毁
}ThreadContext, *PThreadContext;

///</datastruct_info>

///<class_info>
//==============================================================================
//名称:ArNTThreadPool
//功能:线程池类对象

class ArNTThreadPool
{
public:
	enum {MAX_THREAD_NUM = 32};
	///<func_info>
	//描述：初始化线程池，为其创建多个可用的线程。
	//参数：ShortInt iInitThreadNum 需要为线程池初始创建的线程数量。
	//pTPInitFuncDef pInitFunc 线程初始化回调函数，该函数会在每个线程创建完成时调用。
	//PObjPointer pContext  初始化环境参数，该参数会在类稍后调用初始化回调函数时传递给回调函数。
	//返回值：无
	void	  InitPool(ShortInt iInitThreadNum, pTPInitFuncDef pInitFunc, PObjPointer pContext);
	///</func_info>

	///<func_info>
	//描述：将任务函数投递给线程池，由线程池选择一个线程进行执行。
	//参数：pTPProcssFuncDef	pFunc 任务函数的地址。
	//PBufferByte pData 与任务相关的参数数据首地址。
	//LongInt iDataSize 与任务相关的参数数据长度。
	//返回值：返回线程池中线程的索引
	ShortInt  PostFunc(pTPProcssFuncDef	pFunc, PBufferByte pData, LongInt iDataSize, PObjPointer pContext);
	///</func_info>
public:

	ArNTThreadPool();
	~ArNTThreadPool(void);
private:

	CTaskCriticalSection	m_csOper;
	ShortInt		m_iInitThreadNum;
	PThreadContext	m_pThreadContext;
	HANDLE			m_hHeap;
	ArNT_BOOL		ThreadProcess(PThreadContext pContext);

	static ArNT_BOOL ARNT_CALL ThreadContexFunc(PObjPointer pContext, PObjPointer pParent);	
};
///</class_info>

///<macro_info>
/////////////////////////////////////////////////////////////////////////////
//定义辅助宏

//void InitThreadPool(ShortInt iID);
//表示线程池无法找到可用线程的返回值
#define NO_AVAILABLE_THREAD		-1

//用在头文件中定义一个线程池对象，和线程池初始化函数
#define DECL_THREAD_POOL(classobj)					ArNTThreadPool m_ThreadPool;\
													static void ARNT_CALL InitThreadPoolFunc(PObjPointer pContext, ShortInt iID);

#define DECL_THREAD_POOL_SIMPLE(classobj)           DECL_THREAD_POOL(classobj)\
													void  InitThreadPool(ShortInt iID) {};
//用在实现文件中实现线程池初始化函数
#define IMPL_THREAD_POOL(classobj)					void ARNT_CALL classobj::InitThreadPoolFunc(PObjPointer pContext, ShortInt iID){\
													classobj* pParent = (classobj*)pContext;\
													pParent->InitThreadPool(iID); }

//在实现文件中初始化线程池，宏中指定线程数量
#define INIT_THREAD_POOL(threadNum)							m_ThreadPool.InitPool(threadNum, InitThreadPoolFunc, this);
//在头文件中声明线程任务处理函数
#define DECL_THREAD_POOL_ENTRY(Func)						static void ARNT_CALL  Func##_Static(PMemPointer pData, LongInt iDataLen,  PObjPointer pContext);
//在实现文件中实现线程任务处理函数
#define IMPL_THREAD_POOL_ENTRY(classobj, Func, ParaType)	void ARNT_CALL  classobj::Func##_Static(PMemPointer pData, LongInt iDataLen,  PObjPointer pContext){\
																classobj* pParent = (classobj*)pContext;\
																pParent->Func((ParaType*)pData, iDataLen);}
//将任务处理函数投递给线程池
#define THREAD_POOL_POST(Func, DataPtr, iDataSize)			m_ThreadPool.PostFunc(Func##_Static, (PBufferByte)DataPtr, iDataSize, this);
#define THREAD_POST_SIMPLE(Func, Data)				m_ThreadPool.PostFunc(Func##_Static, (PBufferByte)&Data, sizeof(Data), this);



///</macro_info>