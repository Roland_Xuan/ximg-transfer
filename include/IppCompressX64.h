#pragma once
#include "ArNtBaseDataStruct.h"
#include "ArNTGrayBmp.h"
//----------------------------------------------
#include "ipp.h"
#include "ArNTGrayBmp.h"

typedef struct tagHuffTable
{
	enum{MAX_SIZE = 512};
	int  HuffCode[MAX_SIZE];
	int  WriteHuffCode[MAX_SIZE];
	int  HuffSize[MAX_SIZE];
	int  HuffVal[MAX_SIZE];
	int  EnHuffCode[MAX_SIZE];
	int  EnHuffSize[MAX_SIZE];
	int  iCodeNum;
}HuffTable, *PHuffTable;

typedef struct tagDCTableItem
{
	int iSize;
	int iCode;
	int iWriteCode;
}DCTableItem, *PDCTableItem;

typedef struct tagRLEItem
{
	int iPos;
	int iValue;
}RLEItem,*PRLEItem;

typedef struct tagRLEItemSet
{
	enum{MAX_ITEMNUM = 64};
	tagRLEItem  Items[MAX_ITEMNUM];
	int iItemNum;
}RLEItemSet, *PRLEItemSet;
typedef struct tagCompressImgInfo
{
	__int32 dwImgWidth;
	__int32 dwImgHeight;
	__int32 dwRoundImgWidth;
	__int32 dwRoundImgHeight;
	__int32 dwDataLen;
	__int32 dwEncodeBitSize;
}CompressImgInfo, *PCompressImgInfo;


#define  RoundCompressWidth(width)  ((width + 7)/8)*8
#define  RoundCompressHeight(height)  ((height + 7)/8)*8


//
class CIppCompressX64
{
public:
	bool  ChangeQuality(int iQuality);
	bool  ChangeQuality32(int iQuality);
	int   GetQuality(void);
	bool  Compress(GrayBmpData& BmpData, unsigned char* ucDestData, int& iDstDatalen,  int& iEncodeBitSize);
	bool  Compress(unsigned char* ucSrcImgData, int iImgWidth, int iImgHeight, int iActrualImgWidth, unsigned char* ucDestData, int& iDstDatalen,  int& iEncodeBitSize);
	bool  WriteCompressFile(wchar_t* strFileName, GrayBmpData& BmpData);
	bool  WriteCompressFile(wchar_t* strFileName, unsigned char* ucDestData, int iDstDatalen, int iEncodeBitSize);
	bool  WriteCompressFile(HANDLE hFile, unsigned char* ucDestData, int iDstDatalen, int iEncodeBitSize);
	int	  WriteCompressStream(unsigned char* ucStream, int iMaxStreamSize, unsigned char* ucSrcImgData, int iImgWidth, int iImgHeight, int iActrualImgWidth);

	bool  ReadCompressFile(wchar_t* strFileName, GrayBmpData& BmpData);
	bool  ReadCompressFile(HANDLE hFile, GrayBmpData& BmpData);
	bool  DeCompress(unsigned char* ucDestData, int iDstDatalen, int iEncodeBitSize, LongInt iImgWidth, LongInt iImgHeight, GrayBmpData& BmpData);



public:
	CIppCompressX64(void);
	~CIppCompressX64(void);

private:

	HuffTable	m_DCHuffTable;
	HuffTable	m_AcHuffTable;
	int		    m_Quality;

	unsigned char* m_pStreamCompressDestData;
	int         m_iMaxStreamCompressDataLen;
	
	int     m_DeDCHuffTable[1024];
	enum {MAX_DC_SIZE = 128};
	int     m_DeDCMinSize[MAX_DC_SIZE];
	int     m_DeDCMaxSize[MAX_DC_SIZE];
	enum {MAX_BIT_NUM = 12};
	int*    m_DeVICCode[MAX_BIT_NUM];

	int*    m_DeACHuffTable;
	int     m_DeACMinSize[MAX_DC_SIZE];
	int     m_DeACMaxSize[MAX_DC_SIZE];




	GrayBmpData  m_CompressImgData;
	int          m_iImgWidth;
	int          m_iImgHeight;
	int          m_iRoundImgWidth;
	int          m_iRoundImgHeight;

	Ipp8u*		m_IppCompressData;
	int			m_iIppCompressDataStep;

	Ipp16s*		m_Ipp16sCompressData;
	int         m_iIpp16sCompressDataStep;

    Ipp16s*		m_Ipp16sProcessCompressData;//经过DCT和量化后的数据
	Ipp16s*		m_Ipp16sZigProcessCompressData;

	Ipp16s      m_QuantFwdTable[64];
	Ipp16u      m_InvQuantFwdTable[64];
	Ipp8u       m_RawQuantFwdTable[64];


	
	
	Ipp16s*		m_Ipp16sProcessDeCompressData;
	Ipp16s*		m_Ipp16sZigProcessDeCompressData;
	Ipp8u*      m_Ipp8uDeCompressData;
	int         m_iDeCompressImgWidth;
	int         m_iDeCompressImgHeight;
	int         m_iRoundDeCompressImgWidth;
	int         m_iRoundDeCompressImgHeight;
	int         m_iTotalDeCompressDataLen; //需要解压缩的数据大小,以Byte为单位
	int         m_iTotalDeCompressDataBit; //需要解压缩的数据，以Bit为单位
	int         m_iHasProcessDeCompressBitSize;//已经处理的解压缩数据大小

	enum{MAX_DCRANGE = 4096};
	DCTableItem  m_DCTable[MAX_DCRANGE];
	RLEItemSet   m_RLEItemSet;



	void  InitialHuffTable(int iQuality);
	void  InitialHuffTable32(int iQuality);
    void  DCTQuantTransfer(void);
	void  PrepareEncode(void);
	bool  Encode(unsigned char* ucDestData, int& iDstDatalen, int& iEncodeBitSize);
	void  InvertDCTQuantTransfer(void);
	void  DeCompressData(int& iBlockNum);

	void  Reset();
	void  ClearResource();
};
