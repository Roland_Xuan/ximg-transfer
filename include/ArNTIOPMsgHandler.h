#pragma once
//
#include "ImageIOProtocal.h"
//--------------------------------------
//消息响应类
class ArNTIOPEvtHandler
{
public:
	//请求读取指定类型的图像文件
	virtual  void OnIOPAskReadImage(LONGSTR&  strFromChannel, StrPointer strType, StrPointer strDesc, StrPointer strSenderID);
	//请求保存图像为指定类型的图像文件
	virtual  void OnIOPAskWriteImage(LONGSTR&  strFromChannel,
									 StrPointer strType, 
									 StrPointer strDesc, 
									 PRawCaptureImage pRawImg,
									 PBufferByte pImgContext,
									 ShortInt    iContextSize,
									 StrPointer strSenderID);
	//询问程序的运行状态
	virtual  void OnIOPAskAppRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID);

	//询问支持的类型
	virtual  void OnIOPAskSupportType(LONGSTR& strFromChannel, StrPointer strSenderID);
	//查询满足条件的图像
	virtual  void OnIOPAskSearchImg(LONGSTR& strFromChannel, StrPointer strType,  StrPointer strCondition, StrPointer strSenderID);

	//询问支持IOP协议的服务
	virtual  void OnIOPAskService(LONGSTR& strFromChannel, StrPointer strSenderID);
	//询问错误列表
	virtual void OnIOPAskErrorList(LONGSTR& strFromChannel, StrPointer strSenderID);

public:
	void* m_pParent;
	void  RegParent(void* pParent);
public:
	ArNTIOPEvtHandler(void);
	~ArNTIOPEvtHandler(void);

private:
	void DefautNotImplFunc(StrPointer strFuncInfo);
};

//---------------------------------------------------------
//
class ArNTIOPMsgHandler
{
DECL_IOP_LOCAL_MESSAGE(ArNTIOPMsgHandler);
public:
	ArNTIOPMsgHandler();
	~ArNTIOPMsgHandler();
public:
	ArNT_BOOL RegEvtHandler(ArNTIOPEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace, ShortInt iObjID = -1);
	void UnRegEvtHandler(ShortInt iObjID = -1);
public:
	LONGSTR	m_strRegChannel;
private:
	ArNT_BOOL			m_bEnableTrace;	
	ArNTIOPEvtHandler*	m_pEvtHandler;
	//--------------------------------------------------
	//消息处理函数
	void   ProcessIOPAskReadImage(PIOPAskReadImage pPara,  StrPointer strSenderID); 
	void   ProcessIOPAskWriteImage(PIOPAskWriteImage pPara, StrPointer strSenderID); 
	void   ProcessIOPAskCmdList(PIOPAskCmdList pPara, StrPointer strSenderID); 
	void   ProcessIOPAskSupportType(PIOPAskSupportType pPara, StrPointer strSenderID); 
	void   ProcessIOPAskRunInfo(PIOPAskRunInfo pPara, StrPointer strSenderID); 
	void   ProcessIOPAskSearchImage(PIOPAskSearchImage pPara, StrPointer strSenderID); 
	void   ProcessIOPAskService(PIOPAskService pPara,  StrPointer strSenderID); 
	void   ProcessIOPAskErrorList(PIOPAskErrorList pPara,  StrPointer strSenderID); 

public:
	//--------------------------------------------------
	//回复函数

	//回复读取到的图像
	void  AckReadImage(LONGSTR& strFromChannel, 
						PRawCaptureImage pImage, 
						StrPointer strType, 
						PBufferByte pImgageContex = NULL, 
						ShortInt iContextSize = 0,
						StrPointer strSenderID = NULL);
	//回复支持的保存类型
	void AckSupportType(LONGSTR& strFromChannel,  StrPointer strAppName, ShortInt iTypeNum, PArNTImageSupportType pType, StrPointer strSenderID = NULL);

	//回复服务的运行信息
	void  AckRunInfo( LONGSTR& strFromChannel,  PArNTRunInfo pInfo, ShortInt iInfoNum, StrPointer strSenderID = NULL);

	//回复查询到的图像信息
	void AckSearchImage(LONGSTR& strFromChannel, PArNTImageDesc pImageDesc, ShortInt iImgNum, StrPointer strSenderID = NULL);

	//回复支持IOP协议的服务
	void AckService(LONGSTR& strFromChannel, StrPointer strApp, StrPointer strVersion, StrPointer strSenderID = NULL);
	//通知错误消息
	void NtfErrorInfo(LONGSTR& strFromChannel,StrPointer strCmdName,  ArNT_ERROR_CODE ErrorCode, StrPointer strErrorInfo, StrPointer strSenderID = NULL);
	//回复错误列表
	void  AckErrorList(PArNTErrorInfo pError, ShortInt iErrorNum, LONGSTR& strFromChannel, StrPointer strSenderID);
};


//////////////////////////////////////////////////////////////////////////////////////
//宏定义
#define BEGIN_IOPEVT_NEST(classobj) class  ArNTIOPEvt:public ArNTIOPEvtHandler{ 

#define DECL_IOP_ALL_FUNC()				DECL_IOP_ASK_READIMAGE() \
										DECL_IOP_ASK_WRITEIMAGE()\
										DECL_IOP_ASK_APPRUNINFO()\
										DECL_IOP_ASK_SUPPORTTYPE()\
										DECL_IOP_ASK_SEARCHIMG()\
										DECL_IOP_ASK_SERVICE()\
										DECL_IOP_ASK_ERRORLIST()





#define DECL_IOP_ASK_READIMAGE()		public:	virtual		void OnIOPAskReadImage(LONGSTR&  strFromChannel,\
																					StrPointer strType, \
																					StrPointer strDesc, \
																					StrPointer strSenderID);
#define DECL_IOP_ASK_WRITEIMAGE()		public: virtual      void OnIOPAskWriteImage(LONGSTR&  strFromChannel,\
																					StrPointer strType, \
																					StrPointer strFileName, \
																					PRawCaptureImage pRawImg,\
																					PBufferByte pImgContext,\
																					ShortInt    iContextSize,\
																					StrPointer strSenderID);

#define DECL_IOP_ASK_APPRUNINFO()		public: virtual		void OnIOPAskAppRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID);
#define	DECL_IOP_ASK_SUPPORTTYPE()		public: virtual		void OnIOPAskSupportType(LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_IOP_ASK_SEARCHIMG()		public: virtual		void OnIOPAskSearchImg(LONGSTR& strFromChannel, StrPointer strType,  StrPointer strCondition, StrPointer strSenderID);
#define DECL_IOP_ASK_SERVICE()			public:	virtual		void OnIOPAskService(LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_IOP_ASK_ERRORLIST()		public: virtual		void OnIOPAskErrorList(LONGSTR& strFromChannel, StrPointer strSenderID);



#define END_IOPEVT_NEST(classobj)		};\
										private:	ArNTIOPEvt				m_IOPEvtHandler;\
										private:    ArNTIOPMsgHandler		m_IOPMsgHandler;\
										public:     ArNTIOPMsgHandler*      GetIOPMsgHandler() {return &m_IOPMsgHandler;};


#define  REG_IOPEVT_NEST(bEnableTrace, ObjID)  m_IOPEvtHandler.RegParent(this);\
								if(ArNT_FALSE == m_IOPMsgHandler.RegEvtHandler(&m_IOPEvtHandler, bEnableTrace, ObjID))\
								{return ArNT_FALSE;}

#define  UNREG_IOPEVT_NEST(ObjID) m_IOPMsgHandler.UnRegEvtHandler(ObjID);



//定义实现类
#define  IMPL_IOP_ALL_FUNC(classobj)	IMPL_IOP_ASK_READIMAGE(classobj) \
										IMPL_IOP_ASK_WRITEIMAGE(classobj) \
										IMPL_IOP_ASK_APPRUNINFO(classobj)\
										IMPL_IOP_ASK_SUPPORTTYPE(classobj)\
										IMPL_IOP_ASK_SEARCHIMG(classobj)\
										IMPL_IOP_ASK_SERVICE(classobj)\
										IMPL_IOP_ASK_ERROR_LIST(classobj)
										

#define  IMPL_IOP_ASK_READIMAGE(classobj)		void classobj::ArNTIOPEvt::OnIOPAskReadImage(LONGSTR&  strFromChannel, StrPointer strType, StrPointer strDesc, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnIOPAskReadImage(strFromChannel, strType,strDesc, strSenderID);}}

#define  IMPL_IOP_ASK_WRITEIMAGE(classobj)	void classobj::ArNTIOPEvt::OnIOPAskWriteImage(LONGSTR&  strFromChannel, StrPointer strType, StrPointer strDesc, PRawCaptureImage pRawImg, PBufferByte pImgContext, ShortInt iContextSize, StrPointer strSenderID) { \
												classobj* pParent = (classobj*)this->m_pParent;\
												if(pParent){\
												pParent->OnIOPAskWriteImage(strFromChannel, strType, strDesc,pRawImg, pImgContext, iContextSize, strSenderID);}}

#define  IMPL_IOP_ASK_APPRUNINFO(classobj)			 void classobj::ArNTIOPEvt::OnIOPAskAppRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID) {\
												classobj* pParent = (classobj*)this->m_pParent;\
												if(pParent){\
												pParent->OnIOPAskAppRunInfo(strFromChannel, strSenderID);}}

#define	 IMPL_IOP_ASK_SUPPORTTYPE(classobj)		    void classobj::ArNTIOPEvt::OnIOPAskSupportType(LONGSTR& strFromChannel, StrPointer strSenderID) {\
												classobj* pParent = (classobj*)this->m_pParent;\
												if(pParent){\
												pParent->OnIOPAskSupportType(strFromChannel, strSenderID);}}	

#define  IMPL_IOP_ASK_SEARCHIMG(classobj)			 void classobj::ArNTIOPEvt::OnIOPAskSearchImg(LONGSTR& strFromChannel, StrPointer strType,  StrPointer strCondition, StrPointer strSenderID) {\
												classobj* pParent = (classobj*)this->m_pParent;\
												if(pParent){\
												pParent->OnIOPAskSearchImg(strFromChannel, strType, strCondition, strSenderID);}}	

#define IMPL_IOP_ASK_SERVICE(classobj)				 void classobj::ArNTIOPEvt::OnIOPAskService(LONGSTR& strFromChannel, StrPointer strSenderID) {\
												classobj* pParent = (classobj*)this->m_pParent;\
												if(pParent){\
												pParent->OnIOPAskService(strFromChannel, strSenderID);}}	

#define IMPL_IOP_ASK_ERROR_LIST(classobj)			void classobj::ArNTIOPEvt::OnIOPAskErrorList(LONGSTR& strFromChannel, StrPointer strSenderID) {\
												classobj* pParent = (classobj*)this->m_pParent;\
												if(pParent){\
												pParent->OnIOPAskErrorList(strFromChannel, strSenderID);}}	

									
				
//==============================================================================
///链接信息
#ifndef  NOAUTOLINK_IOPHELPER
#define  NOAUTOLINK_IOPHELPER
#pragma comment( lib, "ArNTIOPHelper.lib" )

#endif												