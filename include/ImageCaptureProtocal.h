#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ImageCaptureProtocal.h
// 作  者 ：杨朝霖
// 用  途 ：定义与图像采集程序通讯的协议
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖     2010-8-10    创建

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNTIMGPlatFormDataStruct.h"
#include "ArNTIPCModule.h"
///</header_info>

///<datastruct_info>
//==============================================================================
// 基本数据类型定义

///</datastruct_info>

//==============================================================================
///<macro_info>

#define ICP				L"ImageCapture_9A741E52-48F0-4d73-A8FB-E5D33B015142"   
#define ICPPropUI		L"ImageCapturePropUI_201C7908-D9D6-408f-A795-B23754481988"
#define ICPImgUI		L"ImageCaptureImgUI_201C7908-D9D6-408f-A795-B23754481988"

#define ICP_START_CMD			400

#define DECL_ICP_MESSAGE(classobj,channel) private: static void __stdcall classobj::ICPMessageFunc##channel(ULongInt iMemSize, PMemPointer pMem, PMemPointer pContext);\
										   public: ArNT_BOOL  classobj::Reg##channel(LongInt iChannelSize = 32 * 1024, ShortInt iCamIndex = -1);\
										   public: void		  classobj::UnReg##channel(ShortInt iCamIndex = -1);





#define BEGIN_ICP_MESSAGE(classobj, channel)  ArNT_BOOL  classobj::Reg##channel(LongInt iChannelSize, ShortInt iCamIndex){\
									ArNTIPCInfo Info = {sizeof(ArNTIPCInfo)};\
									if(iCamIndex >= 0){CCommonFunc::SafeStringPrintf(Info.strChannelName, STR_LEN(Info.strChannelName), L"%s%d", channel, iCamIndex);}else{\
									CCommonFunc::SafeStringPrintf(Info.strChannelName, STR_LEN(Info.strChannelName), L"%s", channel);}\
									ArNT_ERROR_CODE code = ArNTIPCRegChannel(Info, ICPMessageFunc##channel, this); \
									return code == SUCCESS_IPC_OPERCHANNEL;}\
									void		  classobj::UnReg##channel(ShortInt iCamIndex) { \
									LONGSTR strRemoveChannel = {0};\
									if(iCamIndex >= 0){CCommonFunc::SafeStringPrintf(strRemoveChannel, STR_LEN(strRemoveChannel), L"%s%d", channel, iCamIndex);}else{\
									CCommonFunc::SafeStringPrintf(strRemoveChannel, STR_LEN(strRemoveChannel), L"%s", channel);}\
									ArNTRemoveChannel(strRemoveChannel);\
									};\
									void __stdcall classobj::ICPMessageFunc##channel(ULongInt iMemSize, PMemPointer pMem, PMemPointer pContext) {\
	                                PArNTCommand pCmd = (PArNTCommand)pMem;\
									if(iMemSize != pCmd->iTotalSize) return;\
									classobj* pParent = (classobj*)pContext;

#define ICP_MESSAGE_ENTRY(id, func, para) if(pCmd->iID == id) {  \
										if(pCmd->bAlloc == ArNT_FALSE)\
										 { \
											pParent->func((para*)pCmd->buffer.Reserve, pCmd->strSenderID);\
										}else{\
										pParent->func((para*)pCmd->buffer.Alloc, pCmd->strSenderID);};\
										return;}

#define END_ICP_MESSAGE(classobj) };

#define  DECL_ICP_LOCAL_MESSAGE(classobj) DECL_ICP_MESSAGE(classobj, ICP)
#define  DECL_ICP_PROPUI_MESSAGE(classobj) DECL_ICP_MESSAGE(classobj, ICPPropUI)
#define  DECL_ICP_IMGUI_MESSAGE(classobj) DECL_ICP_MESSAGE(classobj, ICPImgUI)


#define BEGIN_ICP_LOCAL_MESSAGE(classobj) BEGIN_ICP_MESSAGE(classobj, ICP)
#define BEGIN_ICP_PROPUI_MESSAGE(classobj)  BEGIN_ICP_MESSAGE(classobj, ICPPropUI)
#define BEGIN_ICP_IMGUI_MESSAGE(classobj)  BEGIN_ICP_MESSAGE(classobj, ICPImgUI)

//==============================================================================
///</macro_info>

//==============================================================================
///<message>
#define  ICP_ASK_CAMERALINFO					(ICP_START_CMD + 1)
#define  ICP_ASK_CAMERALINFO_DESC				L"询问采集程序中使用相机的信息"
typedef struct tagICPAskCameralInfo
{
	LongInt iSize;
	LONGSTR strFromChannel;
}ICPAskCameralInfo, *PICPAskCameralInfo;

#define  ICP_ACK_CAMERALINFO					(ICP_START_CMD + 2)
#define  ICP_ACK_CAMERALINFO_DESC				L"回复采集程序中使用相机的信息"
typedef struct tagICPAckCameralInfo
{
	LongInt iSize;
	ImgCaptureCameralInfo Info;
}ICPAckCameralInfo, *PICPAckCameralInfo;


#define  ICP_ASK_CHANGE_EXPTIME					(ICP_START_CMD + 3)
#define  ICP_ASK_CHANGE_EXPTIME_DESC			L"请求更改相机曝光时间"
typedef struct tagICPAskChangeExptime
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LongFloat dExpTime;
	LONGSTR strFromChannel;	
}ICPAskChangeExptime, *PICPAskChangeExptime;

#define  ICP_ASK_EXPTIME						(ICP_START_CMD + 4)
#define  ICP_ASK_EXPTIME_DESC					L"询问相机当前曝光时间"
typedef struct tagICPAskExpTime
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LONGSTR strFromChannel;	
}ICPAskExpTime, *PICPAskExpTime;

#define  ICP_ACK_EXPTIME						(ICP_START_CMD + 5)
#define  ICP_ACK_EXPTIME_DESC					L"回复相机当前曝光时间"
typedef struct tagICPAckExpTime
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LongFloat dExpTime;
}ICPAckExpTime, *PICPAckExpTime;

#define  ICP_ASK_GAIN							(ICP_START_CMD + 6)
#define  ICP_ASK_GAIN_DESC						L"询问相机当前增益"
typedef struct tagICPAskGain
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LONGSTR strFromChannel;	
}ICPAskGain, *PICPAskGain;

#define  ICP_ACK_GAIN							(ICP_START_CMD + 7)
#define  ICP_ACK_GAIN_DESC						L"回复相机当前增益"
typedef struct tagICPAckGain
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LongFloat dGain;
}ICPAckGain, *PICPAckGain;

#define  ICP_ASK_CHANGE_GAIN					(ICP_START_CMD + 8)
#define  ICP_ASK_CHANGE_GAIN_DESC				L"请求更改相机增益"
typedef struct tagICPAskChangeGain
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LongFloat dGain;
	LONGSTR strFromChannel;	
}ICPAskChangeGain, *PICPAskChangeGain;

#define  ICP_ASK_TEMPERATURE					(ICP_START_CMD + 9)
#define  ICP_ASK_TEMPERATURE_DESC				L"询问相机当前温度"
typedef struct tagICPAskTemperature
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LONGSTR strFromChannel;	
}ICPAskTemperature, *PICPAskTemperature;

#define  ICP_ACK_TEMPERATURE					(ICP_START_CMD + 10)
#define  ICP_ACK_TEMPERATURE_DESC				L"回复相机当前温度"
typedef struct tagICPAckTemperature
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LongFloat dTemperature;
}ICPAckTemperature, *PICPAckTemperature;

#define  ICP_ASK_CMDLIST						(ICP_START_CMD + 11)	
#define  ICP_ASK_CMDLIST_DESC					L"询问ICP协议命令列表"
typedef struct tagICPAskCmdList
{
	LongInt iSize;
	LONGSTR strFromChannel;
}ICPAskCmdList, *PICPAskCmdList;

#define  ICP_ACK_CMDLIST						(ICP_START_CMD + 12)	
#define  ICP_ACK_CMDLIST_DESC					L"回复ICP协议命令列表"
typedef struct tagICPAckCmdList
{
	LongInt iSize;
	ShortInt iCmdNum;
	ArNTCommandDesc Desc[1];
}ICPAckCmdList, *PICPAckCmdList;

#define  ICP_ASK_CAPTURESPEED					(ICP_START_CMD + 13)	
#define  ICP_ASK_CAPTURESPEED_DESC				L"询问指定相机的采集速度"
typedef struct tagICPAskCaptureSpeed
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LONGSTR strFromChannel;	
}ICPAskCaptureSpeed, *PICPAskCaptureSpeed;

#define  ICP_ACK_CAPTURESPEED						(ICP_START_CMD + 14)	
#define  ICP_ACK_CAPTURESPEED_DESC					L"回复指定相机的采集速度"
typedef struct tagICPAckCaptureSpeed
{
	LongInt iSize;
	SHORTSTR strCameralID;
	ArNT_BOOL bLine;  //ArNT_TRUE表示线阵相机, ArNT_FALSE表示面阵相机
	LongFloat dSpeed; //线阵相机的时候表示线速率，面阵相机时表示帧速率
}ICPAckCaptureSpeed, *PICPAckCaptureSpeed;

#define  ICP_ASK_CHANGE_CAPTURESPEED				(ICP_START_CMD + 15)	
#define  ICP_ASK_CHANGE_CAPTURESPEED_DESC			L"请求更改指定相机的采集速度"
typedef struct tagICPAskChangeCaptureSpeed
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LongFloat dSpeed;	
	LONGSTR strFromChannel;	
}ICPAskChangeCaptureSpeed, *PICPAskChangeCaptureSpeed;

#define  ICP_ASK_FRAMENUM							(ICP_START_CMD + 16)	
#define  ICP_ASK_FRAMENUM_DESC						L"询问指定相机当前已经采集的图像数量"
typedef struct tagICPAskFrameNum
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LONGSTR strFromChannel;	
}ICPAskFrameNum, *PICPAskFrameNum;

#define  ICP_ACK_FRAMENUM							(ICP_START_CMD + 17)	
#define  ICP_ACK_FRAMENUM_DESC						L"回复指定相机当前已经采集的图像数量"
typedef struct tagICPAckFrameNum
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LongLongInt iFrameNum;	
}ICPAckFrameNum, *PICPAckFrameNum;


#define ICP_ASK_RUNINFO								(ICP_START_CMD + 18)		
#define  ICP_ASK_RUNINFO_DESC						 L"询问当前采集程序的运行信息"
typedef struct tagICPAskRunInfo
{
	LongInt iSize;
	LONGSTR strFromChannel;
}ICPAskRunInfo, *PICPAskRunInfo;


#define ICP_ACK_RUNINFO								(ICP_START_CMD + 19)		
#define  ICP_ACK_RUNINFO_DESC						L"回复当前采集程序的运行信息"
typedef struct tagICPAckRunInfo
{
	LongInt iSize;
	ShortInt iInfoNum;
	ArNTRunInfo strInfo[1];
}ICPAckRunInfo, *PICPAckRunInfo;

#define  ICP_NTF_RAWIMAGE							(ICP_START_CMD + 20)
#define  ICP_NTF_RAWIMAGE_DESC						L"通知采集到的原始图像"
typedef struct tagICPNtfRawImage
{
	LongInt iSize;
	RawCaptureImage RawImg[1];
}ICPNtfRawImage, *PICPNtfRawImage;


#define  ICP_ASK_START_CAPTURE						(ICP_START_CMD + 21)
#define  ICP_ASK_START_CAPTURE_DESC					L"请求指定相机开始采集图像"
typedef struct tagICPAskStartCapture
{
	LongInt iSize;
	SHORTSTR strCameralID;
}ICPAskStartCapture, *PICPAskStartCapture;

#define  ICP_ASK_STOP_CAPTURE						(ICP_START_CMD + 22)
#define  ICP_ASK_STOP_CAPTURE_DESC					L"请求指定相机停止采集图像"
typedef struct tagICPAskStopCapture
{
	LongInt iSize;
	SHORTSTR strCameralID;
}ICPAskStopCapture, *PICPAskStopCapture;

#define  ICP_ASK_CAPTURE_STATUS						(ICP_START_CMD + 23)
#define  ICP_ASK_CAPTURE_STATUS_DESC				L"询问相机当前的采集状态"
typedef struct tagICPAskCaptureStatus
{
	LongInt iSize;
	SHORTSTR strCameralID;
	LONGSTR strFromChannel;
}ICPAskCaptureStatus, *PICPAskCaptureStatus;

#define  ICP_ACK_CAPTURE_STATUS						(ICP_START_CMD + 24)
#define  ICP_ACK_CAPTURE_STATUS_DESC				L"回复相机当前的采集状态"
typedef struct tagICPAckCaptureStatus
{
	LongInt iSize;
	SHORTSTR strCameralID;
	ArNT_BOOL bIsCapture; 
}ICPAckCaptureStatus, *PICPAckCaptureStatus;

#define  ICP_ASK_ERROR_LIST							(ICP_START_CMD + 25)
#define  ICP_ASK_ERROR_LIST_DESC					L"询问服务的错误列表"
typedef struct tagICPAskErrorList
{
	LongInt iSize;
	LONGSTR strFromChannel;
}ICPAskErrorList, *PICPAskErrorList;

#define  ICP_ACK_ERROR_LIST							(ICP_START_CMD + 26)
#define  ICP_ACK_ERROR_LIST_DESC					L"回复服务的错误列表"
typedef struct tagICPAckErrorList
{
	LongInt iSize;
	ShortInt iErrorNum;	
	ArNTErrorInfo ErrorInfo[1];
}ICPAckErrorList, *PICPAckErrorList;

#define ICP_NTF_ERROR								(ICP_START_CMD + 27)	
#define ICP_NTF_ERROR_DESC							L"通知错误信息"
typedef struct tagICPNtfError
{
	LongInt iSize;
	ArNT_ERROR_CODE  code;
	SHORTSTR strCmdName;
	LONGSTR  strErrorInfo;
}ICPNtfError, *PICPNtfError;

#define ICP_ASK_SERVICE								(ICP_START_CMD + 12)		
#define ICP_ASK_SERVICE_DESC						L"查询支持ICP协议的服务"

typedef struct tagICPAskService
{
	LongInt iSize;
	LONGSTR strFromChannel;
}ICPAskService, *PICPAskService;

#define ICP_ACK_SERVICE								(ICP_START_CMD + 13)		
#define ICP_ACK_SERVICE_DESC						L"回复支持ICP协议的服务"
typedef struct tagICPAckService
{
	LongInt iSize;
	ArNServiceAppInfo Service;
	
}ICPAckService, *PICPAckService;





