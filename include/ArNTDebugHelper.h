#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTDebugHelper.h
// 作  者 ：杨朝霖
// 用  途 ：定义了调试相关的部分函数,实现在IDE环境中向Output窗口输出调试信息
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖     2011-11-10    创建

//==============================================================================
///<ver_info>


///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
///</header_info>

///<class_info>
//==============================================================================
//名称:ArNTDebugHelper
//功能:本地调试辅助类
class ArNTDebugHelper
{
public:
	///<func_info>
	//描述：输出出错调试信息，并触发一个调试中断
	//参数： StrChar* strInfo  需要输出的调试信息
	//ArNT_BOOL bBreak  该参数表示是否需要中断程序，ArNT_TRUE表示需要中断
	//返回值：无
	static void  DebugErro(StrChar* strInfo, ArNT_BOOL bBreak = ArNT_TRUE);
	///</func_info>

	///<func_info>
	//描述：输出跟踪调试信息，不触发调试中断
	//参数： StrChar* strInfo  需要输出的调试信息
	//返回值：无
	static void  Trace(StrChar* strInfo);
	///</func_info>

private:
	ArNTDebugHelper(void);
	~ArNTDebugHelper(void);

	static bool m_bInitial;
	static void Initial();
};
///</class_info>