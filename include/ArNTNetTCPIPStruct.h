#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTNetTCPIPStruct.h
// 作  者 ：杨朝霖
// 用  途 ：ArNTNetTCPIPStruct.h定义TCP/IP协议中用到的结构
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>
///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描`述
//1.0     杨朝霖     2012-12-24    创建

//==============================================================================
///<ver_info>
///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
///</header_info>
///<datastruct_info>
//==============================================================================
// 基本数据类型定义

#define ETHTYPE_IP		0x0800   
#define ETHTYPE_ARP		0x0806  
#define ETHTYPE_RARP	0x8035  

// ARP协议   
#define ARP_REQUEST   1       // ARP 请求      
#define ARP_REPLY     2       // ARP 响应  
#define RARP_REQUEST  3       //RARP 请求
#define RARP_REPLY    4       //RARP 响应
#define ARP_ETHER		1   

//IP层上层协议类型   
#define PROTO_ICMP		1   
#define PROTO_IGMP		2   
#define PROTO_TCP		6   
#define PROTO_UDP		17   

// 定义TCP标志   
#define   TCP_FIN   0x01   
#define   TCP_SYN   0x02   
#define   TCP_RST   0x04   
#define   TCP_PSH   0x08   
#define   TCP_ACK   0x10   
#define   TCP_URG   0x20   
#define   TCP_ACE   0x40   
#define   TCP_CWR   0x80   

// 以太网帧结构
typedef struct tagETHDR          
{  
    BufferByte   Dest[6];         
    BufferByte   Src[6];            
    UShortInt	 Type;             
} ETHDR, *PETHDR;  
///</datastruct_info>

//ARP结构头(28)
typedef struct tagARPHDR    
{  
    UShortInt	HardType;			//  ARP协议应用的硬件网络类型，以太网ARP_ETHER		(2) 
    UShortInt	ProtoType;			//  需要映射的协议地址类型，IP协议为  ETHTYPE_IP	(2)
    BufferByte	MACLen;				//  MAC地址的长度，为6								(1)
    BufferByte	IPAddressLen;		//  IP地址的长度，为4								(1)
    UShortInt	OPCode;				//  操作代码，ARP_REQUEST为请求，ARP_REPLY为响应	(2)  
    BufferByte  SrcMAC[6];			//  源MAC地址										(6)
    BufferByte  SrcIP[4];			//  源IP地址										(4)
    BufferByte  DstMAC[6];          //  目的MAC地址										(6)
    BufferByte  DstIP[4];           //  目的IP地址										(4)
} ARPHDR, *PARPHDR;  

//IP头结构(20)
typedef struct tagIPHDR       
{  
    BufferByte		VerHdrLen;      // 版本号和头长度（各占4位）			(1) 
    BufferByte		iTOS;			// 服务类型，前3位不用，MinDelay, MaxThroughput,MaxReliability, MinMoney最后1位为0	(1)
    UShortInt		iTotalLen;      // 封包总长度，即整个IP报的长度			(2)
    UShortInt		iID;            // 封包标识，惟一标识发送的每一个数据报	(2)  
    UShortInt		FlagFragOffset; // 前3位标志，后13位为分片位移			(2)
    BufferByte		iTTL;			// 生存时间，就是TTL					(1)  
    BufferByte		iProtoType;     // 协议，可能是TCP、UDP、ICMP等			(1)
    UShortInt		iCheckSum;		// 校验和								(2)
    ULongInt		SrcIP;			// 源IP地址								(4)
    ULongInt		DstIP;			// 目标IP地址							(4)
} IPHDR , *PIPHDR

//TCP头结构（20)
typedef struct tagTCPHDR       
{  
    UShortInt	iSrcPort;			// 源端口号				(2)
    UShortInt	iDstPort;			// 目的端口号			(2) 
    ULongInt	SeqNumber;			// 序列号				(4)
    ULongInt	AckNumber;			// 确认号				(4)
    BufferByte  DataOffset;         // 高4位表示数据偏移	(1)  
    BufferByte  Flag;               // 标志位				(1)
    UShortInt   WindowSize          // 窗口大小				(2)  
    UShortInt   iCheckSum;          // 校验和				(2)
    UShortInt   URGPtr;				// 紧急数据指针			(2) 
} TCPHDR, *PTCPHDR;  

//UDP头结构(8)
typedef struct tagUDPHDR  
{  
    UShortInt       SrcPort;     // 源端口号	(2)       
    UShortInt       DestPort;	 // 目的端口号	(2)          
    UShortInt       iPkgLen;     // 包长度		(2)
    UShortInt       iCheckSum;    // 校验和		(2)
} UDPHDR, *PUDPHDR ;  


///</datastruct_info>

/*USHORT checksum(USHORT* buff, int size)  
{  
    unsigned long cksum = 0;  
    while(size>1)  
    {  
        cksum += *buff++;  
        size -= sizeof(USHORT);  
    }  
    // 是奇数   
    if(size)  
    {  
        cksum += *(UCHAR*)buff;  
    }  
    // 将32位的chsum高16位和低16位相加，然后取反   
    cksum = (cksum >> 16) + (cksum & 0xffff);  
    cksum += (cksum >> 16);             
    return (USHORT)(~cksum);  
}  */
  