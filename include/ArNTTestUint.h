#pragma once

///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTTestUint.h
// 作  者 ：杨朝霖
// 用  途 ：定义了系统中所用的测试单元类
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖      2011-11-3   创建模块
//2.0     杨朝霖      2011-11-9   支持脚本测试功能
//==============================================================================
///</ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
#include "CommonFunc.h"
///</header_info>

///<class_info>
//==============================================================================
//名称:CMCSITestUnitOutPut
//功能:用于提供输出功能的接口类
class  ArNTTestUnitOutPut
{
public:
	virtual void		WriteInfo(StrChar* strInfo) = 0;
	virtual ArNT_BOOL	Valid() = 0;
};

///</class_info>

///<class_info>
//==============================================================================
//名称:CMCSITestUnit
//功能:测试单元功能类

#define TEST_NAME(strName) CCommonFunc::SafeStringPrintf(m_strTestName, STR_LEN(m_strTestName), L"%s", strName); 

class ArNTTestUnit
{
public:	
	virtual void			Initial(void);
	virtual void			WriteResult(void);
	virtual ArNT_BOOL		Test(LongInt iTimes, LongInt iIndexInGroup, ArNTTestUnit* pGroup);
	virtual void			TestInfo(LongInt iIndex, LongFloat dTime, ArNT_BOOL bStatus);
	virtual void			Free();
	virtual ArNT_BOOL		PreStartTest(); 
	virtual ArNT_BOOL		StartTest();
	virtual ArNT_BOOL		IsValidTest();
	virtual void			SetTestOutPut(ArNTTestUnitOutPut* pOutPut);
	virtual void			SetScriptPath(StrPointer strScriptPath);

	PMemPointer operator new(size_t Count);
	void operator delete(PMemPointer pAddress);	

	void   SetTestTimes(LongInt iTestTimes);
	void   SetTestName(StrChar* strName);
	StrChar* GetTestName(void);
	LongInt    IndexInGroup() {return m_iIndexInGroup;};
public:
	ArNTTestUnit(void);
	~ArNTTestUnit(void);
public:
	LongFloat m_dTotalTestTime;
	LongFloat m_dTestTime;
	
	LONGSTR m_strAppName;
	LONGSTR m_strAppPath;
	LONGSTR m_strScriptPath;
	
	ShortInt m_iIndexInGroup;
	ArNTTestUnit* m_pParent;
	
protected:
	ArNTTestUnitOutPut*			m_pTestOutPut;
	static HANDLE				ArNTTestUnit::hTestUnitHeap;
	LongInt						m_iErrorNum;
	LongInt						m_iTestTimes;
	LongInt						m_iConfigTestTimes; //设置的测试次数
	ArNT_BOOL					m_bIsHeapObj;
	StrChar						m_strSignature[4];
	SHORTSTR					m_strTestName;	
};

///</class_info>

///<class_info>
//==============================================================================
//名称:CMCSITestUnit
//功能:测试单元组类

class ArNTTestGroup:public ArNTTestUnit
{
public:
	virtual void AddTest(ArNTTestUnit* pTest) = 0;
};
///</class_info>

///<class_info>
//==============================================================================
//名称:CMCSITestFactory
//功能:测试类厂,用于获取对应的测试组对象
class ArNTTestFactory
{
public:
	virtual ArNTTestGroup* GetTestGroup(StrChar* strPara) = 0;
	virtual void Free() = 0;

public:
	ArNTTestFactory();
	~ArNTTestFactory();
protected:

	void AddTestGroup(ArNTTestGroup* pGroup);
	enum {MAX_ITEM_NUM = 128};
	ArNTTestGroup* m_pTestGroupArray[MAX_ITEM_NUM];
	ShortInt m_iTestGroupNum;

};
///</class_info>


///<class_info>
//==============================================================================
//名称:用于暂停测试项
class ArNTTestUnitPause:public ArNTTestUnit
{
public:
	virtual ArNT_BOOL		IsValidTest() {return ArNT_FALSE;};
	virtual void			WriteResult(void);
	virtual ArNT_BOOL		PreStartTest();
	virtual ArNT_BOOL		StartTest();
	virtual void			TestInfo(LongInt iIndex, LongFloat dTime, ArNT_BOOL bStatus);
	virtual StrChar*		GetTestName(void);
	virtual void			Free() {};
public:
	ArNTTestUnitPause();
	~ArNTTestUnitPause();
};
///</class_info>

///<class_info>
//==============================================================================
//名称:用于退出后续的测试
class ArNTTestUnitExit:public ArNTTestUnit
{
public:
	virtual ArNT_BOOL		IsValidTest() {return ArNT_FALSE;};
	virtual void			WriteResult(void);
	virtual ArNT_BOOL		PreStartTest();
	virtual ArNT_BOOL		StartTest(){return ArNT_TRUE;}
	virtual void			TestInfo(LongInt iIndex, LongFloat dTime, ArNT_BOOL bStatus);
	virtual StrChar*		GetTestName(void);
	virtual void			Free() {};
public:
	ArNTTestUnitExit();
	~ArNTTestUnitExit();
};
///</class_info>

///<class_info>
//==============================================================================
//名称:用于解析命令测试脚本

#define DECL_CREATE_TESTCLASS(TestClass)  ArNTTestUnit* Create##TestClass(StrChar* strName) \
									{														\
										ArNTTestUnit* pTestUnit = new TestClass();			\
										pTestUnit->SetTestName(strName);					\
										return pTestUnit;\
									}

#define DECL_CREATE_TESTGROUP(GroupClass)  ArNTTestUnit* Create##GroupClass(StrChar* strName) \
									{														\
										ArNTTestUnit* pTestUnit = GroupClass::GetObj().GetTestGroup(strName);\
										pTestUnit->SetTestName(strName);					\
										return pTestUnit;\
									}

#define  TYPE_ENTRY_TESTUNIT(TestClass)  ArNTTestScript::GetObj().AddTypeEntry(tagTestUnitEntry::enTestUnit,  L#TestClass, Create##TestClass);
#define  TYPE_ENTRY_TESTGROUP(GroupClass)  ArNTTestScript::GetObj().AddTypeEntry(tagTestUnitEntry::enTestGroup,  L#GroupClass, Create##GroupClass);


typedef ArNTTestUnit* (*pCreateUnitFuncDef)(StrChar* strName); 


typedef struct tagTestUnitEntry
{
	enum TYPE_TESTUNIT{enTestUnit, enTestGroup};
	TYPE_TESTUNIT type;
	SHORTSTR   strTypeName;
	pCreateUnitFuncDef pCreateFunc;
}TestUnitEntry, *PTestUnitEntry;



typedef struct tagScriptEntry
{
	enum SCRIPT_CMD {enCreate, enRun,enAdd};
	SCRIPT_CMD Cmd; //目前有"CREATE", ADD" "RUN" 三种类型
	union
	{
		SHORTSTR strTestUnitType; //CREATE命令下创建的测试类别
		SHORTSTR strTestGroup; //在RUN命令下需要运行的测试组名，或ADD命令下添加到的祖名
	};
	union
	{
		SHORTSTR strTypeName; //CREATE命令下需要创建的类名
		SHORTSTR strAddTestUnit; //ADD命令下需要添加的测试单元名
	};

	SHORTSTR strInstanceName; //CREATE命令下创建的实例名
	LongInt iTestTimes;
	ArNT_BOOL bHasProcess;
	void*   pObj;
}ScriptEntry, *PScriptEntry;
class ArNTTestScript
{
public:
	void  AddTypeEntry(tagTestUnitEntry::TYPE_TESTUNIT type, StrChar* strName, pCreateUnitFuncDef pFunc);

	//添加测试脚本项, 如:" CREATE   TEST_UNIT  IPCAddLargeChannel  AddLargeChannelTest1  40"
	// "CREATE  TEST_GROUP  MTTestGroup"
	//"ADD   IPAddlargeChannel  MTTestGroup"
	// "START MTTestGroup"
	void  AddScripEntry(StrChar* strCmd, StrChar* strInfo1, StrChar* strInfo2 = NULL, StrChar* strInfo3 = NULL, LongInt iTestTimes = 0);
	void  Start();
	void  Reset();

	void ReadScript(StrChar* strName);
public:
	static ArNTTestScript& GetObj();
private:
		ArNTTestScript();
		~ArNTTestScript();
private:	
	enum {MAX_ITEMNUM = 256};
	TestUnitEntry m_TypeEntryList[MAX_ITEMNUM];
	ShortInt  m_iTypeEntryNum;

	ScriptEntry   m_ScriptEntry[MAX_ITEMNUM];
	ShortInt  m_iScriptEntryNum;

	ArNT_BOOL  IsValidCmd(StrChar* strCmd);

	LONGSTR m_strScriptPathName;
};
///</class_info>





