#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：
// 文件名 ：
// 作  者 ：
// 用  途 ：
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述


//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "CodeTestFrame.h"
///</header_info>

//////////////////////////////////////
//添加测试模块的头文件
//
#include "TUIPCCreate.h"
///</header_info>

/////////////////////////////////////////////////////////////////////////////////
//声明用到到测试工厂类
//DECL_CREATE_TESTGROUP(Factory name)
DECL_CREATE_TESTGROUP(CSTConsoleTestFactory)
DECL_CREATE_TESTGROUP(CMTWindowTestFactory)
DECL_CREATE_TESTGROUP(CMTLogTestFactory)
//声明新的测试工厂类


//////////////////////////////////////////////////////////////////////////////////
//声明用到的测试
//DECL_CREATE_TESTCLASS(TestClassName)
DECL_CREATE_TESTCLASS(ArNTTestUnitPause)
DECL_CREATE_TESTCLASS(ArNTTestUnitExit)
//声明新的测试
DECL_CREATE_TESTCLASS(CTUIPCCreate)


//////////////////////////////////////////////////////////////////////////////////
//初始化测试
void InitialTest()
{
	//添加测试组创建入口函数
	//TYPE_ENTRY_TESTGROUP(Factroy Name);
	//如：TYPE_ENTRY_TESTGROUP(CSTConsoleTestFactory);
	TYPE_ENTRY_TESTGROUP(CSTConsoleTestFactory);
	TYPE_ENTRY_TESTGROUP(CMTWindowTestFactory);
	TYPE_ENTRY_TESTGROUP(CMTLogTestFactory);

	//添加测试入口函数
	//TYPE_ENTRY_TESTUNIT(TestClassName);
	TYPE_ENTRY_TESTUNIT(ArNTTestUnitPause);	
	TYPE_ENTRY_TESTUNIT(ArNTTestUnitExit);	

	//添加新的测试单元
	TYPE_ENTRY_TESTUNIT(CTUIPCCreate);


};
/////////////////////////////////////////////////////////////////////////////////////
//运行单元测试程序
#define  RUN_UNIT_TEST()	InitialTest();\
							CCommonFunc::EnableChineseLocal();\
							if(argc < 2){	\
							CCommonFunc::ErrorBox(L"%s", L"程序运行格式有误，请指定测试脚本");\
							} else { \
								ArNTTestScript::GetObj().ReadScript(argv[1]); \
								ArNTTestScript::GetObj().Start();}




