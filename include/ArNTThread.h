#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTThread.h
// 作  者 ：杨朝霖
// 用  途 ：定义了平台中用到的线程类
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖       2012-12-1    创建模块

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
///</header_info>

///<datastruct_info>
//==============================================================================
//常用数据结构定义
typedef ArNT_BOOL (ARNT_CALL* pThreadFuncDef)(PObjPointer pContext, PObjPointer pParent);
///</datastruct_info>

///<class_info>
//==============================================================================
//名称:ArNTThread
//功能:实现线程类对象
class ArNTThread
{
public:
	enum ThreadStatus{enRun = 0, enSuspend, enExit};
	///<func_info>
	//描述：初始化线程类对象
	//参数：pThreadFuncDef pFunc 线程对象入口函数
	//		 PObjPointer pContext相关环境参数，会被传入到线程入口函数中
	//		 ThreadStatus status 初始化后线程状态
	//       PObjPointer pParent 通常是指创建线程类对象的父类指针
	//		 ArNT_BOOL bDestroy 线程退出时是否自动销毁对象，通常设置为ArNT_FLASE，表示不支持。
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	ArNT_BOOL  Init(pThreadFuncDef pFunc, PObjPointer pContext, ThreadStatus status = enRun, PObjPointer pParent = NULL, ArNT_BOOL bDestroy =  ArNT_FALSE);
	///</func_info>

	///<func_info>
	//描述：将运行中的线程挂起，执行该操作后线程的状态为挂起状态
	//参数：无
	//返回值：无
	void  Suspend(void);
	///</func_info>
	
	///<func_info>
	//描述：将挂起状态中的线程转变为运行状态
	//参数：无
	//返回值：无
	void  Resume(void);
	///</func_info>
	
	///<func_info>
	//描述：等待线程退出
	//参数：无
	//返回值：无
	void  WaitExit(void);
	///</func_info>
	
	///<func_info>
	//描述：获取线程的句柄值
	//参数：无
	//返回值：HANDLE类型的线程句柄值
	HANDLE GetHandle();
	///</func_info>

	
	///<func_info>
	//描述：强制终止线程
	//参数：无
	//返回值：无
	void  Terminate();
	///</func_info>
public:
	ArNTThread(void);
	~ArNTThread(void);
private:
	PObjPointer			m_pParent;
	pThreadFuncDef		m_pFunc;
	PObjPointer			m_pContext;
	HANDLE				m_hThread;
	ThreadStatus		m_Status;
	ArNT_BOOL           m_bDestroy;

	static unsigned int __stdcall threadProcess(void* pPara);
};
///</class_info>

///<macro_info>
/////////////////////////////////////////////////////////////////////////////
//定义辅助宏

#define  DECL_THREAD(ID)			ArNTThread  m_Thread##ID;\
									static  ArNT_BOOL ARNT_CALL ThreadFunc##ID(PObjPointer pContext, PObjPointer pParent);

//在类classobj中需要定义一个类型为ArNT_BOOL OnThread(ULongInt ID, PObjPointer pContext)的成员函数
#define  IMPL_THREAD(ID, classobj)	ArNT_BOOL ARNT_CALL classobj##::ThreadFunc##ID(PObjPointer pContext, PObjPointer pParent){\
									classobj* pClassObj = (classobj*)pParent;\
									return(pClassObj->OnThread(ID, pContext)); }

#define  THREAD_INIT_RUN(ID, pContext)				m_Thread##ID.Init(ThreadFunc##ID,  pContext, ArNTThread::enRun, this);
#define  ITHREAD_INIT_SUSPEND(ID, pContext)			m_Thread##ID.Init(ThreadFunc##ID,  pContext, ArNTThread::enSuspend, this);
#define  ARNT_THREAD_SUSPEND(ID)							m_Thread##ID.Suspend();
#define  ARNT_THREAD_RESUME(ID)							m_Thread##ID.Resume();
#define  THREAD_WAIT_EXIT(ID)						m_Thread##ID.WaitExit();
//线程函数只完成单个任务
#define  THREAD_ONCE(ID, pContext)    ArNTThread* pThread##ID = new ArNTThread(); pThread##ID->Init(ThreadFunc##ID,  pContext, ArNTThread::enRun, this, ArNT_TRUE);
//如果在类中只创建一个线程
#define  ONLY_THREAD_ID							0
#define  DECL_ONLY_THREAD()						DECL_THREAD(ONLY_THREAD_ID)
#define  IMPL_ONLY_THREAD(classobj)				IMPL_THREAD(ONLY_THREAD_ID, classobj)
#define  ONLY_THREAD_INIT_RUN(pContext)			THREAD_INIT_RUN(ONLY_THREAD_ID, pContext)
#define  ONLY_THREAD_INIT_SUSPEND(pContext)		ITHREAD_INIT_SUSPEND(ONLY_THREAD_ID, pContext)
#define  ONLY_THREAD_SUSPEND()					THREAD_SUSPEND(ONLY_THREAD_ID)
#define  ONLY_THREAD_RESUME()					THREAD_RESUME(ONLY_THREAD_ID)
#define  ONLY_THREAD_WAIT_EXIT()				THREAD_WAIT_EXIT(ONLY_THREAD_ID)



///</macro_info>