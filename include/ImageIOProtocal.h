#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ImageIOProtocal.h
// 作  者 ：杨朝霖
// 用  途 ：定义与图像IO相关的协议
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖     2010-8-10    创建

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNTIMGPlatFormDataStruct.h"
#include "ArNTIPCModule.h"
///</header_info>

///<datastruct_info>
//==============================================================================
// 基本数据类型定义

typedef struct tagArNTImageDesc
{
	TINYSTR strType; //图像的保存类型，如"FILE", "DATABASE", L"SAMPLEDB"
	LONGSTR strInfo; //图像的描述信息,可用是图像完整路径名，或根据图像保存的方式而定义的标志图像位置的信息
}*PArNTImageDesc, ArNTImageDesc;

typedef struct tagArNTImageSupportType
{
	TINYSTR strType; //图像的保存类型，如"FILE", "DATABASE", L"SAMPLEDB"
	TINYSTR strDesc; //类型的详细信息
}*PArNTImageSupportType, ArNTImageSupportType;
///</datastruct_info>

//==============================================================================
///<macro_info>

#define IOP				L"ImageIO_D7230C89-08F3-45df-9890-6AB23DD5AE5C"   
#define IOPUI			L"ImageIOUI_D7230C89-08F3-45df-9890-6AB23DD5AE5C"

#define IOP_START_CMD			500
#define DECL_IOP_MESSAGE(classobj,channel) private: static void __stdcall classobj::IOPMessageFunc##channel(ULongInt iMemSize, PMemPointer pMem, PMemPointer pContext);\
										   public: ArNT_BOOL  classobj::Reg##channel(LONGSTR& strChannel, LongInt iChannelSize = 32 * 1024, ShortInt iObjID = -1);\
										   public: void		  classobj::UnReg##channel(ShortInt iObjID = -1);


#define BEGIN_IOP_MESSAGE(classobj, channel)  ArNT_BOOL  classobj::Reg##channel(LONGSTR& strChannel, LongInt iChannelSize, ShortInt iObjID){\
									ArNTIPCInfo Info = {sizeof(ArNTIPCInfo)};\
									if(iObjID >= 0){CCommonFunc::SafeStringPrintf(Info.strChannelName, STR_LEN(Info.strChannelName), L"%s%d", channel, iObjID);}else{\
									CCommonFunc::SafeStringPrintf(Info.strChannelName, STR_LEN(Info.strChannelName), L"%s", channel);}\
									CCommonFunc::SafeStringPrintf(strChannel, STR_LEN(strChannel), Info.strChannelName);\
									ArNT_ERROR_CODE code = ArNTIPCRegChannel(Info, IOPMessageFunc##channel, this); \
									return code == SUCCESS_IPC_OPERCHANNEL;}\
									void		  classobj::UnReg##channel(ShortInt iObjID) { \
									LONGSTR strRemoveChannel = {0};\
									if(iObjID >= 0){CCommonFunc::SafeStringPrintf(strRemoveChannel, STR_LEN(strRemoveChannel), L"%s%d", channel, iObjID);}else{\
									CCommonFunc::SafeStringPrintf(strRemoveChannel, STR_LEN(strRemoveChannel), L"%s", channel);}\
									ArNTRemoveChannel(strRemoveChannel);\
									};\
									void __stdcall classobj::IOPMessageFunc##channel(ULongInt iMemSize, PMemPointer pMem, PMemPointer pContext) {\
	                                PArNTCommand pCmd = (PArNTCommand)pMem;\
									if(iMemSize != pCmd->iTotalSize) return;\
									classobj* pParent = (classobj*)pContext;

#define IOP_MESSAGE_ENTRY(id, func, para) if(pCmd->iID == id) {  \
										if(pCmd->bAlloc == ArNT_FALSE)\
										 { \
											pParent->func((para*)pCmd->buffer.Reserve, pCmd->strSenderID);\
										}else{\
										pParent->func((para*)pCmd->buffer.Alloc, pCmd->strSenderID);};\
										return;}

#define END_IOP_MESSAGE(classobj) };

#define  DECL_IOP_LOCAL_MESSAGE(classobj)	DECL_IOP_MESSAGE(classobj, IOP)
#define  DECL_IOP_UI_MESSAGE(classobj)		DECL_IOP_MESSAGE(classobj, IOPUI)

#define		BEGIN_IOP_LOCAL_MESSAGE(classobj)	BEGIN_IOP_MESSAGE(classobj, IOP)
#define		BEGIN_IOP_UI_MESSAGE(classobj)		BEGIN_IOP_MESSAGE(classobj, IOPUI)

#define		REG_IOPUI(ObjID) RegIOPUI(ObjID)


//==============================================================================
///</macro_info>

//==============================================================================
///<message>
#define  IOP_ASK_READIMAGE					(IOP_START_CMD + 1)
#define  IOP_ASK_READIMAGE_DESC				L"请求读取指定的图像"
typedef struct tagIOPAskReadImage
{
	LongInt iSize;
	LONGSTR strFromChannel;
	ArNTImageDesc ImgDesc;		
}IOPAskReadImage, *PIOPAskReadImage;

#define  IOP_ACK_READIMAGE					(IOP_START_CMD + 2)
#define  IOP_ACK_READIMAGE_DESC				L"回复读取的图像数据"
typedef struct tagIOPAckReadImage
{
	LongInt iSize;
	TINYSTR strType; //用于确定文件附带信息类型
	ShortInt iContextSize;
	enum {MAX_CONTEXT_SIZE = 256};
	BufferByte ImageContex[MAX_CONTEXT_SIZE];//图像文件中附带的信息
	RawCaptureImage RawImg[1];	
}IOPAckReadImage, *PIOPAckReadImage;

#define  IOP_ASK_WRITEIMAGE					(IOP_START_CMD + 3)
#define  IOP_ASK_WRITEIMAGE_DESC			L"请求保存指定的图像"
typedef struct tagIOPAskWriteImage
{
	LongInt iSize;
	LONGSTR strFromChannel;
	ArNTImageDesc ImgDesc;
	ShortInt iContextSize;
	BufferByte ImgageContex[tagIOPAckReadImage::MAX_CONTEXT_SIZE];//图像文件中附带的信息
	RawCaptureImage RawImg[1];	//实际的图像数据
}IOPAskWriteImage, *PIOPAskWriteImage;

#define  IOP_ASK_CMDLIST						(IOP_START_CMD + 4)	
#define  IOP_ASK_CMDLIST_DESC					L"询问ICP协议命令列表"
typedef struct tagIOPAskCmdList
{
	LongInt iSize;
	LONGSTR strFromChannel;
}IOPAskCmdList, *PIOPAskCmdList;

#define  IOP_ACK_CMDLIST						(IOP_START_CMD + 5)	
#define  IOP_ACK_CMDLIST_DESC					L"回复ICP协议命令列表"
typedef struct tagIOPAckCmdList
{
	LongInt iSize;
	ShortInt iCmdNum;
	ArNTCommandDesc Desc[1];
}IOPAckCmdList, *PIOPAckCmdList;

#define  IOP_ASK_SUPPORT_TYPE					(IOP_START_CMD + 6)	
#define  IOP_ASK_SUPPORT_TYPE_DESC				L"询问支持的图像保存类型"
typedef struct tagIOPAskSupportType
{
	LongInt iSize;
	LONGSTR strFromChannel;
}IOPAskSupportType, *PIOPAskSupportType;

#define  IOP_ACK_SUPPORT_TYPE					(IOP_START_CMD + 7)	
#define  IOP_ACK_SUPPORT_TYPE_DESC				L"回复支持的图像保存类型"
typedef struct tagIOPAckSupportType
{
	LongInt iSize;
	SHORTSTR strAppName;
	enum {MAX_ITEM_NUM = 64};
	ShortInt iTypeNum;
	ArNTImageSupportType Items[MAX_ITEM_NUM];
}IOPAckSupportType, *PIOPAckSupportType;

#define IOP_ASK_RUNINFO							(IOP_START_CMD + 8)		
#define IOP_ASK_RUNINFO_DESC					L"询问当前程序的运行信息"
typedef struct tagIOPAskRunInfo
{
	LongInt iSize;
	LONGSTR strFromChannel;
}IOPAskRunInfo, *PIOPAskRunInfo;


#define IOP_ACK_RUNINFO							(IOP_START_CMD + 9)		
#define IOP_ACK_RUNINFO_DESC					L"回复当前程序的运行信息"
typedef struct tagIOPAckRunInfo
{
	LongInt iSize;
	SHORTSTR strAppName;
	ShortInt iInfoNum;
	ArNTRunInfo RunInfo[1];
}IOPAckRunInfo, *PIOPAckRunInfo;

#define IOP_ASK_SEARCHIMAGE						(IOP_START_CMD + 10)		
#define IOP_ASK_SEARCHIMAGE_DESC				L"查询满足条件的图像信息"
typedef struct tagIOPAskSearchImage
{
	LongInt iSize;
	LONGSTR strFromChannel;
	TINYSTR strType;         
	LONGSTR strCondition;
}IOPAskSearchImage, *PIOPAskSearchImage;

#define IOP_ACK_SEARCHIMAGE						(IOP_START_CMD + 11)		
#define IOP_ACK_SEARCHIMAGE_DESC				L"回复满足条件的图像信息"
typedef struct tagIOPAckSearchImage
{
	LongInt iSize;
	ShortInt iImgNum;
	ArNTImageDesc  ImgDesc[1];
}IOPAckSearchImage, *PIOPAckSearchImage;

#define IOP_ASK_SERVICE						(IOP_START_CMD + 12)		
#define IOP_ASK_SERVICE_DESC				L"查询支持IOP协议的服务"

typedef struct tagIOPAskService
{
	LongInt iSize;
	LONGSTR strFromChannel;
}IOPAskService, *PIOPAskService;

#define IOP_ACK_SERVICE						(IOP_START_CMD + 13)		
#define IOP_ACK_SERVICE_DESC				L"回复支持IOP协议的服务"
typedef struct tagIOPAckService
{
	LongInt iSize;
	ArNServiceAppInfo Service;
	
}IOPAckService, *PIOPAckService;

#define IOP_ASK_ERROR_LIST					(IOP_START_CMD + 14)		
#define IOP_ASK_ERROR_LIST_DESC				L"查询服务的错误列表"

typedef struct tagIOPAskErrorList
{
	LongInt iSize;
	LONGSTR strFromChannel;
}IOPAskErrorList, *PIOPAskErrorList;

#define IOP_ACK_ERROR_LIST					(IOP_START_CMD + 15)		
#define IOP_ACK_ERROR_LIST_DESC				L"回复服务的错误列表信息"
typedef struct tagIOPAckErrorList
{
	LongInt iSize;
	LONGSTR strAppName;
	ShortInt iErrorNum;
	ArNTErrorInfo ErrorInfo[1];
}IOPAckErrorList, *PIOPAckErrorList;


#define IOP_NTF_ERROR						(IOP_START_CMD + 16)	
#define IOP_NTF_ERROR_DESC					L"通知错误信息"
typedef struct tagIOPNtfError
{
	LongInt iSize;
	LONGSTR		strAppName;
	TINYSTR		strCmdName;
	ArNT_ERROR_CODE	dwErrorCode;	
	SHORTSTR	strErrorInfo;
}IOPNtfError, *PIOPNtfError;
