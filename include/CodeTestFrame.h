#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：CodeTestFrame.h
// 作  者 ：杨朝霖
// 用  途 ：定义了源码测试平台SDK所需要的头文件
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖       2012-12-1    创建模块

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
//包括通用功能函数
#include "CommonFunc.h"
//包括通用类
#include "ArNTCommonClass.h"
//包括测试单元类
#include "ArNTTestUint.h"
//包括单线程控制台输出工厂类
#include "ArNTSTConsoleTest.h"
//包括多线程窗口方式输出工厂类
#include "ArNTMTWindowTest.h"
//包括多线程日志文件方式输出工厂类
#include "ArNTMTLogTest.h"


//==============================================================================
///链接信息
#ifndef AUTOLINK_CODETESTFRAME
#define  AUTOLINK_CODETESTFRAME
#pragma comment( lib, "CodeTestFrame.lib" )

#endif