#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTMemPool.h
// 作  者 ：杨朝霖
// 用  途 ：定义对指定内存进行操作的类
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>


///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖      2012-9-18   创建模块

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
///</header_info>


typedef struct tagMemDesc
{
	LongInt  iUsedSize;
	LongLongInt iCheckSum;
	LongInt iNextPos;
}*PMemDesc, MemDesc;

class MutexOper
{
public:
	virtual ~MutexOper() {};
	virtual ArNT_BOOL  Enter() = 0;
	virtual ArNT_BOOL  Leave() = 0;
};
typedef struct tagMemPoolHandle
{
	void* pPool;
	LongInt   iPos;
}MemPoolHandle, *PMemPoolHandle;

typedef struct tagMemPoolPara
{
	ShortInt     m_iSize;
	LongInt		 m_iBlockSize;
	LongInt      m_iBlockNum;
	LongInt      m_iTotalSize;
	LongInt      m_iDescArraySize;
	LongInt      m_iFirstAviaIndex;
	ArNT_BOOL    m_bHasInitial;
}*PMemPoolPara, MemPoolPara;

class ArNTMemPool
{

public:
	void SetOperMem(PBufferByte pBuffer, LongInt iTotalBufferSize, LongInt iBlockSize, MutexOper* MtOper = NULL);
	ArNT_BOOL   Alloc(LongInt iDataSize, MemPoolHandle& Handle);
	ArNT_BOOL   Free(MemPoolHandle& Handle);
	ArNT_BOOL   WriteData(MemPoolHandle& DestHandle, PMemPointer pSrcData, LongInt iDataLen);
	ArNT_BOOL   ReadData(MemPoolHandle& SrcHandle,   PMemPointer pDestData, LongInt& iDataLen);

	LongInt     GetFreeSize(void);
	LongInt     GetTotalSize(void);
	LongInt     GetBlockSize(void);
	void        DisableCheckSum(void);
public:
	ArNTMemPool(ArNT_BOOL bEnableCheckSum = false);
	~ArNTMemPool(void);
private:
	PBufferByte  m_pOperBuffer;	
	MutexOper*	 m_pMtOper;
	ArNT_BOOL    m_bAllocMtOper;
	ArNT_BOOL    m_bAllocBuffer;
	ArNT_BOOL    m_bEnableCheckSum;
	PMemPoolPara m_pMemPoolPara;
	PBufferByte  m_pActrualDataBuffer;
	PBufferByte  m_pDescBuffer;
	ShortInt     m_iMemReserve;
};
