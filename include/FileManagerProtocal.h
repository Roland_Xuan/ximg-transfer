#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：FileManagerProtocal.h
// 作  者 ：杨朝霖
// 用  途 ：定义与图像IO相关的协议
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖     2013-1-28    创建
//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNTIMGPlatFormDataStruct.h"
#include "ArNTIPCModule.h"
///</header_info>

///<datastruct_info>
//==============================================================================
// 基本数据类型定义
typedef LongInt		VersionType;
#define INVALID_VERSION			(-1)

//备份项信息
typedef struct tagArNTFMPBackupItem
{
	LongInt			iSize;
	TINYSTR			strUser;		//备份人的信息
	SHORTSTR		strDesc;		//备份项的描述
	VersionType		VerID;			//版本号
	LongLongInt		TotalFileSize;	//该版本所有文件总大小
	TINYSTR			VerDate;		//版本日期
	LongInt			iFileItemNum;	//该次备份下的文件总数
}ArNTFMPBackupItem, *PArNTFMPBackupItem;

typedef struct tagArNTBackupType
{
	LongInt iSize;
	TINYSTR strSaveType;  //工程的保存方式
	enum{MAX_SUPPORT_TYPE_NUM = 64};	//可以支持的最大文件类型数量
	ShortInt	iFileTypeNum;						//支持保存的文件类型数量
	TINYSTR		FileType[MAX_SUPPORT_TYPE_NUM];	//支持保存的文件类型
}ArNTBackupType, *PArNTBackupType;

//备份的工程信息
typedef struct tagArNTFMProject
{
	LongInt				iSize;		//结构的尺寸
	SHORTSTR			strName;	//备份的工程名
	LONGSTR				strDesc;	//工程描述
	TINYSTR				strOwner;	//工程的创建者
	UShortInt			iBackUpItemNum; //备份项的数量
	ArNTBackupType		SupportType;	
	VersionType			LastVerion;//最近一次备份的版本号
}ArNTFMProject, *PArNTFMProject;

typedef struct tagArNTSimpleBackFileItem
{
	LongInt iSize;
	ULongLongInt iFileSize;
	ULongLongInt iCheckSum;
	VersionType VerID;		//版本号
}ArNTSimpleBackFileItem, *PArNTSimpleBackFileItem;


typedef struct tagArNTBackFileItem
{
	LongInt iSize;
	SHORTSTR strName;
	LONGSTR strRelativePath;
	ArNTSimpleBackFileItem Info;
}ArNTBackFileItem, *PArNTBackFileItem;





///</datastruct_info>

//==============================================================================
///<macro_info>
#define MAX_VERSION_NUM(Version)  (Version & 0x000000FF)
#define MID_VERSION_NUM(Version)  ((Version & 0x0000FF00)>> 8)
#define MIN_VERSION_NUM(Version)  ((Version & 0x00FF0000)>> 16)
#define MAKE_VERSION(MaxVer, MidVer, MinVer)  (MaxVer +( MidVer << 8) + (MinVer<< 16))

#define FMP				L"FMP_3F1C8106-46A9-4c6e-B795-764EE132E3B9"   
#define FMPUI			L"FMPUI_3F1C8106-46A9-4c6e-B795-764EE132E3B9"

#define FMP_START_CMD			200

#define DECL_FMP_MESSAGE(classobj,channel) private: static void __stdcall classobj::MessageFunc##channel(ULongInt iMemSize, PMemPointer pMem, PMemPointer pContext);\
										   public: ArNT_BOOL  classobj::Reg##channel(LONGSTR& strChannel, LongInt iChannelSize = 32 * 1024, ShortInt iObjID = -1);\
										   public: void		  classobj::UnReg##channel(ShortInt iCamIndex = -1);


#define BEGIN_FMP_MESSAGE(classobj, channel)  ArNT_BOOL  classobj::Reg##channel(LONGSTR& strChannel, LongInt iChannelSize, ShortInt iObjID){\
									ArNTIPCInfo Info = {sizeof(ArNTIPCInfo)};\
									if(iObjID >= 0){CCommonFunc::SafeStringPrintf(Info.strChannelName, STR_LEN(Info.strChannelName), L"%s%d", channel, iObjID);}else{\
									CCommonFunc::SafeStringPrintf(Info.strChannelName, STR_LEN(Info.strChannelName), L"%s", channel);}\
									CCommonFunc::SafeStringPrintf(strChannel, STR_LEN(strChannel), Info.strChannelName);\
									ArNT_ERROR_CODE code = ArNTIPCRegChannel(Info, MessageFunc##channel, this); \
									return code == SUCCESS_IPC_OPERCHANNEL;}\
									void		  classobj::UnReg##channel(ShortInt iObjID) { \
									LONGSTR strRemoveChannel = {0};\
									if(iObjID >= 0){CCommonFunc::SafeStringPrintf(strRemoveChannel, STR_LEN(strRemoveChannel), L"%s%d", channel, iObjID);}else{\
									CCommonFunc::SafeStringPrintf(strRemoveChannel, STR_LEN(strRemoveChannel), L"%s", channel);}\
									ArNTRemoveChannel(strRemoveChannel);\
									};\
									void __stdcall classobj::MessageFunc##channel(ULongInt iMemSize, PMemPointer pMem, PMemPointer pContext) {\
	                                PArNTCommand pCmd = (PArNTCommand)pMem;\
									if(iMemSize != pCmd->iTotalSize) return;\
									classobj* pParent = (classobj*)pContext;

#define FMP_MESSAGE_ENTRY(id, func, para) if(pCmd->iID == id) {  \
										if(pCmd->bAlloc == ArNT_FALSE)\
										 { \
											pParent->func((para*)pCmd->buffer.Reserve, pCmd->strSenderID);\
										}else{\
										pParent->func((para*)pCmd->buffer.Alloc, pCmd->strSenderID);};\
										return;}

#define END_FMP_MESSAGE(classobj) };

#define  DECL_FMP_LOCAL_MESSAGE(classobj)	DECL_FMP_MESSAGE(classobj, FMP)
#define  DECL_FMP_UI_MESSAGE(classobj)		DECL_FMP_MESSAGE(classobj, FMPUI)

#define		BEGIN_FMP_LOCAL_MESSAGE(classobj)	BEGIN_FMP_MESSAGE(classobj, FMP)
#define		BEGIN_FMP_UI_MESSAGE(classobj)		BEGIN_FMP_MESSAGE(classobj, FMPUI)

#define		REG_FMPUI(ObjID) RegFMPUI(ObjID)
#define     UNREG_FMPUI(ObjID) UnRegFMPUI(ObjID)



//==============================================================================
///</macro_info>

//==============================================================================
///<message>
#define  FMP_ASK_LOGIN						(FMP_START_CMD + 1)
#define  FMP_ASK_LOGIN_DESC					L"指定的用户请求登录"
typedef struct tagFMPAskLogin
{
	LongInt iSize;
	LONGSTR strFromChannel;	
	SHORTSTR strName;		//要登录的用户名			
	SHORTSTR strPassWd;		//用户的密码
}FMPAskLogin, *PFMPAskLogin;



#define  FMP_ACK_LOGIN						(FMP_START_CMD + 2)
#define  FMP_ACK_LOGIN_DESC					L"回复用户登录请求"
typedef struct tagFMPAckLogin
{
	LongInt iSize;
	SHORTSTR strName;		//用户名
	ArNT_BOOL bStatus;		//登录状态，ArNT_TRUE表示成功，ArNT_FALSE表示失败			
	LONGSTR  strDesc;		//如果登录失败，给出的原因
	tagArNTUserInfo::ArNTRank Rank; //登录用户的级别
}FMPAckLogin, *PFMPAckLogin;

#define  FMP_ASK_LOGOFF						(FMP_START_CMD + 3)
#define  FMP_ASK_LOGOFF_DESC				L"请求注销指定的用户"
typedef struct tagFMPAskLogOff
{
	LongInt iSize;
	LONGSTR strFromChannel;	
	SHORTSTR strName;		//要登录的用户名			
}FMPAskLogOff, *PFMPAskLogOff;



#define  FMP_ACK_LOGOFF						(FMP_START_CMD + 4)
#define  FMP_ACK_LOGOFF_DESC				L"回复用户注销请求"
typedef struct tagFMPAckLogOff
{
	LongInt iSize;
	SHORTSTR strName;		//用户名
	ArNT_BOOL bStatus;		//注销状态，ArNT_TRUE表示成功，ArNT_FALSE表示失败			
	LONGSTR  strDesc;		//如果注销失败，给出的原因
}FMPAckLogOff, *PFMPAckLogOff;



#define  FMP_ASK_ADDUSER					(FMP_START_CMD + 5)
#define	 FMP_ASK_ADDUSER_DESC				 L"请求添加新用户"
typedef struct tagFMPAskAddUser
{
	LongInt iSize;
	LONGSTR strFromChannel;	
	SHORTSTR strOperUser;					//执行该请求的用户	
	ArNTUserInfo Info;						//需要添加的用户信息
}FMPAskAddUser, *PFMPAskAddUser;

#define  FMP_ACK_ADDUSER					(FMP_START_CMD + 6)
#define  FMP_ACK_ADDUSER_DESC				L"回复用户添加请求"
typedef struct tagFMPAckAddUser
{
	LongInt iSize;
	SHORTSTR strName;		//用户名
	ArNT_BOOL bStatus;		//登录状态，ArNT_TRUE表示成功，ArNT_FALSE表示失败			
	LONGSTR  strDesc;		//如果登录失败，给出的原因
}FMPAckAddUser, *PFMPAckAddUser;

#define  FMP_ASK_DELUSER					(FMP_START_CMD + 7)
#define	 FMP_ASK_DELUSER_DESC				 L"请求删除新用户"
typedef struct tagFMPAskDelUser
{
	LongInt iSize;
	LONGSTR strFromChannel;	
	SHORTSTR strOperUser;					//执行该请求的用户	
	SHORTSTR strUserName;					//需要删除的用户名
}FMPAskDelUser, *PFMPAskDelUser;

#define  FMP_ACK_DELUSER					(FMP_START_CMD + 8)
#define  FMP_ACK_DELUSER_DESC				L"回复用户删除请求"
typedef struct tagFMPAckDelUser
{
	LongInt iSize;
	SHORTSTR strName;		//用户名
	ArNT_BOOL bStatus;		//删除状态，ArNT_TRUE表示成功，ArNT_FALSE表示失败			
	LONGSTR  strDesc;		//如果删除失败，给出的原因
}FMPAckDelUser, *PFMPAckDelUser;

#define  FMP_ASK_USERLIST					(FMP_START_CMD + 9)     
#define  FMP_ASK_USERLIST_DESC				L"询问用户列表"   
typedef struct tagFMPAskUserList
{
	LongInt iSize;
	LONGSTR strFromChannel;	
	SHORTSTR strOperUser;					//执行该请求的用户	
}FMPAskUserList, *PFMPAskUserList;

#define  FMP_ACK_USERLIST					(FMP_START_CMD + 10)
#define  FMP_ACK_USERLIST_DESC				L"回复用户列表"
typedef struct tagFMPAckUserList
{
	LongInt iSize;
	SHORTSTR strOper;		//操作用户名
	ShortInt iUserNum;		
	ArNTUserInfo Info[1];		//描述信息
}FMPAckUserList, *PFMPAckUserList;

#define FMP_ASK_PROJECTSET					(FMP_START_CMD + 11)		
#define FMP_ASK_PROJECTSET_DESC				L"询问工程信息集合"
typedef struct tagFMPAskProjectSet
{
	LongInt iSize;
	LONGSTR strFromChannel;					
	SHORTSTR strOperUser;					//执行该请求的用户	
}FMPAskProjectSet, *PFMPAskProjectSet;


#define FMP_ACK_PROJECTSET					(FMP_START_CMD + 12)		
#define FMP_ACK_PROJECTSET_DESC				L"回复工程信息集合"
typedef struct tagFMPAckProjectSet
{
	LongInt iSize;
	SHORTSTR strOperUser;		
	ShortInt iProjectNum;
	ArNTFMProject Projects[1];
}FMPAckProjectSet, *PFMPAckProjectSet;

#define FMP_ASK_PROJECT_BACKUP				(FMP_START_CMD + 13)		
#define FMP_ASK_PROJECT_BACKUP_DESC			L"询问工程的备份信息"
typedef struct tagFMPAskProjectBackup
{
	LongInt iSize;
	LONGSTR		strFromChannel;					
	SHORTSTR	strOperUser;					//执行该请求的用户	
	SHORTSTR	strProjectName;					//工程名
}FMPAskProjectBackup, *PFMPAskProjectBackup;


#define FMP_ACK_PROJECT_BACKUP				(FMP_START_CMD + 14)		
#define FMP_ACK_PROJECT_BACKUP_DESC			L"回复工程的备份信息"
typedef struct tagFMPAckProjectBackup
{
	LongInt iSize;
	SHORTSTR	strProjectName;			//工程名
	ShortInt	iBackupNum;				//备份项数量
	ArNTFMPBackupItem BackupItems[1];	//备份项内容
}FMPAckProjectBackup, *PFMPAckProjectBackup;


#define FMP_ASK_EXTRACT_PROJECT				(FMP_START_CMD + 15)		
#define FMP_ASK_EXTRACT_PROJECT_DESC		L"请求提取指定工程的所有文件"
typedef struct tagFMPAskExtractProject
{
	LongInt iSize;
	LONGSTR		strFromChannel;					
	SHORTSTR	strOperUser;					//执行该请求的用户	
	SHORTSTR	strProjectName;					//工程名
	VersionType VerID;							//需要提取的版本
	LONGSTR		strDestPath;					//提取文件的目的目录
}FMPAskExtractProject, *PFMPAskExtractProject;


#define FMP_ACK_EXTRACT_PROJECT				(FMP_START_CMD + 16)		
#define FMP_ACK_EXTRACT_PROJECT_DESC		L"回复提取工程文件的进度"
typedef struct tagFMPAckExtractProject
{
	LongInt iSize;
	SHORTSTR	strProjectName;			//工程名
	ShortInt	iTotalExtractNum;		//需要提取的总的文件数量
	ShortInt    iHasExtractNum;			//已经提取的文件数量
}FMPAckExtractProject, *PFMPAckExtractProject;

#define FMP_ASK_BACKUP_PROJECT				(FMP_START_CMD + 17)	
#define FMP_ASK_BACKUP_PROJECT_DESC			L"请求备份文件到指定的工程"

typedef struct tagFMPAskBackupProject
{
	LongInt iSize;
	LONGSTR			strFromChannel;					
	SHORTSTR		strOperUser;					//执行该请求的用户	
	SHORTSTR		strProjectName;					//工程名
	SHORTSTR		strDesc;						//工程描述信息
	ArNT_BOOL		bLargeChange;                   //工程发生较大改动
	ArNT_BOOL		bRelease;						//发布版本的工程源码
	LONGSTR			strSrcPath;						//需要备份的源文件
}FMPAskBackupProject, *PFMPAskBackupProject;

#define FMP_ACK_BACKUP_PROJECT				(FMP_START_CMD + 18)		
#define FMP_ACK_BACKUP_PROJECT_DESC			L"回复备份工程文件的进度"
typedef struct tagFMPAckBackupProject
{
	LongInt iSize;
	SHORTSTR	strProjectName;			//工程名
	ShortInt	iTotalBackupNum;		//需要提取的总的文件数量
	ShortInt    iHasBackupNum;			//已经提取的文件数量
	ArNT_BOOL   bUpdate;
}FMPAckBackupProject, *PFMPAckBackupProject;

#define FMP_ASK_CREATE_PROJECT				(FMP_START_CMD + 19)	
#define FMP_ASK_CREATE_PROJECT_DESC			L"请求创建指定名称的工程"

typedef struct tagFMPAskCreateProject
{
	LongInt				iSize;
	LONGSTR				strFromChannel;					
	SHORTSTR			strOperUser;					//执行该请求的用户	
	SHORTSTR			strProjectName;					//工程名
	SHORTSTR            strDesc;						//工程的描述信息
	ArNTBackupType		SupportType;					//工程中需要备份的文件类型	
	LONGSTR				strSrcPath;						//要创建工程的源文件路径
}FMPAskCreateProject, *PFMPAskCreateProject;

#define FMP_ACK_CREATE_PROJECT				(FMP_START_CMD + 20)	
#define FMP_ACK_CREATE_PROJECT_DESC			L"回复请求创建工程"

typedef struct tagFMPAckCreateProject
{
	LongInt				iSize;
	SHORTSTR			strProjectName;					//工程名
	ArNT_BOOL			bStatus;						//ArNT_TRUE表示创建成功，ArNT_FALSE表示创建失败
	LONGSTR				strDesc;						//如果创建失败，说明原因。
}FMPAckCreateProject, *PFMPAckCreateProject;


#define FMP_ASK_DEL_PROJECT					(FMP_START_CMD + 21)	
#define FMP_ASK_DEL_PROJECT_DESC			L"请求删除指定的工程"
typedef struct tagFMPAskDeleteProject
{
	LongInt				iSize;
	LONGSTR				strFromChannel;					
	SHORTSTR			strOperUser;					//执行该请求的用户	
	SHORTSTR			strProjectName;					//工程名
}FMPAskDeleteProject, *PFMPAskDeleteProject;


#define FMP_ACK_DEL_PROJECT					(FMP_START_CMD + 22)	
#define FMP_ACK_DEL_PROJECT_DESC			L"回复请求删除工程"
typedef struct tagFMPAckDeleteProject
{
	LongInt				iSize;
	SHORTSTR			strProjectName;					//工程名
	ArNT_BOOL			bStatus;						//ArNT_TRUE表示删除成功，ArNT_FALSE表示删除失败
	LONGSTR				strDesc;						//如果创建失败，说明原因。
}FMPAckDeleteProject, *PFMPAckDeleteProject;


#define FMP_ASK_FILE_LIST					(FMP_START_CMD + 23)	
#define FMP_ASK_FILE_LIST_DESC				L"询问指定文件的版本列表"
typedef struct tagFMPAskFileList
{
	LongInt				iSize;
	LONGSTR				strFromChannel;					
	SHORTSTR			strOperUser;					//执行该请求的用户	
	SHORTSTR			strProjectName;					//工程名
	LONGSTR				strFileName;					//询问的文件(包括相对路径)
}FMPAskFileList, *PFMPAskFileList;

#define FMP_ACK_FILE_LIST					(FMP_START_CMD + 24)	
#define FMP_ACK_FILE_LIST_DESC				L"回复指定文件的版本列表"
typedef struct tagFMPAckFileList
{
	LongInt				iSize;
	SHORTSTR			strProjectName;					//工程名
	LONGSTR				strFileName;					//询问的文件
	ShortInt			iItemNum;						//列表数量
	ArNTFMPBackupItem	Items[1];						//列表信息
}FMPAckFileList, *PFMPAckFileList;

#define FMP_ASK_EXTRACT_FILE				(FMP_START_CMD + 25)	
#define FMP_ASK_EXTRACT_FILE_DESC			L"请求提取指定版本的文件"
typedef struct tagFMPAskExtractFile
{
	LongInt				iSize;
	LONGSTR				strFromChannel;					
	SHORTSTR			strOperUser;					//执行该请求的用户	
	SHORTSTR			strProjectName;					//工程名
	SHORTSTR			strFileName;					//询问的文件
	VersionType			Version;						//要提取的版本
	LONGSTR				strDestPath;					//要提取到的路径

}FMPAskExtractFile, *PFMPAskExtractFile;

#define FMP_ACK_EXTRACT_FILE				(FMP_START_CMD + 26)	
#define FMP_ACK_EXTRACT_FILE_DESC			L"回复指定版本的文件"
typedef struct tagFMPAckExtractFile
{
	LongInt				iSize;
	SHORTSTR			strProjectName;					//工程名
	SHORTSTR			strFileName;					//询问的文件
	VersionType			Version;						//要提取的版本
	ArNT_BOOL			bStatus;						//ArNT_TRUE表示删除成功，ArNT_FALSE表示删除失败
	LONGSTR				strDesc;						//如果创建失败，说明原因。	
}FMPAckExtractFile, *PFMPAckExtractFile;


#define FMP_NTF_ERROR						(FMP_START_CMD + 27)	
#define FMP_NTF_ERROR_DESC					L"通知错误信息"
typedef struct tagFMPNtfError
{
	LongInt iSize;
	LONGSTR		strAppName;
	TINYSTR		strCmdName;
	ArNT_ERROR_CODE	dwErrorCode;	
	SHORTSTR	strErrorInfo;
}FMPNtfError, *PFMPNtfError;

#define FMP_ASK_ERROR_LIST					(FMP_START_CMD + 28)		
#define FMP_ASK_ERROR_LIST_DESC				L"查询服务的错误列表"

typedef struct tagFMPAskErrorList
{
	LongInt iSize;
	LONGSTR strFromChannel;
}FMPAskErrorList, *PFMPAskErrorList;

#define FMP_ACK_ERROR_LIST					(FMP_START_CMD + 29)		
#define FMP_ACK_ERROR_LIST_DESC				L"回复服务的错误列表信息"
typedef struct tagFMPAckErrorList
{
	LongInt iSize;
	LONGSTR strAppName;
	ShortInt iErrorNum;
	ArNTErrorInfo ErrorInfo[1];
}FMPAckErrorList, *PFMPAckErrorList;

#define FMP_ASK_SERVICE						(FMP_START_CMD + 30)		
#define FMP_ASK_SERVICE_DESC				L"查询支持FMP协议的服务"

typedef struct tagFMPAskService
{
	LongInt iSize;
	LONGSTR strFromChannel;
}FMPAskService, *PFMPAskService;

#define FMP_ACK_SERVICE						(FMP_START_CMD + 31)		
#define FMP_ACK_SERVICE_DESC				L"回复支持FMP协议的服务"
typedef struct tagFMPAckService
{
	LongInt iSize;
	ArNServiceAppInfo Service;	
}FMPAckService, *PFMPAckService;


#define FMP_ASK_RUNINFO						(FMP_START_CMD + 32)		
#define FMP_ASK_RUNINFO_DESC				L"询问当前程序的运行信息"
typedef struct tagFMPAskRunInfo
{
	LongInt iSize;
	LONGSTR strFromChannel;
}FMPAskRunInfo, *PFMPAskRunInfo;


#define FMP_ACK_RUNINFO						(FMP_START_CMD + 33)		
#define FMP_ACK_RUNINFO_DESC				L"回复当前程序的运行信息"
typedef struct tagFMPAckRunInfo
{
	LongInt iSize;
	SHORTSTR strAppName;
	ShortInt iInfoNum;
	ArNTRunInfo RunInfo[1];
}FMPAckRunInfo, *PFMPAckRunInfo;

#define  FMP_ASK_CMDLIST					(FMP_START_CMD + 34)	
#define  FMP_ASK_CMDLIST_DESC				L"询问协议命令列表"
typedef struct tagFMPAskCmdList
{
	LongInt iSize;
	LONGSTR strFromChannel;
}FMPAskCmdList, *PFMPAskCmdList;

#define  FMP_ACK_CMDLIST					(FMP_START_CMD + 35)	
#define  FMP_ACK_CMDLIST_DESC				L"回复协议命令列表"
typedef struct tagFMPAckCmdList
{
	LongInt iSize;
	ShortInt iCmdNum;
	ArNTCommandDesc Desc[1];
}FMPAckCmdList, *PFMPAckCmdList;


#define  FMP_ASK_BACKUP_FILELST				(FMP_START_CMD + 36)	
#define  FMP_ASK_BACKUP_FILELST_DESC		L"询问指定备份项的文件列表"
typedef struct tagFMPAskBackupFileLst
{
	LongInt iSize;
	LONGSTR strFromChannel;
	SHORTSTR			strOperUser;					//执行该请求的用户	
	SHORTSTR			strProjectName;					//工程名
	VersionType			VerID;						//要询问的版本
}FMPAskBackupFileLst, *PFMPAskBackupFileLst;

#define  FMP_ACK_BACKUP_FILELST				(FMP_START_CMD + 37)	
#define  FMP_ACK_BACKUP_FILELST_DESC		L"回复指定备份项的文件列表"
typedef struct tagFMPAckBackupFileLst
{
	LongInt iSize;
	ArNT_BOOL		bStatus;
	SHORTSTR		strError;
	SHORTSTR		strProjectName;
	VersionType		VerID;		
	ULongInt		iItemNum;
	ArNTBackFileItem FileItem[1];
}FMPAckBackupFileLst, *PFMPAckBackupFileLst;



 



















//==============================================================================
///</message>