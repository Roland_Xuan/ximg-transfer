#pragma once
#include "FileManagerProtocal.h"

class ArNTFMPEvtHandler
{
public:
	//请求用户登录
	virtual  void OnAskLogin(LONGSTR& strFromChannel, StrPointer strName, StrPointer strPassWd, StrPointer strSenderID);
	//请求用户注销
	virtual  void OnAskLogOff(LONGSTR& strFromChannel, StrPointer strName, StrPointer strSenderID);
	//请求添加用户
	virtual  void OnAskAddUser(LONGSTR& strFromChannel, StrPointer strOper, ArNTUserInfo& Info, StrPointer strSenderID);
	//请求删除用户
	virtual  void OnAskDelUser(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strName, StrPointer strSenderID);
	//询问用户登录状态
	virtual  void OnAskUserList(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strSenderID);
	//询问工程信息集合
	virtual  void OnAskProjectSet(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strSenderID);
	//询问指定工程的备份信息集合
	virtual  void OnAskProjectBackupInfo(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strSenderID);
	//请求提取指定工程的所有文件
	virtual  void OnAskExtractProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, VersionType VerID, StrPointer strDestPath, StrPointer strSenderID);
	//请求备份文件到指定的工程
	virtual  void OnAskBackupProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strDesc, ArNT_BOOL bLargeChange, ArNT_BOOL bRelease, StrPointer strSrcPath, StrPointer strSenderID);
	//请求创建指定名称的工程
	virtual  void OnAskCreateProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strDesc, ArNTBackupType& Type, StrPointer strSrcPath, StrPointer strSenderID);
	//请求删除指定的工程
	virtual  void OnAskDelProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strSenderID);
	//询问指定文件的版本列表
	virtual  void OnAskFileList(LONGSTR& strFromChannel, StrPointer strOper,StrPointer strProjectName, StrPointer strFileName, StrPointer strSenderID);
	//请求提取文件
	virtual  void OnAskExtractFile(LONGSTR& strFromChannel, StrPointer strOper,StrPointer strProjectName, StrPointer strFileName, VersionType Version, StrPointer strDestPath, StrPointer strSenderID);
	//查询服务的错误列表
	virtual  void OnAskErrorList(LONGSTR& strFromChannel, StrPointer strSenderID);
	//查询支持FMP协议的服务
	virtual  void OnAskService(LONGSTR& strFromChannel, StrPointer strSenderID);
	//询问当前程序的运行信息
	virtual  void OnAskRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID);
	//询问协议命令列表
	virtual  void OnAskCmdList(LONGSTR& strFromChannel, StrPointer strSenderID);
	//询问指定备份项的文件列表
	virtual  void OnAskBackupFileList(LONGSTR& strFromChannel, StrPointer strOper,StrPointer strProjectName, VersionType Version, StrPointer strSenderID);

public:
	ArNTFMPEvtHandler(void);
	~ArNTFMPEvtHandler(void);

public:
	void* m_pParent;
	void  RegParent(void* pParent);
private:
	void DefautNotImplFunc(StrPointer strFuncInfo);
};


/////////////////////////////////////////////////////////
//消息处理类
class ArNTFMPMsgHandler
{
	DECL_FMP_LOCAL_MESSAGE(ArNTFMPMsgHandler);
public:
	ArNTFMPMsgHandler(void);
	~ArNTFMPMsgHandler(void);
public:
	ArNT_BOOL RegEvtHandler(ArNTFMPEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace, ShortInt iObjID = -1);
	void	UnRegEvtHandler(ShortInt iObjID = -1);
public:
	LONGSTR	m_strRegChannel;
private:
	ArNT_BOOL			m_bEnableTrace;	
	ArNTFMPEvtHandler*	m_pEvtHandler;
	//--------------------------------------------------
	//消息处理函数
	void   ProcessFMPAskLogin(PFMPAskLogin pPara,  StrPointer strSenderID); 
	void   ProcessFMPAskLogOff(PFMPAskLogOff pPara,  StrPointer strSenderID); 
	void   ProcessFMPAskAddUser(PFMPAskAddUser pPara,  StrPointer strSenderID); 
	void   ProcessFMPAskDelUser(PFMPAskDelUser pPara,  StrPointer strSenderID); 
	void   ProcessFMPAskUserList(PFMPAskUserList pPara,  StrPointer strSenderID); 
	void   ProcessFMPAskProjectSet(PFMPAskProjectSet pPara,  StrPointer strSenderID); 
	void   ProcessFMPAskProjectBackup(PFMPAskProjectBackup pPara,  StrPointer strSenderID); 
	void   ProcessFMPAskExtractProject(PFMPAskExtractProject pPara,  StrPointer strSenderID);   
	void   ProcessFMPAskBackupProject(PFMPAskBackupProject pPara,  StrPointer strSenderID);
	void   ProcessFMPAskCreateProject(PFMPAskCreateProject pPara,  StrPointer strSenderID);
	void   ProcessFMPAskDeleteProject(PFMPAskDeleteProject pPara,  StrPointer strSenderID);
	void   ProcessFMPAskFileList(PFMPAskFileList pPara,  StrPointer strSenderID);
	void   ProcessFMPAskExtractFile(PFMPAskExtractFile pPara,  StrPointer strSenderID);
	void   ProcessFMPAskErrorList(PFMPAskErrorList pPara,  StrPointer strSenderID);
	void   ProcessFMPAskService(PFMPAskService pPara,  StrPointer strSenderID);
	void   ProcessFMPAskRunInfo(PFMPAskRunInfo pPara,  StrPointer strSenderID);
	void   ProcessFMPAskCmdList(PFMPAskCmdList pPara,  StrPointer strSenderID);
	void   ProcessFMPAskBackupFileLst(PFMPAskBackupFileLst pPara,  StrPointer strSenderID);

public:
	//回复支持FMP协议的服务
	void AckService(LONGSTR& strFromChannel, StrPointer strApp, StrPointer strVersion, StrPointer strSenderID = NULL);
	//回复错误列表
	void  AckErrorList(PArNTErrorInfo pError, ShortInt iErrorNum, LONGSTR& strFromChannel, StrPointer strSenderID);
	//回复服务的运行信息
	void  AckRunInfo( LONGSTR& strFromChannel,  PArNTRunInfo pInfo, ShortInt iInfoNum, StrPointer strSenderID = NULL);

	//回复用户登录
	void  AckLogin( LONGSTR& strFromChannel,  StrPointer strUser, ArNT_BOOL bStatus,  StrPointer strDesc, tagArNTUserInfo::ArNTRank Rank, StrPointer strSenderID = NULL);
	//回复用户注销
	void  AckLogOff( LONGSTR& strFromChannel,  StrPointer strUser, ArNT_BOOL bStatus,  StrPointer strDesc, StrPointer strSenderID = NULL);
	//回复用户添加
	void  AckAddUser(LONGSTR& strFromChannel,  StrPointer strUser, ArNT_BOOL bStatus,  StrPointer strDesc, StrPointer strSenderID = NULL);
	//回复用户删除
	void  AckDelUser(LONGSTR& strFromChannel,  StrPointer strUser, ArNT_BOOL bStatus,  StrPointer strDesc, StrPointer strSenderID = NULL);
	//回复获取用户列表
	void  AckUserList(LONGSTR& strFromChannel,  StrPointer strOper, ShortInt iUserNum, PArNTUserInfo pInfo, StrPointer strSenderID = NULL);
	//通知错误信息
	void  NtfErrorInfo(LONGSTR& strFromChannel, StrPointer strCmdName, ArNT_ERROR_CODE code, StrPointer strError, StrPointer strSenderID = NULL);
	//回复工程创建
	void  AckCreateProject(LONGSTR& strFromChannel,  StrPointer strProjectName, ArNT_BOOL bStatus,  StrPointer strDesc, StrPointer strSenderID = NULL);
	//回复工程备份进度
	void AckBackUpProject(LONGSTR& strFromChannel, StrPointer strProjectName, ShortInt iTotalNum, ShortInt iHasBackupNum, ArNT_BOOL bUpdate, StrPointer strSenderID = NULL);
	//回复工程信息集合
	void AckProjectSet(LONGSTR& strFromChannel, StrPointer strUser, PArNTFMProject pItems, ShortInt iItemNum, StrPointer strSenderID = NULL);
	//回复工程的备份集合
	void AckProjectBackupItem(LONGSTR& strFromChannel, StrPointer strUser, StrPointer strProjectName, PArNTFMPBackupItem pBackItem, ShortInt iBackupItemNum, StrPointer strSenderID = NULL);
	//回复工程备份提取进度
	void AckProjectExtract(LONGSTR& strFromChannel, StrPointer strProjectName, ShortInt	iTotalExtractNum, ShortInt iHasExtractNum, StrPointer strSenderID = NULL);
	//回复指定备份的文件列表
	void AckBackupItemFileLst(LONGSTR& strFromChannel, StrPointer strProjectName, VersionType VerID, ArNT_BOOL bStatus, StrPointer strErrorInfo, PArNTBackFileItem pItem, ULongInt	iItemNum, StrPointer strSenderID = NULL);
	//回复指定文件的备份列表
	void AckFileBackupItemLst(LONGSTR& strFromChannel, StrPointer strProjectName,  StrPointer strFileName, PArNTFMPBackupItem pItem, ULongInt	iItemNum, StrPointer strSenderID = NULL);

	//回复文件提取的结果信息
	void AckExtractFile(LONGSTR& strFromChannel, StrPointer strProjectName, StrPointer strFileName,VersionType VerID, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID = NULL);

};

//////////////////////////////////////////////////////////////////////////////////////
//宏定义
#define BEGIN_FMPEVT_NEST(classobj) class  ArNTFMPEvt:public ArNTFMPEvtHandler{ 


#define DECL_FMP_DEFAULT_FUNC()			DECL_FMP_ASK_LOGIN() \
										DECL_FMP_ASK_LOGOFF()\
										DECL_FMP_ASK_ADDUSER()\
										DECL_FMP_ASK_DELUSER()\
										DECL_FMP_ASK_USERLIST()\
										DECL_FMP_ASK_PROJECTSET()\
										DECL_FMP_ASK_PRJ_BACKUPINFO()\
										DECL_FMP_ASK_EXTR_PROJECT()\
										DECL_FMP_ASK_BACK_PROJECT()\
										DECL_FMP_ASK_DEL_PROJECT()\
										DECL_FMP_ASK_CREATE_PROJECT()\
										DECL_FMP_ASK_FILE_LIST()\
										DECL_FMP_ASK_EXTR_FILE()\
										DECL_FMP_ASK_ERRORLIST()\
										DECL_FMP_ASK_SERVICE()\
										DECL_FMP_ASK_RUNINFO()\
										DECL_FMP_ASK_BACKUP_FILELST()
						

#define DECL_FMP_ASK_LOGIN()			public:	virtual		void OnAskLogin(LONGSTR& strFromChannel, StrPointer strName, StrPointer strPassWd, StrPointer strSenderID);
#define DECL_FMP_ASK_LOGOFF()			public:	virtual		void OnAskLogOff(LONGSTR& strFromChannel, StrPointer strName, StrPointer strSenderID);
#define DECL_FMP_ASK_ADDUSER()			public: virtual		void OnAskAddUser(LONGSTR& strFromChannel, StrPointer strOper, ArNTUserInfo& Info, StrPointer strSenderID);
#define DECL_FMP_ASK_DELUSER()			public: virtual		void OnAskDelUser(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strName, StrPointer strSenderID);
#define DECL_FMP_ASK_USERLIST()			public: virtual		void OnAskUserList(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strSenderID);
#define DECL_FMP_ASK_PROJECTSET()		public: virtual		void OnAskProjectSet(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strSenderID);	
#define DECL_FMP_ASK_PRJ_BACKUPINFO()	public:	virtual		void OnAskProjectBackupInfo(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strSenderID);
#define DECL_FMP_ASK_EXTR_PROJECT()		public: virtual		void OnAskExtractProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, VersionType VerID, StrPointer strDestPath, StrPointer strSenderID);
#define DECL_FMP_ASK_BACK_PROJECT()		public: virtual		void OnAskBackupProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strDesc, ArNT_BOOL bLargeChange,	ArNT_BOOL bRelease, StrPointer strSrcPath, StrPointer strSenderID);
#define DECL_FMP_ASK_CREATE_PROJECT()	public: virtual		void OnAskCreateProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strDesc, ArNTBackupType& Type, StrPointer strSrcPath, StrPointer strSenderID);
#define DECL_FMP_ASK_DEL_PROJECT()		public: virtual		void OnAskDelProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strSenderID);
#define DECL_FMP_ASK_FILE_LIST()		public: virtual		void OnAskFileList(LONGSTR& strFromChannel, StrPointer strOper,StrPointer strProjectName, StrPointer strFileName, StrPointer strSenderID);
#define DECL_FMP_ASK_EXTR_FILE()		public: virtual		void OnAskExtractFile(LONGSTR& strFromChannel, StrPointer strOper,StrPointer strProjectName, StrPointer strFileName, VersionType Version, StrPointer strDestPath, StrPointer strSenderID);
#define DECL_FMP_ASK_ERRORLIST()		public: virtual		void OnAskErrorList(LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_FMP_ASK_SERVICE()			public: virtual		void OnAskService(LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_FMP_ASK_RUNINFO()			public: virtual		void OnAskRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID);
#define DECL_FMP_ASK_BACKUP_FILELST()	public: virtual		void OnAskBackupFileList(LONGSTR& strFromChannel, StrPointer strOper,StrPointer strProjectName, VersionType Version, StrPointer strSenderID);


#define END_FMPEVT_NEST(classobj)		};\
										private:	ArNTFMPEvt				m_FMPEvtHandler;\
										private:    ArNTFMPMsgHandler		m_FMPMsgHandler;\
										public:     ArNTFMPMsgHandler*      GetFMPMsgHandler() {return &m_FMPMsgHandler;};


#define  REG_FMPEVT_NEST(bEnableTrace, ObjID)  m_FMPEvtHandler.RegParent(this);\
								if(ArNT_FALSE == m_FMPMsgHandler.RegEvtHandler(&m_FMPEvtHandler, bEnableTrace, ObjID))\
								{return ArNT_FALSE;}

#define	 UNREG_FMPEVT_NEST(ObjID) m_FMPMsgHandler.UnRegEvtHandler(ObjID);


//定义实现类
#define  IMPL_FMP_DEFAULT_FUNC(classobj)		IMPL_FMP_ASK_LOGIN(classobj)\
												IMPL_FMP_ASK_LOGOFF(classobj)\
												IMPL_FMP_ASK_ADDUSER(classobj)\
												IMPL_FMP_ASK_DELUSER(classobj)\
												IMPL_FMP_ASK_USERLIST(classobj)\
												IMPL_FMP_ASK_PROJECTSET(classobj)\
												IMPL_FMP_ASK_PRJ_BACKUPINFO(classobj)\
												IMPL_FMP_ASK_EXTR_PROJECT(classobj)\
												IMPL_FMP_ASK_BACK_PROJECT(classobj)\
												IMPL_FMP_ASK_CREATE_PROJECT(classobj)\
												IMPL_FMP_ASK_DEL_PROJECT(classobj)\
												IMPL_FMP_ASK_FILE_LIST(classobj)\
												IMPL_FMP_ASK_EXTR_FILE(classobj)\
												IMPL_FMP_ASK_ERRORLIST(classobj)\
												IMPL_FMP_ASK_SERVICE(classobj)\
												IMPL_FMP_ASK_RUNINFO(classobj)\
												IMPL_FMP_ASK_BACKUP_FILELST(classobj)
									
												

#define  IMPL_FMP_ASK_LOGIN(classobj)		void classobj::ArNTFMPEvt::OnAskLogin(LONGSTR& strFromChannel, StrPointer strName, StrPointer strPassWd, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskLogin(strFromChannel, strName,strPassWd, strSenderID);}}


#define  IMPL_FMP_ASK_LOGOFF(classobj)		void classobj::ArNTFMPEvt::OnAskLogOff(LONGSTR& strFromChannel, StrPointer strName,  StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskLogOff(strFromChannel, strName, strSenderID);}}


#define  IMPL_FMP_ASK_ADDUSER(classobj)		void classobj::ArNTFMPEvt::OnAskAddUser(LONGSTR& strFromChannel, StrPointer strOper, ArNTUserInfo& Info, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskAddUser(strFromChannel, strOper, Info, strSenderID);}}

#define  IMPL_FMP_ASK_DELUSER(classobj)		void classobj::ArNTFMPEvt::OnAskDelUser(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strName, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskDelUser(strFromChannel, strOper, strName, strSenderID);}}


#define  IMPL_FMP_ASK_USERLIST(classobj)		void classobj::ArNTFMPEvt::OnAskUserList(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskUserList(strFromChannel, strOper, strSenderID);}}

#define	IMPL_FMP_ASK_PROJECTSET(classobj)			void classobj::ArNTFMPEvt::OnAskProjectSet(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskProjectSet(strFromChannel, strOper, strSenderID);}}

#define	IMPL_FMP_ASK_PRJ_BACKUPINFO(classobj)		void classobj::ArNTFMPEvt::OnAskProjectBackupInfo(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskProjectBackupInfo(strFromChannel, strOper, strProjectName, strSenderID);}}

#define	IMPL_FMP_ASK_EXTR_PROJECT(classobj)		void classobj::ArNTFMPEvt::OnAskExtractProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, VersionType VerID, StrPointer strDestPath, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskExtractProject(strFromChannel, strOper, strProjectName, VerID, strDestPath, strSenderID);}}

#define	IMPL_FMP_ASK_BACK_PROJECT(classobj)		void classobj::ArNTFMPEvt::OnAskBackupProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strDesc, ArNT_BOOL bLargeChange, ArNT_BOOL bRelease, StrPointer strSrcPath, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskBackupProject(strFromChannel, strOper, strProjectName, strDesc, bLargeChange, bRelease, strSrcPath, strSenderID);}}

#define	IMPL_FMP_ASK_CREATE_PROJECT(classobj)		void classobj::ArNTFMPEvt::OnAskCreateProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strDesc, ArNTBackupType& Type , StrPointer strSrcPath, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskCreateProject(strFromChannel, strOper, strProjectName, strDesc, Type, strSrcPath, strSenderID);}}

#define	IMPL_FMP_ASK_DEL_PROJECT(classobj)		void classobj::ArNTFMPEvt::OnAskDelProject(LONGSTR& strFromChannel, StrPointer strOper, StrPointer strProjectName, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskDelProject(strFromChannel, strOper, strProjectName, strSenderID);}}

#define	IMPL_FMP_ASK_FILE_LIST(classobj)		void classobj::ArNTFMPEvt::OnAskFileList(LONGSTR& strFromChannel, StrPointer strOper,StrPointer strProjectName, StrPointer strFileName, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskFileList(strFromChannel, strOper, strProjectName, strFileName, strSenderID);}}

#define	IMPL_FMP_ASK_EXTR_FILE(classobj)		void classobj::ArNTFMPEvt::OnAskExtractFile(LONGSTR& strFromChannel, StrPointer strOper,StrPointer strProjectName, StrPointer strFileName, VersionType Version, StrPointer strDestPath, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskExtractFile(strFromChannel, strOper, strProjectName, strFileName, Version, strDestPath, strSenderID);}}

#define	IMPL_FMP_ASK_ERRORLIST(classobj)		void classobj::ArNTFMPEvt::OnAskErrorList(LONGSTR& strFromChannel, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskErrorList(strFromChannel, strSenderID);}}

#define	IMPL_FMP_ASK_SERVICE(classobj)		void classobj::ArNTFMPEvt::OnAskService(LONGSTR& strFromChannel, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskService(strFromChannel, strSenderID);}}

#define	IMPL_FMP_ASK_RUNINFO(classobj)		void classobj::ArNTFMPEvt::OnAskRunInfo(LONGSTR& strFromChannel, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskRunInfo(strFromChannel, strSenderID);}}

#define IMPL_FMP_ASK_BACKUP_FILELST(classobj) void classobj::ArNTFMPEvt::OnAskBackupFileList(LONGSTR& strFromChannel, StrPointer strOper,StrPointer strProjectName, VersionType Version, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAskBackupFileList(strFromChannel, strOper, strProjectName, Version,  strSenderID);}} 


//==============================================================================
///链接信息
#ifndef  NOAUTOLINK_FMPHELPER
#define  NOAUTOLINK_FMPHELPER
#pragma comment( lib, "ArNTFMPHelper.lib" )

#endif