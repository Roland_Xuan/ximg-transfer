#pragma once
#include "ImageCaptureProtocal.h"

//--------------------------------------
//消息响应类
class ArNTICPUIEvtHandler
{
public:
	virtual  void OnICPUINtfCameralRawImage(PRawCaptureImage pRawImage, StrPointer strSenderID);
	
public:
	ArNTICPUIEvtHandler();
	~ArNTICPUIEvtHandler();
public:
	void* m_pParent;
	void  RegParent(void* pParent);
private:
	void DefautNotImplFunc(StrPointer strFuncInfo);

};

//--------------------------------------
//消息注册类
class ArNTICPUIMsgHandler
{
public:
	ArNTICPUIMsgHandler(void);
	~ArNTICPUIMsgHandler(void);
public:
	ArNT_BOOL RegEvtHandler(ArNTICPUIEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace , LongInt iChannelSize, ShortInt iCameralIndex);
	void	  UnRegEvtHandler(ShortInt iCameralIndex);
private:
	ArNT_BOOL				m_bEnableTrace;	
	ArNTICPUIEvtHandler*	m_pEvtHandler;
private:
	DECL_ICP_IMGUI_MESSAGE(ArNTICPUIMsgHandler)
	//---------------------------------------------------
	//消息处理函数
	void	ProcessICPNtfCamRawImg(PICPNtfRawImage pPara, StrPointer strSenderID);

};

//////////////////////////////////////////////////////////////////////////
//宏定义


#define  BEGIN_ICPUIEVT_NEST(classobj) class  ArNTICPUIEvt:public ArNTICPUIEvtHandler{ 

#define  END_ICPUIEVT_NEST(classobj)	};\
									private:	ArNTICPUIEvt				m_ICPUIEvtHandler;\
									public:		ArNTICPUIMsgHandler			m_ICPUIMsgHandler;\
									public:     ArNTICPUIMsgHandler*		GetICPUIMsgHandler() {return &m_ICPUIMsgHandler;};

#define  REG_ICPUIEVT(bEnableTrace, iImgSize, iCamIndex)  m_ICPUIEvtHandler.RegParent(this);\
							LongInt iChannelSize = iImgSize +  sizeof(tagArNTCommand) + sizeof(ICPNtfRawImage);\
							if(ArNT_FALSE == m_ICPUIMsgHandler.RegEvtHandler(&m_ICPUIEvtHandler, bEnableTrace, iChannelSize, iCamIndex)){\
							SHORTSTR strInfo = {0};\
							CCommonFunc::SafeStringPrintf(strInfo, STR_LEN(strInfo), L"注册图像采集通道%d出错", iCamIndex);\
							WRITE_ERROR(strInfo);}

#define  UNREG_ICPUIEVT(iCamIndex)  m_ICPUIMsgHandler.UnRegEvtHandler(iCamIndex);


#define  DECL_ICPUI_ON_NTF_RAWIMG()	public: virtual  void OnICPUINtfCameralRawImage(PRawCaptureImage pRawImage, StrPointer strSenderID);

#define  IMPL_ICPUI_ON_NTF_RAWIMG(classobj)	void classobj::ArNTICPUIEvt::OnICPUINtfCameralRawImage(PRawCaptureImage pRawImage, StrPointer strSenderID) {\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPUINtfCameralRawImage(pRawImage, strSenderID);}}



//==============================================================================
///链接信息
#ifndef  NOAUTOLINK_ICPUIHELPER
#define  NOAUTOLINK_ICPUIHELPER
#pragma comment( lib, "ArNTICPHelper.lib" )

#endif