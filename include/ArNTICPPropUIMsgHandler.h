#pragma once
#include "ImageCaptureProtocal.h"

//--------------------------------------
//消息响应类
class ArNTICPPropUIEvtHandler
{
public:
	virtual  void OnICPPropUIAckCameralInfo(ImgCaptureCameralInfo& Info, StrPointer strSenderID);
	virtual  void OnICPPropUIAckExpTime(SHORTSTR& strCameralID, LongFloat dExpTime,  StrPointer strSenderID);
	virtual  void OnICPPropUIAckGain(SHORTSTR& strCameralID, LongFloat dGain,  StrPointer strSenderID);
	virtual  void OnICPPropUIAckTemperature(SHORTSTR& strCameralID, LongFloat dTemperature,  StrPointer strSenderID);
	virtual  void OnICPPropUIAckCmdList(PArNTCommandDesc pCmdDesc, ShortInt iCmdNum ,  StrPointer strSenderID);
	virtual  void OnICPPropUIAckCaptureSpeed(SHORTSTR& strCameralID,ArNT_BOOL bLine, LongFloat dSpeed,  StrPointer strSenderID);
	virtual  void OnICPPropUIAckFrameNum(SHORTSTR& strCameralID, LongLongInt iFrameNum, StrPointer strSenderID);
	virtual  void OnICPPropUIAckRunInfo(PArNTRunInfo pRunInfo, ShortInt iInfoNum, StrPointer strSenderID);
	virtual  void OnICPPropUIAckCaptureStatus(SHORTSTR& strCameralID, ArNT_BOOL bIsCapture, StrPointer strSenderID);
	virtual  void OnICPPropUIAckErrorList(PArNTErrorInfo pErrorInfo, ShortInt iErrorNum, StrPointer strSenderID);
	virtual  void OnICPPropUINtfErrorInfo(ArNT_ERROR_CODE  code, StrPointer strCmd, StrPointer strErrorInfo);
	virtual  void OnICPPropUIAckService(ArNServiceAppInfo& Info, StrPointer strSenderID);
public:
	ArNTICPPropUIEvtHandler();
	~ArNTICPPropUIEvtHandler();
public:
	void* m_pParent;
	void  RegParent(void* pParent);
private:
	void DefautNotImplFunc(StrPointer strFuncInfo);

};

class ArNTICPPropUIMsgHandler
{

public:
	ArNTICPPropUIMsgHandler(void);
	~ArNTICPPropUIMsgHandler(void);
public:
	ArNT_BOOL RegEvtHandler(ArNTICPPropUIEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace, ShortInt iCameralIndex);
	void	  UnRegEvtHandler(ShortInt iCameralIndex);

public:
	//----------------------------------------------------
	//询问相机信息
	void   AskCameralInfo(StrPointer strSenderID = NULL, ShortInt iCameralIndex = -1);
	//----------------------------------------------------
	//询问相机的曝光时间
	void   AskExptime(StrPointer strCameralID, StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);
	//----------------------------------------------------
	//询问相机的增益
	void   AskGain(StrPointer strCameralID, StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);
	//----------------------------------------------------
	//询问相机的温度
	void   AskTemperature(StrPointer strCameralID, StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);

	//----------------------------------------------------
	//询问采集协议命令列表
	void   AskCameralCmdList(StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);

	//----------------------------------------------------
	//询问服务错误列表
	void   AskErrorList(StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);

	//----------------------------------------------------
	//询问服务信息
	void   AskServiceInfo(StrPointer strSenderID = NULL, ShortInt iCameralIndex = -1);


	//----------------------------------------------------
	//询问相机的采集速率
	void   AskCameralCaptureSpeed(StrPointer strCameralID, StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);

	//----------------------------------------------------
	//请求更改相机的采集速率
	void   AskChangeCaptureSpeed(StrPointer strCameralID, LongFloat dSpeed, StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);

	//----------------------------------------------------
	//请求更改相机的曝光时间
	void   AskChangeExpTime(StrPointer strCameralID, LongFloat dExpTime, StrPointer strSenderID = NULL, ShortInt iCameralIndex = -1);

	//----------------------------------------------------
	//请求更改相机的曝光时间
	void   AskChangeGain(StrPointer strCameralID, LongFloat dGain, StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);


	//----------------------------------------------------
	//询问相机的采集帧数
	void   AskCameralFrameNum(StrPointer strCameralID, StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);

	//----------------------------------------------------
	//请求指定相机的开始采集
	void   AskCameralStartCapture(StrPointer strCameralID, StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);

	//----------------------------------------------------
	//请求指定相机的开始采集
	void   AskCameralStopCapture(StrPointer strCameralID, StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);


	//----------------------------------------------------
	//询问采集程序运行信息
	void   AskCaptureAppRunInfo(StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);
	//----------------------------------------------------
	//询问相机当前是否正在采集
	void  AskCaptureStatus(StrPointer strCameralID, StrPointer strSenderID = NULL , ShortInt iCameralIndex = -1);


private:
	ArNT_BOOL					m_bEnableTrace;	
	ArNTICPPropUIEvtHandler*	m_pEvtHandler;
private:
	DECL_ICP_PROPUI_MESSAGE(ArNTICPPropUIMsgHandler)
	//---------------------------------------------------
	//消息处理函数
	void	ProcessICPAckCameralInfo(PICPAckCameralInfo pPara, StrPointer strSenderID);
	void	ProcessICPAckExpTime(PICPAckExpTime pPara, StrPointer strSenderID);
	void	ProcessICPAckGain(PICPAckGain pPara, StrPointer strSenderID);
	void	ProcessICPAckTemperature(PICPAckTemperature pPara, StrPointer strSenderID);	
	void    ProcessICPAckCmdList(PICPAckCmdList pPara, StrPointer strSenderID);	
	void    ProcessICPAckCaptureSpeed(PICPAckCaptureSpeed pPara, StrPointer strSenderID);
	void    ProcessICPAckFrameNum(PICPAckFrameNum pPara,  StrPointer strSenderID);
	void    ProcessICPAckRunInfo(PICPAckRunInfo pPara, StrPointer strSenderID);
	void    ProcessICPAckCaptureStatus(PICPAckCaptureStatus pPara, StrPointer strSenderID);
	void    ProcessICPAckErrorList(PICPAckErrorList pPara, StrPointer strSenderID);
	void    ProcessICPNtfErrorInfo(PICPNtfError pPara, StrPointer strSenderID);
	void    ProcessICPAckService(PICPAckService pPara, StrPointer strSenderID);
};
//////////////////////////////////////////////////////////////////////////
//宏定义


#define  BEGIN_ICPPorpUIEVT_NEST(classobj) class  ArNTICPPropUIEvt:public ArNTICPPropUIEvtHandler { 

#define  END_ICPPropUIEVT_NEST(classobj)	};\
									private:	ArNTICPPropUIEvt				m_ICPPropUIEvtHandler;\
									public:		ArNTICPPropUIMsgHandler			m_ICPPropUIMsgHandler;\
									public:     ArNTICPPropUIMsgHandler*		GetICPPropUIMsgHandler() {return &m_ICPPropUIMsgHandler;};

#define  REG_ICPPropUIEVT_NEST(bEnableTrace, CameralID)  m_ICPPropUIEvtHandler.RegParent(this);\
								if(ArNT_FALSE == m_ICPPropUIMsgHandler.RegEvtHandler(&m_ICPPropUIEvtHandler, bEnableTrace, CameralID))\
								{return ArNT_FALSE;}
#define UNREG_ICPPropUIEVT_NEST(CameralID)	m_ICPPropUIMsgHandler.UnRegEvtHandler(CameralID);	

//声明消息处理函数
#define  DECL_ICPPropUI_ALL_FUNC()		DECL_ICPPropUI_ON_ACK_CAMERALINFO();\
										DECL_ICPPropUI_ON_ACK_EXPTIME();\
										DECL_ICPPropUI_ON_ACK_GAIN();\
										DECL_ICPPropUI_ON_ACK_TEMPERATURE();\
										DECL_ICPPropUI_ON_ACK_CMDLIST();\
										DECL_ICPPropUI_ON_ACK_CAPTURESPEED();\
										DECL_ICPPropUI_ON_ACK_FRAMENUM();\
										DECL_ICPPropUI_ON_ACK_RUNINFO();\
										DECL_IPCPropUI_ON_ACK_CAPTURESTATUS();\
										DECL_IPCPropUI_ON_ACK_ERRORLIST();\
										DECL_IPCPropUI_ON_NTF_ERRORINFO();\
										DECL_IPCPropUI_ON_ACK_SERVICE();

#define  DECL_ICPPropUI_ON_ACK_CAMERALINFO()	public: virtual  void OnICPPropUIAckCameralInfo(ImgCaptureCameralInfo& Info, StrPointer strSenderID);
#define  DECL_ICPPropUI_ON_ACK_EXPTIME()		public: virtual  void OnICPPropUIAckExpTime(SHORTSTR& strCameralID, LongFloat dExpTime,  StrPointer strSenderID);
#define  DECL_ICPPropUI_ON_ACK_GAIN()			public: virtual  void OnICPPropUIAckGain(SHORTSTR& strCameralID, LongFloat dGain,  StrPointer strSenderID);
#define  DECL_ICPPropUI_ON_ACK_TEMPERATURE()	public: virtual  void OnICPPropUIAckTemperature(SHORTSTR& strCameralID, LongFloat dTemperature,  StrPointer strSenderID);
#define  DECL_ICPPropUI_ON_ACK_CMDLIST()		public: virtual  void OnICPPropUIAckCmdList(PArNTCommandDesc pCmdDesc, ShortInt iCmdNum ,  StrPointer strSenderID);
#define  DECL_ICPPropUI_ON_ACK_CAPTURESPEED()   public: virtual  void OnICPPropUIAckCaptureSpeed(SHORTSTR& strCameralID,ArNT_BOOL bLine, LongFloat dSpeed,  StrPointer strSenderID);
#define  DECL_ICPPropUI_ON_ACK_FRAMENUM()		public: virtual  void OnICPPropUIAckFrameNum(SHORTSTR& strCameralID, LongLongInt iFrameNum, StrPointer strSenderID);
#define  DECL_ICPPropUI_ON_ACK_RUNINFO()		public: virtual  void OnICPPropUIAckRunInfo(PArNTRunInfo pRunInfo, ShortInt iInfoNum, StrPointer strSenderID);
#define  DECL_IPCPropUI_ON_ACK_CAPTURESTATUS()  public: virtual  void OnICPPropUIAckCaptureStatus(SHORTSTR& strCameralID, ArNT_BOOL bIsCapture, StrPointer strSenderID);
#define	 DECL_IPCPropUI_ON_ACK_ERRORLIST()		public:	virtual  void OnICPPropUIAckErrorList(PArNTErrorInfo pErrorInfo, ShortInt iErrorNum, StrPointer strSenderID);
#define	 DECL_IPCPropUI_ON_NTF_ERRORINFO()		public:	virtual  void OnICPPropUINtfErrorInfo(ArNT_ERROR_CODE  code, StrPointer strCmd, StrPointer strErrorInfo);
#define	 DECL_IPCPropUI_ON_ACK_SERVICE()		public:	virtual  void OnICPPropUIAckService(ArNServiceAppInfo& Info, StrPointer strSenderID);
//定义消息处理函数
#define  IMPL_ICPPropUI_ALL_FUNC(classobj)	IMPL_ICPPropUI_ON_ACK_CAMERALINFO(classobj);\
											IMPL_ICPPropUI_ON_ACK_EXPTIME(classobj);\
											IMPL_ICPPropUI_ON_ACK_GAIN(classobj);\
											IMPL_ICPPropUI_ON_ACK_TEMPERATURE(classobj);\
											IMPL_ICPPropUI_ON_ACK_CMDLIST(classobj);\
											IMPL_ICPPropUI_ON_ACK_CAPTURESPEED(classobj);\
											IMPL_ICPPropUI_ON_ACK_FRAMENUM(classobj);\
											IMPL_ICPPropUI_ON_ACK_RUNINFO(classobj);\
											IMPL_ICPPropUI_ON_ACK_CAPTURE_STATUS(classobj);\
											IMPL_ICPPropUI_ON_ACK_ERROR_LIST(classobj);\
											IMPL_ICPPropUI_ON_NTF_ERROR_INFO(classobj);\
											IMPL_ICPPropUI_ON_ACK_SERVICE(classobj);

										


#define  IMPL_ICPPropUI_ON_ACK_CAMERALINFO(classobj)	void classobj::ArNTICPPropUIEvt::OnICPPropUIAckCameralInfo(ImgCaptureCameralInfo& Info, StrPointer strSenderID) {\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckCameralInfo(Info, strSenderID);}}

#define  IMPL_ICPPropUI_ON_ACK_EXPTIME(classobj)		void classobj::ArNTICPPropUIEvt::OnICPPropUIAckExpTime(SHORTSTR& strCameralID, LongFloat dExpTime,  StrPointer strSenderID){\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckExpTime(strCameralID, dExpTime, strSenderID);}}

#define  IMPL_ICPPropUI_ON_ACK_GAIN(classobj)			void classobj::ArNTICPPropUIEvt::OnICPPropUIAckGain(SHORTSTR& strCameralID, LongFloat dGain,  StrPointer strSenderID){\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckGain(strCameralID, dGain, strSenderID);}}
#define  IMPL_ICPPropUI_ON_ACK_TEMPERATURE(classobj)	void classobj::ArNTICPPropUIEvt::OnICPPropUIAckTemperature(SHORTSTR& strCameralID, LongFloat dTemperature,  StrPointer strSenderID){\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckTemperature(strCameralID, dTemperature, strSenderID);}}

#define  IMPL_ICPPropUI_ON_ACK_CMDLIST(classobj)		void classobj::ArNTICPPropUIEvt::OnICPPropUIAckCmdList(PArNTCommandDesc pCmdDesc, ShortInt iCmdNum ,  StrPointer strSenderID){\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckCmdList(pCmdDesc, iCmdNum, strSenderID);}}

#define IMPL_ICPPropUI_ON_ACK_CAPTURESPEED(classobj)	void classobj::ArNTICPPropUIEvt::OnICPPropUIAckCaptureSpeed(SHORTSTR& strCameralID,ArNT_BOOL bLine, LongFloat dSpeed,  StrPointer strSenderID){\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckCaptureSpeed(strCameralID, bLine, dSpeed, strSenderID);}}

#define IMPL_ICPPropUI_ON_ACK_FRAMENUM(classobj)		void classobj::ArNTICPPropUIEvt::OnICPPropUIAckFrameNum(SHORTSTR& strCameralID, LongLongInt iFrameNum, StrPointer strSenderID){\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckFrameNum(strCameralID, iFrameNum, strSenderID);}}
#define IMPL_ICPPropUI_ON_ACK_RUNINFO(classobj)			void classobj::ArNTICPPropUIEvt::OnICPPropUIAckRunInfo(PArNTRunInfo pRunInfo, ShortInt iInfoNum, StrPointer strSenderID){\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckRunInfo(pRunInfo, iInfoNum, strSenderID);}}

#define IMPL_ICPPropUI_ON_ACK_CAPTURE_STATUS(classobj)	void classobj::ArNTICPPropUIEvt::OnICPPropUIAckCaptureStatus(SHORTSTR& strCameralID, ArNT_BOOL bIsCapture, StrPointer strSenderID){\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckCaptureStatus(strCameralID, bIsCapture, strSenderID);}}

#define IMPL_ICPPropUI_ON_ACK_ERROR_LIST(classobj)		void classobj::ArNTICPPropUIEvt::OnICPPropUIAckErrorList(PArNTErrorInfo pErrorInfo, ShortInt iErrorNum, StrPointer strSenderID){\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckErrorList(pErrorInfo, iErrorNum, strSenderID);}}

#define IMPL_ICPPropUI_ON_NTF_ERROR_INFO(classobj)		void classobj::ArNTICPPropUIEvt::OnICPPropUINtfErrorInfo(ArNT_ERROR_CODE  code, StrPointer strCmd, StrPointer strErrorInfo){\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUINtfErrorInfo(code, strCmd, strErrorInfo);}}

#define IMPL_ICPPropUI_ON_ACK_SERVICE(classobj)			void classobj::ArNTICPPropUIEvt::OnICPPropUIAckService(ArNServiceAppInfo& Info, StrPointer strSenderID) {\
																classobj* pParent = (classobj*)this->m_pParent;\
																if(pParent){\
																	pParent->OnICPPropUIAckService(Info, strSenderID);}}

//==============================================================================
///链接信息
#ifndef  NOAUTOLINK_ICPPropUIHELPER
#define  NOAUTOLINK_ICPPropUIHELPER
#pragma comment( lib, "ArNTICPHelper.lib" )

#endif