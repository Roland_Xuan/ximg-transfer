#pragma once
#include "LocalConfigProtocal.h"
//--------------------------------------
//消息响应类
class ArNTLCPUIEvtHandler
{
public:
	virtual  void OnLCPUIAckRunInfo(StrPointer strAppName,PArNTRunInfo pInfo, ShortInt iInfoNum, StrPointer strSenderID);
	virtual  void OnLCPUIAckCmdList(PArNTCommandDesc pInfo, ShortInt iInfoNum, StrPointer strSenderID);
	virtual  void OnLCPUIAckSysPara(StrPointer strName, StrPointer strValue, StrPointer strSenderID);
	virtual  void OnLCPUIAckService(StrPointer strAppName, StrPointer strVersion, StrPointer strSenderID);
	virtual  void OnLCPNtfErrorInfo(StrPointer strAppName,	ArNT_ERROR_CODE  code, 	StrPointer strCmd, StrPointer strErrorInfo);
	virtual  void OnLCPUIAckErrorList(StrPointer strAppName, PArNTErrorInfo pErrorInfo, ShortInt iErrorNum, StrPointer strSenderID);

public:
	ArNTLCPUIEvtHandler();
	~ArNTLCPUIEvtHandler();
public:
	void* m_pParent;
	void  RegParent(void* pParent);

private:
	void DefautNotImplFunc(StrPointer strFuncInfo);
};
//---------------------------------------
//消息处理类
class ArNTLCPUIMsgHandler
{
public:
	DECL_LCP_UI_MESSAGE(ArNTLCPUIMsgHandler)
public:
	ArNTLCPUIMsgHandler(void);
	~ArNTLCPUIMsgHandler(void);

public:

	ArNT_BOOL	RegEvtHandler(ArNTLCPUIEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace);
	void		UnRegEvtHandler(void);


private:
	ArNT_BOOL  m_bEnableTrace;	
	ArNTLCPUIEvtHandler* m_pEvtHandler;
	void  PostMessageToLCP(ShortInt iID, PMemPointer pPara, ShortInt iSize, StrPointer strSenderID);
	

	//---------------------------------------------------
	//消息处理函数
	void   ProcessAckRunInfo(PLCPAckRunInfo pPara, StrPointer strSenderID);
	void   ProcessAckCmdList(PLCPAckCmdList pPara, StrPointer strSenderID);
	void   ProcessAckService(PLCPAckService pPara, StrPointer strSenderID);
	void   ProcessNtfErrorInfo(PLCPNtfError pPara, StrPointer strSenderID);
	void   ProcessAckPara(PLCPAckSysPara pPara, StrPointer strSenderID);
	void   ProcessAckErrorList(PLCPAckErrorList pPara, StrPointer strSenderID);

	public:
	//--------------------------------------------------
	//发送消息请求
	//--------------------------------------------------
	//询问运行信息
	void  AskRunInfo(StrPointer strSenderID = NULL);
	//--------------------------------------------------
	//询问命令列表
	void  AskCommandList(StrPointer strSenderID = NULL);
	//--------------------------------------------------
	//询问参数
	void  AskSysPara(StrPointer strName, StrPointer strSenderID = NULL);

	//--------------------------------------------------
	//请求更改参数
	void  AskChangeSysPara(StrPointer strName, StrPointer strValue, StrPointer strSenderID = NULL);

	//--------------------------------------------------
	//请求添加参数
	void  AskAddSysPara(StrPointer strName, StrPointer strValue, StrPointer strSenderID = NULL);

	//--------------------------------------------------
	//请求删除参数
	void  AskDelSysPara(StrPointer strName, StrPointer strSenderID = NULL);
	//--------------------------------------------------
	//询问错误列表
	void  AskErrorList(StrPointer strSenderID = NULL);
	//---------------------------------------------------
	//询问支持LCP的服务
	void  AskService(StrPointer strSenderID = NULL);
};

//////////////////////////////////////////////////////////////////////////////////////
//宏定义

//将ArNTLCPUIEvtHandler作为内嵌类进行定义

#define  BEGIN_LCPUIEVT_NEST(classobj) class  ArNTLCPUIEvt:public ArNTLCPUIEvtHandler { 

#define  END_LCPUIEVT_NEST(classobj)	};\
									private:	ArNTLCPUIEvt			m_LCPUIEvtHandler;\
									public:	ArNTLCPUIMsgHandler			m_LCPUIMsgHandler;\
									public: ArNTLCPUIMsgHandler*		GetLCPUIMsgHandler() {return &m_LCPUIMsgHandler;}

#define DECL_LCPUI_DEFAULT_FUNC()			DECL_LCPUI_ACK_RUNINFO();\
											DECL_LCPUI_ACK_CMDLIST();\
											DECL_LCPUI_ACK_SYSPARA();\
											DECL_LCPUI_ACK_SERVICE();\
											DECL_LCPUI_NTF_ERRORINFO();\
											DECL_LCPUI_ACK_ERRORLIST();

#define DECL_LCPUI_ACK_RUNINFO()			public: virtual  void OnLCPUIAckRunInfo(StrPointer strAppName,PArNTRunInfo pInfo, ShortInt iInfoNum, StrPointer strSenderID);
#define DECL_LCPUI_ACK_CMDLIST()			public: virtual  void OnLCPUIAckCmdList(PArNTCommandDesc pInfo, ShortInt iInfoNum, StrPointer strSenderID);
#define DECL_LCPUI_ACK_SYSPARA()			public:	virtual  void OnLCPUIAckSysPara(StrPointer strName, StrPointer strValue, StrPointer strSenderID);
#define DECL_LCPUI_ACK_SERVICE()			public:	virtual  void OnLCPUIAckService(StrPointer strAppName, StrPointer strVersion, StrPointer strSenderID);
#define DECL_LCPUI_NTF_ERRORINFO()			public:	virtual  void OnLCPNtfErrorInfo(StrPointer strAppName,	ArNT_ERROR_CODE  code, 	StrPointer strCmd, StrPointer strErrorInfo);
#define DECL_LCPUI_ACK_ERRORLIST()			public:	virtual  void OnLCPUIAckErrorList(StrPointer strAppName, PArNTErrorInfo pErrorInfo, ShortInt iErrorNum, StrPointer strSenderID);
																		
#define  REG_LCPUIEVT_NEST(bEnableTrace)  m_LCPUIEvtHandler.RegParent(this);\
								if(ArNT_FALSE == m_LCPUIMsgHandler.RegEvtHandler(&m_LCPUIEvtHandler, bEnableTrace))\
								{return ArNT_FALSE;}

#define  UNREG_LCPUIEVT_NEST()		m_LCPUIMsgHandler.UnRegEvtHandler();

#define IMPL_LCPUI_DEFAULT_FUNC(classobj)		IMPL_LCPUI_ACK_RUNINFO(classobj);\
												IMPL_LCPUI_ACK_CMDLIST(classobj);\
												IMPL_LCPUI_ACK_SYSPARA(classobj);\
												IMPL_LCPUI_ACK_SERVICE(classobj);\
												IMPL_LCPUI_NTF_ERRORINFO(classobj);\
												IMPL_LCPUI_ACK_ERRORLIST(classobj);


#define  IMPL_LCPUI_ACK_RUNINFO(classobj) void classobj::ArNTLCPUIEvt::OnLCPUIAckRunInfo(StrPointer strAppName, PArNTRunInfo pInfo, ShortInt iInfoNum, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPUIAckRunInfo(strAppName, pInfo,iInfoNum, strSenderID);}}

#define  IMPL_LCPUI_ACK_CMDLIST(classobj) void classobj::ArNTLCPUIEvt::OnLCPUIAckCmdList(PArNTCommandDesc pInfo, ShortInt iInfoNum, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPUIAckCmdList(pInfo,iInfoNum, strSenderID);}}

#define  IMPL_LCPUI_ACK_SYSPARA(classobj) void classobj::ArNTLCPUIEvt::OnLCPUIAckSysPara(StrPointer strName, StrPointer strValue, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPUIAckSysPara(strName,strValue, strSenderID);}}

#define  IMPL_LCPUI_ACK_SERVICE(classobj) void classobj::ArNTLCPUIEvt::OnLCPUIAckService(StrPointer strAppName, StrPointer strVersion, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPUIAckService(strAppName,strVersion, strSenderID);}}

#define  IMPL_LCPUI_NTF_ERRORINFO(classobj) void classobj::ArNTLCPUIEvt::OnLCPNtfErrorInfo(StrPointer strAppName, ArNT_ERROR_CODE  code, StrPointer strCmd, StrPointer strErrorInfo) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPNtfErrorInfo(strAppName,code, strCmd, strErrorInfo);}}

#define  IMPL_LCPUI_ACK_ERRORLIST(classobj) void classobj::ArNTLCPUIEvt::OnLCPUIAckErrorList(StrPointer strAppName, PArNTErrorInfo pErrorInfo, ShortInt iErrorNum, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnLCPUIAckErrorList(strAppName,pErrorInfo, iErrorNum, strSenderID);}}


//==============================================================================
///链接信息
#ifndef  NOAUTOLINK_LPCHELPER
#define  NOAUTOLINK_LPCHELPER
#pragma comment( lib, "ArNTLCPHelper.lib" )

#endif



