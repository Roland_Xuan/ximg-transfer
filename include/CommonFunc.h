#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：CommonFunc.h
// 作  者 ：杨朝霖
// 用  途 ：定义了一些常用的操作函数
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖      2009-03-23	创建模块
//1.1     杨朝霖      2010-08-25	添加了进程权限修改函数
//1.2     杨朝霖      2010-08-29	将其它函数中字符串格式化得函数改为了SafeStringPrintf
//1.3     杨朝霖      2011-1-19		TimeOffset时间类型改为ULONGLONG
//1.4     杨朝霖      2011-9-26		添加了CSystemInfo类
//1.5     杨朝霖      2011-11-1		添加了CreateNullLimetMutex
//1.6     杨朝霖      2011-11-29	添加了IsFirstApp
//1.7	  杨朝霖	  2011-12-7		添加了GetVersion函数
//1.8     杨朝霖      2011-12-19	添加了GetMoudleFileSize函数
//1.9     杨朝霖      2012-2-14		修改了SearchFile函数可能存在的内存访问问题
//2.0     杨朝霖      2012-2-15		添加了WriteStringToFile函数
//2.1     杨朝霖      2012-7-10		添加了StrUpper函数
//2.2     杨朝霖      2012-10-15    添加了ChangeParaValue函数
//2.3     杨朝霖      2012-10-17    添加了StrInRunPara函数
//2.4     杨朝霖	  2012-10-24	添加了FindStrInRunPara函数
//2.5     杨朝霖      2012-10-25    修改了空字符串会导致的,GetNameExt等从字串中取得部分字串的操作发生异常。
//==============================================================================
///</ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
///</header_info>

///<datastruct_info>
//==============================================================================
// 基本数据类型定义

//定义参数相关数据类型
typedef StrChar  ParaTitle[64];
typedef StrChar  ParaValue[256];
typedef struct tagParaInfo
{
	ParaTitle  Title; //参数名称最多64个字节
	ParaValue  Info;//参数内容
}ParaInfo, *PParaInfo;

typedef struct tagParaInfoSet
{
	enum {MAX_ITEM_NUM = 256}; //最多允许有MAX_ITEM_NUM个参数
	ParaInfo  Items[MAX_ITEM_NUM];
	LongInt    iItemNum;
}ParaInfoSet, *PParaInfoSet;

//定义文件搜索相关数据类型
typedef StrChar FileItemName[256];
typedef struct tagFileNameSet
{
	enum {MAX_ITEM_NUM = 500};
	FileItemName  Items[MAX_ITEM_NUM];
	LongInt  iItemNum;
}FileNameSet, *PFileNameSet;
//==============================================================================
///</datastruct_info>

#ifdef  WINX64 
#define COMMONFUNC_X64
#endif

///<class_info>
//==============================================================================
//名称:CCommonFunc
//功能:通过类静态函数的方式定义常用操作函数
class CCommonFunc
{
public:
	///<func_info>
	//描述：获取当前进程的应用程序完整路径和文件名称
	//参数：StrPointer strAppPath 读取到的文件名
	//ULongInt strLen 用于保存路径和文件名的缓冲区长度
	//返回值：无

	static void GetAppPathName(StrPointer strAppPath , ULongInt strLen);
	///</func_info>
	
	///<func_info>
	//描述：获取当前进程的应用程序所在路径
	//参数：StrPointer strAppPath 读取到的路径
	//ULongInt strLen 用于保存路径名的缓冲区长度
	//返回值：无
	static void GetAppPath(StrPointer strAppPath , ULongInt strLen);
	///</func_info>
	
	///<func_info>
	//描述：获取指定模块的所在路径
	//参数：HMODULE hModuel 模块的句柄，如果为NULL，表示获取当前应用程序所在的路径
	// StrPointer strPath 保存路径的缓冲区
	//ULongInt strLen 用于保存路径名的缓冲区长度
	//返回值：无
	static void GetModulePath(HMODULE hModuel,StrPointer strPath , ULongInt strLen);
	///</func_info>
	
	///<func_info>
	//描述：获取当前进程的应用程序名称，不包括路径
	//参数：StrPointer strAppName 应用程序的名称
	//ULongInt strLen 用于保存文件名的缓冲区长度
	//返回值：无
	static void GetAppName(StrPointer strAppName, ULongInt iStrLen);
	///</func_info>
	
	///<func_info>
	//描述：从完整路径文件名中获取文件名
	//参数：StrPointer strPathName 完整的路径文件名
	// StrPointer strFileName 保存文件名的缓冲
	//ULongInt iStrFileNameLen 用于保存文件名的缓冲区长度
	//返回值：无
	static void GetFileName(StrPointer strPathName, StrPointer strFileName, ULongInt iStrFileNameLen);
	///</func_info>
	
	///<func_info>
	//描述：获取文件名的后缀
	//参数：StrPointer strPathName 完整的路径文件名
	// StrPointer strExt 保存后缀的缓冲区
	//ULongInt iStrExtLen 用于保存后缀的缓冲区长度
	//返回值：无
	static void GetFileExt(StrPointer strPathName, StrPointer strExt, ULongInt iStrExtLen);
	///</func_info>

	///<func_info>
	//描述：获取不包括后缀的文件名
	//参数：StrPointer strPathName 完整的路径文件名
	// StrPointer strName 存放文件名的缓冲区
	//ULongInt iNameLen 存放文件名的缓冲区长度
	//返回值：无
	static void GetFileNoExt(StrPointer strPathName, StrPointer strName, ULongInt iNameLen);
	///</func_info>

	
	///<func_info>
	//描述：从完整路径文件名中获取路径
	//参数：StrPointer strPathName 完整的路径文件名
	// StrPointer strPath 保存路径的缓冲
	//ULongInt iStrPathLen 用于保存路径的缓冲区长度
	static void GetFilePath(StrPointer strPathName, StrPointer strPath, ULongInt iStrPathLen);//从文件名获取路径名
	///</func_info>

	///<func_info>
	//描述：创建目录，支持多级目录的创建
	//参数：StrPointer strDir 要创建的目录
	static void CreateDir(StrPointer strDir);
	///</func_info>

	///<func_info>
	//描述：判断目录是否存在
	//参数：StrPointer strDir 要判断的目录
	//返回值:ArNT_TRUE表示存在，ArNT_FALSE表示不存在
	static ArNT_BOOL DirExisit(StrPointer strDir);
	///</func_info>

	///<func_info>
	//描述：判断文件是否存在
	//参数：wchar_t* strDir 要判断的目录
	//返回值:true表示存在，false表示不存在
	static ArNT_BOOL FileExist(StrPointer strFile);
	///</func_info>

	///<func_info>
	//描述：目录中的文件数（仅限于1级）
	//参数：StrPointer strDir 指定的目录名
	//      StrPointer strExt 指定的后缀，格式如"exe",不是".exe"
	//返回值:文件的数目
	static LongInt  FileNumInDir(StrPointer strDir,  StrPointer strExt);
	///</func_info>
	
	///<func_info>
	//描述：从文件中获取参数信息，参数格式：参数名=参数值，每个参数独占一行，注释格式为;格式内容
	//参数：StrPointer strParaFile 要读取的参数文件名
	//      ParaInfoSet& Set 读取到的参数集合
	//返回值:ArNT_TRUE 表示读取成功，ArNT_FALSE表示读取失败
	static ArNT_BOOL  GetParaInfo(StrPointer strParaFile, ParaInfoSet& Set);
	///</func_info>
	
	///<func_info>
	//描述：更新参数文件
	//参数：StrPointer strParaFile 要更新的参数文件名
	//      ParaInfoSet& Set 需要更新的参数集合
	//返回值:ArNT_TRUE 表示更新成功，ArNT_FALSE表示更新失败
	static ArNT_BOOL  UpdateParaInfo(StrPointer strParaFile, ParaInfoSet& Set);
	///</func_info>
	
	///<func_info>
	//描述：从参数集合中获取指定的参数
	//参数：ParaInfoSet& Set  参数集合
	//      StrPointer strTitle 参数的名称
	//		ParaValue& Value  
	//返回值:ArNT_TRUE 表示读取成功，ArNT_FALSE 表示读取失败
	static ArNT_BOOL  GetParaValue(ParaInfoSet& Set,StrPointer strTitle, ParaValue& Value); 
	///</func_info>

	///<func_info>
	//描述：更改参数集合中指定的参数
	//参数：ParaInfoSet& Set  参数集合
	//      StrPointer strTitle 参数的名称
	//		ParaValue& Value  
	//返回值:ArNT_TRUE 表示更改成功，ArNT_FALSE 表示更改失败
	static ArNT_BOOL  ChangeParaValue(ParaInfoSet& Set, StrPointer strTitle, StrPointer Value); 
	///</func_info>


	///<func_info>
	//描述：进行文件复制
	//参数：StrPointer strSourceFile 指定的源文件
	//     StrPointer strDestFile 指定的目标文件
	//返回值:ArNT_TRUE 表示成功，ArNT_FALSE表示失败
	static ArNT_BOOL  CopyModuleFile(StrPointer strSourceFile, StrPointer strDestFile); 
	///</func_info>

	///<func_info>
	//描述：获取桌面尺寸
	//参数：LongInt& iWidth   桌面的宽度
	//      LongInt&iHeight   桌面的高度
	//返回值：无
	static void  GetDesktopSize(LongInt& iWidth, LongInt&iHeight); 
	///</func_info>

	///<func_info>
	//描述：搜索指定目录下的文件(仅限一级), 但要求搜索到的文件数量小于tagFileNameSet::MAX_ITEM_NUM
	//参数：StrPointer strDir 指定的目录
	//      StrPointer strExt 指定的文件名后缀
	//      FileNameSet& Set 搜索到文件名集合
	//返回值:ArNT_TRUE 表示成功，ArNT_FALSE表示失败
	static ArNT_BOOL SearchFile(StrPointer strDir, StrPointer strExt,FileNameSet& Set); 
	///</func_info>

	///<func_info>
	//描述：搜索指定目录下的文件(仅限一级), 如果pFileItems = NULL，则返回搜索到的图像数量, 调用程序需要分配pFileItems
	//参数：StrPointer strDir 指定的目录
	//      StrPointer strExt 指定的文件名后缀,不需要加上.。
	//      FileItemName* pFileItems 搜索到文件名数组
	//		LongInt& iFindItemNum 搜索到的文件名数量
	//返回值:ArNT_TRUE 表示成功，ArNT_FALSE表示失败
    static ArNT_BOOL SearchFile(StrPointer strDir, StrPointer strExt,FileItemName* pFileItems,  LongInt& iFindItemNum);
	///</func_info>

	///<func_info>
	//描述：搜索指定目录下的子目录(仅限一级), 但要求搜索到的目录数量小于tagFileNameSet::MAX_ITEM_NUM
	//参数：StrPointer strDir 指定的目录
	//      FileNameSet& Set搜索到的目录数组
	//返回值:ArNT_TRUE 表示成功，ArNT_FALSE表示失败
	static ArNT_BOOL SearchDir(StrPointer strDir, FileNameSet& Set); 
	///</func_info>

	///<func_info>
	//描述：搜索指定目录下的子目录(仅限一级), 搜索到的目录数量没有限制
	//参数：StrPointer strDir 指定的目录
	//      FileNameSet& Set搜索到的目录数组
	//返回值:ArNT_TRUE 表示成功，ArNT_FALSE表示失败
	static ArNT_BOOL SearchDir(StrPointer strDir, FileItemName* pFileItems,  LongInt& iFindItemNum); 
	///</func_info>

	
	///<func_info>
	//描述：多字节到宽字节的转换
	//参数：ANSIChar* strMutiByte 需要转换的多字节缓冲
	//      WideChar* strWideChar 转换后的宽字节缓冲
	//      LongInt strWideCharLen 宽字节缓冲区的最大尺寸
	//返回值：无
	static void  AnsiToUnicode(ANSIChar* strMutiByte, StrPointer strWideChar, ULongInt strWideCharLen);
		///</func_info>

		///<func_info>
		//描述：宽字节到多字节的转换
		//参数：StrPointer strWideChar 需要转换的宽字节缓冲
		//      ANSIChar* strMutiByte 转换后的多字节缓冲
		//      ULongInt strMutiByteLen 多字节缓冲区的最大尺寸
		//返回值：无
		static void  UnicodeToAnsi(StrPointer strWideChar, ANSIChar* strMutiByte, ULongInt strMutiByteLen);
	///</func_info>

	///<func_info>
	//描述：多字节到宽字节的转换
	//参数：ANSIChar* strMutiByte 需要转换的多字节缓冲
	//      WideChar* strWideChar 转换后的宽字节缓冲
	//      ULongInt strWideCharLen 宽字节缓冲区的最大尺寸
	static void  UTF8ToUnicode(ANSIChar* strMutiByte, StrPointer strWideChar, ULongInt strWideCharLen);
	///</func_info>
	
	///<func_info>
	//描述：宽字节到多字节的转换，但采用UTF8的格式
	//参数：WideChar* strWideChar 需要转换的宽字节缓冲
	//      ANSIChar* strMutiByte 转换后的多字节缓冲
	//      ULongInt strMutiByteLen 多字节缓冲区的最大尺寸
	//返回值：无
	static void  UnicodeToUTF8(StrPointer strWideChar, ANSIChar* strMutiByte, ULongInt strMutiByteLen);
	///</func_info>


	///<func_info>
	//描述：安全的宽字符串格式化函数
	//参数：StrPointer strDest 需要格式化的宽字节缓冲
	//      ULongInt iStrLen  宽字节缓冲的最大尺寸(以字符的个数为单位，不是字节大小,可用STR_LEN宏获取)
	//      StrPointer strFormat 格式化字符串
	//返回值：无
	static void  SafeStringPrintf(StrPointer strDest, ULongInt iStrLen, StrPointer strFormat, ...);
	///</func_info>

	///<func_info>
	//描述：安全的宽字符串拷贝函数
	//参数：StrPointer strDest 需要拷贝的目的宽字符串
	//      ULongInt iStrLen  宽字节缓冲的最大尺寸(以字符的个数为单位，不是字节大小,可用STR_LEN宏获取)
	//      StrPointer strSource 需要拷贝的源字符串
	//返回值：无
	static void  SafeStringCpy(StrPointer strDest, ULongInt iStrLen, StrPointer strSource);
	///</func_info>

	
	///<func_info>
	//描述：将所有字符转化为大写方式
	//参数：StrPointer strDest 需要转换的字符串
	//		ULongInt iStrLen 字符串的长度(以字符的个数为单位，不是字节大小,可用STR_LEN宏获取)
	//返回值：无
	static void  StrUpper(StrPointer strDest, ULongInt iStrLen);
	///</func_info>


	///<func_info>
	//描述：获取当前时间，并格式化为字符串
	//参数：StrPointer  strNowTime 当前时间的字符串
	//      ULongInt iDestLen  时间字符串的最大尺寸(以字符的个数为单位，不是字节大小,可用STR_LEN宏获取)
	//      ArNT_BOOL bHigh 是否支持高精度的时间信息，ArNT_TRUE表示支持,ArNT_FALSE表示不支持
	//返回值:无
	static void  GetNowTime(StrPointer  strNowTime, ULongInt iDestLen, ArNT_BOOL bHigh = ArNT_FALSE);
	///</func_info>

	///<func_info>
	//描述：获取当前时间
	//参数：ArNTSysTime&  NowTime 存放当前获取到的时间
	//返回值:无
	static void  GetNowTime(ArNTSysTime&  NowTime);
	///</func_info>

	///<func_info>
	//描述：将字符串转化为时间
	//参数：StrPointer  strNowTime 需要转换的时间字符串，格式为"年-月-日 小时:分钟:秒:毫秒"
	//LongInt iStrLen  字符串的长度，防止缓冲区溢出
	//ArNTSysTime&  NowTime 存放当前获取到的时间
	//返回值:ArNT_BOOL ArNT_TRUE表示成功转换
	static ArNT_BOOL  StringToTime(StrPointer  strNowTime,  ArNTSysTime&  NowTime);
	///</func_info>

	///<func_info>
	//描述：将字符串转化为时间
	//参数：StrPointer  strNowTime 需要转换的时间字符串，格式为"年-月-日 小时:分钟:秒:毫秒"
	//ArNTSysTime&  NowTime 存放当前获取到的时间
	//返回值:ArNT_BOOL ArNT_TRUE表示成功转换
	static void  TimeToString(ArNTSysTime&  NowTime, TINYSTR&  strNowTime, ArNT_BOOL bHigh = ArNT_FALSE);
	///</func_info>

	///<func_info>
	//描述：用指定的字符分割字符串
	//参数：StrPointer  strSrc 需要进行分割的字符串
	//LONGSTR& strFirstHalf  分割得到的前半字符串
	//LONGSTR& strSecondHalf 分割得到的后半字符串
	//StrChar strDiv  分割字符
	//返回值:ArNT_BOOL ArNT_TRUE表示成功转换
	static ArNT_BOOL  SplitString(StrPointer  strSrc, LONGSTR& strFirstHalf, LONGSTR& strSecondHalf, StrChar strDiv);
	///</func_info>
	
	///<func_info>
	//描述：格式化字符，或控制台输出支持中文显示
	//参数：无
	//返回值：无
	static void  EnableChineseLocal(void); 
	///</func_info>

	///<func_info>
	//描述：创建可以被所有进程访问的Event
	//参数：ArNT_BOOL bManual  事件是否是手动设置事件，ArNT_TRUE表示手动设置事件
	// ArNT_BOOL bSigned  表示创建时是否处于发信号状态, ArNT_TRUE表示处于发信号状态
	//StrPointer strName 表示事件的名称
	//返回值：无
	static HANDLE CreateNullLimetEvent(ArNT_BOOL bManual, ArNT_BOOL bSigned, StrPointer strName);
	///</func_info>



	///<func_info>
	//描述：创建可以被所有进程访问的Mutex
	//参数：StrPointer strName 表示Mutex的名称
	//ArNT_BOOL InitiaOwner 表示初始时创建线程是否拥有该Mutex, ArNT_TRUE表示拥有
	//返回值：无
	static HANDLE CreateNullLimetMutex(StrPointer strName, ArNT_BOOL bInitiaOwner);
	///</func_info>


	///<func_info>
	//描述：获取文件的相关时间信息
	//参数:StrPointer strFileName 指定的完整路径文件名
	//	SYSTEMTIME& tmCreate 文件的创建时间
	//   SYSTEMTIME& tmAccess 文件的访问时间
	//	SYSTEMTIME&  tmWrite 文件的修改时间
	//返回值：ArNT_TRUE表示成功获取，ArNT_FALSE表示获取失败
	static ArNT_BOOL   GetFileTimes(StrPointer strFileName, SYSTEMTIME& tmCreate, SYSTEMTIME& tmAccess, SYSTEMTIME&  tmWrite);
	///</func_info>

	///<func_info>
	//描述：获取文件的尺寸大小
	//参数:StrPointer strFileName 要获取尺寸的文件名
	//LARGE_INTEGER & iSize 64位的整数值表示文件的大小
	//返回值：ArNT_TRUE表示成功获取，ArNT_FALSE表示获取失败
	static ArNT_BOOL	GetMoudleFileSize(StrPointer strFileName,  LARGE_INTEGER & iSize);
	///</func_info>


	///<func_info>
	//描述：更改进程的权限
	//参数:PCTSTR pszPrivName 需要修改的权限名,目前可用的权限名为
	/*#define SE_CREATE_TOKEN_NAME            TEXT("SeCreateTokenPrivilege")
	#define SE_ASSIGNPRIMARYTOKEN_NAME        TEXT("SeAssignPrimaryTokenPrivilege")
	#define SE_LOCK_MEMORY_NAME               TEXT("SeLockMemoryPrivilege")
	#define SE_INCREASE_QUOTA_NAME            TEXT("SeIncreaseQuotaPrivilege")
	#define SE_UNSOLICITED_INPUT_NAME         TEXT("SeUnsolicitedInputPrivilege")
	#define SE_MACHINE_ACCOUNT_NAME           TEXT("SeMachineAccountPrivilege")
	#define SE_TCB_NAME                       TEXT("SeTcbPrivilege")
	#define SE_SECURITY_NAME                  TEXT("SeSecurityPrivilege")
	#define SE_TAKE_OWNERSHIP_NAME            TEXT("SeTakeOwnershipPrivilege")
	#define SE_LOAD_DRIVER_NAME               TEXT("SeLoadDriverPrivilege")
	#define SE_SYSTEM_PROFILE_NAME            TEXT("SeSystemProfilePrivilege")
	#define SE_SYSTEMTIME_NAME                TEXT("SeSystemtimePrivilege")
	#define SE_PROF_SINGLE_PROCESS_NAME       TEXT("SeProfileSingleProcessPrivilege")
	#define SE_INC_BASE_PRIORITY_NAME         TEXT("SeIncreaseBasePriorityPrivilege")
	#define SE_CREATE_PAGEFILE_NAME           TEXT("SeCreatePagefilePrivilege")
	#define SE_CREATE_PERMANENT_NAME          TEXT("SeCreatePermanentPrivilege")
	#define SE_BACKUP_NAME                    TEXT("SeBackupPrivilege")
	#define SE_RESTORE_NAME                   TEXT("SeRestorePrivilege")
	#define SE_SHUTDOWN_NAME                  TEXT("SeShutdownPrivilege")
	#define SE_DEBUG_NAME                     TEXT("SeDebugPrivilege")
	#define SE_AUDIT_NAME                     TEXT("SeAuditPrivilege")
	#define SE_SYSTEM_ENVIRONMENT_NAME        TEXT("SeSystemEnvironmentPrivilege")
	#define SE_CHANGE_NOTIFY_NAME             TEXT("SeChangeNotifyPrivilege")
	#define SE_REMOTE_SHUTDOWN_NAME           TEXT("SeRemoteShutdownPrivilege")
	#define SE_UNDOCK_NAME                    TEXT("SeUndockPrivilege")
	#define SE_SYNC_AGENT_NAME                TEXT("SeSyncAgentPrivilege")
	#define SE_ENABLE_DELEGATION_NAME         TEXT("SeEnableDelegationPrivilege")
	#define SE_MANAGE_VOLUME_NAME             TEXT("SeManageVolumePrivilege")
	#define SE_IMPERSONATE_NAME               TEXT("SeImpersonatePrivilege")
	#define SE_CREATE_GLOBAL_NAME             TEXT("SeCreateGlobalPrivilege")
	#define SE_TRUSTED_CREDMAN_ACCESS_NAME    TEXT("SeTrustedCredManAccessPrivilege")
	#define SE_RELABEL_NAME                   TEXT("SeRelabelPrivilege")
	#define SE_INC_WORKING_SET_NAME           TEXT("SeIncreaseWorkingSetPrivilege")
	#define SE_TIME_ZONE_NAME                 TEXT("SeTimeZonePrivilege")
	#define SE_CREATE_SYMBOLIC_LINK_NAME      TEXT("SeCreateSymbolicLinkPrivilege")
	#define SE_INCREASE_QUOTA_NAME            TEXT("SeIncreaseQuotaPrivilege")
	*/
	//	ArNT_BOOL bEnable 设置是否支持, ArNT_TRUE表示支持
	//返回值： ArNT_TRUE 表示设置成功, ArNT_FALSE表示设置失败
	static ArNT_BOOL EnablePrivilege(PCTSTR pszPrivName, ArNT_BOOL bEnable = ArNT_TRUE);

	///</func_info>

	///<func_info>
	//描述：计算时间间隔(以ms为单位)
	//参数:
	//	SYSTEMTIME tm1 时间1
	//   SYSTEMTIME tm2 时间2
	//返回值：返回的时间间隔
	static LongInt TimeOffset(SYSTEMTIME tm1, SYSTEMTIME tm2);
	///</func_info>

	///<func_info>
	//描述：运行指定位置的程序
	//参数:
	//	StrPointer strAppName 需要运行的程序，可以带有命令行参数
	// ArNT_BOOL bNeedHandle 是否需要返回程序句柄，ArNT_TRUE表示需要返回，如果为ArNT_FALSE, 程序始终返回NULL;
	//返回值：运行的进程句柄
	static HANDLE  RunApp(StrPointer strAppName, ArNT_BOOL bNeedHandle = ArNT_FALSE);
	///</func_info>

	///<func_info>
	//描述：输出数据到指定文本中
	//参数:
	//	StrPointer strFileName 要写入文件的文件名
	//PLongFloat pData 需要写入的浮点数所在内存地址
	// LongInt iDatalen 写入浮点数的数量
	// LongInt iColNum 文件中数据显示时的列数
	//返回值： ArNT_TRUE 表示写入成功, ArNT_FALSE表示写入失败
	static ArNT_BOOL  WriteFloatToText(StrPointer strFileName, PLongFloat pData, LongInt iDatalen, LongInt iColNum = 20);
	///</func_info>

	///<func_info>
	//描述：输出数据到指定文本中
	//参数:
	//	StrPointer strFileName 要写入文件的文件名
	//PLongInt pData  需要写入的整数所在内存地址
	//LongInt iDatalen 写入整数的数量
	// LongInt iColNum 文件中数据显示时的列数
	//返回值： ArNT_TRUE 表示写入成功, ArNT_FALSE表示写入失败
	static ArNT_BOOL  WriteIntToText(StrPointer strFileName, PLongInt pData, LongInt iDatalen, LongInt iColNum = 20);
	///</func_info>
	
	///<func_info>
	//描述：输出数据到指定文本中
	//参数:
	//	StrPointer strFileName 要写入文件的文件名
	//PBufferByte pData  需要写入的字节数据所在内存地址
	//LongInt iDatalen 写入字节数的数量
	// LongInt iColNum 文件中数据显示时的列数
	//返回值： ArNT_TRUE 表示写入成功, ArNT_FALSE表示写入失败
	static ArNT_BOOL  WriteBufferToText(StrPointer strFileName, PBufferByte pData, LongInt iDatalen, LongInt iColNum = 20);
	///</func_info>

	///<func_info>
	//描述：输出文本到指定文件中
	//参数:
	//	StrPointer strFileName 要写入文件的文件名
	//StrPointer pStr  需要写入的字符串
	//返回值： ArNT_TRUE 表示写入成功, ArNT_FALSE表示写入失败
	static ArNT_BOOL  WriteStringToFile(StrPointer strFileName, StrPointer pStr);
	///</func_info>

	///<func_info>
	//描述：从指定文件中读取文件，但要求文件中包括的字符内容不能超过256个
	//参数:
	//	StrPointer strFileName 需要读取文件的文件名
	//LONGSTR* pStr 存放读取的字符串信息
	//LongInt& iItemNum 读取到的字符串长度
	//返回值： ArNT_TRUE 表示写入成功, ArNT_FALSE表示写入失败
	static ArNT_BOOL  ReadStringFromFile(StrPointer strFileName, LONGSTR* pStr, LongInt& iItemNum);
	///</func_info>


	///<func_info>
	//描述：判断运行的是否是该程序的第一个进程
	//参数:StrPointer* strAppName 指定的应用程序名
	//返回值： ArNT_TRUE 表示是, ArNT_FALSE表示否
	static ArNT_BOOL  IsFirstApp(StrPointer strAppName);
	///</func_info>
	
	///<func_info>
	//描述：获取资源文件中的版本信息
	//参数:TINYSTR& strFileVer 资源中包括的文件版本信息
	//		TINYSTR& strProductVer 资源中包括的产品版本信息
	//		SHORTSTR& strComment  产品的注释信息
	//		SHORTSTR& strCompanyName产品的开发公司信息
	//		SHORTSTR& strProductName 产品名称
	//		SHORTSTR& strOriginalFilename 产品原始名信息
	//      HMODULE hModuel 包括版本信息的模块句柄
	//返回值： ArNT_TRUE 表示获取成功, ArNT_FALSE表示获取失败
	static ArNT_BOOL	GetVersionInfo(TINYSTR& strFileVer, 
										TINYSTR& strProductVer,
										SHORTSTR& strComment,
										SHORTSTR& strCompanyName,
										SHORTSTR& strProductName,
										SHORTSTR& strOriginalFilename,
										HMODULE hModuel = NULL);
	///</func_info>

	///<func_info>
	//描述：获取资源文件中的版本描述信息
	//参数:LONGSTR& strInfo 版本描述信息
	//	   HMODULE hModuel 包括版本信息的模块句柄
	//返回值： ArNT_TRUE 表示获取成功, ArNT_FALSE表示获取失败
	static ArNT_BOOL	GetVersionInfo(LONGSTR& strInfo, HMODULE hModuel = NULL);
	///</func_info>

	///<func_info>
	//描述：判断运行参数中是否包括指定字符串
	//参数 
	// StrPointer strInfo 需要查找的字符串
	//返回值： ArNT_TRUE 表示存在, ArNT_FALSE表示不存在
	static ArNT_BOOL	StrInRunPara(StrPointer strInfo);
	///</func_info>

	///<func_info>
	//描述：指定字符串在运行参数中的位置
	//参数：StrPointer strInfo
	//返回值：-1表示不存在
	static ShortInt	FindStrInRunPara(StrPointer strInfo);
	///</func_info>

	///<func_info>
	//描述：指定字符串在运行参数中的位置
	//参数：StrPointer strInfo
	//返回值：-1表示不存在
	static ArNT_BOOL GetStrInRunPara(ShortInt iIndex, LONGSTR& strPara);
	///</func_info>

	///<func_info>
	//描述：删除文件
	//参数：StrPointer strFileName 指定的文件名
	//返回值：ArNT_TRUE 表示成功, ArNT_FASLE表示失败
	static ArNT_BOOL DeleteFile(StrPointer strFileName);
	///</func_info>

	///<func_info>
	//描述：获取CPU信息
	//参数：SHORTSTR& strInfo
	//返回值：ArNT_TRUE 表示成功, ArNT_FASLE表示失败
	static ArNT_BOOL GetCPUInfo(SHORTSTR& strInfo);
	///</func_info>

	///<func_info>
	//描述：显示信息对话框
	//参数：StrPointer strFormat 格式化字符串
	//返回值：无
	static void NoticeBox(StrPointer strFormat, ...);
	///</func_info>

	///<func_info>
	//描述：显示警告对话框
	//参数：StrPointer strFormat 格式化字符串
	//返回值：无
	static void WarnBox(StrPointer strFormat, ...);
	///</func_info>

	///<func_info>
	//描述：显示错误对话框
	//参数：StrPointer strFormat 格式化字符串
	//返回值：无
	static void ErrorBox(StrPointer strFormat, ...);
	///</func_info>

	///<func_info>
	//描述：进行字符串连接
	//参数：StrPointer strDest 连接到的目标字符串
	//		ULongInt iDestLen 目标字符串的长度
	//		StrPointer strCat 被连接的字符串
	//返回值：无
	static void SafeStringCat(StrPointer strDest, ULongInt iDestLen, StrPointer strCat);
	///</func_info>

	///<func_info>
	//描述：弹出选择目录对话框
	//参数：LONGSTR& strDst
	//返回值：无
	static void ShowDirDialog(StrPointer strTitle, LONGSTR& strDst);
	///</func_info>

	///<func_info>
	//描述：获取与PC相关的Identify信息
	//参数：SHORTSTR& strInfo
	//返回值：ArNT_TRUE 表示成功, ArNT_FASLE表示失败
	static ArNT_BOOL GetPCIdentify(SHORTSTR& strInfo);
	///</func_info>

	///<func_info>
	//描述：比较两个字符串是否相等
	//参数：StrPointer strSrc 需要比较源字符串
	//StrPointer strDst 需要比较目的字符串
	//LongInt iMaxLen
	//返回值：ArNT_TRUE 表示成功, ArNT_FASLE表示失败
	static ArNT_BOOL CompareString(StrPointer strSrc, StrPointer strDst, LongInt iMaxLen);
	///</func_info>

	///<func_info>
	//描述：登录到指定的服务器上
	//参数：StrPointer strIP
	//StrPointer strUser
	////StrPointer strPasswd
	//返回值：ArNT_TRUE 表示成功, ArNT_FASLE表示失败
	static ArNT_BOOL LoginToServer(StrPointer strIP, StrPointer strUser,StrPointer strPasswd);
	///</func_info>
	///<func_info>
	//描述：获取硬盘空间容量
	//参数：StrPointer strDisk  指定需要获取的分区名称如C:, D:
	//LongFloat& fTotalSpace  该分区中总的容量，以GByte为单位
	//LongFloat& fFreeSpace   该分区中空余的容量，以GByte为单位
	//返回值：ArNT_TRUE 表示成功, ArNT_FASLE表示失败
	static ArNT_BOOL GetDiskFreespace(StrPointer strDisk, LongFloat& fTotalSpace,LongFloat& fFreeSpace);
	///</func_info>

	///<func_info>
	//描述：删除目录中所有的文件
	//参数：StrPointer strDir  指定需要删除文件所在的目录
	//LongInt& iDelFileNum  该分区中总的容量，以字节为单位
	//返回值：ArNT_TRUE 表示成功, ArNT_FASLE表示失败
	static ArNT_BOOL ClearDir(StrPointer strDir, LongInt& iDelFileNum );
	///</func_info>
	
	///<func_info>
	//描述：获取当前系统时间
	//参数：ArNTSysTime& time 用于存放当前的系统时间
	//返回值：无
	static void		 GetCurrSysTime(ArNTSysTime& time);
	///</func_info>

	///<func_info>
	//描述：获取与当前系统时间相差指定时间的系统时间
	//参数：ArNTSysTime& time 用于存放当前的系统时间
	//返回值：无
	static void		 GetOffsetSysTime(ArNTSysTime& time, UShortInt iDay, UShortInt iHour, UShortInt iMinute, UShortInt iSecond);
	///</func_info>


	///<func_info>
	//描述：将字符串转换为整数值
	//参数：StrPointer strInfo 需要转换的字符串
	//		LongInt& iData 转换后的整数值
	//返回值：ArNT_TRUE表示成功，ArNT_FALSE表示失败
	static ArNT_BOOL  ConverToInt(StrPointer strInfo, LongInt& iData);
	///</func_info>

	///<func_info>
	//描述：将字符串转换为浮点数值
	//参数：StrPointer strInfo 需要转换的字符串
	//		LongFloat& fData 转换后的浮点数值
	//返回值：ArNT_TRUE表示成功，ArNT_FALSE表示失败
	static ArNT_BOOL  ConverToFloat(StrPointer strInfo, LongFloat& fData);
	///</func_info>
	
	static ArNT_BOOL  IsSubStr(StrPointer strSrc, LongInt iMaxSrcLen, StrPointer strSub);

private:
	CCommonFunc(void);
	~CCommonFunc(void);
};
//==============================================================================
///</class_info>

////////////////////////////////////////////////////////////////////////////////
//helper class

///<class_info>
//==============================================================================
//名称:CSystemInfo
//功能:用于获取系统信息
class CSystemInfo : public SYSTEM_INFO {
public:
   CSystemInfo() { GetSystemInfo(this); }
};
//==============================================================================
///</class_info>

///<class_info>
//==============================================================================
//名称:CAutoSetEvt
//功能:用于实现自动设置事件对象
class CAutoSetEvt 
{
public:
	DWORD Wait(int iTime);
   CAutoSetEvt(HANDLE hEvt);
   ~CAutoSetEvt();
private:
	HANDLE m_hEvt;
};
//==============================================================================
///</class_info>


void _stdcall  GetCommonFuncVersion(LONGSTR & strVersion);

#ifndef AUTONOLINK_COMMONFUNC
#define AUTONOLINK_COMMONFUNC
#ifdef _WIN64
#pragma comment( lib, "CommonFunc64.lib" )
#else
#pragma comment( lib, "CommonFunc.lib" )
#endif

#endif


