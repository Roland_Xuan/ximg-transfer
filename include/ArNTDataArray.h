#pragma once
#include "ArNtBaseDataStruct.h"
#include "ArNTCriticalSection.h"

typedef struct tagDataArrayItem
{
	ULongLongInt  iID;
	LongInt iDataSize;
	enum {RESERVE_SIZE = 1024};
	BufferByte Data[RESERVE_SIZE];
	PBufferByte pAlloc;
	LongInt iAllocSize;
}DataArrayItem, *PDataArrayItem;


class ArNTDataArray
{
public:
	ArNT_BOOL AddData(PMemPointer pData, LongInt iDataSize);
	ArNT_BOOL ClearData(void);
	LongInt		Size();
	ArNT_BOOL GetSubArray(ArNTDataArray* pDstArray, LongInt& iDataNum);
	PMemPointer GetLastData(LongInt& iDataSize, ArNT_BOOL& bSuccess);
	PMemPointer GetPrioData(LongInt& iDataSize, ArNT_BOOL& bSuccess);
	PMemPointer GetFirstData(LongInt& iDataSize, ArNT_BOOL& bSuccess);
	PMemPointer GetNextData(LongInt& iDataSize, ArNT_BOOL& bSuccess);
	PMemPointer GetIndexData(LongInt iIndex, LongInt& iDataSize, ArNT_BOOL& bSuccess);

public:
	ArNTDataArray(LongInt iMaxItemNum = 32);
	ArNTDataArray(LongInt iMaxItemNum, LongInt iAllocSize);
	~ArNTDataArray(void);
private:

	PDataArrayItem			m_pDataArrayItem;
	LongInt					m_iMaxItemNum;
	LongLongInt				m_iID;
	LongLongInt				m_iCurrID;
	CTaskCriticalSection	m_csOperArray;
	friend class ArNTDataArray;
};
