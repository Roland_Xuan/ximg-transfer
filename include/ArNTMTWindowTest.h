#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：MCSISTConsoleTest.h
// 作  者 ：杨朝霖
// 用  途 ：定义了单线程测试类
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

#include "ArNTTestUint.h"

class  CMTTestWindow:public ArNTTestUnitOutPut
{
public:
	virtual void  WriteInfo(StrChar* strInfo);
	virtual bool  Valid();
	virtual bool ProcessMsg(UINT uMsg, WPARAM wParam, LPARAM lParam);
	bool  Create(void);
	void  SetTitle(StrChar* strTitle);
    void  Show(bool bTrue);

public:
	CMTTestWindow();
	~CMTTestWindow();
private:
	HWND  m_hMainWnd;
	HWND  m_hListBox;

	HFONT m_hFont;

	int   m_iStartX;
	int   m_iStartY;
	

	WNDCLASSEX m_WndClass;
	static ATOM  m_ClassAtom;
	static int   m_iWindowIndex;
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);	
};

typedef struct tagMTTestContex
{
	ArNTTestUnit*	pTest;
	CMTTestWindow*  pWnd;
	unsigned int	iThreadID;
	HANDLE			hThreadTest;
	HANDLE          hThreadUI;
	bool			bExit;
}MTTestContex, *PMTTestContex;


class CMTConsoleTestGroup:public ArNTTestGroup
{
public:
	bool StartTest(void);
	void AddTest(ArNTTestUnit* pTest);
	void  Free();
	static CMTConsoleTestGroup& GetObj();
private:
	CMTConsoleTestGroup();
	~CMTConsoleTestGroup();

	enum {MAX_TEST_NUM = 1024};
	MTTestContex  m_TestArray[MAX_TEST_NUM];
	int   m_iTestNum;
	int   m_iValidTestNum;
	static unsigned int __stdcall threadTest(void* pPara);
	static unsigned int __stdcall threadUI(void* pPara);
	CMTTestWindow  m_TestOutPut;

	friend class CMTWindowTestFactory;	
};

class CMTWindowTestFactory:public  ArNTTestFactory
{
public:
	virtual ArNTTestGroup* GetTestGroup(StrChar* strPara);
	virtual void Free();
public:
	static CMTWindowTestFactory& GetObj();
private:
	~CMTWindowTestFactory() {};
	CMTWindowTestFactory() {};	
};

