#pragma once
#include "FileManagerProtocal.h"

class ArNTFMPUIEvtHandler
{
public:
	//回复用户登录
	virtual  void OnAckLogin(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc, tagArNTUserInfo::ArNTRank Rank, StrPointer strSenderID);
	//回复用户注销
	virtual  void OnAckLogOff(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc,  StrPointer strSenderID);
	//回复添加用户
	virtual  void OnAckAddUser(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID);
	//回复删除用户
	virtual  void OnAckDelUser(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID);
	//回复用户列表
	virtual  void OnAckUserList(StrPointer strUserName,ShortInt iUserNum, PArNTUserInfo pInfo, StrPointer strSenderID);
	//回复工程信息集合
	virtual  void OnAckProjectSet(StrPointer strUserName, PArNTFMProject pProject, ShortInt iProjectNum, StrPointer strSenderID);
	//回复指定工程的备份信息集合
	virtual  void OnAckProjectBackupInfo(StrPointer strProjectName, PArNTFMPBackupItem pItem, ShortInt	iBackupNum, StrPointer strSenderID);
	//回复提取指定工程的所有文件
	virtual  void OnAckExtractProject(StrPointer strProjectName, ShortInt iTotalExtractNum, ShortInt iHasExtractNum, StrPointer strSenderID);
	//回复备份文件到指定的工程
	virtual  void OnAckBackupProject(StrPointer strProjectName, ShortInt  iTotalBackupNum, ShortInt iHasBackupNum, ArNT_BOOL bUpdate, StrPointer strSenderID);
	//回复创建指定名称的工程
	virtual  void OnAckCreateProject(StrPointer strProjectName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID);
	//回复删除指定的工程
	virtual  void OnAckDelProject(StrPointer strProjectName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID);
	//回复指定文件的版本列表
	virtual  void OnAckFileList(StrPointer strProjectName, StrPointer strFileName, PArNTFMPBackupItem pItem, ShortInt  iItemNum, StrPointer strSenderID);
	//回复提取文件
	virtual  void OnAckExtractFile(StrPointer strProjectName, StrPointer strFileName, VersionType Version, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID);
	//通知错误信息
	virtual  void OnNtfErrorInfo(StrPointer strAppName, StrPointer strCmdName, ArNT_ERROR_CODE Code, StrPointer strErrorInfo, StrPointer strSenderID);
	//回复服务的错误列表
	virtual  void OnAckErrorList(StrPointer strAppName, PArNTErrorInfo pErrorInfo, ShortInt iErrorNum , StrPointer strSenderID);
	//回复支持FMP协议的服务
	virtual  void OnAckService(ArNServiceAppInfo& Info, StrPointer strSenderID);
	//回复当前程序的运行信息
	virtual  void OnAckRunInfo(StrPointer strAppName, PArNTRunInfo pInfo, ShortInt iInfoNum, StrPointer strSenderID);
	//回复协议命令列表
	virtual  void OnAckCmdList(PArNTCommandDesc pCmd, ShortInt iCmdNum, StrPointer strSenderID);
	//回复备份项的文件列表
	virtual  void OnAckBackupFileLst(StrPointer strProjectName, VersionType	Version, PArNTBackFileItem pItem, LongInt iItemNum, StrPointer strSenderID);

public:
	ArNTFMPUIEvtHandler(void);
	~ArNTFMPUIEvtHandler(void);

public:
	void* m_pParent;
	void  RegParent(void* pParent);
private:
	void DefautNotImplFunc(StrPointer strFuncInfo);
};


/////////////////////////////////////////////////////////
//消息处理类
class ArNTFMPUIMsgHandler
{
	DECL_FMP_UI_MESSAGE(ArNTFMPUIMsgHandler);
public:
	ArNTFMPUIMsgHandler(void);
	~ArNTFMPUIMsgHandler(void);
public:
	ArNT_BOOL RegEvtHandler(ArNTFMPUIEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace, ShortInt iObjID = -1);
	void UnRegEvtHandler(ShortInt iObjID);
public:
	LONGSTR	m_strRegChannel;
private:
	ArNT_BOOL				m_bEnableTrace;	
	ArNTFMPUIEvtHandler*	m_pEvtHandler;
	//--------------------------------------------------
	//消息处理函数
	void   ProcessFMPAckLogin(PFMPAckLogin pPara,  StrPointer strSenderID); 
	void   ProcessFMPAckLogOff(PFMPAckLogOff pPara,  StrPointer strSenderID); 
	void   ProcessFMPAckAddUser(PFMPAckAddUser pPara,  StrPointer strSenderID); 
	void   ProcessFMPAckDelUser(PFMPAckDelUser pPara,  StrPointer strSenderID); 
	void   ProcessFMPAckUserList(PFMPAckUserList pPara,  StrPointer strSenderID); 
	void   ProcessFMPAckProjectSet(PFMPAckProjectSet pPara,  StrPointer strSenderID); 
	void   ProcessFMPAckProjectBackup(PFMPAckProjectBackup pPara,  StrPointer strSenderID); 
	void   ProcessFMPAckExtractProject(PFMPAckExtractProject pPara,  StrPointer strSenderID);   
	void   ProcessFMPAckBackupProject(PFMPAckBackupProject pPara,  StrPointer strSenderID);
	void   ProcessFMPAckCreateProject(PFMPAckCreateProject pPara,  StrPointer strSenderID);
	void   ProcessFMPAckDeleteProject(PFMPAckDeleteProject pPara,  StrPointer strSenderID);
	void   ProcessFMPAckFileList(PFMPAckFileList pPara,  StrPointer strSenderID);
	void   ProcessFMPAckExtractFile(PFMPAckExtractFile pPara,  StrPointer strSenderID);
	void   ProcessFMPAckErrorList(PFMPAckErrorList pPara,  StrPointer strSenderID);
	void   ProcessFMPAckService(PFMPAckService pPara,  StrPointer strSenderID);
	void   ProcessFMPAckRunInfo(PFMPAckRunInfo pPara,  StrPointer strSenderID);
	void   ProcessFMPAckCmdList(PFMPAckCmdList pPara,  StrPointer strSenderID);
	void   ProcessFMPNtfErrorInfo(PFMPNtfError pPara,  StrPointer strSenderID);
	void   ProcessFMPAckBackupFileLst(PFMPAckBackupFileLst pPara,  StrPointer strSenderID);
public:
	//询问FMP协议支持的命令
	void	AskCmdList(LONGSTR& strFromChannel,StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//询问服务的运行信息
	void	AskAppRunInfo(LONGSTR& strFromChannel,StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//询问当前支持FMP协议的服务
	void	AskServerice(LONGSTR& strFromChannel, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//询问服务的错误码
	void    AskErrorList(LONGSTR& strFromChannel,StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//请求用户登录
	void    AskLogIn(LONGSTR& strFromChannel,StrPointer strUser, StrPointer strPassWd, StrPointer strSenderID = NULL, ShortInt iObjID = -1);

	//请求用户注销
	void    AskLogOff(LONGSTR& strFromChannel,StrPointer strUser, StrPointer strSenderID = NULL, ShortInt iObjID = -1);

	//请求添加新用户
	void    AskAddUser(LONGSTR& strFromChannel, StrPointer strOperUser, ArNTUserInfo& Info, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//请求删除用户
	void	AskDelUser(LONGSTR& strFromChannel, StrPointer strOperUser, StrPointer strDelUser, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//询问用户列表
	void	AskUserList(LONGSTR& strFromChannel, StrPointer strOperUser, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//创建工程
	void    AskCreateProject(LONGSTR& strFromChannel, StrPointer strOperUser, StrPointer strProjectName,  StrPointer strDesc,  ArNTBackupType& Type, StrPointer strSrcPath, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//备份工程
	void    AskBackUpProject(LONGSTR& strFromChannel, StrPointer strOperUser, StrPointer strProjectName,  StrPointer strDesc, StrPointer strSrcPath, ArNT_BOOL bLargeChange, ArNT_BOOL bRelease, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//询问工程集合
	void    AskUserProjectSet(LONGSTR& strFromChannel, StrPointer strOperUser,  StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//询问工程的备份项信息
	void	AskProjectBackupItem(LONGSTR& strFromChannel, StrPointer strOperUser,   StrPointer strProjectName, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	//请求提取指定版本的工程备份
	void    AskExtractProjectBackup(LONGSTR& strFromChannel, StrPointer strOperUser,  StrPointer strProjectName, VersionType VerID, StrPointer strDestPath, StrPointer strSenderID = NULL, ShortInt iObjID = -1); 
	//询问指定备份项的文件列表
	void    AskBackupItemFileLst(LONGSTR& strFromChannel, StrPointer strOperUser,  StrPointer strProjectName, VersionType VerID, StrPointer strSenderID = NULL, ShortInt iObjID = -1); 
	//询问指定文件的备份列表
	void    AskFileBackupLst(LONGSTR& strFromChannel, StrPointer strOperUser,  StrPointer strProjectName, StrPointer strFileName, StrPointer strSenderID = NULL, ShortInt iObjID = -1); 

	//请求导出指定文件
	void    AskExtractFile(LONGSTR& strFromChannel, StrPointer strOperUser,  StrPointer strProjectName, StrPointer strFileName, VersionType VerID, StrPointer strDestPath, StrPointer strSenderID = NULL, ShortInt iObjID = -1); 
};

//////////////////////////////////////////////////////////////////////////////////////
//宏定义
#define BEGIN_FMPUIEVT_NEST(classobj) class  ArNTFMPUIEvt:public ArNTFMPUIEvtHandler{ 


#define DECL_FMPUI_DEFAULT_FUNC()		DECL_FMP_ACK_LOGIN() \
										DECL_FMP_ACK_LOGOFF() \
										DECL_FMP_ACK_ADDUSER()\
										DECL_FMP_ACK_DELUSER()\
										DECL_FMP_ACK_USERLIST()\
										DECL_FMP_ACK_PROJECTSET()\
										DECL_FMP_ACK_PRJ_BACKUPINFO()\
										DECL_FMP_ACK_EXTR_PROJECT()\
										DECL_FMP_ACK_BACK_PROJECT()\
										DECL_FMP_ACK_DEL_PROJECT()\
										DECL_FMP_ACK_CREATE_PROJECT()\
										DECL_FMP_ACK_FILE_LIST()\
										DECL_FMP_ACK_EXTR_FILE()\
										DECL_FMP_NTF_ERROR()\
										DECL_FMP_ACK_ERRORLIST()\
										DECL_FMP_ACK_SERVICE()\
										DECL_FMP_ACK_RUNINFO()\
										DECL_FMP_ACK_CMDLIST()\
										DECL_FMP_ACK_BACKUPFILELST()


#define DECL_FMP_ACK_LOGIN()			public:	virtual		void OnAckLogin(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc, tagArNTUserInfo::ArNTRank Rank, StrPointer strSenderID);
#define DECL_FMP_ACK_LOGOFF()			public:	virtual		void OnAckLogOff(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc,  StrPointer strSenderID);
#define DECL_FMP_ACK_ADDUSER()			public: virtual		void OnAckAddUser(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID);
#define DECL_FMP_ACK_DELUSER()			public: virtual		void OnAckDelUser(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID);
#define DECL_FMP_ACK_USERLIST()			public: virtual		void OnAckUserList(StrPointer strUserName,ShortInt iUserNum, PArNTUserInfo pInfo, StrPointer strSenderID);
#define DECL_FMP_ACK_PROJECTSET()		public: virtual		void OnAckProjectSet(StrPointer strUserName, PArNTFMProject pProject, ShortInt iProjectNum, StrPointer strSenderID);	
#define DECL_FMP_ACK_PRJ_BACKUPINFO()	public:	virtual		void OnAckProjectBackupInfo(StrPointer strProjectName, PArNTFMPBackupItem pItem, ShortInt	iBackupNum, StrPointer strSenderID);
#define DECL_FMP_ACK_EXTR_PROJECT()		public: virtual		void OnAckExtractProject(StrPointer strProjectName, ShortInt iTotalExtractNum, ShortInt iHasExtractNum, StrPointer strSenderID);
#define DECL_FMP_ACK_BACK_PROJECT()		public: virtual		void OnAckBackupProject(StrPointer strProjectName, ShortInt  iTotalBackupNum, ShortInt iHasBackupNum, ArNT_BOOL bUpdate, StrPointer strSenderID);
#define DECL_FMP_ACK_CREATE_PROJECT()	public: virtual		void OnAckCreateProject(StrPointer strProjectName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID);
#define DECL_FMP_ACK_DEL_PROJECT()		public: virtual		void OnAckDelProject(StrPointer strProjectName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID);
#define DECL_FMP_ACK_FILE_LIST()		public: virtual		void OnAckFileList(StrPointer strProjectName,StrPointer strFileName, PArNTFMPBackupItem pItem, ShortInt  iItemNum, StrPointer strSenderID);
#define DECL_FMP_ACK_EXTR_FILE()		public: virtual		void OnAckExtractFile(StrPointer strProjectName, StrPointer strFileName, VersionType Version, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID);
#define DECL_FMP_NTF_ERROR()			public:	virtual     void OnNtfErrorInfo(StrPointer strAppName, StrPointer strCmdName, ArNT_ERROR_CODE Code, StrPointer strErrorInfo, StrPointer strSenderID);
#define DECL_FMP_ACK_ERRORLIST()		public: virtual		void OnAckErrorList(StrPointer strAppName, PArNTErrorInfo pErrorInfo, ShortInt iErrorNum, StrPointer strSenderID);
#define DECL_FMP_ACK_SERVICE()			public: virtual		void OnAckService(ArNServiceAppInfo& Info, StrPointer strSenderID);
#define DECL_FMP_ACK_RUNINFO()			public: virtual		void OnAckRunInfo(StrPointer strAppName, PArNTRunInfo pInfo, ShortInt iInfoNum, StrPointer strSenderID);
#define DECL_FMP_ACK_CMDLIST()			public: virtual		void OnAckCmdList(PArNTCommandDesc pCmd, ShortInt iCmdNum, StrPointer strSenderID);
#define DECL_FMP_ACK_BACKUPFILELST()	public: virtual     void OnAckBackupFileLst(StrPointer strProjectName, VersionType	Version, PArNTBackFileItem pItem, LongInt iItemNum, StrPointer strSenderID);

#define END_FMPUIEVT_NEST(classobj)		};\
										private:	ArNTFMPUIEvt			m_FMPUIEvtHandler;\
										private:    ArNTFMPUIMsgHandler		m_FMPUIMsgHandler;\
										public:     ArNTFMPUIMsgHandler*    GetFMPUIMsgHandler() {return &m_FMPUIMsgHandler;};


#define  REG_FMPUIEVT_NEST(bEnableTrace, ObjID)  m_FMPUIEvtHandler.RegParent(this);\
								if(ArNT_FALSE == m_FMPUIMsgHandler.RegEvtHandler(&m_FMPUIEvtHandler, bEnableTrace, ObjID))\
								{return ArNT_FALSE;}

#define UNREG_FMPUIEVT_NEST(ObjID) m_FMPUIMsgHandler.UnRegEvtHandler(ObjID);


//定义实现类
#define  IMPL_FMPUI_DEFAULT_FUNC(classobj)		IMPL_FMP_ACK_LOGIN(classobj)\
												IMPL_FMP_ACK_LOGOFF(classobj)\
												IMPL_FMP_ACK_ADDUSER(classobj)\
												IMPL_FMP_ACK_DELUSER(classobj)\
												IMPL_FMP_ACK_USERLIST(classobj)\
												IMPL_FMP_ACK_PROJECTSET(classobj)\
												IMPL_FMP_ACK_PRJ_BACKUPINFO(classobj)\
												IMPL_FMP_ACK_EXTR_PROJECT(classobj)\
												IMPL_FMP_ACK_BACK_PROJECT(classobj)\
												IMPL_FMP_ACK_CREATE_PROJECT(classobj)\
												IMPL_FMP_ACK_DEL_PROJECT(classobj)\
												IMPL_FMP_ACK_FILE_LIST(classobj)\
												IMPL_FMP_ACK_EXTR_FILE(classobj)\
												IMPL_FMP_NTF_ERROR(classobj)\
												IMPL_FMP_ACK_ERRORLIST(classobj)\
												IMPL_FMP_ACK_SERVICE(classobj)\
												IMPL_FMP_ACK_RUNINFO(classobj)\
												IMPL_FMP_ACK_CMDLIST(classobj)\
												IMPL_FMP_ACK_BACKUPFILELST(classobj)
												

#define  IMPL_FMP_ACK_LOGIN(classobj)		void classobj::ArNTFMPUIEvt::OnAckLogin(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc, tagArNTUserInfo::ArNTRank Rank, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckLogin(strUserName, bStatus, strDesc,Rank, strSenderID);}}

#define  IMPL_FMP_ACK_LOGOFF(classobj)		void classobj::ArNTFMPUIEvt::OnAckLogOff(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckLogOff(strUserName, bStatus, strDesc,strSenderID);}}

#define  IMPL_FMP_ACK_ADDUSER(classobj)		void classobj::ArNTFMPUIEvt::OnAckAddUser(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckAddUser(strUserName, bStatus, strDesc, strSenderID);}}

#define  IMPL_FMP_ACK_DELUSER(classobj)		void classobj::ArNTFMPUIEvt::OnAckDelUser(StrPointer strUserName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckDelUser(strUserName, bStatus, strDesc, strSenderID);}}


#define  IMPL_FMP_ACK_USERLIST(classobj)		void classobj::ArNTFMPUIEvt::OnAckUserList(StrPointer strUserName,ShortInt iUserNum, PArNTUserInfo pInfo, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckUserList(strUserName, iUserNum, pInfo, strSenderID);}}

#define	IMPL_FMP_ACK_PROJECTSET(classobj)			void classobj::ArNTFMPUIEvt::OnAckProjectSet(StrPointer strUserName, PArNTFMProject pProject, ShortInt iProjectNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckProjectSet(strUserName,pProject, iProjectNum, strSenderID);}}

#define	IMPL_FMP_ACK_PRJ_BACKUPINFO(classobj)		void classobj::ArNTFMPUIEvt::OnAckProjectBackupInfo(StrPointer strProjectName, PArNTFMPBackupItem pItem, ShortInt	iBackupNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckProjectBackupInfo(strProjectName, pItem, iBackupNum, strSenderID);}}

#define	IMPL_FMP_ACK_EXTR_PROJECT(classobj)		void classobj::ArNTFMPUIEvt::OnAckExtractProject(StrPointer strProjectName, ShortInt iTotalExtractNum, ShortInt iHasExtractNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckExtractProject(strProjectName, iTotalExtractNum, iHasExtractNum, strSenderID);}}

#define	IMPL_FMP_ACK_BACK_PROJECT(classobj)		void classobj::ArNTFMPUIEvt::OnAckBackupProject(StrPointer strProjectName, ShortInt  iTotalBackupNum, ShortInt iHasBackupNum, ArNT_BOOL bUpdate, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckBackupProject(strProjectName, iTotalBackupNum, iHasBackupNum, bUpdate, strSenderID);}}

#define	IMPL_FMP_ACK_CREATE_PROJECT(classobj)		void classobj::ArNTFMPUIEvt::OnAckCreateProject(StrPointer strProjectName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckCreateProject(strProjectName, bStatus, strDesc, strSenderID);}}

#define	IMPL_FMP_ACK_DEL_PROJECT(classobj)		void classobj::ArNTFMPUIEvt::OnAckDelProject(StrPointer strProjectName, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckDelProject(strProjectName, bStatus, strDesc, strSenderID);}}

#define	IMPL_FMP_ACK_FILE_LIST(classobj)		void classobj::ArNTFMPUIEvt::OnAckFileList(StrPointer strProjectName, StrPointer strFileName, PArNTFMPBackupItem pItem, ShortInt  iItemNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckFileList(strProjectName,strFileName, pItem, iItemNum, strSenderID);}}

#define	IMPL_FMP_ACK_EXTR_FILE(classobj)		void classobj::ArNTFMPUIEvt::OnAckExtractFile(StrPointer strProjectName, StrPointer strFileName, VersionType Version, ArNT_BOOL bStatus, StrPointer strDesc, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckExtractFile(strProjectName, strFileName, Version, bStatus, strDesc, strSenderID);}}

#define IMPL_FMP_NTF_ERROR(classobj)	void classobj::ArNTFMPUIEvt::OnNtfErrorInfo(StrPointer strAppName, StrPointer strCmdName, ArNT_ERROR_CODE Code, StrPointer strErrorInfo, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnNtfErrorInfo(strAppName, strCmdName, Code, strErrorInfo, strSenderID);}}
    

#define	IMPL_FMP_ACK_ERRORLIST(classobj)		void classobj::ArNTFMPUIEvt::OnAckErrorList(StrPointer strAppName, PArNTErrorInfo pErrorInfo, ShortInt iErrorNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckErrorList(strAppName, pErrorInfo, iErrorNum, strSenderID);}}

#define	IMPL_FMP_ACK_SERVICE(classobj)		void classobj::ArNTFMPUIEvt::OnAckService(ArNServiceAppInfo& Info, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckService(Info, strSenderID);}}

#define	IMPL_FMP_ACK_RUNINFO(classobj)		void classobj::ArNTFMPUIEvt::OnAckRunInfo(StrPointer strAppName, PArNTRunInfo pInfo, ShortInt iInfoNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckRunInfo(strAppName,pInfo, iInfoNum, strSenderID);}}

#define	IMPL_FMP_ACK_CMDLIST(classobj)		void classobj::ArNTFMPUIEvt::OnAckCmdList(PArNTCommandDesc pCmd, ShortInt iCmdNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckCmdList(pCmd, iCmdNum, strSenderID);}}


#define	IMPL_FMP_ACK_BACKUPFILELST(classobj)	void classobj::ArNTFMPUIEvt::OnAckBackupFileLst(StrPointer strProjectName, VersionType	Version, PArNTBackFileItem pItem, LongInt iItemNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnAckBackupFileLst(strProjectName, Version, pItem, iItemNum, strSenderID);}}



//==============================================================================
///链接信息
#ifndef  NOAUTOLINK_FMPUIHELPER
#define  NOAUTOLINK_FMPUIHELPER
#pragma comment( lib, "ArNTFMPHelper.lib" )

#endif