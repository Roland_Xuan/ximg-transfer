
#pragma once

///<proj_info>
//==============================================================================
// 项目名 ：在线检测系统
// 文件名 ：ArNTIMGPlatFormDataStaruct.h
// 作  者 ：杨朝霖
// 用  途 ：定义了在线检测系统中的常用数据结构。
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖       2012-07-9    创建模块

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
///</header_info>


///<datastruct_info>
//==============================================================================
//常用数据结构定义

//图像保存目录结构
typedef struct tagImageSaveDirPath
{
	LongInt iSize;
	LONGSTR strPath;//保存的目录路径
	LongInt iDirNum;//目录下子目录的数量
}ImageSaveDirPath, *PImageSaveDirPath;

//图像保存目录集合结构
typedef struct tagImageSaveDirPathSet
{
	LongInt iSize;
	enum {MAX_ITEM_NUM = 32}; //集合中允许的最大图像保存目录的数量
	ImageSaveDirPath  Items[MAX_ITEM_NUM];//图像保存目录列表
	ShortInt		  iItemNum;	//图像保存目录的数量
}ImageSaveDirPathSet, *PImageSaveDirPathSet;

//原始图像采集相机信息集合
typedef struct tagImgCaptureCameralInfo
{
	LongInt iSize;
	enum {MAX_ITEM_NUM = 8}; //集合中允许的相机最大数量
	ShortInt iCameralNum;	 //实际使用的相机数量
	ArNTCameralInfo CameralArray[MAX_ITEM_NUM];//相机信息列表
}ImgCaptureCameralInfo, *PImgCaptureCameralInfo;

//原始图像采集相机运行状态集合
typedef struct tagCameralStatusSet
{
	LongInt iSize;
	enum {MAX_ITEM_NUM = 8};	//集合中允许的相机最大数量
	ShortInt iCameralNum;		//实际使用的相机数量
	ArNTCameralStatus CameralArray[MAX_ITEM_NUM];//相机运行状态信息列表
}CameralStatusSet,*PCameralStatusSet;

//原始图像信息结构
typedef struct tagRawCaptureImage
{
	LongInt		iSize;
	SHORTSTR	strID;			//图像所属相机的ID
	LongInt     iToatalImgSize;	//图像的总尺寸
	ShortInt	iChannelNum;	//图像包括的通道数
	ShortInt	iWidth;			//图像的宽度
	ShortInt	iHeight;		//图像的高度	
	ShortInt    iWidthStep;		//图像每行的保存宽度
	ShortInt	iTemperature;	//图像所属相机的温度
	BufferByte  pImgData[1];	//图像数据的首地址
}RawCaptureImage, *PRawCaptureImage;

///</datastruct_info>
