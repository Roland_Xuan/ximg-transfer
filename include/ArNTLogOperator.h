#pragma once
#include "ArNtBaseDataStruct.h"

typedef struct tagLogRecord
{
	ArNTSysTime		Time;
	LONGSTR			strInfo;
}LogRecord, *PLogRecord;

class ArNTLogOperator
{
public:
	ArNTLogOperator(void);
	~ArNTLogOperator(void);
public:
	void AddLog(StrPointer strLogInfo);
	void AddFormatLog(StrPointer strFormat, ...);
	void ChangeLogPath(StrPointer strLogPath);
	void AddSubDirToLogPath(StrPointer strDir);
	ArNT_BOOL  ReadLog(ShortInt iYear, TinyInt iMonth, TinyInt iDay, PLogRecord pRecord, LongInt& iRecordNum);
	ArNT_BOOL  ReadLog(LONGSTR& strLogFileName, PLogRecord pRecord, LongInt& iRecordNum);
private:
	LONGSTR m_strLogPath;

};
