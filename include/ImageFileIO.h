#pragma once
#include "ipp.h"
#include "ArNTGrayBmp.h"
//压缩图像读写的基类,其他读写方法从此类继承
//作用是规定处理文件句柄和文件流,并将数据/图像文件读写的方法
//具体解压和压缩函数设为虚函数,有子类进行处理
//被继承的函数是:SetSize Compress PreDeCompress DeCompress

typedef struct tagCompressImgInfoEx//图像数据头文件
{
	unsigned long dwImgWidth;
	unsigned long dwImgHeight;
	unsigned long dwRoundImgWidth;
	unsigned long dwRoundImgHeight;
	unsigned long dwDataLen;
	unsigned long dwEncodeBitSize;//保留位未使用
	unsigned long dwMathod;//0表示Huffman压缩;-1表示HuffmanDC压缩;-2:LZ77,新添加的方法需从此处注册
	//DWORD dwEncodeBitSize;
	unsigned long pDCSign;//记录DC符号数据位置
	unsigned long pTable;//记录频率表位置
	unsigned long pDCHaffed;//记录DC数据位置
	unsigned long pReserved;
}CompressImgInfoEx, *PCompressImgInfoEx;

class  ImageFileIO
{
public:
	ImageFileIO(int iWidth=0,int iHeight=0);
	~ImageFileIO(void);
	//图像信息头
	CompressImgInfoEx fileHead;
	//根据图像大小设置缓存
	//计算缓存
	Ipp8u* pBuffer8u;
	Ipp8u* pBuffer1u;
	Ipp16s* pBuffer16s;
	//去相关图像转换后的存贮空间
	Ipp16s* pSrcBuffer16s;
	Ipp16s* pDstBuffer16s;
	ArNT_BOOL bufferOK;
	ArNT_BOOL SetSize(int srcImgWidth,int srcImgHeight);
	ArNT_BOOL SetSize(	CompressImgInfoEx *pFileHead);
	//数据压缩和解压方法为纯虚函数,必须在子类中实现
	virtual ArNT_BOOL Compress(Ipp8u* srcImg,int srcImgWidth,int srcImgActualWidth,int srcImgHeight,
		Ipp8u*dstData,int* dstLen, int* iEncodeBitSize)=0;
	//首先根据读取的文件信息确定缓存空间
	virtual ArNT_BOOL PreDeCompress(Ipp8u* srcData,int* srcLen,int* srcImgWidth,int* srcImgHeight)=0;
	//解压图像数据
	virtual ArNT_BOOL DeCompress(Ipp8u*srcData,int srcLen,
		Ipp8u* dstImg,int dstImgWidth,int dstImgActualWidth,int dstImgHeight)=0;

	//支持文件头的直接文件读写，不包含压缩解压操作
	//以下函数将数据读写到IO设备,默认已经包含文件数据头
	ArNT_BOOL  WriteToFile(void * hFile, unsigned char* ucDestData, int iDstDatalen);
	ArNT_BOOL  WriteToFile(wchar_t* strFileName, unsigned char* ucDestData, int iDstDatalen);
	//读取文件到指定内存,当空间不足时返回false
	ArNT_BOOL  RreReadFile(void * hFile, int *iDstDatalen);
	ArNT_BOOL  RreReadFile(wchar_t* strFileName,int *iDstDatalen);
	ArNT_BOOL  ReadFromFile(void * hFile,unsigned char* ucDestData, int iDstDatalen);
	ArNT_BOOL  ReadFromFile(wchar_t* strFileName,unsigned char* ucDestData, int iDstDatalen);

	//压缩图像文件读写包含压缩解压操作
	ArNT_BOOL  WriteCompressFile(void * hFile,GrayBmpData &srcImg);
	ArNT_BOOL  WriteCompressFile(wchar_t* strFileName,GrayBmpData &srcImg);
	ArNT_BOOL  WriteCompressFile(void * hFile,Ipp8u* srcImg,int srcImgWidth,int srcImgActualWidth,int srcImgHeight);
	ArNT_BOOL  WriteCompressFile(wchar_t* strFileName,Ipp8u* srcImg,int srcImgWidth,int srcImgActualWidth,int srcImgHeight);
	//以下自动分配目标内存
	ArNT_BOOL  ReadCompressFile(void * hFile,GrayBmpData &dstImg);
	ArNT_BOOL  ReadCompressFile(wchar_t* strFileName,GrayBmpData &dstImg);
	//获取图像信息,用于分配内存,与下属两种ReadCompressFile配合使用,
	//仅用于himg图像,对附加工程文件头的图像由ImgDataOper中的预读文件做处理
	ArNT_BOOL  PreReadCompressFile(void * hFile,CompressImgInfoEx& headInfo);
	ArNT_BOOL  PreReadCompressFile(wchar_t* strFileName,CompressImgInfoEx& headInfo);
	//以下默认已经分配好内存
	ArNT_BOOL  ReadCompressFile(void * hFile,Ipp8u* dstImg,int dstImgWidth,int dstImgActualWidth,int dstImgHeight);
	ArNT_BOOL  ReadCompressFile(wchar_t* strFileName,Ipp8u* dstImg,int dstImgWidth,int dstImgActualWidth,int dstImgHeight);

	int	  WriteCompressStream(unsigned char* ucStream, int iMaxStreamSize, unsigned char* ucSrcImgData, int iImgWidth, int iImgHeight, int iActrualImgWidth);
	ArNT_BOOL  ReadCompressStream(unsigned char* ucStream,int iStreamSize, GrayBmpData& BmpData);
	ArNT_BOOL  ReadCompressStream(unsigned char* ucStream,int iStreamSize, unsigned char* ucDestImgData, int* iDestImgWidth, int* iDestImgHeight);
};