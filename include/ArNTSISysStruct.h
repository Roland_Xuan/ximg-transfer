///<proj_info>
//==============================================================================
// 项目名 ：表面缺陷在线检测系统
// 文件名 ：ArNTSISysStruct.h
// 作  者 ：杨朝霖
// 用  途 ：该类用于定义检测系统中所用到的数据结构
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>
#pragma once
#include "ArNtBaseDataStruct.h"

//==============================================================================
//常用数据结构定义
#pragma pack(push, 4)

//系统配置
typedef struct tagArNTSysConfig
{
	LongInt		iSize;
	LongInt		iMaxImgDirNum;
	LongInt		iMaxImgInDir;
	LongInt		iInitialLineRate;
	LongInt		iMaxLineRate;
	LongFloat   dVerResolution;
	ArNT_BOOL	bAutoJudgeSteel;
	ArNT_BOOL   bTopBottomJudgeSteel;
	ArNT_BOOL	bEnableHeadMiddleTrail;
	ArNT_BOOL	bTopAhead;
	ArNT_BOOL	bLastRoolSignal;
	ArNT_BOOL	bNoDuplicateID;
	ArNT_BOOL	bWidthDetect;
	TINYSTR		strClientHostName;
	TINYSTR		strClientDBName;
	TINYSTR		strClientDBUser;
	TINYSTR		strClientDBPassword;
}ArNTSysConfig, *PArNTSysConfig;

//网络配置
typedef struct tagArNTNetConfig
{
	LongInt		iSize;
	NetIP		ServerIP;
	ShortInt	iClienCmdtPort;
	ShortInt    iServeCmdPort;
	ShortInt	iServerDefectPort;
}ArNTNetConfig, *PArNTNetConfig;

//客户机配置相关结构

//客户机配置子项
typedef struct tagArNTClientConfigItem
{
	LongInt		iSize;
	TinyInt		iID;
	ArNT_BOOL	bUse;
	NetIP		IP;	
}ArNTClientItem, *PArNTClientItem;
//客户机配置集合
typedef struct tagArNTClientConfigSet
{
	LongInt iSize;
	enum {MAX_ITEM_NUM = 10};
	ArNTClientItem	Items[MAX_ITEM_NUM];
	ShortInt	iItemNum;
}ArNTClientConfigSet, *PArNTClientConfigSet;


//相机配置相关结构
//图像保存目录
typedef struct tagArNTImgSaveDir
{
	LongInt		iSize;
	StrChar		strImgPath[64];
	LongInt		iSaveImgDirNum;
}ArNTImgSaveDir, *PArNTImgSaveDir;
//图像保存目录集合
typedef struct tagArNTImgSaveDirSet
{
	LongInt iSize;
	enum {MAX_ITEM_NUM = 4};
	ArNTImgSaveDir  Items[MAX_ITEM_NUM];
	ShortInt iItemNum;
}ArNTImgSaveDirSet, *PArNTImgSaveDirSet;

//相机配置子项
typedef struct tagArNTCamConfigItem
{
	LongInt		iSize;
	TinyInt		iID;
	TinyInt		iClientID;
	ShortInt    iInitExpTime;

	NetIP		IP;
	TINYSTR		strMAC;
	ArNT_BOOL   bUse;
	ArNT_BOOL	bTop;
	ArNT_BOOL	bJudgeSteel;
	ShortInt	iLeftPos;
	ShortInt    iRightPos;
	ShortInt    iLeftDelPixel;
	ShortInt	iRightDelPixel;	

	tagArNTImgSaveDirSet	ImgSaveDirSet;
}ArNTCamConfigItem, *PArNTCamConfigItem;

//相机配置子项集合
typedef struct tagArNTCamConfigSet
{
	LongInt iSize;
	enum {MAX_ITEM_NUM = 10};
	tagArNTCamConfigItem Items[MAX_ITEM_NUM];
	ShortInt  iItemNum;
}ArNTCamConfigSet, *PArNTCamConfigSet;

//缺陷类别相关结构
//缺陷类别子项
typedef struct tagArNTDefectClassItem
{
	LongInt iSize;
	UTinyInt	iID;
	ArNT_BOOL   bEnable;
	TINYSTR		strDesc;

}ArNTDefectClassItem, *PArNTDefectClassItem;
//缺陷类别集合
typedef struct tagArNTDefectClassSet
{
	LongInt iSize;
	enum {MAX_ITEM_NUM = 128};
	ArNTDefectClassItem  Items[MAX_ITEM_NUM];
	ShortInt iItemNum;
}ArNTDefectClassSet, *PArNTDefectClassSet;

//服务器配置信息
typedef struct tagArNTServerConfig
{
	LongInt iSize;
	tagArNTSysConfig		SysConfig;
	tagArNTNetConfig		NetConfig;
	tagArNTClientConfigSet	ClientConfig;
	tagArNTCamConfigSet		CameralConfig;
}ArNTServerConfig, *PArNTServerConfig;

#pragma pack(pop)