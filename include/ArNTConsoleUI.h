#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTConsoleUI.h
// 作  者 ：杨朝霖
// 用  途 ：定义了平台中用到的控制台程序界面类
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖       2012-12-1    创建模块

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
#include "ArNTCriticalSection.h"
///</header_info>

///<class_info>
//==============================================================================
//名称:ArNTConsoleUI
//功能:控制台界面类
class ArNTConsoleUI
{
public:
	///<func_info>
	//描述：初始化控制台界面
	//参数：StrPointer strTitle 控制台程序的标题。
	// ShortInt iWidth  控制台窗口的宽度，以字符数为单位
	//ShortInt iHeight 控制台窗口的高度，以字符数为单位
	//ArNT_BOOL bMaxSize 是否将控制台窗口最大化，ArNT_TRUE表示将窗口最大化。
	//返回值：无
	void  Init(StrPointer strTitle, ShortInt iWidth = 256, ShortInt iHeight = 1024, ArNT_BOOL bMaxSize = ArNT_TRUE);
	///</func_info>
	
	///<func_info>
	//描述：设置无标题类型消息的起始显示位置
	//参数：ShortInt iPos 起始位置，指定是高度方向。
	//返回值：无
	void  SetAddInfoStartPos(ShortInt iPos);
	///</func_info>

	///<func_info>
	//描述：添加无标题类型信息
	//参数：StrPointer strInfo需要添加的信息内容。
	// ArNT_BOOL bLog 是否需要添加日志的布尔变量，ArNT_TRUE表示添加。
	//返回值：无
	void  AddInfo(StrPointer strInfo, ArNT_BOOL bLog = ArNT_TRUE);
	///</func_info>
	
	///<func_info>
	//描述：显示有标题类型信息
	//参数：StrPointer strName 信息的名称。
	//StrPointer strInfo需要添加的信息内容。
	// ArNT_BOOL bLog 是否需要添加日志的布尔变量，ArNT_TRUE表示添加。
	//返回值：无
	void  DisplayInfo(StrPointer strName, StrPointer strInfo, ArNT_BOOL bLog = ArNT_FALSE);
	///</func_info>
public:
	ArNTConsoleUI(void);
	~ArNTConsoleUI(void);
private:
	HANDLE		m_hConsole;
	ShortInt    m_iInfoStartPos;

	CTaskCriticalSection	m_csAddInfo;

	LONGSTR		m_strLogPath;
	enum {MAX_ARRAY_NUM = 128};
	ArNTRunInfo m_InfoArray[MAX_ARRAY_NUM];
	ShortInt	m_iInfoNum;
};
///</class_info>
