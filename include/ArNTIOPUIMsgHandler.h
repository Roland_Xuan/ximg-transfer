#pragma once
#include "ImageIOProtocal.h"


//--------------------------------------
//消息响应类
class ArNTIOPUIEvtHandler
{
public:
	//接收到回复的图像数据信息
	virtual  void OnIOPAckReadImage(StrPointer strType, ShortInt iContextSize, PBufferByte ImageContext, PRawCaptureImage pImage, StrPointer strSenderID);
	//回复IOP协议支持的命令
	virtual  void OnIOPAckCmdList(PArNTCommandDesc pCmd, ShortInt iCmdNum, StrPointer strSenderID);
	//回复服务支持的图像保存类型
	virtual  void OnIOPAckSupportType(StrPointer strAppName, ShortInt iTypeNum, PArNTImageSupportType pTypes, StrPointer strSenderID);
	//回复服务的运行信息
	virtual  void OnIOPAckRunInfo(StrPointer strAppName, PArNTRunInfo pRunInfo, ShortInt iInfoNum, StrPointer strSenderID);
	//回复搜索到的图像信息
	virtual  void OnIOPAckSearchImage(PArNTImageDesc pDesc, ShortInt iImgNum, StrPointer strSenderID);
	//回复支持IOP协议的服务
	virtual  void OnIOPAckService(StrPointer strApp, StrPointer strVerison, StrPointer strSenderID);
	//回复服务的错误列表
	virtual  void OnIOPAckErrorList(StrPointer strAppName, PArNTErrorInfo pError, ShortInt iItemNum, StrPointer strSenderID);
	//通知错误消息
	virtual  void OnIOPNTFErrorInfo(StrPointer strAppName, StrPointer strCmdName, ArNT_ERROR_CODE Code, StrPointer strError, StrPointer strSenderID);
	
public:
	ArNTIOPUIEvtHandler();
	~ArNTIOPUIEvtHandler();
public:
	void* m_pParent;
	void  RegParent(void* pParent);
private:
	void DefautNotImplFunc(StrPointer strFuncInfo);

};

/////////////////////////////////////////////////////////////////////////////////
//消息处理类

class ArNTIOPUIMsgHandler
{
	
public:
	ArNTIOPUIMsgHandler(void);
	~ArNTIOPUIMsgHandler(void);
public:
	ArNT_BOOL	RegEvtHandler(ArNTIOPUIEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace, ShortInt iObjID);
	void		UnRegEvtHandler(ShortInt iObjID);

public:
	LONGSTR	m_strRegChannel;

private:
	ArNT_BOOL					m_bEnableTrace;	
	ArNTIOPUIEvtHandler*		m_pEvtHandler;
	//---------------------------------------------------
	//消息处理函数
	DECL_IOP_UI_MESSAGE(ArNTIOPUIMsgHandler)
	
	void	ProcessIOPAckReadImage(PIOPAckReadImage pPara, StrPointer strSenderID);
	void    ProcessIOPAckCmdList(PIOPAckCmdList pPara, StrPointer strSenderID);
	void    ProcessIOPAckSupportType(PIOPAckSupportType pPara, StrPointer strSenderID);
	void    ProcessIOPAckRunInfo(PIOPAckRunInfo pPara,  StrPointer strSenderID);
	void    ProcessIOPAckSearchImage(PIOPAckSearchImage pPara,  StrPointer strSenderID);
	void    ProcessIOPAckService(PIOPAckService pPara, StrPointer strSenderID);
	void    ProcessIOPAckErrorList(PIOPAckErrorList pPara, StrPointer strSenderID);
	void    ProcessIOPNtfError(PIOPNtfError pPara, StrPointer strSenderID);

	//----------------------------------------------------
	//请求函数

	void	AskReadImage(LONGSTR& strFromChannel, StrPointer strType, StrPointer strDesc, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	void	AskWriteImage(LONGSTR& strFromChannel, ArNTImageDesc& ImageDesc, ShortInt iContextSize, PBufferByte pContex, PRawCaptureImage pImage, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	void	AskCmdList(LONGSTR& strFromChannel,StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	void	AskSupportType(LONGSTR& strFromChannel,StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	void	AskAppRunInfo(LONGSTR& strFromChannel,StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	void	AskServerice(LONGSTR& strFromChannel, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	void	AskSearchImage(LONGSTR& strFromChannel, StrPointer strType, StrPointer strCondition, StrPointer strSenderID = NULL, ShortInt iObjID = -1);
	void    AskErrorList(LONGSTR& strFromChannel,StrPointer strSenderID = NULL, ShortInt iObjID = -1);
};

//////////////////////////////////////////////////////////////////////////////////////
//宏定义
#define BEGIN_IOPUIEVT_NEST(classobj) class  ArNTIOPUIEvt:public ArNTIOPUIEvtHandler{ 

#define DECL_IOPUI_ALL_FUNC()		DECL_IOPUI_ACK_READIMAGE()\
									DECL_IOPUI_ACK_CMDLIST()\
									DECL_IOPUI_ACK_SUPPORTTYPE()\
									DECL_IOPUI_ACK_RUNINFO()\
									DECL_IOPUI_ACK_SEARCHIMAGE()\
									DECL_IOPUI_ACK_SERVICE()\
									DECL_IOPUI_ACK_ERRORLIST()\
									DECL_IOPUI_NTF_ERRORINFO();

#define DECL_IOPUI_ACK_READIMAGE()		public: void	OnIOPAckReadImage(StrPointer strType, ShortInt iContextSize, PBufferByte ImgageContex, PRawCaptureImage pImage, StrPointer strSenderID);
#define DECL_IOPUI_ACK_CMDLIST()		public: void	OnIOPAckCmdList(PArNTCommandDesc pCmd, ShortInt iCmdNum, StrPointer strSenderID);
#define DECL_IOPUI_ACK_SUPPORTTYPE()	public:	void	OnIOPAckSupportType(StrPointer strAppName, ShortInt iTypeNum, PArNTImageSupportType pTypes, StrPointer strSenderID);
#define DECL_IOPUI_ACK_RUNINFO()		public: void	OnIOPAckRunInfo(StrPointer strAppName, PArNTRunInfo pRunInfo, ShortInt iInfoNum, StrPointer strSenderID);
#define DECL_IOPUI_ACK_SEARCHIMAGE()	public:	void	OnIOPAckSearchImage(PArNTImageDesc pDesc, ShortInt iImgNum, StrPointer strSenderID);
#define DECL_IOPUI_ACK_SERVICE()		public: void	OnIOPAckService(StrPointer strApp, StrPointer strVerison, StrPointer strSenderID);
#define DECL_IOPUI_ACK_ERRORLIST()		public: void	OnIOPAckErrorList(StrPointer strAppName, PArNTErrorInfo pError, ShortInt iItemNum, StrPointer strSenderID);
#define DECL_IOPUI_NTF_ERRORINFO()      public: void	OnIOPNTFErrorInfo(StrPointer strAppName, StrPointer strCmdName, ArNT_ERROR_CODE Code, StrPointer strError, StrPointer strSenderID);

#define END_IOPUIEVT_NEST(classobj)		};\
										private:	ArNTIOPUIEvt				m_IOPUIEvtHandler;\
										private:    ArNTIOPUIMsgHandler			m_IOPUIMsgHandler;\
										public:     ArNTIOPUIMsgHandler*		GetIOPUIMsgHandler() {return &m_IOPUIMsgHandler;};

#define  NO_OBJ_ID   -1
#define  REG_IOPUIEVT_NEST(bEnableTrace, ObjID)  m_IOPUIEvtHandler.RegParent(this);\
								if(ArNT_FALSE == m_IOPUIMsgHandler.RegEvtHandler(&m_IOPUIEvtHandler, bEnableTrace, ObjID))\
								{return ArNT_FALSE;}

#define  UNREG_IOPUIEVT_NEST(ObjID) m_IOPUIMsgHandler.UnRegEvtHandler(ObjID);

//定义实现类
#define  IMPL_IOPUI_ALL_FUNC(classobj)		IMPL_IOPUI_ACK_READIMAGE(classobj)\
											IMPL_IOPUI_ACK_CMDLIST(classobj)\
											IMPL_IOPUI_ACK_RUNINFO(classobj)\
											IMPL_IOPUI_ACK_SEARCHIMAGE(classobj)\
											IMPL_IOPUI_ACK_SERVICE(classobj)\
											IMPL_IOPUI_ACK_SUPPORTTYPE(classobj)\
											IMPL_IOPUI_ACK_ERRORLIST(classobj)\
											IMPL_IOPUI_NTF_ERRORINFO(classobj)


#define  IMPL_IOPUI_ACK_READIMAGE(classobj)		void classobj::ArNTIOPUIEvt::OnIOPAckReadImage(StrPointer strType, ShortInt iContextSize, PBufferByte ImageContext, PRawCaptureImage pImage, StrPointer strSenderID) {\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnIOPAckReadImage(strType, iContextSize, ImageContext, pImage, strSenderID);}}

#define  IMPL_IOPUI_ACK_CMDLIST(classobj)		void classobj::ArNTIOPUIEvt::OnIOPAckCmdList(PArNTCommandDesc pCmd, ShortInt iCmdNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnIOPAckCmdList(pCmd, iCmdNum, strSenderID);}}

#define  IMPL_IOPUI_ACK_SUPPORTTYPE(classobj)		void classobj::ArNTIOPUIEvt::OnIOPAckSupportType(StrPointer strAppName, ShortInt iTypeNum, PArNTImageSupportType pTypes, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnIOPAckSupportType(strAppName, iTypeNum, pTypes, strSenderID);}}

#define  IMPL_IOPUI_ACK_RUNINFO(classobj)		void classobj::ArNTIOPUIEvt::OnIOPAckRunInfo(StrPointer strAppName, PArNTRunInfo pRunInfo, ShortInt iInfoNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnIOPAckRunInfo(strAppName, pRunInfo, iInfoNum, strSenderID);}}

#define  IMPL_IOPUI_ACK_SEARCHIMAGE(classobj)	void classobj::ArNTIOPUIEvt::OnIOPAckSearchImage(PArNTImageDesc pDesc, ShortInt iImgNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnIOPAckSearchImage(pDesc, iImgNum, strSenderID);}}

#define IMPL_IOPUI_ACK_SERVICE(classobj)	void classobj::ArNTIOPUIEvt::OnIOPAckService(StrPointer strApp, StrPointer strVerison,  StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnIOPAckService(strApp, strVerison, strSenderID);}}

#define IMPL_IOPUI_ACK_ERRORLIST(classobj)	void classobj::ArNTIOPUIEvt::OnIOPAckErrorList(StrPointer strAppName, PArNTErrorInfo pError, ShortInt iItemNum, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnIOPAckErrorList(strAppName, pError, iItemNum, strSenderID);}}

#define IMPL_IOPUI_NTF_ERRORINFO(classobj)  void classobj::ArNTIOPUIEvt::OnIOPNTFErrorInfo(StrPointer strAppName, StrPointer strCmdName, ArNT_ERROR_CODE Code, StrPointer strError, StrPointer strSenderID){\
									classobj* pParent = (classobj*)this->m_pParent;\
									if(pParent){\
									pParent->OnIOPNTFErrorInfo(strAppName, strCmdName, Code,  strError, strSenderID);}}


//==============================================================================
///链接信息
#ifndef  NOAUTOLINK_IOPUIHELPER
#define  NOAUTOLINK_IOPUIHELPER
#pragma comment( lib, "ArNTIOPHelper.lib" )

#endif			