#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTUserManager.h
// 作  者 ：杨朝霖
// 用  途 ：定义了常用的对用户进行操作的函数
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖      2013-02-21	创建模块

//==============================================================================
///</ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
///</header_info>

//==============================================================================
///<export_info>
//==============================================================================
//dll中导出的函数
extern "C"{

///<func_info>
//描述：验证输入的用户信息是否正确
//参数：StrPointer strUser	用户名
//StrPointer strPassWd		用户密码
//返回值：ArNT_TRUE表示用户信息正确，ArNT_FALSE表示用户信息不正确
ArNT_BOOL __stdcall ArNTVerifyUser(StrPointer strUser, StrPointer strPassWd);
///</func_info>

///<func_info>
//描述：添加新用户
//参数：StrPointer strUser		用户名
//StrPointer strPassWd			用户密码
//tagArNTUserInfo::ArNTRank		用户级别
//返回值：ArNT_TRUE表示用户添加成功，ArNT_FALSE表示用户添加失败。
ArNT_BOOL __stdcall ArNTAddUser(StrPointer strUser, StrPointer strPassWd,tagArNTUserInfo::ArNTRank Rank );
///</func_info>

///<func_info>
//描述：删除用户
//参数：StrPointer strUser		要删除的用户名
//返回值：ArNT_TRUE表示用户删除成功，ArNT_FALSE表示用户删除失败。
ArNT_BOOL __stdcall ArNTDeleteUser(StrPointer strUser);
///</func_info>


///<func_info>
//描述：获取用户级别
//参数：StrPointer strUser			用户名
//		StrPointer strPassWd        用户密码
//tagArNTUserInfo::ArNTRank& Rank 返回的用户级别
//返回值：ArNT_TRUE表示成功查找到用户，ArNT_FALSE表示无法查找到用户。
ArNT_BOOL __stdcall ArNTGetUserRank(StrPointer strUser, StrPointer strPassWd, tagArNTUserInfo::ArNTRank& Rank );
///</func_info>

///<func_info>
//描述：获取用户列表
//参数：ShortInt&  iUserNum			返回的用户数量
//		PArNTUserInfo pInfo			如果不为NULL，则返回用户的信息列表
//返回值：ArNT_TRUE表示成功获取用户列表，ArNT_FALSE表示获取用户列表失败！
ArNT_BOOL __stdcall ArNTGetUserList(ShortInt&  iUserNum, PArNTUserInfo pInfo);
///</func_info>

}
//==============================================================================
///</export_info>


//==============================================================================
///链接信息
#ifndef  AUTOLINK_USER_MANAGE
#define  AUTOLINK_USER_MANAGE
#pragma comment( lib, "ArNTUserManager.lib" )

#endif