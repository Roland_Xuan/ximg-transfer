#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：LocalConfigProtocal.h
// 作  者 ：杨朝霖
// 用  途 ：定义与本地配置进程通讯的协议
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖     2010-8-10    创建

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNTIMGPlatFormDataStruct.h"
#include "ArNTIPCModule.h"
///</header_info>

///<datastruct_info>
//==============================================================================
// 基本数据类型定义
///</datastruct_info>

//==============================================================================
///<macro_info>
#define LCP				L"LocalConfig_0A33ED09-3203-4d23-99AA-C499408F48D8"
#define LCPUI			L"LocalConigUI_A4DE295F-6F6C-46ad-B97B-05CADF580FD3"
#define LPC_START_CMD			100

#define DECL_LPC_MESSAGE(classobj,channel) private: static void __stdcall classobj::MessageFunc##channel(ULongInt iMemSize, PMemPointer pMem, PMemPointer pContext);\
											ArNT_BOOL  classobj::RegMessageFunc##channel();\
											void  classobj::UnRegMessageFunc##channel();

#define DECL_LCP_LOCAL_MESSAGE(classobj)	DECL_LPC_MESSAGE(classobj, LCP)
#define DECL_LCP_UI_MESSAGE(classobj)		DECL_LPC_MESSAGE(classobj, LCPUI)		

#define BEGIN_LCP_MESSAGE(classobj, channel, channelsize)  ArNT_BOOL  classobj::RegMessageFunc##channel(){\
									ArNTIPCInfo Info = {sizeof(ArNTIPCInfo)};\
									CCommonFunc::SafeStringCpy(Info.strChannelName, STR_LEN(Info.strChannelName), channel);\
									ArNT_ERROR_CODE code = ArNTIPCRegChannel(Info, MessageFunc##channel, this); \
									return code == SUCCESS_IPC_OPERCHANNEL;}\
									void  classobj::UnRegMessageFunc##channel() { ArNTRemoveChannel(channel);}\
									void __stdcall classobj::MessageFunc##channel(ULongInt iMemSize, PMemPointer pMem, PMemPointer pContext) {\
	                                PArNTCommand pCmd = (PArNTCommand)pMem;\
									if(iMemSize != pCmd->iTotalSize) return;\
									classobj* pParent = (classobj*)pContext;


#define BEGIN_LCP_LOCAL_MESSAGE(classobj) BEGIN_LCP_MESSAGE(classobj, LCP, 1024 * 6)
#define BEGIN_LCP_UI_MESSAGE(classobj)  BEGIN_LCP_MESSAGE(classobj, LCPUI, 1024 * 1024)
#define LPC_MESSAGE_ENTRY(id, func, para) if(pCmd->iID == id) {  \
										if(pCmd->bAlloc == ArNT_FALSE)\
										 { \
											pParent->func((para*)pCmd->buffer.Reserve,  pCmd->strSenderID);\
										}else{\
										pParent->func((para*)pCmd->buffer.Alloc, pCmd->strSenderID);};\
										return;}
#define END_LPC_MESSAGE(classobj) };


//==============================================================================
///</macro_info>

//==============================================================================
///<message>
#define LCP_ASK_RUNINFO										(LPC_START_CMD + 1)		
#define  LCP_ASK_RUNINFO_DESC								L"询问当前程序的运行信息"
typedef struct tagLCPAskRunInfo
{
	LongInt iSize;
	LONGSTR strFromChannel;
}LCPAskRunInfo, *PLCPAskRunInfo;


#define LCP_ACK_RUNINFO										(LPC_START_CMD + 2)		
#define  LCP_ACK_RUNINFO_DESC								L"回复当前程序的运行信息"
typedef struct tagLCPAckRunInfo
{
	LongInt iSize;
	LONGSTR strAppName;
	ShortInt iInfoNum;
	ArNTRunInfo RunInfo[1];
}LCPAckRunInfo, *PLCPAckRunInfo;

#define LCP_ASK_CMDLIST										(LPC_START_CMD + 3)	
#define LCP_ASK_CMDLIST_DESC								L"询问支持命令列表"
typedef struct tagLCPAskCmdList
{
	LongInt iSize;
	LONGSTR strFromChannel;
}LCPAskCmdList, *PLCPAskCmdList;

#define LCP_ACK_CMDLIST										(LPC_START_CMD + 4)	
#define LCP_ACK_CMDLIST_DESC								L"回复支持命令列表"
typedef struct tagLCPAckCmdList
{
	LongInt iSize;
	ShortInt iCmdNum;
	ArNTCommandDesc Desc[1];
}LCPAckCmdList, *PLCPAckCmdList;

#define LCP_ASK_SYSPARA										(LPC_START_CMD + 5)
#define LCP_ASK_SYSPARA_DESC								L"询问指定名称的系统参数"
typedef struct tagLCPAskSysPara
{
	LongInt iSize;
	LONGSTR strName;
	LONGSTR strFromChannel;
}LCPAskSysPara, *PLCPAskSysPara;

#define LCP_ACK_SYSPARA										(LPC_START_CMD + 6)
#define LCP_ACK_SYSPARA_DESC								L"回复指定名称的系统参数(文本方式)"
typedef struct tagLCPAckSysPara
{
	LongInt iSize;
	LONGSTR strName;
	LONGSTR strValue;
}LCPAckSysPara, *PLCPAckSysPara;

#define LCP_ASK_CHANGE_SYSPARA								(LPC_START_CMD + 7)
#define LCP_ASK_CHANGE_SYSPARA_DESC							L"请求更改指定名称的系统参数"
typedef struct tagLCPAskChangeSysPara
{
	LongInt iSize;
	LONGSTR strFromChannel;
	LONGSTR strName;
	LONGSTR strValue;
}LCPAskChangeSysPara, *PLCPAskChangeSysPara;

#define LCP_ASK_SERVICE										(LPC_START_CMD + 8)		
#define LCP_ASK_SERVICE_DESC								L"查询支持LCP协议的服务"
typedef struct tagLCPAskService
{
	LongInt iSize;
	LONGSTR strFromChannel;
}LCPAskService, *PLCPAskService;

#define LCP_ACK_SERVICE										(LPC_START_CMD + 9)		
#define LCP_ACK_SERVICE_DESC								L"回复支持LCP协议的服务"
typedef struct tagLCPAckService
{
	LongInt iSize;
	ArNServiceAppInfo Service;	
}LCPAckService, *PLCPAckService;

#define  LCP_ASK_ERROR_LIST									(LPC_START_CMD + 10)
#define  LCP_ASK_ERROR_LIST_DESC							L"询问服务的错误列表"
typedef struct tagLCPAskErrorList
{
	LongInt iSize;
	LONGSTR strFromChannel;
}LCPAskErrorList, *PLCPAskErrorList;

#define  LCP_ACK_ERROR_LIST									(LPC_START_CMD + 11)
#define  LCP_ACK_ERROR_LIST_DESC							L"回复服务的错误列表"
typedef struct tagLCPAckErrorList
{
	LongInt iSize;
	LONGSTR strAppName;
	ShortInt iErrorNum;	
	ArNTErrorInfo ErrorInfo[1];
}LCPAckErrorList, *PLCPAckErrorList;

#define LCP_NTF_ERROR										(LPC_START_CMD + 12)	
#define LCP_NTF_ERROR_DESC									L"通知错误信息"
typedef struct tagLCPNtfError
{
	LongInt iSize;
	LONGSTR strAppName;
	ArNT_ERROR_CODE  code;
	SHORTSTR strCmdName;
	LONGSTR  strErrorInfo;
}LCPNtfError, *PLCPNtfError;

#define LCP_ASK_ADD_SYSPARA									(LPC_START_CMD + 13)
#define LCP_ASK_ADD_SYSPARA_DESC							L"请求添加指定名称的系统参数"
typedef struct tagLCPAskAddSysPara
{
	LongInt iSize;
	LONGSTR strFromChannel;
	LONGSTR strName;
	LONGSTR strValue;
}LCPAskAddSysPara, *PLCPAskAddSysPara;

#define LCP_ASK_DEL_SYSPARA									(LPC_START_CMD + 14)
#define LCP_ASK_DEL_SYSPARA_DESC							L"请求删除指定名称的系统参数"
typedef struct tagLCPAskDelSysPara
{
	LongInt iSize;
	LONGSTR strFromChannel;
	LONGSTR strName;
	LONGSTR strValue;
}LCPAskDelSysPara, *PLCPAskDelSysPara;











//==============================================================================
///</message>