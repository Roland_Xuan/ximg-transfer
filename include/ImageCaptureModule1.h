#pragma once
#include "ArNTIMGPlatFormDataStruct.h"

typedef struct tagICMCameraInfo
{
	SHORTSTR	strID;			//相机的ID
	ShortInt	iChannelNum;	//相机的通道数
	ShortInt	iWidth;			//相机的宽度
	ShortInt	iHeight;		//相机的高度	
}ICMCameraInfo, *PICMCameraInfo;

typedef void			(*POnCaptureImageFuncDef)(PRawCaptureImage pRawCaptureImage, PObjPointer pContext);



class IBKCameraInterface
{
public:

virtual		ArNT_BOOL   Init(LONGSTR& strError) = 0;
virtual		ArNT_BOOL   Clear(void)= 0;
virtual		ArNT_BOOL	RestartCapture(void)=0;
virtual		ShortInt    GetCameraNum(void)= 0;
virtual		ArNT_BOOL   GetCameraInfo(ShortInt iIndex, ICMCameraInfo& Info)= 0;
virtual		ArNT_BOOL	ChangeCamExptime(StrPointer strID, ShortInt iExpTime)= 0;
virtual		ArNT_BOOL   GetCamExptime(StrPointer strID, ShortInt& iExpTime)= 0;
virtual		ArNT_BOOL	ChangeCamRate(StrPointer strID, ShortInt iRate)= 0;
virtual		ArNT_BOOL	GetCamRate(StrPointer strID, ShortInt& iRate)= 0;
virtual		ArNT_BOOL   GetCamTemperature(StrPointer strID, ShortInt& Temperature)= 0;
virtual		void		RegOnCaptureFunc(POnCaptureImageFuncDef  pFunc, PObjPointer pContext)= 0;
virtual		ArNT_BOOL	Start() = 0;
virtual		ArNT_BOOL   Stop() = 0;
};

ArNT_BOOL __stdcall  InitICMModuel(IBKCameraInterface** pInterface, LONGSTR& strError);
void	__stdcall	 GetICMModuleDesc(LONGSTR& strDesc);

typedef ArNT_BOOL  (__stdcall *PInitICMModuelDef)(IBKCameraInterface** pInterface, LONGSTR& strError);
typedef void	(__stdcall*	PGetICMModuleDescDef)(LONGSTR& strDesc);




