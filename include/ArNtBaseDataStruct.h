#ifndef ArNtBassDataStruct_H
#define ArNtBassDataStruct_H

///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTBaseDataStruct.h
// 作  者 ：杨朝霖
// 用  途 ：定义了ArNT系统中所用的的基本数据结构
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖       2012-07-9    创建模块

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include <windows.H>
///</header_info>

///<datastruct_info>
//==============================================================================
// 基本数据类型定义

typedef __int32				ModuleHandle;
typedef __int32				MsgID;
typedef __int32				MsgLParam;
typedef __int32				MsgWParam;
typedef __int8				TinyInt;
typedef __int8*				PTinyInt;
typedef unsigned __int8		UTinyInt;
typedef unsigned __int8*	PUTinyint;
typedef __int16				ShortInt;
typedef __int16*			PShortInt;
typedef unsigned __int16	UShortInt;
typedef unsigned __int16*	PUShortInt;
typedef __int16				TaskID; 
typedef __int32				LongInt;
typedef __int32*			PLongInt;
typedef unsigned __int32    ULongInt;
typedef unsigned __int32*   PULongInt;
typedef  unsigned __int16	NetPort;
typedef __int64				LongLongInt;
typedef unsigned __int64    ULongLongInt;
typedef __int64*			PLongLongInt;
typedef unsigned __int64*   PULongLongInt;
typedef double				LongFloat;
typedef double*				PLongFloat;
typedef unsigned char		ImgDataType;
typedef unsigned char*		PImgDataType;
typedef unsigned char		BufferByte; 
typedef unsigned char*		PBufferByte; 
typedef bool				MsgBool;
typedef void*				PObjPointer;
typedef void*				PMemPointer;
typedef char				Byte;
typedef unsigned char		UByte;
typedef char				ANSIChar;
typedef char*				PByteBuffer;
typedef wchar_t				TINYSTR[32];
typedef TINYSTR*			PTINYSTR;
typedef wchar_t				FILENAMESTR[64];
typedef FILENAMESTR*        PFILENAMESTR;
typedef wchar_t				SHORTSTR[128];
typedef SHORTSTR*			PSHORTSTR;
typedef wchar_t				LONGSTR[256];
typedef LONGSTR*			PLONGSTR;
typedef wchar_t				HUGESTR[512];


typedef HUGESTR*            PHUGESTR;
typedef wchar_t             SENDERID[64];
typedef SENDERID*			PSENDERID;
typedef wchar_t				NetIP[16];
typedef NetIP*				PNetIP;
typedef wchar_t*			StrPointer;
typedef wchar_t				StrChar;
typedef wchar_t				WideChar;
typedef DWORD               ArNT_ERROR_CODE;
typedef bool                ArNT_BOOL;
typedef __int32				ArNT_HANDLE;   
typedef unsigned long		IPv4Addr;
typedef unsigned long*		PIPv4Addr;
typedef unsigned short		NetPORT;
typedef unsigned short*		PNetPORT;




#pragma pack(push, 4)
typedef struct tagModuleInfo
{
	LongInt			iSize;
	LONGSTR			strFullName;
	HMODULE			hDll; //装载模块时的句柄
	TINYSTR			strDate;
	SHORTSTR		strDevelop;
	LONGSTR			strDesc;//模块描述
	LARGE_INTEGER 	iModuleSize; //模块的大小
	ArNT_BOOL		bInitial; //模块是否已经初始化
	LONGSTR			strErrorInfo;//模块错误的原因
	PMemPointer		pFunc[64]; //预留的64个参数,用于保存动态加载得到的Dll中函数指针
}ModuleInfo, *PModuleInfo;

typedef struct tagModuleInfoSet
{
	LongInt iSize;
	enum {MAX_ITEMNUM = 128}; //总共允许的模块数量
	ModuleInfo  Items[MAX_ITEMNUM];
	ShortInt	iItemNum;
	ShortInt    iCurrSel;
}ModuleInfoSet, *PModuleInfoSet;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct tagArNTCommand
{
	LongInt iSize;
	LongInt iID; //命令的ID
	LongInt iDataSize; //携带数据的尺寸
	LongInt iTotalSize; //命令对象的总尺寸，包括命令头
	PBufferByte pStartAddr;	//命令携带数据的首地址
	ArNT_BOOL   bAlloc;	//是否分配了新空间来存放数据,ArNT_TRUE表示分配了新空间
	SENDERID    strSenderID; //发送命令方的描述信息
	enum {MAX_RESERVE_SIZE = 2048};//预留的数据空间大小
	union Buffer 
	{
		BufferByte Reserve[MAX_RESERVE_SIZE];//预留的数据空间
		BufferByte Alloc[1];		//分配的数据空间位置
	}buffer;
}ArNTCommand, *PArNTCommand;


typedef struct tagArNTCommandDesc
{
	ShortInt    iID;  //命令ID
	TINYSTR		strName; //命令的名称
	SHORTSTR    strDesc; //命令的描述
}ArNTCommandDesc, *PArNTCommandDesc;

typedef struct tagArNTCommandDescDef
{
	ShortInt      iID;  //命令ID
	StrChar*    strName; //命令的名称
	StrChar*    strDesc; //命令的描述
}ArNTCommandDescDef, *PArNTCommandDescDef;

typedef struct tagArNTRunInfo
{
	LongInt iSize;
	TINYSTR strTitle;//信息标题
	TINYSTR strTime; //信息添加时间
	SHORTSTR strInfo;//信息内容
}ArNTRunInfo, *PArNTRunInfo;

typedef struct tagArNTCameralInfo
{
	LongInt iSize;
	SHORTSTR strID;				//相机ID
	ShortInt  iChannelNum;		//相机通道数量
	ShortInt  iImgWidth;		//相机图像宽度
	ShortInt  iImgWidthStep;	//相机图像每行实际所用字节数
	ShortInt  iImgHeight;		//相机图像高度
}ArNTCameralInfo, *PArNTCameralInfo;

typedef struct tagArNTCameralStatus
{
	LongInt iSize;
	SHORTSTR strID;			//相机ID
	LongFloat dExpTime;		//相机曝光时间
	LongFloat dGain;		//相机增益
	LongFloat dTemperature; //相机温度
	LongInt	  iLineRate;	//相机线速率
	LongFloat dCaptureSpeed;//当前相机的采集线速度	 
}ArNTCameralStatus, *PArNTCameralStatus;

typedef struct tagArNServiceAppInfo
{
	LongInt iSize;
	LONGSTR strAppName;//服务名
	LONGSTR strVersion;//当前版本信息
}ArNServiceAppInfo, *PArNServiceAppInfo;

typedef struct tagArNTUserInfo
{
	LongInt iSize;
	TINYSTR strName;	//用户名
	TINYSTR strPassWd;	//用户密码
	enum ArNTRank{enNormal = 0, enHigh = 1, enAddmin = 2, enRoot = 3};//用户级别定义
	ArNTRank  Rank;//用户级别
}ArNTUserInfo, *PArNTUserInfo;

//定义操作与用户权限的关系
typedef struct tagArNTUserOperationRank
{
	PMemPointer pFunc;		//操作的函数地址
	StrPointer  strFunc;	//函数名
	StrPointer  strDesc;	//函数描述
	tagArNTUserInfo::ArNTRank PermitRank;//所需要的用户级别
}ArNTUserOperationRank, *PArNTUserOperationRank;

//名称:ArNTSysTime
//描述:该结构体用于描述系统时间
typedef struct tagArNTSysTime
{
	LongInt		iSize;
	ShortInt    iYear;
	TinyInt		iMonth;
	TinyInt		iDay;
	TinyInt     iDayOfWeek;
	TinyInt		iHour;
	TinyInt		iMinute;
	TinyInt		iSecond;
	ShortInt	iMillSecond;
}ArNTSysTime, *PArNTSysTime;

typedef struct tagArNTRectInfo
{
	LongInt iSize;
	LongInt iLeft;
	LongInt iRight;
	LongInt iTop;
	LongInt iBottom;
}ArNTRectInfo, *PArNTRectInfo;
typedef struct tagArNTPointInfo
{
	LongInt iSize;
	LongInt iX;
	LongInt iY;
}ArNTPointInfo, *PArNTPointInfo;

typedef struct tagGrayPixelInfo
{
	UShortInt iX;
	UShortInt iY;
	UTinyInt  iValue;
}GrayPixelInfo, *PGrayPixelInfo;
#pragma pack(pop)

///</datastruct_info>

///<macro_info>
//==============================================================================
//常用宏定义
//<macro_info>
#define ARNT_CALL   __stdcall
#define STR_LEN(x)  (sizeof(x)/sizeof(x[0]))

#define MaxULongInt   ((ULongInt)0xFFFFFFFF)
#define MaxLongInt    ((LongInt) 0x7FFFFFFF)
#define ArNT_TRUE      true
#define ArNT_FALSE     false
#define ArNT_NO_PARAM  void
#define GetUserNameInSenderID(ID) (ID + 32)


#define  BEGIN_CMD_LIST(type)   static ArNTCommandDescDef gCmdDescList##type[] = {
#define  CMD_ENTRY(ID) {ID, L#ID, ID##_DESC},
#define  END_CMD_LIST {0, NULL, NULL}};

#define  BEGIN_OPER_RANK(type) static ArNTUserOperationRank gUOR##type[] = {
#define  OPER_RANK_ENTRY(pFunc, strFunc, strDesc, rank) {pFunc, L#strFunc, strDesc, rank},
#define  END_OPER_RANK()	{NULL, NULL, NULL, ArNTUserInfo::enNormal}};


#define WIDEN2(x) L ## x
#define WIDEN(x) WIDEN2(x)
#define __WFILE__ WIDEN(__FILE__)
#define __WFUNCTION__ WIDEN(__FUNCTION__)
#ifdef _UNICODE

#define __TFILE__ __WFILE__
#define __TFUNCTION__ __WFUNCTION__
#else
#define __TFILE__ __FILE__
#define __TFUNCTION__ __FUNCTION__
#endif
#define __STR2__(x) #x
#define __STR1__(x) __STR2__(x)

#define __LOC2__ __FILE__ "("__STR1__(__LINE__)") "
#define __FLOC__ __TFILE__ "("__STR1__(__LINE__)"): "
#define __MLOC__ __TFUNCTION__ "("__STR1__(__LINE__)"): "
#define _WFUNCTION_  WIDEN(__FUNCTION__)

//错误码定义
//31-30:Severity 0=Success 1 = Informational 2 = Warning 3 = Error
//29:Microsoft/customer 0/1
//28:0
//27-16:Facility code
//15-0:Exception code
//备注：检测平台核心模块的Faility在1000一下，平台默认服务在1000-2000之间，用户扩展服务在2000以上
#define  MAKE_ERROR_CODE(Facility, Code)  (ArNT_ERROR_CODE)(0xE0000000 + (Facility<< 16) + Code)
#define  MAKE_SUCCESS_CODE(Facility, Code)(ArNT_ERROR_CODE)(0x20000000 + (Facility<< 16) + Code)
#define  ERRORCODE_SUCCESS(Code) (((LongInt)Code) > 0)
#define  ERRORCODE_ERROR(Code) (((LongInt)Code) <= 0)
#pragma pack(push, 1)
typedef struct tagArNTErrorDesc
{
	ArNT_ERROR_CODE code;//错误码
	StrChar*    strName; //错误名
	StrChar*    strDesc; //错误描述
}ArNTErrorDesc, *PArNTErrorDesc;


typedef struct tagArNTErrorInfo
{
	ArNT_ERROR_CODE code;	//错误码
	TINYSTR			strName;//错误名
	LONGSTR			strDesc;//错误描述
}ArNTErrorInfo, *PArNTErrorInfo;

typedef struct tagArNTWidthEdge
{
	ShortInt iLeftPos;
	ShortInt iRightPos;
	LongInt iLen;
}ArNTWidthEdge, *PArNTWidthEdge;

typedef struct tagArNTOutlineItem
{
	LongInt iX;
	LongInt iY;
}ArNTOutlineItem, *PArNTOutlineItem;

typedef struct tagArNTEdgePoint
{
	LongInt iMin;
	LongInt iMax;
	TinyInt iPtNum;
}ArNTEdgePoint, *PArNTEdgePoint;

typedef struct tagArNTParamByGroup
{
	TINYSTR strGroupName;
	LONGSTR strGroupDesc;
	enum {MAX_ITEM_NUM = 256};
	LongInt iItemNum;
	TINYSTR strName[MAX_ITEM_NUM];
	LONGSTR strValue[MAX_ITEM_NUM];
	LONGSTR strDesc[MAX_ITEM_NUM];
}ArNTParamByGroup, *PArNTParamByGroup;



#pragma pack(pop)

#define  DECL_ERROR_LIST(type)	  extern PArNTErrorDesc type##ErrorList;\
	extern  ShortInt giErrorNum_##type;								  
#define  BEGIN_ERROR_LIST(type)   static ArNTErrorDesc gErrorList_##type[] = {
#define  ERROR_ENTRY(erro, desc) {erro, L#erro, desc},
#define  ERROR_ENTRY_SIMPLE(erro) {erro, L#erro, erro##_DESC},
#define  END_ERROR_LIST(type) {0, NULL, NULL}};\
						ShortInt giErrorNum_##type = (sizeof(gErrorList_##type) / sizeof(gErrorList_##type[0])) - 1;\
						PArNTErrorDesc type##ErrorList = &gErrorList_##type[0];





#define  ArNT_SAFE_DELETE(pointer) if(pointer) {delete pointer; pointer = NULL;};
#define  ArNT_SAFE_DELETE_FILE(handle) if(handle != INVALID_HANDLE_VALUE) {::CloseHandle(handle); handle = INVALID_HANDLE_VALUE;}

//用于实现单一实例的创建与销毁
//需要在状态类的类声明中添加
#define		DECL_SINGLE_STATE(classobj) public: static classobj* GetState(); \
	                                 static classobj* m_pState;
//需要在状态类的实现文件中添加
#define		IMPL_SINGLE_STATE(classobj) classobj* classobj##::m_pState = NULL;	classobj* classobj##::GetState() { if(NULL == m_pState) m_pState = new classobj();	return m_pState;}

//需要在状态类的类声明中添加
#define		DECL_SINGLE_STATE_DESTROY(classobj) public: static void DestroyState();\
											public: static ArNT_BOOL   m_bExitApp;
//需要在状态类的实现文件中添加
#define		IMPL_SINGLE_STATE_DESTROY(classobj) ArNT_BOOL   classobj::m_bExitApp = ArNT_FALSE;		 void classobj##::DestroyState() {if(m_pState){ delete m_pState; m_pState = NULL;};}

//在单一实例类的类声明中添加
#define		DECL_SINGLE_OBJECT(classobj)  public: static classobj* GetSingleObject(); \
										  public: static classobj* m_pObject;
//在单一实例类的实现文件中添加
#define		IMPL_SINGLE_OBJECT(classobj) classobj* classobj##::m_pObject = NULL;	classobj* classobj##::GetSingleObject() { if(NULL == m_pObject) m_pObject = new classobj();	return m_pObject;}
//在单一实例类的类声明中添加
#define		DECL_SINGLE_OBJECT_DESTROY(classobj) public: static void DestroyObject();										
//在单一实例类的实现文件中添加
#define		IMPL_SINGLE_OBJECT_DESTROY(classobj)  void classobj##::DestroyObject() {if(m_pObject){ delete m_pObject; m_pObject = NULL;};}

//用于在控制台类程序中将main函数挂起

#define  DECL_WAIT_MAIN(classobj) private: static HANDLE	m_hEvtExit; public: static void	WaitMainExit(void);\
								  public: void  ExitApp();
#define  IMPL_WAIT_MAIN(classobj)   HANDLE	classobj::m_hEvtExit = NULL;\
	void classobj::WaitMainExit(void)\
	{\
	LONGSTR strEvtName = {0};\
	CCommonFunc::SafeStringPrintf(strEvtName, STR_LEN(strEvtName), L"EvtWaitExit%s", L#classobj);\
	if(NULL == m_hEvtExit) m_hEvtExit = CCommonFunc::CreateNullLimetEvent(TRUE, FALSE, strEvtName);\
		::WaitForSingleObject(m_hEvtExit, INFINITE);\
	}\
	void  classobj::ExitApp(){\
		 LONGSTR strEvtName = {0};\
		CCommonFunc::SafeStringPrintf(strEvtName, STR_LEN(strEvtName), L"EvtWaitExit%s", L#classobj);\
		HANDLE hEvtExit = CCommonFunc::CreateNullLimetEvent(TRUE, FALSE, strEvtName);\
		::SetEvent(hEvtExit);\
		::CloseHandle(hEvtExit);};

#define  EXIT_WAIT_MAIN     ::SetEvent(m_hEvtExit);

//在状态类的类声明中添加
#define DECL_CONSOLE_HANDLE_ROUTINE(classobj) private: static BOOL WINAPI HandlerRoutine( DWORD dwCtrlType);
//在状态类的类实现中添加
#define IMPL_CONSOLE_HANDLE_ROUTINE(classobj) BOOL WINAPI classobj##::HandlerRoutine( DWORD dwCtrlType){\
	if(CTRL_CLOSE_EVENT == dwCtrlType){ if(classobj::GetState()->m_bTrace) WRITE_WARN_INSTANT(L"强制退出程序");  classobj::GetState()->AddInfo(L"强制退出程序"); classobj::DestroyState();}\
	return TRUE;}
//用于注册事件响应函数
#define  REG_CONSOLE_HANDLE_ROUTINE() ::SetConsoleCtrlHandler(HandlerRoutine, TRUE);

//在状态类的类声明中添加
#define DECL_CONSOLE_START_RUN(classobj) public: ArNT_BOOL			m_bTrace;\
										 public: LONGSTR			m_strAppPath;\
										 public: LONGSTR			m_strAppPathName;\
										 public: void  StartRun(StrPointer strAppTitle = NULL, ArNT_BOOL bMaxSize = ArNT_TRUE);
//在状态类的类实现中添加
#define IMPL_CONSOLE_START_RUN(classobj)  void  classobj::StartRun(StrPointer strAppTitle, ArNT_BOOL bMaxSize){LONGSTR strInfo = {0};\
	CCommonFunc::GetAppPath(m_strAppPath, STR_LEN(m_strAppPath));\
	CCommonFunc::GetAppPathName(m_strAppPathName, STR_LEN(m_strAppPathName));\
	REG_CONSOLE_HANDLE_ROUTINE();\
	if(CCommonFunc::StrInRunPara(L"-trace")){m_bTrace = ArNT_TRUE;}else{m_bTrace = ArNT_FALSE;}\
	if(CCommonFunc::StrInRunPara(L"-exit")){\
		if(strAppTitle){\
			CCommonFunc::SafeStringPrintf(strInfo,STR_LEN(strInfo), L"请求关闭%s", strAppTitle);}else {\
			CCommonFunc::SafeStringPrintf(strInfo,STR_LEN(strInfo), L"请求关闭%s", L#classobj);}\
			WRITE_NOTICE_INSTANT(strInfo);\
			ExitApp();\
			return;};\
		REG_CONSOLE_HANDLE_ROUTINE();\
		if(CCommonFunc::IsFirstApp(L#classobj) == ArNT_FALSE){\
		LONGSTR strErrorInfo = {0};\
		if(strAppTitle){\
			CCommonFunc::SafeStringPrintf(strErrorInfo,STR_LEN(strErrorInfo), L"已经有一个%s在运行中", strAppTitle);}else {\
			CCommonFunc::SafeStringPrintf(strErrorInfo,STR_LEN(strErrorInfo), L"已经有一个%s在运行中", L#classobj);}\
			if(m_bTrace) WRITE_ERROR_INSTANT(strErrorInfo); AddInfo(strErrorInfo);\
		Sleep(2000);\
		return;}\
		m_ConsoleUI.Init(strAppTitle, 256, 1024, bMaxSize);\
		if(strAppTitle){\
		CCommonFunc::SafeStringPrintf(strInfo,STR_LEN(strInfo), L"%s开始运行", strAppTitle);}else{\
		CCommonFunc::SafeStringPrintf(strInfo,STR_LEN(strInfo), L"%s开始运行", L#classobj);}\
		WRITE_NOTICE(strInfo);\
		LONGSTR strVersion = {0};\
		TINYSTR strFileVer = {0};\
		TINYSTR strProductVer = {0};\
		SHORTSTR strComment = {0};\
		SHORTSTR strCompany = {0};\
		SHORTSTR strProductName = {0};\
		SHORTSTR strOriginalFilename = {0};\
		if(ArNT_FALSE == CCommonFunc::GetVersionInfo(strFileVer, strProductVer, strComment, strCompany, strProductName, strOriginalFilename, NULL)) {\
			CCommonFunc::SafeStringCpy(strVersion, STR_LEN(strVersion), L"程序中没有存放版本资源");	}else {  \
			CCommonFunc::SafeStringPrintf(strVersion, STR_LEN(strVersion), L"%s (%s) %s 版本:%s", \
									 strProductName, \
									 strOriginalFilename,\
									 strComment,\
									 strProductVer); }\
		DisplayInfo(L"程序信息", strVersion);\
		DisplayInfo(L"开发单位", strCompany);\
		DisplayInfo(L"程序路径", m_strAppPath);\
		SHORTSTR strStartTime = {0};		   \
		CCommonFunc::GetNowTime(strStartTime, STR_LEN(strStartTime), ArNT_TRUE);\
		DisplayInfo(L"程序开始运行时间", strStartTime, ArNT_TRUE);\
		if(Init(strAppTitle) == ArNT_FALSE) {\
		if(strAppTitle){\
			CCommonFunc::SafeStringPrintf(strInfo,STR_LEN(strInfo), L"%s初始化出错！", strAppTitle);}else {\
			CCommonFunc::SafeStringPrintf(strInfo,STR_LEN(strInfo), L"%s初始化出错！", L#classobj);}\
		if(m_bTrace) WRITE_ERROR_INSTANT(strInfo); AddInfo(strInfo);\
		return;}\
		WaitMainExit();\
		classobj::DestroyState();}

//在状态类的类声明中添加
#define  DECL_CONSOLE_UI_LOG(classobj)		ArNTConsoleUI		m_ConsoleUI;\
											ArNTRunInfoOper		m_RunInfo;	\
											void  DisplayInfo(StrPointer strName, StrPointer strInfo, ArNT_BOOL bLog = ArNT_FALSE);\
											void  AddInfo(StrPointer strInfo, ArNT_BOOL bLog = ArNT_TRUE);
//在状态类的类实现中添加
#define IMPL_CONSOLE_UI_LOG(classobj)		void  classobj::DisplayInfo(StrPointer strName, StrPointer strInfo, ArNT_BOOL bLog){\
											m_ConsoleUI.DisplayInfo(strName,strInfo, bLog);\
											m_RunInfo.AddRunInfo(strName, strInfo);}\
											void  classobj::AddInfo(StrPointer strInfo, ArNT_BOOL bLog){\
											m_ConsoleUI.AddInfo(strInfo, bLog);\
											m_RunInfo.AddRunInfo(NULL, strInfo);}


		
//在状态类的类声明中添加
#define  DECL_CONSOLE_COMMON_FUNC(classobj) DECL_SINGLE_STATE(classobj) \
											DECL_SINGLE_STATE_DESTROY(classobj)\
											DECL_WAIT_MAIN(classobj)\
											DECL_CONSOLE_HANDLE_ROUTINE(classobj);\
											DECL_CONSOLE_START_RUN(classobj);\
											DECL_CONSOLE_UI_LOG(classobj);
//在状态类的类实现中添加
#define  IMPL_CONSOLE_COMMON_FUNC(classobj)  IMPL_WAIT_MAIN(classobj)\
											 IMPL_CONSOLE_HANDLE_ROUTINE(classobj)\
											 IMPL_SINGLE_STATE(classobj)\
											 IMPL_SINGLE_STATE_DESTROY(classobj)\
											 IMPL_CONSOLE_START_RUN(classobj)\
											 IMPL_CONSOLE_UI_LOG(classobj)
											

//==============================================================================
///</macro_info>
#endif













	
	





