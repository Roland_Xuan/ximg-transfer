/*
* Copyright (c) 2004, 北京科技大学高效轧制国家工程研究中心
* All rights reserved.
* 
* 文件名称：GrayBmp.h
* 摘    要：定义了一些常用的操作函数
* 版本:V3.4
* 版本历史：
* 2009-10-25  添加了=符号重载
* 2009-10-26  添加了获取子图像数据的功能
* 2009-12-15  添加了灰度变换功能
* 2010-01-11 添加对16位数据的支持
* 2010-01-12 添加对指定宽度的8位数据赋值操作
* 2010-11-20 在GrayBmpData中添加获取像素值的函数
* 2015-02-06  在GrayBmpData中添加设置像素值的函数
*/

#pragma once
#include <windows.h>
#include "ArNTCommonClass.h"

#define WIDTHBYTES(i)    ((i * 8 +31) / 32 * 4)


class GrayBmpData
{
public:
	static int ActrualWidth(LongInt iImgWidth);
	void Alloc(LongInt iWidth, LongInt iHeight);
	void Alloc2(LongInt iImgSize);
	void Alloc3(LongInt iWidth, LongInt iHeight);
	void Free(void);
	void Reset(void);
	static void SetHeap(HANDLE hHeap);

	void Colone(GrayBmpData& SrcData);
	void Colone2(GrayBmpData& SrcData);
	void SetData(ImgDataType* pSrcImgData, LongInt iImgWidth, LongInt iImgHeight);
	void SetData(ImgDataType* pSrcImgData, LongInt iImgWidth, LongInt iImgHeight, LongInt iWidthStep);
	void SetData(UShortInt* pSrcImgData, LongInt iImgWidth, LongInt iImgHeight, LongFloat dRation, ArNT_BOOL bMap = ArNT_TRUE);
	void SetData(LongFloat* pSrcImgData, LongInt iImgWidth, LongInt iImgHeight, LongFloat dRation, ArNT_BOOL bMap = ArNT_FALSE);
	void GetSubGrayBmpData(LongInt iLeft, LongInt iTop, LongInt iSubImgWidth, LongInt iSubImgHeight, GrayBmpData& DestData);
	ImgDataType GetPixelValue(LongInt iLeft, LongInt iTop); //iLeft 和 iTop都是从0开始
	void	SetPixelValue(ImgDataType data, LongInt iLeft, LongInt iTop);
	void	ZeroData(void); //将数据清空
	
	ArNT_BOOL ZoomImg(GrayBmpData& data);
	ArNT_BOOL ZoomImg2(GrayBmpData& data);
	LongFloat Aver();

	LongInt			iImgWidth;
	LongInt			iImgHeight;
	LongInt			iActrualImgWidth;
	LongInt			iImgSize;
	ImgDataType*	pImgData;

	static HANDLE hGrayBmpHeap;
	static CTaskCriticalSection csOperBmpHeap;
	static void DestroyHeap(void);
	GrayBmpData();	
	~GrayBmpData();	
};

//灰度变换参数
//线性变换参数
typedef struct tagLineGrayChangePara
{
	LongInt			iStartGray;
	LongInt			iEndGray;
	LongFloat		dRation;
	ArNT_BOOL		bMap; //如果该参数为true,则将图像映射到iStartGray 和 iEndGray的范围
}LineGrayChangePara, *PLineGrayChangePara;

//对数变换参数
typedef struct tagLogGrayChangePara
{
	LongFloat		dConst;
}LogGrayChangePara, *PLogGrayChangePara;
//指数变换参数
typedef struct tagPowGrayChangePara
{
	LongFloat	dConst;
	LongFloat	dPow;
}PowGrayChangePara, *PPowGrayChangePara;


//------------------------------------------------
class CGrayBmp
{
public:

	//----------------------------------------------------------
	//描述：缩放图像
	static bool ZoomImg(ImgDataType* ucImgData, LongInt iImgWidth, LongInt iImgWidthStep, LongInt iImgHeight, GrayBmpData& data);
	static bool ZoomImg2(ImgDataType* ucImgData, LongInt iImgWidth, LongInt iImgWidthStep, LongInt iImgHeight, GrayBmpData& data);

	//----------------------------------------------------------
	//描述：缩放图像
	enum ZoomType{enZoomNear = 0, enZoomBilinear, enZoomCube};
	static bool ZoomImg(GrayBmpData& SrcData, GrayBmpData& data, ZoomType type = enZoomNear);//iType = 0:最近像素插值法
	static bool ZoomImg2(GrayBmpData& SrcData, GrayBmpData& data, ZoomType type = enZoomNear);
	//----------------------------------------------------------
	//描述：垂直旋转图像
	static void Reverse(ImgDataType* ucImgData, LongInt iImgWidth, LongInt iImgHeight, GrayBmpData& data);
	//----------------------------------------------------------
	//描述：进行直方图均衡化变换
	static void HistChange( GrayBmpData& SrcData, GrayBmpData& DestData);
	//----------------------------------------------------------
	//描述：图像线性变换
	enum GrayChangeType{enLinearGrayChange = 0, enLogGrayChange, enPowGrayChange};
	static void GrayChange(GrayBmpData& SrcData, GrayBmpData& DestData, PObjPointer pPara, GrayChangeType type = enLinearGrayChange);
	//----------------------------------------------------------
	//描述：读取灰度位图文件
	ArNT_BOOL ReadBmp(StrChar* strFileName, GrayBmpData& Data);
	//----------------------------------------------------------
	//描述：读取灰度位图文件
	HANDLE ReadBmp(StrChar* strFileName, LongInt& iImgWidth, LongInt& iImgActrualWidth, LongInt& iImgHeight);
	//----------------------------------------------------------
	//描述：显示位图
	ArNT_BOOL Display(HWND hWnd, ImgDataType* ucImgData, LongInt iImgWidth, LongInt iImgHeight, ArNT_BOOL bChangeWindow = ArNT_FALSE,  ArNT_BOOL bStretch = ArNT_FALSE);

	//----------------------------------------------------------
	//描述：显示位图
	ArNT_BOOL Display(HWND hWnd, ImgDataType* ucImgData, LongInt iDestX, LongInt iDestY, LongInt iImgWidth, LongInt iImgHeight, ArNT_BOOL bStretch = ArNT_FALSE);


	//----------------------------------------------------------
	//描述：显示位图
	ArNT_BOOL Display(HWND hWnd, ImgDataType* ucImgData,  LongInt iImgWidth, LongInt iImgHeight, LongInt iDestX, LongInt iDestY, LongInt iSrcX, LongInt iSrcY, LongInt iShowWidth, LongInt iShowHeight);

	//----------------------------------------------------------
	//描述：显示位图
	ArNT_BOOL Display(HWND hWnd, GrayBmpData& Data, ArNT_BOOL bStretch = ArNT_FALSE);
	//----------------------------------------------------------
	//描述：显示位图
	ArNT_BOOL Display(HWND hWnd, GrayBmpData& Data, LongInt iDestX, LongInt iDestY);



	//----------------------------------------------------------
	//描述：横向显示位图
	ArNT_BOOL DisplayRow(HWND hWnd, ImgDataType* ucImgData, LongInt iImgWidth, LongInt iImgHeight, LongInt iLeftOffset = 0, LongInt iRightOffset = 0);

	//----------------------------------------------------------
	//描述：根据图像数据创建位图
	HBITMAP  CreateBmp(HWND hWnd, ImgDataType* ucImgData, LongInt iImgWidth, LongInt iImgHeight, BITMAPINFO& BmpInfo);
	//----------------------------------------------------------
	//描述：获取光标坐标对应的图像位置
	ArNT_BOOL  GetImgPos(LongInt iXPos, LongInt iYPos, LongInt& iImgXPos, LongInt& iImgYPos, LongInt& iImgOffset);
	//--------------------------------------------------------------------------
	//描述：创建位图文件
	ArNT_BOOL   CreateBmpFile(StrChar* strFileName, ImgDataType* ucImgData, LongInt iImgWidth, LongInt iImgHeight);

	//--------------------------------------------------------------------------
	//描述：创建位图数据文件
	ArNT_BOOL   CreateBmpDataFile(StrChar* strFileName, UShortInt* ucImgData, LongInt iImgWidth, LongInt iImgHeight);

	//--------------------------------------------------------------------------
	//描述：读取位图数据文件
	ArNT_BOOL   ReadBmpDataFile(StrChar* strFileName, UShortInt* ucImgData, LongInt& iImgWidth, LongInt& iActrualWidth, LongInt& iImgHeight);
	//--------------------------------------------------------------------------
	//描述：读取位图数据文件
	ArNT_BOOL   ReadBmpDataFile(StrChar* strFileName, UShortInt* ucImgData, LongInt& iImgWidth, LongInt& iActrualWidth, LongInt& iImgHeight,
		                   StrChar* strDateTime);

	//--------------------------------------------------------------------------
	//描述：读取位图数据文件
	ArNT_BOOL  ReadBmpDataFileTime(StrChar* strFileName, SYSTEMTIME& tm);
	

	ArNT_BOOL   CreateBmpFile(StrChar* strFileName, GrayBmpData& BmpData);


public:
	CGrayBmp(void);	
public:
	~CGrayBmp(void);
private:
	ImgDataType*		m_pInfoData;
	HDC                 m_hMemDC;
	HWND                m_hDisplayWnd;
	LongInt             m_iMaxDataLen;

	GrayBmpData         m_GrayBmpData;

	LongInt		m_iImgWidth;
	LongInt     m_iWindowWidth;
	LongInt     m_iActrualImgWidth;
	LongInt     m_iImgHeight;
	LongInt     m_iStartLeft;
};



