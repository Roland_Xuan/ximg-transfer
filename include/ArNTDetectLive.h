#pragma once
#include "ArNtBaseDataStruct.h"
#include "ArNTCriticalSection.h"


class ArNTDetectLive
{
public:
	ArNT_BOOL	AppIsLive();
	ArNT_BOOL	AppIsLive(StrPointer strAppName);
	void		GetRecentRunTime(ArNTSysTime& NowTime);
public:
	ArNTDetectLive(void);
	~ArNTDetectLive(void);

private:
	ArNTSysTime		m_RecentRunTime;
	LONGSTR			m_AppPath;
	LONGSTR			m_strTimeFile;
	LongInt			m_DetectInteval;
	ArNT_BOOL       m_bRun;
	static unsigned int __stdcall threadWriteLive(void* pPara);	
};
