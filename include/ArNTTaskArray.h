#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTTaskArray.h
// 作  者 ：杨朝霖
// 用  途 ：定义了用于单个进程内的任务队列，该任务队列属于多生产者单消费者模型
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖      2012-9-10   创建模块

//==============================================================================
///<ver_info>


///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
#include "ArNTCriticalSection.h"
///</header_info>

///<datastruct_info>
//==============================================================================
// 基本数据类型定义
//任务描述
typedef struct tagArNTTaskInfo
{
	enum TaskType{enNormal = 0, enSystem};
	TaskType		type;	
	ULongInt		iMemSize;
	ULongInt        iAllocMemSize;
	ShortInt        iTTL;
	ArNT_BOOL		bInAllocMem;	
	PByteBuffer		pAllocMem;	
	enum {MAX_LOCAL_MEM_SIZE = 2048};
	Byte  pReservTaskMem[MAX_LOCAL_MEM_SIZE];	
}ArNTTaskInfo, *PArNTTaskInfo;
//队列统计信息
typedef struct tagArNTTaskTJInfo
{
	LongLongInt iRcvTaskNum;
	LongLongInt iProcessTaskNum;
	LongLongInt iErrorNum;
}ArNTTaskTJInfo, *PArNTTaskTJInfo;

//队列回调函数
typedef void (__stdcall *PProcessTaskFuncDef)(StrPointer strTaskArrayName, ULongInt iMemSize, PMemPointer pMem, PMemPointer pPara, LongLongInt iTaskID); //任务处理回调函数
typedef ArNT_BOOL (__stdcall *PInitialTaskFuncDef)(StrPointer strTaskArrayName, PMemPointer pPara);//处理任务线程初始化函数，可以做COM等初始过程
typedef void (__stdcall *PExitTaskFuncDef)(ArNT_BOOL bExitStatus, StrPointer strTaskArrayName, PMemPointer pPara);//任务退出回调函数, bExitStatus = true表示正常退出，false表示强制退出
typedef void (__stdcall *PNtfTaskEmptyFuncDef)(StrPointer strTaskArrayName, PMemPointer pPara); //队列数据处理完成通知回调函数
// 
//==============================================================================
///</datastruct_info>

//==============================================================================
///<class_info>
//名称:ArNTTaskArrayMemAlloc
//功能:用于给TaskArray提供内存分配方法
class ArNTTaskArrayMemAlloc
{
public:
	virtual ~ArNTTaskArrayMemAlloc() {};
	virtual PMemPointer  Alloc(LongInt iSize) = 0;
	virtual ArNT_BOOL	 Destory(PMemPointer) = 0;
};
//==============================================================================
///</class_info>
//==============================================================================
///<class_info>
//名称:CTaskArray
//功能:通过将多个请求放入一个队列中，提供对任务的缓冲
class CTaskArray
{
	typedef struct tagrArNTInneTaskInfo
	{
		LongLongInt		iTaskID; //当前的任务ID
		LongLongInt		iAddTime; //添加任务的时间
		LongLongInt	    iStarTime; //开始处理该数据的时间
		LongLongInt		iCheckSum;
		PMemPointer     pPara;
		ArNTTaskInfo	Info;
		
	}ArNTInneTaskInfo, *PArNTInneTaskInfo;
public:
	///<func_info>
	//描述：开始运行任务队列
	//参数：pProcessTaskFunc pProcessFunc 当队列中有任务存在时，调用的处理函数
	//PMemPointer pPara 队列相关参数,该参数会被传递给pProcessFunc，任务处理函数
	//PInitialTaskFuncDef pInitialFunc 初始化函数,任务队列在开始运行时执行的函数
	//线程安全：否
	//返回值:  true 表示成功  false 表示失败
	          
	ArNT_BOOL	StartRun(PProcessTaskFuncDef pProcessFunc, PMemPointer pPara, PInitialTaskFuncDef pInitialFunc = NULL); 
	///</func_info>
	///<func_info>
	//描述：注册通知队列处理完成回调函数
	//参数: PNtfTaskEmptyFuncDef pFunc 需要注册的回调函数
	void  RegNtfTaskEmptyFunc(PNtfTaskEmptyFuncDef pFunc);
	///</func_info>
	
	///<func_info>
	//描述：添加数据到任务队列
	//参数:ULongInt iMemSize 添加数据的尺寸
	//     PMemPointer pData 添加数据所在的内存地址
	//	   ShortInt iTTL  该数据的生命期
	//	   PMemPointer pPara 任务运行参数	
	//线程安全：是
	//返回值:添加的任务ID
	LongLongInt	AddTask(ULongInt iMemSize, PMemPointer pData, ShortInt iTTL = 10,  PMemPointer pPara = NULL);
	///</func_info>

	///<func_info>
	//描述：退出任务队列
	//参数: pExitTaskFunc pExitTaskFunc 任务队列成功退出后会调用该函数
	//      PMemPointer pContext 退出函数的运行环境参数
	//线程安全：否
	void	ExitTask(PExitTaskFuncDef pExitTaskFunc = NULL, PMemPointer pContext = NULL); 
	///</func_info>

	///<func_info>
	//描述：获取任务队列的统计信息,该统计数据由于性能关系未做同步仅用于参考，
	//参数：ArNTTaskTJInfo& TJInfo 返回的任务队列统计信息
	//线程安全：否
	void	GetTJInfo(ArNTTaskTJInfo& TJInfo);
	///</func_info>
	///<func_info>
	//描述：清除任务
	//线程安全:否
	void    ClearTask(void); 
	///</func_info>

	void   Reset(void);


	///<func_info>
	//描述：判断队列中是否为空，即没有可以处理的任务
	//返回值：true表示队列空，false表示队列非空
	//线程安全:否
	ArNT_BOOL    IsEmpty(void);
	///</func_info>

	///<func_info>
	//描述：设置任务处理线程为高优先级
	//线程安全:否
	void    ChangeToHighPriority(void);
	///</func_info>
	
	///<func_info>
	//描述：获取池中当前创建的任务队列的数量
	//返回值： 任务队列的数量
	//线程安全:是
	static ShortInt    TaskArrayNumInPool(void); 
	///</func_info>

	///<func_info>
	//描述：获取指定名称的任务队列
	//返回值：任务队列的指针
	//线程安全：是
	static CTaskArray*    TaskArrayInPool(StrPointer strName); 
	///</func_info>	

	///<func_info>
	//描述：获取任务队列当前状态
	//返回值: true表示队列状态正常
	//线程安全: 否
	ArNT_BOOL    IsValidTaskArray(SHORTSTR& strErrorInfo); 
	///</func_info>	

	///<func_info>
	//描述：等待任务队列运行
	//返回值: true表示队列开始正常运行，false表示队列无法正常运行
	//线程安全: 否
	ArNT_BOOL    WaitToRun(); 
	///</func_info>	

	///<func_info>
	//描述：设置内存分配类
	//线程安全: 否
	void    SetMemAlloc(ArNTTaskArrayMemAlloc* pAlloc); 
	///</func_info>	

	void   AllocMemory(ULongInt iMemSize);

public:
	///<func_info>
	//描述：构造函数
	//参数： StrPointer strName 任务队列的名称
	//UShortInt iMaxTaskNum 最大可容纳的任务数量
	CTaskArray(StrPointer strName = NULL, UShortInt iMaxTaskNum = 10);
	///</func_info>
	~CTaskArray(void);
private:
	int  FindFreeTask(void); //查找可以写入数据的任务
	ArNT_BOOL ProcessTask(void); //进行任务处理
	void DetectTaskStatus(void); //检查队列是否依然处于活动状态
	void OnTimer();
	//操作锁
	CTaskCriticalSection		m_Lock;
	SHORTSTR					m_strTaskName;
	ULongInt					m_iStartProcessTime;   
	LongLongInt					m_iCurrTaskID;

	//注册函数
	PInitialTaskFuncDef			m_pInitialFunc;
	PProcessTaskFuncDef         m_pProcessTaskFunc;
	PExitTaskFuncDef            m_pExitTaskFunc;
	PNtfTaskEmptyFuncDef        m_pNtfTaskEmptyFunc;
	PMemPointer					m_pPara;
	PMemPointer                 m_pExitPara;

	
	HANDLE						m_hEvtRead; 
	ShortInt					m_iMaxTaskNum;
	ShortInt                    m_iCurrTaskIndex;
	ShortInt                    m_iLastTaskIndex;
	PArNTInneTaskInfo			m_pTaskItems;
	ArNTTaskArrayMemAlloc*		m_pMemAlloc;
	
	ArNTTaskTJInfo              m_TaskTJInfo;
	HANDLE						m_hThreadProcess;
	static unsigned int __stdcall threadProcessTask(void* pPara);
private:	
	ArNT_BOOL				m_bExitTask;
	ArNT_BOOL				m_bValidTaskArray;
	ArNT_BOOL				m_bTaskArrayRun;
	ArNT_BOOL				m_bTaskArrayStartRun;
	ArNT_BOOL               m_bThreadProcessCreate;
	ArNT_BOOL               m_bClearTask;
	SHORTSTR				m_strErrorInfo;
	LongLongInt				m_iTaskArrayTime;
	unsigned int			m_iProcessThreadID;

	enum {MAX_TASKARRAY_NUM = 512};
	static CTaskArray*		m_pTaskArrayInPool[MAX_TASKARRAY_NUM];
	static ShortInt         m_iTaskArrayNum;
	static  HANDLE			m_hStatusDetectThread;
	static  ShortInt        m_iDetectInterval;
	static CTaskCriticalSection	    m_LockOperTaskArray;
	static unsigned int __stdcall threadDetectStatus(void* pPara);

	static  HANDLE  m_hHeap;
public:
	StrChar  m_strTag[2];
};

//==============================================================================
///</class_info>
