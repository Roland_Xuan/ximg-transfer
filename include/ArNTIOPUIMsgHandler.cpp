#define  NOAUTOLINK_IOPUIHELPER
#include "ArNTIOPUIMsgHandler.h"
#include "CommonFunc.h"
#include "ArNTDebugModule.h"
#include "ArNTCommandOper.h"
////////////////////////////////////////////////////////////////////////
//消息响应类

ArNTIOPUIEvtHandler::ArNTIOPUIEvtHandler()
{



}

ArNTIOPUIEvtHandler::~ArNTIOPUIEvtHandler()
{



}
////////////////////////////////////////////////////////////
//接口函数
//--------------------------------------------------------
//注册父类
void  ArNTIOPUIEvtHandler::RegParent(void* pParent)
{
	m_pParent = pParent;	
}

//--------------------------------------------------------
//
void ArNTIOPUIEvtHandler::OnIOPAckReadImage(StrPointer strType, ShortInt iContextSize, PBufferByte ImageContext, PRawCaptureImage pImage, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ACK_READIMAGE_DESC);

}

//--------------------------------------------------------
//回复IOP协议支持的命令
void ArNTIOPUIEvtHandler::OnIOPAckCmdList(PArNTCommandDesc pCmd, ShortInt iCmdNum, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ACK_CMDLIST_DESC);
}
//--------------------------------------------------------
//
void ArNTIOPUIEvtHandler::OnIOPAckSupportType(StrPointer strAppName, ShortInt iTypeNum, PArNTImageSupportType pTypes, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ACK_SUPPORT_TYPE_DESC);

}
//--------------------------------------------------------
//
void ArNTIOPUIEvtHandler::OnIOPAckRunInfo(StrPointer strAppName, PArNTRunInfo pRunInfo, ShortInt iInfoNum, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ACK_RUNINFO_DESC);
}

//--------------------------------------------------------
//
void ArNTIOPUIEvtHandler::OnIOPAckSearchImage(PArNTImageDesc pDesc, ShortInt iImgNum, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ACK_SEARCHIMAGE_DESC);	
}

//--------------------------------------------------------
//
void ArNTIOPUIEvtHandler::OnIOPAckService(StrPointer strApp, StrPointer strVerison, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ACK_SERVICE_DESC);	
}

//--------------------------------------------------------
//
void ArNTIOPUIEvtHandler::OnIOPAckErrorList(StrPointer strAppName, PArNTErrorInfo pError, ShortInt iItemNum, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_ACK_ERROR_LIST_DESC);
}

//--------------------------------------------------------
//
void ArNTIOPUIEvtHandler::OnIOPNTFErrorInfo(StrPointer strAppName, StrPointer strCmdName, ArNT_ERROR_CODE Code, StrPointer strError, StrPointer strSenderID)
{
	DefautNotImplFunc(IOP_NTF_ERROR_DESC);
}
////////////////////////////////////////////////////////////
//私有函数

void ArNTIOPUIEvtHandler::DefautNotImplFunc(StrPointer strFuncInfo)
{
	SHORTSTR strInfo = {0};
	CCommonFunc::SafeStringPrintf(strInfo, STR_LEN(strInfo), L"消息%s未实现", strFuncInfo); 	
	WRITE_WARN(strInfo);
}








////////////////////////////////////////////////////////////////////////
//消息处理类

ArNTIOPUIMsgHandler::ArNTIOPUIMsgHandler(void)
{
}

ArNTIOPUIMsgHandler::~ArNTIOPUIMsgHandler(void)
{
	UnRegIOPUI();
}

//////////////////////////////////////////////////////////////
//接口函数
//-----------------------------------------------------------
//注册通道
ArNT_BOOL ArNTIOPUIMsgHandler::RegEvtHandler(ArNTIOPUIEvtHandler* pEvtHandler, ArNT_BOOL bEnableTrace, ShortInt iObjID)
{
	m_pEvtHandler = pEvtHandler;
	m_bEnableTrace = bEnableTrace;
	ArNT_BOOL bResutl = ArNT_TRUE;
	bResutl = RegIOPUI(m_strRegChannel, 1024 * 1024, iObjID);
	return bResutl;
}
//-----------------------------------------------------------
//取消通道注册
void	 ArNTIOPUIMsgHandler::UnRegEvtHandler(ShortInt iObjID)
{
	UnRegIOPUI(iObjID);
}
/////////////////////////////////////////////////////////////
//消息处理函数

BEGIN_IOP_UI_MESSAGE(ArNTIOPUIMsgHandler)
	IOP_MESSAGE_ENTRY(IOP_ACK_READIMAGE, ProcessIOPAckReadImage, IOPAckReadImage)
	IOP_MESSAGE_ENTRY(IOP_ACK_CMDLIST, ProcessIOPAckCmdList, IOPAckCmdList)
	IOP_MESSAGE_ENTRY(IOP_ACK_SUPPORT_TYPE, ProcessIOPAckSupportType, IOPAckSupportType)
	IOP_MESSAGE_ENTRY(IOP_ACK_RUNINFO, ProcessIOPAckRunInfo, IOPAckRunInfo)
	IOP_MESSAGE_ENTRY(IOP_ACK_SEARCHIMAGE, ProcessIOPAckSearchImage, IOPAckSearchImage)
	IOP_MESSAGE_ENTRY(IOP_ACK_SERVICE, ProcessIOPAckService, IOPAckService)
	IOP_MESSAGE_ENTRY(IOP_ACK_ERROR_LIST, ProcessIOPAckErrorList, IOPAckErrorList)
	IOP_MESSAGE_ENTRY(IOP_NTF_ERROR,  ProcessIOPNtfError,IOPNtfError)
END_IOP_MESSAGE(ArNTIOPUIMsgHandler)


//-----------------------------------------------------------
//回复读取的图像
void ArNTIOPUIMsgHandler::ProcessIOPAckReadImage(PIOPAckReadImage pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ACK_READIMAGE_DESC);	
	if(m_pEvtHandler)
	{
		m_pEvtHandler->OnIOPAckReadImage(pPara->strType, pPara->iContextSize, pPara->ImageContex, pPara->RawImg, strSenderID);
	}

}
//-----------------------------------------------------------
//回复命令列表
void    ArNTIOPUIMsgHandler::ProcessIOPAckCmdList(PIOPAckCmdList pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ACK_CMDLIST_DESC);	
	if(m_pEvtHandler)
	{
		m_pEvtHandler->OnIOPAckCmdList(pPara->Desc,  pPara->iCmdNum, strSenderID);
	}
}
//-----------------------------------------------------------
//回复支持的文件保存类型
void    ArNTIOPUIMsgHandler::ProcessIOPAckSupportType(PIOPAckSupportType pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ACK_SUPPORT_TYPE_DESC);	
	if(m_pEvtHandler)
	{
		m_pEvtHandler->OnIOPAckSupportType(pPara->strAppName, pPara->iTypeNum, pPara->Items, strSenderID);
	}
}

//-----------------------------------------------------------
//回复服务的运行信息
void    ArNTIOPUIMsgHandler::ProcessIOPAckRunInfo(PIOPAckRunInfo pPara,  StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ACK_RUNINFO_DESC);	
	if(m_pEvtHandler)
	{
		m_pEvtHandler->OnIOPAckRunInfo(pPara->strAppName, pPara->RunInfo, pPara->iInfoNum, strSenderID);
	}
}
//-----------------------------------------------------------
//回复搜索到的图像信息
void    ArNTIOPUIMsgHandler::ProcessIOPAckSearchImage(PIOPAckSearchImage pPara,  StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ACK_SEARCHIMAGE_DESC);	
	if(m_pEvtHandler)
	{
		m_pEvtHandler->OnIOPAckSearchImage(pPara->ImgDesc,  pPara->iImgNum, strSenderID);
	}
}
//-----------------------------------------------------------
//回复支持IOP协议的服务信息
void    ArNTIOPUIMsgHandler::ProcessIOPAckService(PIOPAckService pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ACK_SERVICE_DESC);	
	if(m_pEvtHandler)
	{
		m_pEvtHandler->OnIOPAckService(pPara->Service.strAppName,  pPara->Service.strVersion, strSenderID);
	}
}
//-----------------------------------------------------------
//回复服务的错误列表信息
void    ArNTIOPUIMsgHandler::ProcessIOPAckErrorList(PIOPAckErrorList pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_ACK_ERROR_LIST_DESC);	
	if(m_pEvtHandler)
	{
		m_pEvtHandler->OnIOPAckErrorList(pPara->strAppName,  pPara->ErrorInfo, pPara->iErrorNum, strSenderID);
	}

}
//-----------------------------------------------------------
//通知错误信息
void    ArNTIOPUIMsgHandler::ProcessIOPNtfError(PIOPNtfError pPara, StrPointer strSenderID)
{
	if(m_bEnableTrace) WRITE_NOTICE(IOP_NTF_ERROR_DESC);	
	if(m_pEvtHandler)
	{
		m_pEvtHandler->OnIOPNTFErrorInfo(pPara->strAppName,  pPara->strCmdName, pPara->dwErrorCode, pPara->strErrorInfo, strSenderID);
	}
}

//----------------------------------------------------
//请求函数

//请求读取图像
void   ArNTIOPUIMsgHandler::AskReadImage(LONGSTR& strFromChannel, StrPointer strType, StrPointer strDesc, StrPointer strSenderID, ShortInt iObjID)
{
	IOPAskReadImage Para = {sizeof(IOPAskReadImage)};
	CCommonFunc::SafeStringCpy(Para.ImgDesc.strType, STR_LEN(Para.ImgDesc.strType), strType); 
	CCommonFunc::SafeStringCpy(Para.ImgDesc.strInfo, STR_LEN(Para.ImgDesc.strInfo), strDesc); 
	CCommonFunc::SafeStringCpy(Para.strFromChannel, STR_LEN(Para.strFromChannel), strFromChannel);

	LONGSTR strToChannel = {0};
	if(iObjID >= 0)
	{
		CCommonFunc::SafeStringPrintf(strToChannel, STR_LEN(strToChannel), L"%s%d", IOP, iObjID);
	}else
	{
		CCommonFunc::SafeStringCpy(strToChannel, STR_LEN(strToChannel), IOP);
	}

	ArNTCommandOper::PostCmdToChannel(IOP_ASK_READIMAGE, &Para, Para.iSize, strToChannel, strSenderID);
}


void   ArNTIOPUIMsgHandler::AskWriteImage(LONGSTR& strFromChannel, ArNTImageDesc& ImageDesc, ShortInt iContextSize, PBufferByte pContex, PRawCaptureImage pImage,  StrPointer strSenderID, ShortInt iObjID)
{
	LongInt iSize = sizeof(IOPAskWriteImage) + pImage->iToatalImgSize;
	PBufferByte pBuffer = new BufferByte[iSize];
	PIOPAskWriteImage pAskInfo = (PIOPAskWriteImage)pBuffer;
	pAskInfo->iSize = sizeof(IOPAskWriteImage);
	CCommonFunc::SafeStringCpy(pAskInfo->strFromChannel, STR_LEN(pAskInfo->strFromChannel), strFromChannel);
	memcpy(&pAskInfo->ImgDesc, &ImageDesc, sizeof(pAskInfo->ImgDesc));
	pAskInfo->iContextSize = iContextSize;
	if(iContextSize > tagIOPAckReadImage::MAX_CONTEXT_SIZE)  iContextSize = tagIOPAckReadImage::MAX_CONTEXT_SIZE;
	memcpy(pAskInfo->ImgageContex, pContex, iContextSize);

	pAskInfo->RawImg->iSize  = pImage->iSize;
	pAskInfo->RawImg->iChannelNum = pImage->iChannelNum;
	pAskInfo->RawImg->iHeight = pImage->iHeight;
	pAskInfo->RawImg->iWidth = pImage->iWidth;
	pAskInfo->RawImg->iWidthStep  = pImage->iWidthStep;
	pAskInfo->RawImg->iToatalImgSize = pImage->iToatalImgSize;
	CCommonFunc::SafeStringCpy(pAskInfo->RawImg->strID, STR_LEN(pAskInfo->RawImg->strID), pImage->strID);
	memcpy(pAskInfo->RawImg->pImgData, pImage->pImgData, pImage->iToatalImgSize);

	LONGSTR strToChannel = {0};
	if(iObjID >= 0)
	{
		CCommonFunc::SafeStringPrintf(strToChannel, STR_LEN(strToChannel), L"%s%d", IOP, iObjID);
	}else
	{
		CCommonFunc::SafeStringCpy(strToChannel, STR_LEN(strToChannel), IOP);
	}
	ArNTCommandOper::PostCmdToChannel(IOP_ASK_WRITEIMAGE, pBuffer, iSize, strToChannel, strSenderID);
	delete pBuffer;

}

//询问协议支持的命令
void   ArNTIOPUIMsgHandler::AskCmdList(LONGSTR& strFromChannel,StrPointer strSenderID,  ShortInt iObjID)
{
	IOPAskCmdList Para = {sizeof(IOPAskCmdList)};
	CCommonFunc::SafeStringCpy(Para.strFromChannel, STR_LEN(Para.strFromChannel), strFromChannel);

	LONGSTR strToChannel = {0};
	if(iObjID >= 0)
	{
		CCommonFunc::SafeStringPrintf(strToChannel, STR_LEN(strToChannel), L"%s%d", IOP, iObjID);
	}else
	{
		CCommonFunc::SafeStringCpy(strToChannel, STR_LEN(strToChannel), IOP);
	}
	ArNTCommandOper::PostCmdToChannel(IOP_ASK_CMDLIST, &Para, Para.iSize, strToChannel, strSenderID);
}
//询问错误列表命令
void    ArNTIOPUIMsgHandler::AskErrorList(LONGSTR& strFromChannel,StrPointer strSenderID, ShortInt iObjID)
{
	IOPAskErrorList Para = {sizeof(IOPAskErrorList)};
	CCommonFunc::SafeStringCpy(Para.strFromChannel, STR_LEN(Para.strFromChannel), strFromChannel);

	LONGSTR strToChannel = {0};
	if(iObjID >= 0)
	{
		CCommonFunc::SafeStringPrintf(strToChannel, STR_LEN(strToChannel), L"%s%d", IOP, iObjID);
	}else
	{
		CCommonFunc::SafeStringCpy(strToChannel, STR_LEN(strToChannel), IOP);
	}
	ArNTCommandOper::PostCmdToChannel(IOP_ASK_ERROR_LIST, &Para, Para.iSize, strToChannel, strSenderID);
}

//询问支持的图像保存类型
void   ArNTIOPUIMsgHandler::AskSupportType(LONGSTR& strFromChannel,StrPointer strSenderID, ShortInt iObjID)
{
	IOPAskSupportType Para = {sizeof(IOPAskSupportType)};
	CCommonFunc::SafeStringCpy(Para.strFromChannel, STR_LEN(Para.strFromChannel), strFromChannel);

	LONGSTR strToChannel = {0};
	if(iObjID >= 0)
	{
		CCommonFunc::SafeStringPrintf(strToChannel, STR_LEN(strToChannel), L"%s%d", IOP, iObjID);
	}else
	{
		CCommonFunc::SafeStringCpy(strToChannel, STR_LEN(strToChannel), IOP);
	}
	ArNTCommandOper::PostCmdToChannel(IOP_ASK_SUPPORT_TYPE, &Para, Para.iSize, strToChannel, strSenderID);

}
//询问服务运行信息
void   ArNTIOPUIMsgHandler::AskAppRunInfo(LONGSTR& strFromChannel,StrPointer strSenderID, ShortInt iObjID)
{
	IOPAskRunInfo Para = {sizeof(IOPAskRunInfo)};
	CCommonFunc::SafeStringCpy(Para.strFromChannel, STR_LEN(Para.strFromChannel), strFromChannel);

	LONGSTR strToChannel = {0};
	if(iObjID >= 0)
	{
		CCommonFunc::SafeStringPrintf(strToChannel, STR_LEN(strToChannel), L"%s%d", IOP, iObjID);
	}else
	{
		CCommonFunc::SafeStringCpy(strToChannel, STR_LEN(strToChannel), IOP);
	}
	ArNTCommandOper::PostCmdToChannel(IOP_ASK_RUNINFO, &Para, Para.iSize, strToChannel, strSenderID);
}
//询问支持IOP协议的服务
void   ArNTIOPUIMsgHandler::AskServerice(LONGSTR& strFromChannel, StrPointer strSenderID, ShortInt iObjID)
{
	IOPAskService Para = {sizeof(IOPAskService)};
	CCommonFunc::SafeStringCpy(Para.strFromChannel, STR_LEN(Para.strFromChannel), strFromChannel);

	LONGSTR strToChannel = {0};
	if(iObjID >= 0)
	{
		CCommonFunc::SafeStringPrintf(strToChannel, STR_LEN(strToChannel), L"%s%d", IOP, iObjID);
	}else
	{
		CCommonFunc::SafeStringCpy(strToChannel, STR_LEN(strToChannel), IOP);
	}
	ArNTCommandOper::PostCmdToChannel(IOP_ASK_SERVICE, &Para, Para.iSize, strToChannel, strSenderID);

}
//查询满足条件的图像
void	ArNTIOPUIMsgHandler::AskSearchImage(LONGSTR& strFromChannel, StrPointer strType, StrPointer strCondition, StrPointer strSenderID, ShortInt iObjID)
{
	IOPAskSearchImage Para = {sizeof(IOPAskSearchImage)};
	CCommonFunc::SafeStringCpy(Para.strFromChannel, STR_LEN(Para.strFromChannel), strFromChannel);
	CCommonFunc::SafeStringCpy(Para.strType, STR_LEN(Para.strType), strType);
	CCommonFunc::SafeStringCpy(Para.strCondition, STR_LEN(Para.strCondition), strCondition);

	LONGSTR strToChannel = {0};
	if(iObjID >= 0)
	{
		CCommonFunc::SafeStringPrintf(strToChannel, STR_LEN(strToChannel), L"%s%d", IOP, iObjID);
	}else
	{
		CCommonFunc::SafeStringCpy(strToChannel, STR_LEN(strToChannel), IOP);
	}
	ArNTCommandOper::PostCmdToChannel(IOP_ASK_SEARCHIMAGE, &Para, Para.iSize, strToChannel, strSenderID);
}
