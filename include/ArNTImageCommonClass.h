#pragma once
#include "ArNTGrayBmp.h"

typedef struct tagImgDataHeadExt
{
	LongInt		iSize;		//结构体大小
	StrChar		tag[4];		//标志
	LongInt 	iDetectLen;	//毫米为单位
	LongInt		iSpeed;		//毫米/秒
	ULongInt	iSteelNo;	//流水号
	LongInt		iImgWidth;	//图像宽度
	LongInt		iImgHeight;	//图像高度
	ShortInt	iLineRate;	//采集的线速率
	ShortInt	iPos;		//图像位置，0表示头部，1表示中部，2表示尾部
	ShortInt	iCameralNo;	//相机号
	SYSTEMTIME	NowTime;	//文件写入的时间
	BufferByte ucReserver[32];//预留的结构体
}ImgDataHeadExt, *PImgDataHeadExt;

typedef  struct tagSimpleImgHeadItem
{
	LongInt iStartLen;
	LongInt iEndLen;
	LongInt iImgStartPos;
	LongInt  iDataLen;
}SimpleImgItem, *PSimpleImgItem;

typedef struct tagImgEdgeInfo
{
	ShortInt iCamNo;
	LongInt iLeftEdge;
	LongInt iRightEdge;
}ImgEdgeInfo, *PImgEdgeInfo;

typedef struct tagSimpleImgHead
{
	LongInt			iSize;
	ULongInt		iSteelNo;	//流水号
	LongInt			iImgWidth;	//子图像宽度
	LongInt			iImgHeight;	//子图像高度
	LongInt			iLeftInSteel; //在钢板上的左坐标
	LongInt			iRightInSteel;
	LongInt			iItemNum; //包括的子图像数量
	ShortInt		iQuality; //图像压缩质量，0表示原图
	enum {MAX_CAM_NUM = 6};
	ImgEdgeInfo		CameraNo[MAX_CAM_NUM];
	ShortInt		iCameraNum;
	SimpleImgItem	Item[1];
}SimpleImgHead, *PSimpleImgHead;




typedef LongInt			HistGramDataType[256];

class ArNTImageOperator
{
public:
	///<func_info>
	//描述：写入单幅图像
	//参数：LONGSTR strImgFile 要写入图像的文件名，压缩格式下后缀为cimg,非压缩格式下为img。
	//		ImgDataType* pImgData,要写入图像的原始数据，图像的宽度高度等信息在另一个HeadInfo参数中进行说明
	//		ImgDataHeadExt& HeadInfo  要写入图像的描述信息
	//      ArNT_BOOL bCompress  是否采用压缩格式进行图像压缩，默认采用，记得在压缩格式的情况下，图像名后缀必须为cimg。
	//		LongInt iQuality 如果使用压缩格式默认采用压缩质量为70的压缩比，该数值越大，压缩的比例越小，图像越接近未压缩情况。
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	virtual ArNT_BOOL  WriteImg(LONGSTR strImgFile, ImgDataType* pImgData, ImgDataHeadExt& HeadInfo, ArNT_BOOL bCompress = ArNT_TRUE, LongInt iQuality = 70) = 0;
	///<func_info>
	//描述：读取单幅图像
	//参数：LONGSTR strImgFile 要读取图像的文件名，压缩格式下后缀为cimg,非压缩格式下为img。
	//		ImgDataHeadExt& HeadInfo  读入得到的图像描述信息
	//		GrayBmpData& Data 返回得到实际图像数据
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
    virtual ArNT_BOOL  ReadImg(LONGSTR strImgFile, ImgDataHeadExt& HeadInfo, GrayBmpData& Data) = 0;
	///<func_info>
	//描述：读取单幅图像的文件描述信息
	//参数：LONGSTR strImgFile 要读取图像的文件名，压缩格式下后缀为cimg,非压缩格式下为img。
	//		ImgDataHeadExt& HeadInfo  读入得到的图像描述信息
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	virtual ArNT_BOOL  ReadImgHeadInfo(LONGSTR strImgFile, ImgDataHeadExt& HeadInfo) = 0;
	
	///<func_info>
	//描述：读取单幅图像中指定位置的子图像
	//参数：LONGSTR strImgFile 要读取图像的文件名，压缩格式下后缀为cimg,非压缩格式下为img。
	//		ImgDataHeadExt& HeadInfo  读入得到的图像描述信息
	//		LongInt& iLeftPos         读取区域的左坐标，如果实际坐标无法进行截取会返回实际使用的坐标
	//		LongInt& iRightPos		  读取区域的右坐标，如果实际坐标无法进行截取会返回实际使用的坐标
	//		LongInt& iTopPos          读取区域的顶坐标，如果实际坐标无法进行截取会返回实际使用的坐标
	//		LongInt& iBottomPos		  读取区域的底坐标，如果实际坐标无法进行截取会返回实际使用的坐标
	//		GrayBmpData& Data		 返回得到实际子图像数据
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	virtual ArNT_BOOL  ReadSubImg(LONGSTR strImgFile, 
								 ImgDataHeadExt& HeadInfo, 
								 LongInt& iLeftPos, 
								 LongInt& iRightPos, 
								 LongInt& iTopPos,
								 LongInt& iBottomPos,
								GrayBmpData& Data) = 0;
	///<func_info>
	//描述：读取单幅图像中指定区域的平均灰度
	//参数：ImgDataType* pImgData	  需要计算的原始图像数据
	//		LongInt iXStart			  宽度方向上的起始位置
	//		LongInt iXEnd,			  宽度方向上的终止位置
	//		LongInt iYStart			  高度方向上的起始位置
	//		LongInt iYEnd			  高度方向上的终止位置
	//		LongInt& iBottomPos		  读取区域的底坐标，如果实际坐标无法进行截取会返回实际使用的坐标
	//		LongInt iWidthStep       原始图像中实际存储每一行数据的宽度，通常为4个字节的整数倍
	//		LongFloat* pAverGay		 返回计算得到的平均灰度数组，该存储空间需要从外部分配完成传入
	//      LongInt& iAverGrayNum    返回的用于存储平均灰度数组需要分配的LongFloat格式数据的个数，通常为iXEnd - iXStart个，不包括iXEnd点。
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	virtual void		CountAverGray( ImgDataType* pImgData, LongInt iXStart, LongInt iXEnd,  LongInt iYStart, LongInt iYEnd, LongInt iWidthStep, LongFloat* pAverGay, LongInt& iAverGrayNum) = 0;
	///<func_info>
	//描述：计算单幅图像的灰度直方图
	//参数：ImgDataType* pImgData	  需要计算的原始图像数据
	//		LongInt iWidthStep		  原始图像中实际存储每一行数据的宽度，通常为4个字节的整数倍
	//		LongInt iWidth			  原始图像的宽度
	//		LongInt iHeight			  原始图像的高度
	//		HistGramDataType& Data    计算得到的直方图数据
	//		LongFloat& Entropy		  计算得到的信息熵数值
	virtual void		CountHistGram( ImgDataType* pImgData, LongInt iWidthStep, LongInt iWidth, LongInt iHeight, HistGramDataType& Data, LongFloat& Entropy) = 0;
	///<func_info>
	//描述：对图像进行灰度变换
	//参数：ImgDataType* pImgData	  需要计算的原始图像数据
	//		LongInt iWidthStep		  原始图像中实际存储每一行数据的宽度，通常为4个字节的整数倍
	//		LongInt iWidth			  原始图像的宽度
	//		LongInt iHeight			  原始图像的高度
	//		LongInt* pPara			  灰度变换的参数数组
	virtual void		GrayChangeImg(ImgDataType* pImgData, LongInt iImgWidth, LongInt iImgHeight, LongInt iWidthStep, LongInt* pPara) = 0;
	///<func_info>
	//描述：对图像质量进评估
	//参数：ImgDataType* pImgData	  需要计算的原始图像数据
	//		LongInt iWidthStep		  原始图像中实际存储每一行数据的宽度，通常为4个字节的整数倍
	//		LongInt iWidth			  原始图像的宽度
	//		LongInt iHeight			  原始图像的高度
	//		LongInt& iValue			  返回评估的数值
	virtual void		ImageQulity(ImgDataType* pImgData, LongInt iImgWidth, LongInt iImgHeight, LongInt iWidthStep,  LongInt& iValue) = 0;
	///<func_info>
	//描述：将图像压缩到数据流
	//参数：ImgDataType* pImgData	  需要压缩的原始图像数据
	//		LongInt iWidth			  原始图像的宽度
	//		LongInt iHeight			  原始图像的高度
	//		PBufferByte pDestData    存放压缩后数据的内存区域，通常开始先将该指针设置为NULL,通过下一个参数获取实际需要的存储空间后，重新分配指定大小的内存区域后再次调用该函数，也可以直接分配一块与原图一样大的空间，
	//								 因为压缩后的图像数据肯定是小于原图大小的。
	//		LongInt& iDstDatalen     返回压缩图像数据需要的存放空间尺寸大小
	//		LongInt iQuality		压缩的图像质量系数，默认取70，数值越大，压缩比就越小，越接近原图
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	virtual ArNT_BOOL	WriteStream(ImgDataType* pImgData,LongInt  iImgWidth, LongInt iImgHeight, PBufferByte pDestData, LongInt& iDstDatalen, LongInt iQuality = 70) = 0;
	///<func_info>
	//描述：将压缩数据流解压缩到图像，该函数通常与WriteStream函数配合使用
	//参数：PBufferByte pDestData  压缩数据流
	//		LongInt& iDstDatalen   数据流的长度
	//		GrayBmpData& BmpData   解压得到的灰度图像数据
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	virtual ArNT_BOOL	ReadStream(PBufferByte pDestData, LongInt& iDstDatalen, GrayBmpData& BmpData) = 0;
	
	///<func_info>
	//描述：将图像数据写入到合并图像文件中，文件以bimg做为扩展名
	//参数：LONGSTR strImgFile			指定的合并图像文件名称
	//		ImgDataType* pImgData		需要写入的图像数据
	//		SimpleImgHead& HeadInfo		写入图像的描述信息
	//		LongInt iQuality			写入时的压缩质量，默认为70，如果该数值为0则表示存储为合并图像时不进行压缩。
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	virtual ArNT_BOOL	WriteCombineImg(LONGSTR strImgFile, ImgDataType* pImgData, SimpleImgHead& HeadInfo, LongInt iQuality = 70) = 0;
	///<func_info>
	//描述：从合并图像文件中读取图像数据，文件以bimg做为扩展名
	//参数：LONGSTR strImgFile			需要读取的合并图像文件名称
	//		PBufferByte pFileData		读取到存储数据，需要使用ReadStream函数解压缩得到实际图像数据
	//		LongInt& iImgDataLen		读取到存储数据的长度
	//		PSimpleImgHead& pHeadInfo	该指针存储子图像的信息，通过该参数可以获取子图像在读取到的存储数据中的位置
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	virtual ArNT_BOOL	ReadCombineImg(LONGSTR strImgFile, PBufferByte pFileData, LongInt& iImgDataLen, PSimpleImgHead& pHeadInfo) = 0;
	///<func_info>
	//描述：从合并图像文件中读取指定的子图像，文件以bimg做为扩展名
	//参数：LONGSTR strImgFile			需要读取的合并图像文件名称
	//		ShortInt iImgIndex			需要读取的子图像索引
	//		GrayBmpData& Data			读取到的子图像数据
	//		SimpleImgItem& Head,		对应子图像的描述信息
	//		LongInt& iSteelNo			返回子图像所在的钢板流水号
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	virtual ArNT_BOOL	ReadImgFromCombineImg(LONGSTR strImgFile, ShortInt iImgIndex, GrayBmpData& Data, SimpleImgItem& Head, LongInt& iSteelNo)= 0;

	///<func_info>
	//描述：查找合并图像中指定长度位置处的图像索引号
	//参数：LONGSTR strImgFile			需要查找的合并图像文件名称
	//		ShortInt& iImgIndex			返回对应指定长度位置的图像索引，可以使用该索引调用ReadImgFromCombineImg函数获取对应的子图像数据
	//		LongInt iDataLen			指定的长度位置
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败	
	virtual ArNT_BOOL	FindImgInCombineImg(LONGSTR strImgFile, ShortInt& iImgIndex, LongInt iDataLen) = 0;
	///<func_info>
	//描述：读取合并图像中子图像信息描述列表
	//参数：LONGSTR strImgFile			需要读取的合并图像文件名称
	//		PSimpleImgHead pHeadInfox	子图像信息列表指针，通常可以先将该参数设置为NULL，然后调用函数获取下一个参数指定的内存大小，分配好以后再次调用该函数获取结果。
	//		LongInt& iDataLen			获取到的信息在内存中的长度
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败	
	virtual ArNT_BOOL   ReadCombineImgHead(LONGSTR strImgFile, PSimpleImgHead pHeadInfo, LongInt& iDataLen) = 0;
		///<func_info>
	//描述：写入合并文件，但采用的是压缩过的数据流
	//参数：LONGSTR strImgFile			要保存的合并文件名，以扩展名bimg结尾
	//		SimpleImgHead& HeadInfo	    要写入的子图像信息。
	//		ImgDataType* pCompressImgData		要写入的子图像压缩数据
	//		LongInt iDataLen                    要写入的子图像压缩数据长度
	//		LongInt iQuality					写入的压缩数据对应的压缩系数
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败	
	virtual ArNT_BOOL	WriteCombineImgByData(LONGSTR strImgFile, SimpleImgHead& HeadInfo, ImgDataType* pCompressImgData, LongInt iDataLen, LongInt iQuality) = 0;

	///<func_info>
	//描述：从合并图像文件中读取指定的子图像，文件以bimg做为扩展名,该函数可获取合并图像在钢板中的位置
	//参数：LONGSTR strImgFile			需要读取的合并图像文件名称
	//		ShortInt iImgIndex			需要读取的子图像索引
	//		GrayBmpData& Data			读取到的子图像数据
	//		SimpleImgItem& Head,		对应子图像的描述信息
	//		LongInt& iLeftPos			返回图像对应的钢板左坐标
	//		LongInt& iRightPos			返回图像对应的钢板右坐标
	//		LongInt& iSteelNo			返回子图像所在的钢板流水号
	//返回值：ArNT_BOOL ArNT_TRUE表示成功，ArNT_FALSE表示失败
	virtual ArNT_BOOL	ReadImgFromCombineImg(LONGSTR strImgFile, ShortInt iImgIndex, GrayBmpData& Data, SimpleImgItem& Head, LongInt& iLeftPos, LongInt& iRightPos, LongInt& iSteelNo)= 0;


};


