#pragma once
#include "ArNtBaseDataStruct.h"

class ArNTRegOper
{
public:

	static  void ComputeRegCode(LONGSTR& strSrc, LONGSTR& strCode);
	static  ArNT_BOOL  Verify(LONGSTR& strCode);
	ArNTRegOper(void);
	~ArNTRegOper(void);
};
