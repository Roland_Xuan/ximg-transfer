#pragma once
#include "ImageFileIO.h"
//图像去相关之后分离符号位,再进行 Huffman编码,平均读写时间~160ms
class IppHuffmanDc: public ImageFileIO
{
public:
	unsigned long dwMathod;
	IppHuffmanDc(int iWidth=0,int iHeight=0);
	~IppHuffmanDc(void);
	bool SetSize(int iWidth,int iHeight);
	bool SetSize(CompressImgInfoEx *pFileHead);
	//图像相关预测参数
	int P;
	int Pt;
	int predictor;
	//图像去相关//srcImgActualWidth非字节长度,目标存贮空间与原相同,按块执行//默认dstImg==NULL时存储至源图像,块内运算
	void Decorrection(Ipp16s* srcImg,int srcImgWidth,int srcImgActualWidth,int srcImgHeight,
		Ipp16s* dstImg=0,int dstImgActualWidth=0);
	//图像重相关//srcImgActualWidth非字节长度,目标存贮空间与原相同,按块执行//默认dstImg==NULL时存储至源图像,块内运算
	void Recorrection(Ipp16s* srcImg,int srcImgWidth,int srcImgActualWidth,int srcImgHeight,
		Ipp16s* dstImg=0,int dstImgActualWidth=0);

	//图像编码数据
	int pStatistics[256]; 
	int codeLenTable[256];

	//数据压缩,被重载的
	bool Compress(Ipp8u* srcImg,int srcImgWidth,int srcImgActualWidth,int srcImgHeight,
		Ipp8u*dstData,int* dstLen, int* iEncodeBitSize=0);
	//首先根据读取的文件信息确定缓存空间,被重载的
	bool PreDeCompress(Ipp8u* srcData,int* srcLen,int* srcImgWidth,int* srcImgHeight);
	//解压图像数据,被重载的
	bool DeCompress(Ipp8u*srcData,int srcLen,
		Ipp8u* dstImg,int dstImgWidth,int dstImgActualWidth,int dstImgHeight);

	//转换8u至1u数组要求内存连续,threshold=1, f(<1)==0, f(>=1)==1,采用bigendian方式存储
	//srcOffset表示从左到右的起始位的index(最左边0开始),lastLen表示最后一个占用位的index+1(最左边0开始)
	//对于下一次存储,firstIndex=上一次的lastLen%8(取值1~8)
	void Convert8uTo1u(Ipp8u* srcData,int srcLen,Ipp8u* dstData,int* dstLen,Ipp8u threshold=1,
		int firstIndex=0,int* lastLen=0);
	//上一运算的反向运算,firstIndex=0时表示取整首位
	//lastLen==8时是取整结束(为与上函数统一lastLen(取值1~8)),lastLen==1时表示srcData[srcLen-1]中最后一个数据在首位上
	void Convert1uTo8u(Ipp8u* srcData,int srcLen,int firstIndex,int lastLen,Ipp8u* dstData,int* dstLen,Ipp8u lValue=0,Ipp8u hValue=1);

	//快速算法://注意BigEndian
	Ipp64u ConvertTable1uTo8u[256];
	//1uTo8u
	//为给以下函数提速,使用查表方法一次性提取一个字节,取值在0~255之间,通过查表得出一个
	//Ipp64u型的数值,经类型转换直接赋值到相应内存:table=Ipp64u[255]={0x00000000,0x00000001,0x00000010......}
	//8uTo1u
	//反查表构建复杂,如直接构建因图像稀疏表结构会很大(2^64),顾采用switch语句或取余操作
	//快速算法不支持位移操作,数据长度必须是8的整倍数,数据操作前后都只有 0 和 1两种值
	void FastConvert8uTo1u(Ipp8u* srcData,int srcLen,Ipp8u* dstData,int* dstLen);
	//上一运算的反向运算,firstIndex=0时表示取整首位
	//lastLen==8时是取整结束(为与上函数统一lastLen(取值1~8)),lastLen==1时表示srcData[srcLen-1]中最后一个数据在首位上
	void FastConvert1uTo8u(Ipp8u* srcData,int srcLen,Ipp8u* dstData,int* dstLen);

	//检查数据错误
	//bool ConvertTest(wchar_t* strFileName);
};
//图像直接Huffman编码
class IppHuffman: public ImageFileIO
{
public:
	unsigned long dwMathod;
	int pStatistics[256]; 
	int codeLenTable[256];
	IppHuffman(int iWidth=0,int iHeight=0);
	~IppHuffman();
	bool SetSize(int iWidth,int iHeight);
	bool SetSize(	CompressImgInfoEx *pFileHead);
	bool Compress(Ipp8u* srcImg,int srcImgWidth,int srcImgActualWidth,int srcImgHeight,
		Ipp8u*dstData,int* dstLen,int* iEncodeBitSize=0);
	bool PreDeCompress(Ipp8u* srcData,int* srcLen,int* srcImgWidth,int* srcImgHeight);
	bool DeCompress(Ipp8u*srcData,int srcLen,
		Ipp8u* dstImg,int dstImgWidth,int dstImgActualWidth,int dstImgHeight);
};
//图像LZ77编码（未实现）
class IppLZ77: public ImageFileIO
{
public:
	int pStatistics[256]; 
	int codeLenTable[256];
	IppLZ77(int iWidth=0,int iHeight=0);
	~IppLZ77();
	bool SetSize(int iWidth,int iHeight);
	bool Compress(Ipp8u* srcImg,int srcImgWidth,int srcImgActualWidth,int srcImgHeight,
		Ipp8u*dstData,int* dstLen,int* iEncodeBitSize=0);
	bool PreDeCompress(Ipp8u* srcData,int* srcLen,int* srcImgWidth,int* srcImgHeight);
	bool DeCompress(Ipp8u*srcData,int srcLen,
		Ipp8u* dstImg,int dstImgWidth,int dstImgActualWidth,int dstImgHeight);
};
