#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTNetTransfer.h
// 作  者 ：杨朝霖
// 用  途 ：ArNTNetTransfer模块接口定义文件,该模块用于实现网络数据传输
// 版  权 ：北京科技大学冶金工程研究院
//==============================================================================
///</proj_info>
///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描`述
//1.0     杨朝霖     2012-12-24    创建

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
#include "ArNtBaseDataStruct.h"
///</header_info>

#define  INIT_CHANNEL(channel) memset(&channel, 0, sizeof(channel)); channel.iSize = sizeof(ArNTNetChannelInfo);
///<datastruct_info>
//==============================================================================
// 基本数据类型定义
// tagNetConfig定义本机上配置的网络适配器信息
typedef struct tagNetConfig
{
	LongInt     iSize;			//结构体的大小，用于版本匹配
	ShortInt    iIndex;			//网卡的索引，用于主机上有多块网卡的情况
	StrChar		strDesc[64];	//网卡的描述
	StrChar		strName[64];	//网卡的名称
	StrChar     strMAC[32];		//网卡的MAC地址的文本描述

	ULongInt	ulLocalIP;		//网卡的本地IP，采用32位的数据值表示
	ULongInt	ulMask;			//网卡的掩码，采用32位的数据值表示
	ULongInt	ulGatewayIp;	//网卡的网关设置，采用32位的数据值表示
	ArNT_BOOL   bConnected;		//网卡是否已经连接，ArNT_TRUE表示已经连接

	NetIP		strIP;			//字符串形式的IP地址
	NetIP       strMask;		//字符串形式的掩码
	NetIP		strGateWay;		//字符串形式的网关	
}NetConfig, *PNetConfig;

//tagNetConfigSet定义本机上配置的网络适配器信息集合
typedef struct tagNetConfigSet
{
	enum {MAX_ITEMNUM = 64}; //最多支持MAX_ITEMNUM个适配器
	NetConfig   Items[MAX_ITEMNUM];	//网卡信息列表
	ShortInt    iItemNum;			//网卡数量
	SHORTSTR	strHostName;		//主机名称
}NetConfigSet,*PNetConfigSet;

//tagArNTNetChannelInfo定义通道信息
typedef struct tagArNTNetChannelInfo
{
	LongInt		iSize;
	TINYSTR		strChannelName; //进程下的通道名称
	ULongInt	ID; //注册后返回的通道ID
	NetIP		IP; //注册到的网络接口IP
	NetIP       Mask;//网络接口的掩码，不需要用户设定
	NetPORT		Port; //注册到的网络端口号	
	Byte		iPermitRemoteNum; //允许与该通道建立连接的通道数量
    ArNT_BOOL	bEnableMutiCast; //是否支持多播，ArNT_TRUE支持多播，ArNT_FALSE不支持
	BufferByte		Reserve[9]; //预留的数据
}ArNTNetChannelInfo, *PArNTNetChannelInfo;

//定义数据压缩算法信息
typedef struct tagArNTCompressInfo
{
	LongInt			iSize;
	StrChar			strName[16]; //压缩算法的名称
	ULongInt		iCompressDataOffset; //待压缩数据在源数据中的位置
	Byte			iCompressRate; //压缩比
	BufferByte		Para[30]; //其他参数数据
}ArNTCompressInfo, *PArNTCompressInfo;
//定义网络通道的运行统计信息
typedef struct tagNetChannelProfile
{
	ULongInt	m_iRcvPackageNum;
	ULongInt	m_iRcvPackageProcessNum;
	ULongInt	m_iSendPackageNum;
	ULongInt	m_iSendFailPackageNum;
	LongFloat	m_fRcvRate;
	LongFloat	m_fSendRate;
}NetChannelProfile, *PNetChannelProfile;


//接收到数据回调函数指针类型
//ArNTNetChannelInfo& FromChannel 发送数据的通道信息
//PMemPointer pData  接收到的数据的首地址
//LongInt iDataLen   接收到的数据的长度
//PMemPointer  pContext 与接收通道相关的环境参数
//StrPointer strLocalChanne 接收到数据的通道本地名称，适用于多个通道使用同一个回调函数的情况
typedef void (__stdcall * PNetRcvDataFuncDef)( ArNTNetChannelInfo& FromChannel, 
											PMemPointer pData,
											LongInt iDataLen,
											PMemPointer  pContext, 
											StrPointer strLocalChannel);

//通道初始化完成回调函数指针类型
//PMemPointer  pContext与通道相关的环境参数
//StrPointer strLocalChannel 通道的名称
typedef  void (__stdcall * PNetInitFuncDef)(  PMemPointer  pContext, StrPointer strLocalChannel);

//通道退出回调函数指针类型
//PMemPointer  pContext与通道相关的环境参数
//StrPointer strLocalChannel 通道的名称
typedef  void (__stdcall * PNetExitFuncDef)(  PMemPointer  pContext, StrPointer strLocalChannel);

//远端通道连接回调函数指针类型
//ArNTNetChannelInfo& FromChannel	连接的远端通道信息
//ArNT_BOOL    bConnect			ArNT_TRUE表示成功连接，ArNT_FALSE表示断开连接。
//PMemPointer  pContext			与本地通道相关的环境参数
//StrPointer strLocalChannel	本地通道的名称
typedef  void (__stdcall * PNetOnRemoteConnectFuncDef) ( ArNTNetChannelInfo& FromChannel, 
															  ArNT_BOOL    bConnect,
															  PMemPointer  pContext, 
															  StrPointer strLocalChannel);

//本地通道与远端通道连接回调函数指针类型
//ArNT_BOOL			bConnect  ArNT_TRUE表示连接成功，ArNT_FALSE表示断开连接
// PMemPointer  	pContext 与通道相关的环境参数
// StrPointer 		strLocalChannel	本地通道的名称
// NetIP 	  		strRemoteIP 远端通道的IP
//UShortInt 		iRemotePort 远端通道的端口
typedef void (__stdcall * PNetConnectFuncDef)(ArNT_BOOL		bConnect, 
										 PMemPointer  	pContext, 
										 StrPointer 	strLocalChannel,	
										 NetIP 	  		strRemoteIP,
										 UShortInt 		iRemotePort);

//异常回调函数指针类型
//ArNT_ERROR_CODE code					错误码
//StrPointer   strErrorDesc				错误描述
//StrPointer 	 strLocalChannel		本地通道名称
//PObjPointer	pContext				与通道相关的环境参数
typedef void (__stdcall * PExceptionFuncDef)( ArNT_ERROR_CODE code, 
											StrPointer   strErrorDesc, 
											StrPointer 	 strLocalChannel,
											PObjPointer	pContext);

//==============================================================================
///<export_info> 
//dll中的导出信息

//导出的函数信息
extern "C" {


	///<func_info>
	//描述：获取版本信息
	//参数：LONGSTR& strVersion 获取的版本信息
	//返回值：无
	void   __stdcall ArNTNetGetVersion(LONGSTR& strVersion);
	///</func_info>

	///<func_info>
	//描述：获取网卡配置信息
	//参数：NetConfigSet& InfoSet 存放返回的适配器信息集合
	// ArNT_BOOL bOnlyEthenet ArNT_TRUE表示只返回以太网类型网卡
	//返回值：无
	void __stdcall  ArNTNetGetNetConfigInfo(NetConfigSet& InfoSet, ArNT_BOOL bOnlyEthenet);
	///</func_info>

	///<func_info>
	//描述：设置指定MAC网卡的IP配置信息
	//参数：StrPointer strMAC设置的网卡MAC地址 
	//StrPointer strIP 设置的网卡IP地址
	//StrPointer strMask 网卡的掩码
	//StrPointer strGateWay网卡的网关地址
	//返回值：错误码
	ArNT_ERROR_CODE __stdcall  ArNTNetSetIPAddressByMAC(StrPointer strMAC, StrPointer strIP, StrPointer strMask, StrPointer strGateWay);
	///</func_info>

	///<func_info> 
	//描述：根据错误码查询错误描述
	//参数：ArNT_ERROR_CODE code 错误码
	//		LONGSTR& strInfo 错误信息描述
	//返回值：ArNT_BOOL 布尔变量，ArNT_TRUE表示成功，ArNT_FALSE表示失败。
	ArNT_BOOL   __stdcall ArNTNetGetErrorInfo(ArNT_ERROR_CODE code, LONGSTR& strInfo);
	///</func_info>

	///<func_info> 
	//描述：注册网络通道
	//参数：ArNTNetChannelInfo& Info 需要注册的通道信息
	// PNetRcvDataFuncDef pFunc 通道接收到数据的回调函数
	// PMemPointer pContext 相关环境参数指针
	// PNetInitFuncDef pInit 通道初始化完成回调函数
	// PNetExitFuncDef pExit 通道退出回调函数
	//  PNetOnRemoteConnectFuncDef pOnConnect 远端通道连接或断开回调函数
	//返回值：错误码
	ArNT_ERROR_CODE   __stdcall ArNTNetRegChannel(ArNTNetChannelInfo& Info, PNetRcvDataFuncDef pFunc, PMemPointer pContext, PNetInitFuncDef pInit, PNetExitFuncDef pExit, PNetOnRemoteConnectFuncDef pOnConnect);
	///</func_info>

	///<func_info> 
	//描述：注册网络通道
	//参数：StrPointer strName 需要注册的通道的通道名
	// StrPointer strIP  通道使用的本机IP
	//NetPORT iPort		 通道使用的端口
	//ULongInt&  iChannelID  返回通道的ID
	// PNetRcvDataFuncDef pFunc 接收数据回调函数
	// PMemPointer pContext 相关环境参数指针
	// PNetInitFuncDef pInit 通道初始化完成回调函数
	// PNetExitFuncDef pExit 通道退出回调函数
	//  PNetOnRemoteConnectFuncDef pOnConnect 远端通道连接或断开回调函数
	//返回值：错误码
	ArNT_ERROR_CODE  __stdcall ArNTNetSimpleRegChannel(StrPointer strName, StrPointer strIP, NetPORT iPort, ULongInt&  iChannelID, PNetRcvDataFuncDef pFunc, PMemPointer pContext = NULL, PNetInitFuncDef pInit = NULL, PNetExitFuncDef pExit = NULL, PNetOnRemoteConnectFuncDef pOnConnect = NULL);
	///</func_info>

	///<func_info>
	//描述：删除已有的通道
	//参数：StrPointer strChannel需要删除的通道名称
	//返回值：错误码
	ArNT_ERROR_CODE   __stdcall ArNTNetRemoveChannel(StrPointer strName);
	///</func_info>

	///<func_info>
	//描述：本地通道连接到远端通道
	//参数：StrPointer strLocalChannel 本地通道名称
	//		 NetIP strRemoteIP 远端通道的IP
	//		 ShortInt iRemotePort 远端通道端口
	//		 PNetConnectFuncDef pConnectFunc 连接成功回调函数
	//返回值：错误码
	ArNT_ERROR_CODE   __stdcall ArNTNetConnect(StrPointer strLocalChannel, NetIP strRemoteIP, UShortInt iRemotePort, PNetConnectFuncDef pConnectFunc);
	///</func_info>

	///<func_info>
	//描述：通道发送数据,采用稳定传输的方式
	//参数：StrPointer strLocalChannel  本地通道名，需要提前与接收端通道建立连接。
	//      PMemPointer pData 需要发送数据的首地址
	//		LongInt iDataLen 需要发送数据的长度
	//		PArNTCompressInfo pCompressInfo 压缩算法信息,NULL表示不使用任何压缩算法
	//返回值：错误码
	ArNT_ERROR_CODE   __stdcall ArNTNetSendData(StrPointer strLocalChannel, PMemPointer pData, LongInt iDataLen, PArNTCompressInfo pCompressInfo);
	///</func_info>

	///<func_info>
	//描述：发送数据到指定通道，采用非连接方式
	//参数： StrPointer strLocalChannel 需要发送数据的本地通道
	// NetIP RemoteIP 远端通道的IP地址
	//ShortInt iRemotePort 远端通道的端口号
	// PMemPointer pData 发送的数据的指针
	//LongInt iDataLen 发送数据的长度
	//返回值：错误码
	ArNT_ERROR_CODE   __stdcall ArNTNetPostData(StrPointer strLocalChannel, NetIP RemoteIP, UShortInt iRemotePort, PMemPointer pData, LongInt iDataLen);
	///</func_info>

	///<func_info>
	//描述：采用非ITP协议发送数据到指定通道
	//参数： StrPointer strLocalChannel 需要发送数据的本地通道
	// NetIP RemoteIP 远端通道的IP地址
	//ShortInt iRemotePort 远端通道的端口号
	// PMemPointer pData 发送的数据的指针
	//LongInt iDataLen 发送数据的长度
	//返回值：错误码
	ArNT_ERROR_CODE   __stdcall ArNTNetPostRawData(StrPointer strLocalChannel, NetIP RemoteIP, UShortInt iRemotePort, PMemPointer pData, LongInt iDataLen);
	///</func_info>


	///<func_info>
	//描述：注册异常通知函数
	//参数：ArNTNetChannelInfo& Info	注册的通道信息
	//PExceptionFuncDef pExceptionFunc  异常回调函数
	//返回值：错误码
	ArNT_ERROR_CODE   __stdcall ArNTNetRegException(ArNTNetChannelInfo& Info, PExceptionFuncDef pExceptionFunc, PObjPointer	pContext);
	///</func_info>

	///<func_info>
	//描述：断开通道的连接
	//参数：StrPointer strLocalChannel 需要断开连接的本地通道名
	//返回值：错误码
	ArNT_ERROR_CODE   __stdcall ArNTNetDisConnect(StrPointer strLocalChannel);
	///</func_info>

	///<func_info>
	//描述：根据通道名获取通道信息
	//StrPointer strName 指定的通道名
	//ArNTNetChannelInfo & Info 获取的通道信息
	//返回值：布尔类型值， ArNT_TRUE表示成功获取到通道信息
	ArNT_BOOL   __stdcall ArNTNetGetChannelInfo(StrPointer strName, ArNTNetChannelInfo & Info);
	///</func_info>

	 ///<func_info>
	//描述：判断两个IP地址是否是同一个网段
	//StrPointer strIP1 需要比较的IP1
	//StrPointer strMask1 需要比较的IP1的掩码
	//StrPointer strIP2 需要比较的IP2
	//StrPointer strMask2 需要比较的IP2的掩码
	 //返回值：ArNT_TRUE表示两个IP地址位于同一网段内
	ArNT_BOOL   __stdcall ArNTNetIPInSameNet(StrPointer strIP1, StrPointer strMask1, StrPointer strIP2, StrPointer strMask2);
	///</func_info>

	///<func_info>
	//描述：获取IP地址中的各部分值
	//参数：StrPointer strIP 要获取的IP地址
	//		ShortInt iIndex IP地址的部分，索引范围从1到4
	//		Byte& byValue 获取的数值
	//返回值：ArNT_TRUE 表示成功获取到相应的值
	ArNT_BOOL   __stdcall ArNTNetGetIPPartValue(StrPointer strIP, ShortInt iIndex, ShortInt& iValue);
	///</func_info>


	///<func_info>
	//描述：将数据广播给通道所在网段的所有机器
	//参数： StrPointer strLocalChannel 指定的通道名
	// PMemPointer pData 发送的数据的指针
	//LongInt iDataLen 发送数据的长度
	//返回值：错误码
	ArNT_ERROR_CODE   __stdcall ArNTNetBroadCastDataToClient(StrPointer strLocalChannel, PMemPointer pData, LongInt iDataLen);
	///</func_info>

	///<func_info>
	//描述：测试远端机器的网络状态是否正常
	//参数：
	// NetIP& strRemoteIP 远端机的IP
	//  TinyInt& iQulity 返回网络质量，范围从0到100，100最佳
	//返回值：ArNT_TRUE表示网络正常，ArNT_FALSE表示ping失败
	ArNT_BOOL	 __stdcall ArNTNetPingRemote(NetIP& strRemoteIP, TinyInt& iQulity);
	///</func_info>

	///<func_info>
	//描述：获取指定通道的统计信息
	//参数：
	// StrPointer strChannel指定通道名
	//   NetChannelProfile& Profile 通道统计信息
	//返回值：ArNT_TRUE表示网络正常，ArNT_FALSE表示ping失败

	ArNT_BOOL  __stdcall ArNTNetGetNetChannelProfile(StrPointer strChannel, NetChannelProfile& Profile);
	///</func_info>

	///<func_info>
	//描述：设置网络传输模块的传输性能，性能越高相应的稳定性会有所降低，仅对发生通道有影响。
	//参数：TinyInt iRation  性能百分比，取值范围从0到100。
	//返回值：无

	void  __stdcall ArNTNetChangePerformance(TinyInt iRation);
	///</func_info>

	///<func_info>
	//描述：获取注册通道的列表
	//参数：PArNTNetChannelInfo& pChannelArray 通道列表引用
	//返回值：注册通道数量
	UShortInt   __stdcall ArNTNetGetChannelList(PArNTNetChannelInfo& pChannelArray);
	///</func_info>

	///<func_info>
	//描述：销毁注册通道的列表
	//参数：PArNTNetChannelInfo pChannelArray 需要消毁的注册通道列表
	//返回值：无
	void	__stdcall	  ArNTNetFreeChannelList(PArNTNetChannelInfo pChannelArray);
	///</func_info>

	///<func_info>
	//描述：移除所有通道
	 //参数：ArNT_BOOL bSync 是否采用同步模式, ArNT_TRUE表示采用同步模式
	 //返回值：无
	void	__stdcall		ArNTNetRemoveAllChannel(ArNT_BOOL bSync = ArNT_FALSE);
	///</func_info>

	///<func_info>
	//描述：返回与指定IP同网段的一个IP地址
	 //参数：StrPointer strIP  指定的IP地址
	 //NetIP& DestIP			返回的目标IP
	 //NetIP& strMask			返回的目标IP的掩码
	 //返回值：ArNT_BOOL类型  ArNT_TRUE表示查找到同网段的地址
	ArNT_BOOL	__stdcall	ArNTNetFindIPInHost(StrPointer strIP, NetIP& DestIP, NetIP& strMask);
	///</func_info>

	 ///<func_info>
	//描述：将字符串形式的IP地址转换为整数形式的地址
	 //参数：StrPointer strIP  指定的IP地址
	 //返回值：整数形式的IP地址
	IPv4Addr	_stdcall	ArNTNetIPStr2IPv4(StrPointer strIP);
	///</func_info>

	///<func_info>
	//描述：将整数形式的IP地址转换为字符串形式的IP地址
	//参数：IPv4Addr iIP 整数形式的IP地址
	//NetIP& strIP  字符串形式的IP
	//返回值：整数形式的IP地址
	void	_stdcall	ArNTNetIPv42IPStr(IPv4Addr iIP, NetIP& strIP);
	///</func_info>	
   //==============================================================================
   ///</export_info>
}

#ifndef ArNTNetModuleNOAutoLink
#ifdef _WIN64
#pragma comment( lib, "ArNTNetTransfer64.lib" )
#pragma message("======== 程序隐式链接到 ArNTNetTransfer64.dll...")
#else
#pragma comment(lib,  "ArNTNetTransfer.lib")
#pragma message("======== 程序隐式链接到 ArNTNetTransfer.dll...")
#endif
#endif
