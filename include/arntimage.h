#pragma once
///<proj_info>
//==============================================================================
// 项目名 ：图像处理平台
// 文件名 ：ArNTImage.h
// 作  者 ：杨朝霖
// 用  途 ：ArNTImage图像库的头文件，该库定义了图像处理平台中的常用的图像数据类型和操作类
// 版  权 ：北京科技大学冶金工程研究院
//适用平台：WindowsNT 
//==============================================================================
///</proj_info>

///<ver_info>
// 版本记录	
//==============================================================================
//版本号  开发人员      时间      描述
//1.0     杨朝霖       2013-1-10   创建模块

//==============================================================================
///<ver_info>

///<header_info>
//添加所需的头文件
//灰度图操作类
#include "ArNTImageCommonClass.h"
//Cimg和img格式的图像操作类
///</header_info>


void _stdcall  GetImageLibVersion(LONGSTR& strVersion);
void _stdcall  AllocImageOperator(ArNTImageOperator** pImageOperator);
void _stdcall  FreeImageOperator(ArNTImageOperator** pImageOperator);

//==============================================================================
///链接信息
#ifndef AUTOLINK_IMAGE
#define  AUTOLINK_IMAGE
	#ifdef _WIN64
		#pragma comment( lib, "ArNTImage64.lib" )
		#pragma message("======== 程序隐式链接到 ArNTImage64.dll...")
	#else
		#pragma comment( lib, "ArNTImage.lib" )
		#pragma message("======== 程序隐式链接到 ArNTImage.dll...")
	#endif
#endif