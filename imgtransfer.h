#ifndef IMGTRANSFER_H
#define IMGTRANSFER_H

#include <ArNTGrayBmp.h>
#include <QObject>
#include <QSqlDatabase>
#include"arntimage.h"
#include <QMutex>
#include <QMutexLocker>

static QMutex pImgOperatorMutex;

class ImgTransfer : public QObject
{
    Q_OBJECT
public:
    explicit ImgTransfer(QObject *parent = nullptr);
    //
   // bool READFileToXimg(const QString & FilePAth, GrayBmpData &BmpData);
    bool READFileToImg(const QString & srcPath,int sequenceNum, int CamNo, int ImgIndex, ArNTImageOperator *pIOper);
    QString GetSavePath(int sequenceNum, int CamNo, int ImgIndex,int defectNo);
signals:
public:
    QSqlDatabase db;
    QSqlDatabase fullProcessdb;
};

#endif // IMGTRANSFER_H
