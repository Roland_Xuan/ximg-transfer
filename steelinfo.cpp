#include "steelinfo.h"

#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QtDebug>

SteelInfo::SteelInfo(QObject *parent)
    : QObject{parent}
{

    SteelTimer = new QTimer(this);
    SteelTimer->setInterval(1000);
    connect(SteelTimer,&QTimer::timeout,this,&SteelInfo::tryFindlatestSteel);
    QString connName = QString("dbFullProcess");
    db = QSqlDatabase::addDatabase("QODBC","db_full_process_thread");
    db.setHostName("127.0.0.1");
    db.setUserName("root");
    db.setPassword("nercar");
    db.setDatabaseName("db_device7");
    if(!db.open()){
        qDebug()<<db.lastError().text();
    }
    qDebug() << QSqlDatabase::drivers();
    SteelTimer->start();
}



void SteelInfo::tryFindlatestSteel()
{
    QString str = R"(
    SELECT Steel_ID
    FROM tb_device_steel_info
    WHERE Device_ID = 7
    ORDER BY Detect_Time DESC
    LIMIT 1
    OFFSET 1
    )";
    QSqlQuery query(db);
    query.exec(str);
    while (query.next()) {
        QString SteelName = query.record().value("Steel_ID").toString();
        // qDebug()<<"check SteelName:..." << SteelName;
        if(!SteelName.isEmpty() && SteelName.compare(LastSteelName) != 0){
            LastSteelName = SteelName;
            qDebug()<<"Start new QThread..... for :"<<SteelName;
            ThreadForXimg *thread =  new ThreadForXimg(SteelName);
            connect(thread, &QThread::finished, thread, &QObject::deleteLater);
            thread->start();
        }
    }
}





