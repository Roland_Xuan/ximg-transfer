#include "database.h"
#include <QDebug>

DataBase::DataBase(QObject *parent)
    : QObject{parent}
{}

QSqlDatabase DataBase::openDataBase(QString connectionnName, QString Table)
{
    if ( true == QSqlDatabase::contains(connectionnName))
    {
        db = QSqlDatabase::database(connectionnName); //存在就使用这个连接
    }else {
        db = QSqlDatabase::addDatabase(SqlDatabaseName, connectionnName);
        db.setHostName(hostName);
        db.setUserName(User);
        db.setPassword(Pwd);
        db.setDatabaseName(Table);
        if(!db.open()){
            qDebug()<<":";
        }
    }
    return db;
}

void DataBase::closeDataBase(QString connectionName)
{
    db.close();
    QSqlDatabase::removeDatabase(connectionName);
}
