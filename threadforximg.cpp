#include "threadforximg.h"

#include <QtDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <ArNTGrayBmp.h>
#include <QString>

ThreadForXimg::ThreadForXimg(const QString &s):steelName(s)
{

}

void ThreadForXimg::run()
{
    qDebug()<<"ThreadForXimg::run.....";
    //1.读取图像库
    ArNTImageOperator *pImgOperator = nullptr;
    AllocImageOperator(&pImgOperator);

    Qt::HANDLE threadId = QThread::currentThreadId();
    QString connectionName = QString("db_full_process_thread_%1").arg(reinterpret_cast<quintptr>(threadId));


    QSqlDatabase db = QSqlDatabase::addDatabase("QODBC",connectionName);
    db.setHostName("127.0.0.1");
    db.setUserName("root");
    db.setPassword("nercar");
    db.setDatabaseName("db_device7");
    if(!db.open()){
        qDebug()<< "ThreadForImg:" <<db.lastError().text();
    }

    //初始化表检数据库
    ImgTransfer ximgTransfer;

    qDebug()<<"running Task";

    //去重查询缺陷的位置
    QString str = R"(
    SELECT DISTINCT
    Defect_Min_Image_Name
    FROM tb_device_detect_detail_%1
    WHERE Steel_ID = '%2';
    )";


    for(int i = 1; i<=2; ++i)
    {
        QString sql_str = str.arg(i).arg(steelName);
        QSqlQuery query(db);
        query.exec(sql_str);
        while (query.next()) {
            //  Path: CamDefectImage1/120623/0011.ximg
            QString XimgFile = query.record().value("Defect_Min_Image_Name").toString();
            QStringList parts = XimgFile.split("/");

            int SequenceNum = parts[1].toInt(); //流水号
            int CameraNumer = parts[0].right(1).toInt();
            int ImgIdx = parts.last().split(".").first().toInt();

            //Path:  \\172.31.202.2\CamDefectImage1/120623/0011.ximg
            QString prefix = R"(\\172.31.202.2\)";
            QString TotalPath = prefix + XimgFile;
            ximgTransfer.READFileToImg(TotalPath,SequenceNum,CameraNumer,ImgIdx,pImgOperator);
        }
    }

    db.close();
    ximgTransfer.db.close();
    ximgTransfer.fullProcessdb.close();
    FreeImageOperator(&pImgOperator);
    pImgOperator = nullptr;
    qDebug()<<"ThreadForXimg::run...end";
}
