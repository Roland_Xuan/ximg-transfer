#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QSqlDatabase>

class DataBase : public QObject
{
    Q_OBJECT
public:
    explicit DataBase(QObject *parent = nullptr);
    DataBase(QString dbname,QString hn,QString u,QString p):SqlDatabaseName(dbname),hostName(hn),User(u),Pwd(p){

    }
public:
    //连接数据库的基本方法
    QSqlDatabase openDataBase(QString connectionnName,QString Table);
    //关闭数据库
    void closeDataBase(QString connectionName);
public:
    QSqlDatabase db;

    QString SqlDatabaseName;
    QString hostName;
    QString User;
    QString Pwd;
};

#endif // DATABASE_H
