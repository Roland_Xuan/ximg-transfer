#ifndef THREADFORXIMG_H
#define THREADFORXIMG_H

#include <QObject>
#include <QThread>
#include <QSqlDatabase>
#include "imgtransfer.h"
#include <QMutex>

class ArNTImageOperator;
class ThreadForXimg : public QThread
{
    Q_OBJECT
public:
    ThreadForXimg(const QString &s);
signals:



    // QThread interface
protected:
    void run();
private:
    QString steelName;
    // QMutex &pImgOperatorMutex;


};

#endif // THREADFORXIMG_H
