#ifndef STEELINFO_H
#define STEELINFO_H

#include <QObject>
#include <QSqlDatabase>
#include <QTimer>
#include <QMutex>
#include "database.h"
#include "threadforximg.h"
#include"arntimage.h"

class SteelInfo : public QObject
{
    Q_OBJECT
public:
    explicit SteelInfo(QObject *parent = nullptr);

public:
    void tryFindlatestSteel();
private:
    QTimer *SteelTimer;
    QSqlDatabase db;
    QString LastSteelName;//上卷卷号
};

#endif // STEELINFO_H
