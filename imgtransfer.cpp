#include "imgtransfer.h"
#include <ArNTGrayBmp.h>
#include <QFile>
#include "include/ArNTImageCommonClass.h"
#include "qsqlrecord.h"
#include <QDate>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QImage>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QThread>

ImgTransfer::ImgTransfer(QObject *parent)
    : QObject{parent}
{

    Qt::HANDLE threadId = QThread::currentThreadId();

    QString connectionName = QString("FindDefectInfo_%1").arg(reinterpret_cast<quintptr>(threadId));
    db = QSqlDatabase::addDatabase("QODBC",connectionName);
    db.setHostName("172.31.202.2");
    db.setUserName("ARNTUSER");
    db.setPassword("ARNTUSER");
    db.setDatabaseName("ClientDefectDB1");
    if(!db.open()){
        qDebug()<<db.lastError().text();
    }

    // QString connectionName2 = QString("db_full_process_steelInfo_threads_%1").arg(reinterpret_cast<quintptr>(threadId));
    // fullProcessdb = QSqlDatabase::addDatabase("QODBC",connectionName2);
    // fullProcessdb.setHostName("127.0.0.1");
    // fullProcessdb.setUserName("root");
    // fullProcessdb.setPassword("nercar");
    // fullProcessdb.setDatabaseName("db_device7");
    // if(!fullProcessdb.open()){
    //     qDebug()<<db.lastError().text();
    // }
}


/*

bool ImgTransfer::READFileToXimg(const QString &FilePAth, GrayBmpData &BmpData)
{

     //Ximg代码解析成png
    //  //  Path:     \\\\172.31.202.2\\CamDefectImage1/120635/0007.img
    QString normalizedPath = FilePAth;
    normalizedPath.replace("/", "\\");
    qDebug()<<"Src:"<<normalizedPath;
    QFile file(normalizedPath);
    if(!file.open(QIODevice::ReadOnly)){
        qDebug()<<"File Read Failure";
        return false;
    }
    ImgDataHeadExt HeadInfo;
    file.read(reinterpret_cast<char*>(&HeadInfo), sizeof(HeadInfo));//读取ximg中的数据
    BmpData.Alloc(HeadInfo.iImgWidth,HeadInfo.iImgHeight);//开辟空间

    quint32 dwImgNum = 0;
    file.read(reinterpret_cast<char*>(&dwImgNum), sizeof(dwImgNum));//读取指定大小内容
    qDebug()<<"DefectNumber:" << QString::number(dwImgNum);

    for (quint32 i = 0; i < dwImgNum; i++) {
        quint32 dwLeftInImg, dwTopInImg, dwSubImgWidth, dwSubImgHeight;
        //按字节读取
        file.read(reinterpret_cast<char*>(&dwLeftInImg), sizeof(dwLeftInImg));
        file.read(reinterpret_cast<char*>(&dwTopInImg), sizeof(dwTopInImg));
        file.read(reinterpret_cast<char*>(&dwSubImgWidth), sizeof(dwSubImgWidth));
        file.read(reinterpret_cast<char*>(&dwSubImgHeight), sizeof(dwSubImgHeight));

        ImgDataType* pCurrImg = BmpData.pImgData + dwTopInImg * BmpData.iActrualImgWidth + dwLeftInImg;
        for (quint32 j = 0; j < dwSubImgHeight; j++) {
            file.read(reinterpret_cast<char*>(pCurrImg), dwSubImgWidth);
            pCurrImg += BmpData.iActrualImgWidth;
        }
        GrayBmpData defectdata;
        defectdata.Alloc(dwSubImgWidth,dwSubImgHeight);
        BmpData.GetSubGrayBmpData(dwLeftInImg,dwTopInImg,dwSubImgWidth,dwSubImgHeight,defectdata);



        //    \\\\172.31.202.2\\CamDefectImage2\\120628\\649.ximg
        // 1. 固定路径
        QString targetPath = "D:\\Images\\7\\DefectImage";
        // 分割路径
        QStringList parts = normalizedPath.split("\\", Qt::SkipEmptyParts);
        if (parts.size() < 4) {
            qWarning() << "Invalid path format!";
            return false;
        }

        QString camDefectImage = parts[1]; // "CamDefectImage1" or "CamDefectImage3", etc.
        QString subFolder1 = parts[2];     // e.g., "118610"
        QString subFolder2 = parts[3];     // e.g., "22.ximg"
        // 2. 根据规则追加 1 或 2
        if (camDefectImage == "CamDefectImage1" || camDefectImage == "CamDefectImage2") {
            targetPath += "\\1";
        } else if (camDefectImage == "CamDefectImage3" || camDefectImage == "CamDefectImage4") {
            targetPath += "\\2";
        } else {
            qWarning() << "Unexpected camDefectImage value!";
            return false;
        }
        // 3. 追加当前日期
        QString currentDate = QDate::currentDate().toString("yyyyMMdd");
        targetPath += "\\" + currentDate;
        // 4. 追加原始路径中的部分118610和22（提取文件路径中的倒数第二部分和最后一部分）
        targetPath += "\\" + subFolder1;  // 添加 "118610"
        QString lastPart = subFolder2.split('.').first(); // 获取 "22" （去掉扩展名）
        targetPath += "\\" + lastPart;

        auto ImgFile = QString("%1\\%2.png").arg(targetPath).arg(i);

        qDebug()<<"des:"<<ImgFile;
        QImage img(defectdata.pImgData,dwSubImgWidth,dwSubImgHeight,QImage::Format_Indexed8);
        img.save(ImgFile);
        defectdata.Free();
    }
    file.close();
    return true;
}
*/


wchar_t *QString2Wchar(QString buf)
{
    return (wchar_t*)reinterpret_cast<const wchar_t *>(buf.utf16());
}

//返回指定格式路径
QString ImgTransfer::GetSavePath(int sequenceNum, int CamNo, int ImgIndex,int defectNo){
    // 1. 固定路径
    QString targetPath = "D:\\Images\\7\\DefectImage";
    //2. 追加当前日期
    QString currentDate = QDate::currentDate().toString("yyyyMMdd");
    targetPath += "\\" + currentDate;
    int SelectDbIndex = 0;
    //3. 根据规则追加 1 或 2
    // targetPath += QString("\\%1").arg(QString::number(CamNo));
    if (CamNo == 1 || CamNo == 2) {
        targetPath += "\\1";
        SelectDbIndex = 1;
    } else if (CamNo == 3 || CamNo == 4) {
        targetPath += "\\2";
         SelectDbIndex = 2;
    } else {
        qWarning() << "Unexpected camDefectImage value!";
        targetPath += "error";
    }

    //4. 追加流水号
    targetPath += QString("\\%1").arg(sequenceNum);

    //区分相机防止名称相同导致覆盖
    QString steelName = QString("\\%1_%2.png").arg(CamNo).arg(ImgIndex);

    targetPath += steelName;

    // QString tempstr1 = R"(
    // UPDATE tb_device_detect_detail_%1
    // SET Defect_Image_Name = '%2'
    // WHERE Defect_No = %3;
    // )";

    // QSqlQuery query(fullProcessdb);
    // QString sql1 = tempstr1.arg(SelectDbIndex).arg(steelName).arg(defectNo);
    // if (!query.exec(sql1)) {
    //     qDebug() << "failure To Update Defect_Image_Name: " << query.lastError();
    // }

    return targetPath;
}

bool ImgTransfer::READFileToImg(const QString &FilePAth,int sequenceNum, int CamNo, int ImgIndex,ArNTImageOperator *pImgOperator)
{
    qDebug()<<"ImgTransfer::READFileToImg ... start";
    //全流程的服务器存储的是原图的图片路径
    //需要通过流水号将缺陷位置在该图片上一个一个截取下来
    QString str = R"(
    USE [ClientDefectDB%1]
    Select *
    From [ClientDefectDB%1].[dbo].[Defect]
    where
    CameraNo = %2
    and
    SteelNo = %3
    and
    ImageIndex = %4
    )";
    auto sqlStr =   str.arg(CamNo).arg(CamNo).arg(sequenceNum).arg(ImgIndex);

    ImgDataHeadExt header;
    GrayBmpData data;
    QDir dir;

    {
        QSqlQuery query(db);
        query.exec(sqlStr);
        while(query.next()){
            qint32 dL = qAbs(query.record().value("LeftInImg").toInt());
            qint32 dR = qAbs(query.record().value("RightInImg").toInt());
            qint32 dT = qAbs(query.record().value("TopInImg").toInt());
            qint32 dB = qAbs(query.record().value("BottomInImg").toInt());

            qint32 dW = qAbs(dR - dL);
            qint32 dH = qAbs(dB - dT);

            int defectNo = query.record().value("DefectNo").toInt();


            pImgOperator->ReadSubImg(QString2Wchar(FilePAth),header,dL,dR,dT,dB,data);
            QImage image(data.pImgData,dW,dH,QImage::Format_Indexed8);


            QString savePath = GetSavePath(sequenceNum,CamNo,ImgIndex,defectNo);
            QFileInfo fileInfo(savePath);
            QString directoryPath = fileInfo.absolutePath();
            if (!dir.exists(directoryPath)) {
                if (!dir.mkpath(directoryPath)) {
                    qDebug() << "Failed to create directory: " << directoryPath;
                    return false;
                }
            }

            qDebug()<<savePath;
            image.save(savePath);
        }

        data.Free();
    }
     qDebug()<<"ImgTransfer::READFileToImg ... end";
    return true;
}
